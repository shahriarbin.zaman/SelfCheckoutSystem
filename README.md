# Self Checkout System
Project of SENG300

[Project Manager]
Suiba Sultana Swapnil, Sukhpreet Sandhu, Khushi Choksi

[Management Team]
Suiba Sultana Swapnil - Design/Dev and Prj Manager,
Wasif ud Dowlah - Testing,
Shahriar Bin Zaman- Documentation ,
Doug Strueby - Demonstration 

[Demo Team]
Doug Strueby- LEAD,
Chetas Patel ,
Nolan Ruzicki,
Anshi Khatri,

[Github Administrator]
Shahriar Bin Zaman,
Wasif ud Dowlah

[Meeting Minutes]
Sukhpreet Sandhu,

[Design and Development Team]
Suiba Sultana Swapnil - LEAD,
Lev Sinaga (GUI),
Nolan Ruzicki (GUI),
Doug Strueby (GUI),
Khushi Choksi,
Soila Pertet,
Sukhpreet Sandhu ,
Nabihah Hussaini, 
Mahnoor Hameed,
Shahriar Bin Zaman 

[Documentation Team]
Shahriar Bin Zaman - LEAD,
Anshi Khatri,

[Testing Team]
Wasif ud Dowlah- LEAD,
Chetas Patel ,
Sadeeb Hossain,
Shahriar Khan,
Navid Rahman,
Maria Manosalva Rojas,
Ayesha Saeed,
Jui Das,
Kazi Mysha Moontaha,
Hareem Arif
