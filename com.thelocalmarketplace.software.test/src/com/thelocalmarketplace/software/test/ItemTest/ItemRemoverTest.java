package com.thelocalmarketplace.software.test.ItemTest;

/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818 
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

import org.junit.Assert;
import org.junit.Before;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.junit.Test;

import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.SelfCheckoutStationSilver;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.software.ShoppingCart;

import powerutility.PowerGrid;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;


public class ItemRemoverTest {
	
	int totalItems;
	ArrayList<BarcodedProduct> totalProductsList;
	ItemRemover removeItem;
	BarcodedItem item;
	BarcodedItem item2;
	PLUCodedItem pluItem;
	PLUCodedItem pluItem2;
	PLUCodedProduct pluProduct;
	Barcode testBarcode;
	Barcode testBarcode2;
	Mass testMass;
	PriceLookUpCode pluCode;
	PriceLookUpCode pluCode2;
	String pluCodeString;
	String pluCodeString2;
	String productDescription;
	long price;
	ShoppingCart shoppingCart;
	BarcodedProduct barcodedProduct;
	ElectronicScaleBronze bronze;
	ElectronicScaleSilver silver;
	ElectronicScaleGold gold;
    	private PowerGrid grid;
    	SelfCheckoutStationBronze stationBronze;
   	SelfCheckoutStationSilver stationSilver;
    	SelfCheckoutStationGold stationGold;
   	private final PrintStream standardOut = System.out;
   	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
   	private BigDecimal initialTotalWeight;
   	private long initialTotalPrice;
	CentralCheckoutStationLogic logic;
	private IElectronicScale eScale;

	
	@Before
	public void setup() {
		System.setOut(new PrintStream(outputStreamCaptor));
		stationBronze = new SelfCheckoutStationBronze();
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(stationBronze);
		this.logic.hardware.getBaggingArea();
		
		Numeral[] numerals = {Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
		Numeral[] numeral2 = {Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.six };
        
		//creating test barcodes
		pluCodeString = "12345";
		pluCodeString2 = "67890";
		testBarcode = new Barcode(numerals); 
        testBarcode2 = new Barcode(numeral2);
        pluCode = new PriceLookUpCode(pluCodeString);
        pluCode2 = new PriceLookUpCode(pluCodeString2);
        productDescription = "apple"; 
        price = 67;
		
        //creating scale objects
		bronze = new ElectronicScaleBronze();
		silver = new ElectronicScaleSilver();
		gold = new ElectronicScaleGold();
		
		this.shoppingCart = ShoppingCart.getShoppingCart(logic);
		totalItems = shoppingCart.getTotalNumberOfItems(); // getting totalItems of shopping cart
		totalProductsList = shoppingCart.getShoppingCartProducts();
		
		removeItem = new ItemRemover(logic); //creating instance of RemoveItem class
		testMass = new Mass(BigDecimal.valueOf(500));  //creating test mass
		
		//creating item instances
		item = new BarcodedItem(testBarcode, testMass);  
		item2 = new BarcodedItem(testBarcode2, testMass);
		barcodedProduct = new BarcodedProduct(testBarcode, "Sample Product", 123,  4.56);
		pluItem = new PLUCodedItem(pluCode, testMass);
		pluItem2 = new PLUCodedItem(pluCode2, testMass); 
		pluProduct = new PLUCodedProduct(pluCode, productDescription, price );
       
		grid = PowerGrid.instance();
        
		initialTotalWeight = shoppingCart.getTotalExpectedWeight(); //initial total weight of shoppingCart
        initialTotalPrice = shoppingCart.getTotalPrice(); //initial total price of shopping cart
	}
	
	
	@Test
	public void testIfCartIsEmpty() {
		bronze.plugIn(grid);
		bronze.turnOn(); // powering on bronze scale
		
		removeItem.removeBarcodedItem(item, bronze);
		
		assertEquals("There are no items to be removed from your shopping cart.", outputStreamCaptor.toString().trim());

	}
	
	@Test
	public void testIfPLUCartIsEmpty() {
        
		removeItem.removePLUItem(pluItem);
				
		assertEquals("There are no items to be removed from your shopping cart.", outputStreamCaptor.toString().trim());
	}
	
	@Test
	public void testRemoveBarcodedItem() {
		bronze.plugIn(grid);
        bronze.turnOn(); // powering on bronze scale
        
		ArrayList<BarcodedItem> arraylist1 = shoppingCart.getShoppingCartItems(); //getting array list with one item in cart
		
		//adding and then removing an item
		shoppingCart.addBarcodedItemToCart(item);
		removeItem.removeBarcodedItem(item, bronze);

		ArrayList<BarcodedItem> arraylist2 = shoppingCart.getShoppingCartItems(); //getting array list with one item
		assertEquals(arraylist1.size(),arraylist2.size()); //checking if item was removed by comparing lists
	} 
	
	@Test
	public void testRemoveBarcodedItem2() {
		bronze.plugIn(grid);
        bronze.turnOn(); // powering on bronze scale
        
		shoppingCart.addBarcodedItemToCart(item);
		shoppingCart.addBarcodedItemToCart(item);
		shoppingCart.addBarcodedItemToCart(item);
		shoppingCart.addBarcodedItemToCart(item);
		shoppingCart.addBarcodedItemToCart(item);

		removeItem.removeBarcodedItem(item, bronze);
		
		ArrayList<BarcodedItem> cart = shoppingCart.getShoppingCartItems();
		 
		assertEquals(4, cart.size());
	}
	
	@Test
	public void testRemovePLUcodedItem() {
		bronze.plugIn(grid);
	    bronze.turnOn(); // powering on bronze scale

	    shoppingCart.addPLUItemToCart(pluItem);
	    shoppingCart.addPLUItemToCart(pluItem);
	    shoppingCart.addPLUItemToCart(pluItem);
	    shoppingCart.addPLUItemToCart(pluItem);
	    shoppingCart.addPLUItemToCart(pluItem);
			
	    removeItem.removePLUItem(pluItem);
	     
	    ArrayList<PLUCodedItem> cart = shoppingCart.getShoppingCartPLUItems(); //getting array list with one item in cart
			
	    assertEquals(4, cart.size());
	} 
	
	
	@Test
	public void testRemoveNonexistentItem() {
		shoppingCart.addBarcodedItemToCart(item2); //adding item to cart

		ItemRemover removeItem = new ItemRemover(logic); //instance of RemoveItem class
		removeItem.removeBarcodedItem(item, bronze); //removing item that does not exist in cart
	    
		String output = outputStreamCaptor.toString().trim();
		assertTrue(output.endsWith("This item is not in your shopping cart.")); //checking if expected and actual output are equal
	}
	
	
	@Test 
	public void testRemoveNonexistentPLUItem() {
		shoppingCart.addPLUItemToCart(pluItem); //adding item to cart

		ItemRemover removeItem = new ItemRemover(logic); //instance of RemoveItem class
		removeItem.removePLUItem(pluItem2); //removing item that does not exist in cart
	    
		String output = outputStreamCaptor.toString().trim();
		assertTrue(output.endsWith("This item is not in your shopping cart.")); //checking if expected and actual output are equal
	}


	@Test
    public void removeBarcodedProduct_ShouldDecreaseTotalPriceAndWeight() {
        
        removeItem.removeBarcodedProductAttributes(barcodedProduct);  //removing barcoded product
        
        // Assert
       	BigDecimal finalTotalWeight = shoppingCart.getTotalExpectedWeight(); //getting total wight after removing product
        long finalTotalPrice = shoppingCart.getTotalPrice(); //getting total price after removing product
        
        assertEquals(initialTotalPrice - barcodedProduct.getPrice(), finalTotalPrice); //subtracting price of product from initial price and comparing with the finalTotalPrice
        assertEquals(0, initialTotalWeight.subtract(BigDecimal.valueOf(barcodedProduct.getExpectedWeight())).compareTo(finalTotalWeight)); //subtracting weight of product from initial weight and comparing with finalTotalWeight
	}
	
}