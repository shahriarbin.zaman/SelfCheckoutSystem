/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test.ItemTest;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.Product;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Controller.PLUController;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Item.VisualCatalogueSearch;


import powerutility.PowerGrid;

public class VisualCatalogueSearchTest {
    private VisualCatalogueSearch visualCatalogueSearch;
    private CentralCheckoutStationLogic logicStub;
    AbstractSelfCheckoutStation station;
    PowerGrid grid;
    AttendantStation attendant = new AttendantStation();
    ShoppingCart shoppingCart;
    
    
    @Before
    public void setUp() {
    	station = new SelfCheckoutStationBronze();
    	grid = PowerGrid.instance();
    	station.plugIn(grid);
    	station.turnOn();
        
        logicStub = new CentralCheckoutStationLogicStub23(station);
        
        logicStub.scannerController = new ScannerController(logicStub);
        logicStub.signalAttendantController = new SignalAttendantController(logicStub, attendant);
        logicStub.priceLookupCodeController = new PLUController(logicStub);
       
        visualCatalogueSearch = new VisualCatalogueSearch(logicStub);
        
        shoppingCart = ShoppingCart.getShoppingCart(logicStub);
        
        SessionStarter.getSessionStarter(logicStub).startSession();
        
        ItemDatabase.loadItems();
        ItemDatabase.initBarcodedItems();
    }

    @Test
    public void testAddBarcodedProductFromCatalogue() {        
        Numeral[] numerals = { Numeral.one };
		Barcode code = new Barcode(numerals);
		
        visualCatalogueSearch.addProductFromCatalogue(ItemDatabase.getBarcodeProduct().get(0));

        BarcodedItem temp = null;
        ArrayList<BarcodedItem> items = shoppingCart.getShoppingCartItems();
        for (int i =0; i < items.size(); i++) {
        	if ((items.get(i).getBarcode()).equals(code)) {
        		temp = items.get(i);
        	}
        }
        assertNotNull(temp);
    } 
    
    @After
    public void tearDown() {
    	SessionStarter.getSessionStarter(logicStub).endSession();
    }

    @Test
    public void testAddPLUCodedProductFromCatalogue() {
		PriceLookUpCode bananaPLU = new PriceLookUpCode("0001");
        PLUCodedProduct bananaProduct = new PLUCodedProduct(bananaPLU, "BANANA", 2);
        
        visualCatalogueSearch.addProductFromCatalogue(bananaProduct);
        
        PLUCodedItem temp = null;
        ArrayList<PLUCodedItem> items = shoppingCart.getShoppingCartPLUItems();
        for (int i =0; i < items.size(); i++) {
        	if ((items.get(i).getPLUCode()).equals(bananaPLU)) {
        		temp = items.get(i);
        	}
        }
        assertNotNull(temp);
    }
    
    @Test
    public void testAddInvalidItem() {
        Product invalid = null;
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        
        visualCatalogueSearch.addProductFromCatalogue(invalid);
        
        assertEquals("Invalid product. Cannot add to cart.\n", outContent.toString());
        // Assertions to verify behavior for PLU-coded product
    }
}





// ---------- STUBS FOR TESTING

class CentralCheckoutStationLogicStub23 extends CentralCheckoutStationLogic {

	 	public CentralCheckoutStationLogic logic;
		
	    public MethodsOfPayment methodsOfPayment;
	    public MethodsOfCardPayment methodsOfCardPayment;
	    public TypeOfCard typeOfCard;
		public CardIssuer cardIssuer;
		public TypeOfScanner scannerType; 
		
		public String currentMemberShipCard;
		public HashMap<String, Boolean> membershipCards = new HashMap<>();
		PowerGrid grid;
		public CentralCheckoutStationLogicStub23(AbstractSelfCheckoutStation station) {
		    
			super(null); // Pass null for the logic parameter, since we're not using it in the stub
		    this.hardware = station;
		    this.scaleController = new ScaleController(this);

		    grid = PowerGrid.instance();
		    ElectronicScaleBronzeStub2 scale = new ElectronicScaleBronzeStub2();
	        Mass mass = Mass.ONE_GRAM;
	        scale.setSimulatedMass(mass);
	        
	        scale.plugIn(grid);
	        scale.turnOn();
	        
	       // need to add setter to ScaleController.java
	        this.scaleController.setScaleScanningArea(scale);
	        
	     // need to add setter to ScaleController.java
	        OwnBagAdder ownBags;
			ownBags = new OwnBagAdder(this);
			this.scaleController.setOwnBagAdder(ownBags);
		
		}
		
		public AbstractSelfCheckoutStation getHardware() {
		    return hardware;
		}
		
		@Override
		public void setMethodsOfPayment(MethodsOfPayment method) {
			methodsOfPayment = method;
		}
		
		@Override
		public void setTypeOfCard(TypeOfCard type) {
			typeOfCard = type;
		}
		
		@Override
		public void setMethodsOfCardPayment(MethodsOfCardPayment method) {
			methodsOfCardPayment = method;
		}
		
		
		
		@Override
		public MethodsOfPayment getMethodsOfPayment() 
		{
			return methodsOfPayment;
		}
		@Override
		public TypeOfCard getTypeOfCard() 
		{
			return typeOfCard;
		}
		@Override
		public MethodsOfCardPayment getMethodsOfCardPayment() 
		{
			return methodsOfCardPayment;
		}
		
		public void addMembershipCard(String membershipNumber) {
			this.membershipCards.put(membershipNumber, true);
		}
		
		public HashMap getMembershipCards() {
		    return membershipCards;
		}
		
		public void setCurrentMemberShipCard(String card) {
			this.currentMemberShipCard = card;
		}
		
		public String getCurrentMemberShipCard() {
			return this.currentMemberShipCard;
		}
		
		public String getCardReaderType() 
		{
			return "Bronze";
		}
		
		public CardIssuer getCardIssuer() 
		{
			return cardIssuer;
		}
		
		public void setCardIssuer(CardIssuer cardIssuer) 
		{
			this.cardIssuer = cardIssuer;
		}
		
		public TypeOfScanner getTypeOfScanner() {
			return scannerType;
		}
		
		public void setTypeOfScanner(TypeOfScanner scannerType) {
			this.scannerType = scannerType;
		}
		
		
		public String getScanningAreaType() 
		{
			return "Bronze";
		}
		
		
		
}
class ElectronicScaleBronzeStub2 extends ElectronicScaleBronze {
    private Mass simulatedMass;
    private boolean simulateOverload = false;

    public ElectronicScaleBronzeStub2() {
        super();
        
        simulatedMass = new Mass(BigInteger.ZERO);
    }

    public void setSimulatedMass(Mass mass) {
        this.simulatedMass = mass;
    }

    public void setSimulateOverload(boolean simulateOverload) {
        this.simulateOverload = simulateOverload;
    }

    @Override
    public synchronized Mass getCurrentMassOnTheScale() throws OverloadedDevice {

        if (simulateOverload || (simulatedMass != null && simulatedMass.compareTo(massLimit) > 0)) {
            throw new OverloadedDevice();
        }

        // Return the simulated mass, or a default if not set.
        return simulatedMass != null ? simulatedMass : new Mass(BigInteger.ZERO);
    }
}
