/*	GROUP MEMBERS' NAMES AND UCIDs:
	Sukhpreet Kaur Sandhu	30174163
Suiba Sultana Swapnil	30174800
Soila Pertet	30102008
Khushi Choksi	30168167
Doug Strueby	30122048
Chetas Patel	30173271
Lev Sinaga	30191948
Ayesha Saeed	30174420
Sadeeb Hossain	30145821
Nolan Ruzicki	30132405
Shahriar Bin Zaman	30111988
Anshi Khatri	30172820
Kazi Mysha Moontaha	30179818
Nabihah Hussaini	30175407
Hareem Arif	30169729
Mahnoor Hameed	30076366
Maria Manosalva Rojas	30146319
Jui Das	30176206
Wasif Ud Dowlah	30130706
Navid Rahman	30186149
Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.test.ItemTest;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scanner.Barcode;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Controller.PLUController;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Item.TextSearch;

import powerutility.PowerGrid;

public class TextSearchTest {

private TextSearch textSearch;
private CentralCheckoutStationLogic logicStub;
AbstractSelfCheckoutStation station;
private Barcode barcode;
private BarcodedProduct product;
private PLUCodedProduct pluProduct;
PowerGrid grid;
PLUController pluController;
ShoppingCart shoppingCart;
private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

@Before
public void setUp() {
   	System.setOut(new PrintStream(outputStreamCaptor));

	station = new SelfCheckoutStationBronze();
	grid = PowerGrid.instance();
	station.plugIn(grid);
	station.turnOn();
	logicStub = new CentralCheckoutStationLogicStub3(station);
	pluController = new PLUController(logicStub);
	logicStub.priceLookupCodeController = new PLUController(logicStub);
    textSearch = new TextSearch(logicStub);
    shoppingCart = ShoppingCart.getShoppingCart(logicStub);
    ItemDatabase.loadItems();
    SessionStarter.getSessionStarter(logicStub).startSession();
    PowerGrid.engageUninterruptiblePowerSource();
}

@After
public void tearDown() {
	ProductDatabases.PLU_PRODUCT_DATABASE.clear();
}


@Test
public void testFindItemByName_ProductFound() {
	PriceLookUpCode pluCode = new PriceLookUpCode("0011");
    String description = "CHAIR";
    long price = 90; // Example price, in cents or smallest currency unit
    pluProduct = new PLUCodedProduct(pluCode, description, price);
    
    textSearch.findItemByName(description);
   
    assertEquals(pluCode, textSearch.findItemByName(description));
}

@Test
public void testFindSearchedProduct_ProductThatIsNotInDatabase() {
    String description = "Sample Product Description";
    assertNull(textSearch.findItemByName(description));
}

@Test
public void testFindItemByName_Null() {
    assertNull(textSearch.findItemByName(null));
}

@Test
public void testAddItemByName_ItemIsAdded() {
	PriceLookUpCode pluCode = new PriceLookUpCode("0009");
    String description = "CEREAL";
    long price = 5; // Example price, in cents or smallest currency unit
    pluProduct = new PLUCodedProduct(pluCode, description, price);
    
    textSearch.addItemByName(description);
    
    PLUCodedProduct temp = null;
    ArrayList<PLUCodedProduct> items = shoppingCart.getShoppingCartPLUProducts();
    for (int i = 0; i < items.size(); i++) {
    	if ((items.get(i).getPLUCode()).equals(pluCode)) {
    		temp = items.get(i);
    	}
    }
    
    assertNotNull(temp);
}

@Test
public void testAddItemByName_NullName() {
	textSearch.addItemByName(null);
	String output = outputStreamCaptor.toString().trim();
	assertTrue(output.endsWith("Item not found"));
}

}



//-------- STUBS FOR TESTING

class CentralCheckoutStationLogicStub3 extends CentralCheckoutStationLogic {

public CentralCheckoutStationLogic logic;
	
public MethodsOfPayment methodsOfPayment;
public MethodsOfCardPayment methodsOfCardPayment;
public TypeOfCard typeOfCard;
public CardIssuer cardIssuer;
public TypeOfScanner scannerType; 

public String currentMemberShipCard;
public HashMap<String, Boolean> membershipCards = new HashMap<>();

public CentralCheckoutStationLogicStub3(CentralCheckoutStationLogic logic) 
{
	super(logic);
}
PowerGrid grid;
	
public CentralCheckoutStationLogicStub3(AbstractSelfCheckoutStation station) {

super(null); // Pass null for the logic parameter, since we're not using it in the stub

this.hardware = station;
grid = PowerGrid.instance();
this.scaleController = new ScaleController(this);
ElectronicScaleBronzeStub3 scale = new ElectronicScaleBronzeStub3();
Mass mass = Mass.ONE_GRAM;
scale.setSimulatedMass(mass);

// give power to scale
scale.plugIn(grid);
scale.turnOn();
this.scaleController.setScaleScanningArea(scale);

OwnBagAdder ownBags;
ownBags = new OwnBagAdder(this);
this.scaleController.setOwnBagAdder(ownBags);


}

public AbstractSelfCheckoutStation getHardware() {
return hardware;
}

@Override
public void setMethodsOfPayment(MethodsOfPayment method) {
methodsOfPayment = method;
}

@Override
public void setTypeOfCard(TypeOfCard type) {
typeOfCard = type;
}

@Override
public void setMethodsOfCardPayment(MethodsOfCardPayment method) {
methodsOfCardPayment = method;
}


@Override
public MethodsOfPayment getMethodsOfPayment() 
{
return methodsOfPayment;
}
@Override
public TypeOfCard getTypeOfCard() 
{
return typeOfCard;
}
@Override
public MethodsOfCardPayment getMethodsOfCardPayment() 
{
return methodsOfCardPayment;
}

public void addMembershipCard(String membershipNumber) {
this.membershipCards.put(membershipNumber, true);
}

public HashMap getMembershipCards() {
return membershipCards;
}

public void setCurrentMemberShipCard(String card) {
this.currentMemberShipCard = card;
}

public String getCurrentMemberShipCard() {
return this.currentMemberShipCard;
}

public String getCardReaderType() 
{
return "Bronze";
}

public CardIssuer getCardIssuer() 
{
return cardIssuer;
}

public void setCardIssuer(CardIssuer cardIssuer) 
{
this.cardIssuer = cardIssuer;
}

public TypeOfScanner getTypeOfScanner() {
return scannerType;
}

public void setTypeOfScanner(TypeOfScanner scannerType) {
this.scannerType = scannerType;
}


public String getScanningAreaType() 
{
return "Bronze";
}


}
class ElectronicScaleBronzeStub3 extends ElectronicScaleBronze {
private Mass simulatedMass;
private boolean simulateOverload = false;

public ElectronicScaleBronzeStub3() {
    super();
    
    simulatedMass = new Mass(BigInteger.ZERO);
}

public void setSimulatedMass(Mass mass) {
    this.simulatedMass = mass;
}

public void setSimulateOverload(boolean simulateOverload) {
    this.simulateOverload = simulateOverload;
}

@Override
public synchronized Mass getCurrentMassOnTheScale() throws OverloadedDevice {

    if (simulateOverload || (simulatedMass != null && simulatedMass.compareTo(massLimit) > 0)) {
        throw new OverloadedDevice();
    }

    // Return the simulated mass, or a default if not set.
    return simulatedMass != null ? simulatedMass : new Mass(BigInteger.ZERO);
}
}