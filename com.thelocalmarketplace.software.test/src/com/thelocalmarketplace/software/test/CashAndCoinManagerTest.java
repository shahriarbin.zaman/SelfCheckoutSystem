/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/


package com.thelocalmarketplace.software.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CashAndCoinManager;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

import powerutility.PowerGrid;

public class CashAndCoinManagerTest {
	
	//Coin coin;
	int coinQuantity;
	CashAndCoinManager manager;
	CentralCheckoutStationLogic logic;
	SelfCheckoutStationBronze bronzeStation;
	PowerGrid grid;
	
	@Before
	public void setUp() {
		grid = PowerGrid.instance();
		
		
		bronzeStation = new SelfCheckoutStationBronze();
		bronzeStation.plugIn(grid);
		bronzeStation.turnOn();
		
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(bronzeStation);
		manager = new CashAndCoinManager(logic);
		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCoinQuantityIsLessThanOrEqualToZero() {
		coinQuantity = -2;
		
		manager.addCoins(4, coinQuantity);
		
	}
	
	@Test
	public void Should_AddCoins_IfQuantityIsGreaterThanZero() {
		int initialQuantity = 10;
        manager.addCoins(4, initialQuantity);

        int expectedQuantity = initialQuantity + 5; 
        manager.addCoins(4, 5);
        
		//assertEquals(expectedQuantity, manager.getCurrentQuantity(4));
        		// old method
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCoinQuantityWhenRemovingIsLessThanOrEqualToZero() {
		coinQuantity = -2;
		
		manager.removeCoins(4, coinQuantity);
		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCoinQuantityWhenRemovingIsGreaterThanCurrentQuantity() {
		coinQuantity = 12;
		
		int initialQuantity = 10;

        manager.addCoins(4, initialQuantity);

		manager.removeCoins(4, coinQuantity);
		
	}
	
	@Test
	public void Should_RemoveCoins_IfQuantityIsInRange() {
		int initialQuantity = 10;
        manager.addCoins(4, initialQuantity);

        manager.removeCoins(4, 5);
        
        int expectedQuantity = initialQuantity - 5; 
        
		//assertEquals(expectedQuantity, manager.getCurrentQuantity(4));
        		// old method
	}
	
	@Test
	public void Should_CalculateCashBalance_WhenInvoked() {
		manager.addCoins(4, 5);
		manager.addCoins(4, 3);

		
		int expectedBalance = (4*5) + (4*3);
		
		assertEquals(expectedBalance, manager.calculateCashBalance());	
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCentsIsLessThanZero() {
		int amountInCents = -2;
		
        manager.addCash(amountInCents);
		
	}
	
	@Test
	public void Should_AddCash_IfQuantityIsGreaterThanZero() {
		int initialQuantity = 10;
		
        manager.addCash(initialQuantity);

        int expectedQuantity = initialQuantity + 5; 
        manager.addCash(5);
        
		assertEquals(expectedQuantity, manager.getCashBalance());

	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCashQuantityWhenRemovingIsLessThanOrEqualToZero() {
		int amountInCents = -2;
		
		manager.removeCash(amountInCents);
		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void Should_ThrowIllegalArgumentException_IfCoinQuantityWhenRemovingCashIsGreaterThanCurrentQuantity() {
		int amountInCents = 10;
  
        manager.addCash(amountInCents);

		manager.removeCash(12);
		
	}
	
	@Test
	public void Should_RemoveCash_IfQuantityIsInRange() {
		int amountInCents= 10;
        manager.addCash(amountInCents);

        manager.removeCash(5);
        
        int expectedQuantity = amountInCents - 5; 
        
		assertEquals(expectedQuantity, manager.getCashBalance());

	} 

}
