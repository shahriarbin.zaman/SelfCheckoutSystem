/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.VirtualKeyboard;

import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.List;

public class VirtualKeyboardTest 
{
    private VirtualKeyboard virtualKeyboard;
    private CentralCheckoutStationLogic centralCheckoutStationLogic;

    @Before
    public void setUp() 
    {
    	centralCheckoutStationLogic= new CentralCheckoutStationLogic(centralCheckoutStationLogic);
        virtualKeyboard = new VirtualKeyboard(null, centralCheckoutStationLogic);
    }

    
    @Test
    public void testKeyPressesReflectOnTextFieldForAllKeys() 
    {
        JPanel keyboardPanel = findKeyboardPanel(virtualKeyboard.getPanel());
        
        Assert.assertNotNull("The keyboard panel was not found.", keyboardPanel);

        // For alphabets.
        for (char ch = 'A'; ch <= 'Z'; ch++) 
        {
            String key = String.valueOf(ch);
            JButton keyButton = findButtonWithText(keyboardPanel, key);

            Assert.assertNotNull("The button with the text '" + key + "' was not found.", keyButton);
            
            pressButton(keyButton);
            
            // Clearing the text field to test the next key independently.
            virtualKeyboard.getTextField().setText("");
        }
        
        // For space bar
        JButton keyButton_Spacebar = findButtonWithText(keyboardPanel, "Spacebar");
        Assert.assertNotNull( "The button with the text 'Spacebar' was not found." , keyButton_Spacebar);
        
        pressButton(keyButton_Spacebar);
        
        // Clearing the text field to test the next key independently.
        virtualKeyboard.getTextField().setText("");
        
        // For numbers
        for (int i = 0; i <= 9; i++) 
        {
            String number = String.valueOf(i);
           
            JButton keyButton = findButtonWithText(keyboardPanel, number);

            Assert.assertNotNull("The button with the text '" + number + "' was not found.", keyButton);
            
            pressButton(keyButton);
            
            virtualKeyboard.getTextField().setText("");
        }
    }
    
    
    @Test
    public void testBackspaceWithEmptyTextField() 
    {
        virtualKeyboard.getTextField().setText("");
      
        JButton keyButton_Backspace = findButtonWithText(virtualKeyboard.getPanel(), "Backspace");
        Assert.assertNotNull("The button with the text 'Backspace' was not found.", keyButton_Backspace);
        
        pressButton(keyButton_Backspace);
        
        Assert.assertEquals("Text field should remain empty after pressing backspace on an empty field.", "", virtualKeyboard.getTextField().getText());
    }
    
    
    @Test
    public void testBackspaceFunctionality() 
    {
        virtualKeyboard.getTextField().setText("Test123");
        
        JButton keyButton_Backspace = findButtonWithText(virtualKeyboard.getPanel(), "Backspace");
        Assert.assertNotNull("The button with the text 'Backspace' was not found.", keyButton_Backspace);

        pressButton(keyButton_Backspace);
      
        Assert.assertEquals("Backspace key did not remove the last character as expected.", "Test12", virtualKeyboard.getTextField().getText());
    }
    
    
    @Test
    public void testContinuousKeyPressesForAllKeys() 
    {
        String[] keysToTest = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
                               "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                               "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Spacebar", "Enter"};
        
        for (String key : keysToTest) {
            virtualKeyboard.getTextField().setText(""); 
            JButton keyButton = findButtonWithText(virtualKeyboard.getPanel(), key.equals("Spacebar") ? key : key.substring(0, 1));
            Assert.assertNotNull("The button with the text '" + key + "' was not found.", keyButton);
           
            for (int i = 0; i < 5; i++) {
                pressButton(keyButton);
            }
            
            String expectedResult = key.equals("Spacebar") ? "     " : key.repeat(5);
        }
    
        virtualKeyboard.getTextField().setText("ABCDE12345");
        JButton keyButton_Backspace = findButtonWithText(virtualKeyboard.getPanel(), "Backspace");
        Assert.assertNotNull("The button with the text 'Backspace' was not found.", keyButton_Backspace);
        for (int i = 0; i < 10; i++) 
        { 
            pressButton(keyButton_Backspace);
        }
       
    }


    private JPanel findKeyboardPanel(JPanel mainPanel) 
    {
        for (Component comp : mainPanel.getComponents()) 
        {
            if (comp instanceof JPanel && ((JPanel) comp).getLayout() instanceof GridLayout) 
            {
                return (JPanel) comp;
            }
        }
        return null;
    } 

    private JButton findButtonWithText(JPanel panel, String text) 
    {
        for (Component comp : panel.getComponents()) 
        {
        	if (comp instanceof JButton) 
        	{
        	    JButton button = (JButton) comp;
        	    char firstCharOfButton = button.getText().charAt(0);
        	    char charToCompare = text.charAt(0); 
        	   
        	    if (charToCompare == firstCharOfButton) 
        	    {
        	        return button;
        	    }
        	}
        	else if (comp instanceof JPanel) 
            {
                JButton nestedButton = findButtonWithText((JPanel) comp, text);
                if (nestedButton != null) 
                {
                    return nestedButton;
                }
            }
        }
        return null;
    }
    private void pressButton(JButton button) 
    {
    	Assert.assertNotNull("Attempted to press a null button.", button);
        for (ActionListener listener : button.getActionListeners()) 
        {
            listener.actionPerformed(null); 
        }
    }

    @Test
    public void testExcludedKeysDoNothing() 
    {
        virtualKeyboard.getTextField().setText("");

        List<String> exclusions = virtualKeyboard.getExclusions();
        for (String key : exclusions) {
            JButton keyButton = findButtonWithText_Excluded(virtualKeyboard.getPanel(), key);
            if (keyButton != null) 
            { 
                pressButton(keyButton);
                String textAfterPress = virtualKeyboard.getTextField().getText();
                Assert.assertEquals("Excluded key '" + key + "' changed the text field content.", "", textAfterPress);
            }
        }
    }



    private JButton findButtonWithText_Excluded(JPanel panel, String text) 
    {
        for (Component comp : panel.getComponents()) 
        {
            if (comp instanceof JButton) 
            {
                JButton button = (JButton) comp;
                if (button.getText().equals(text)) 
                {
                    return button;
                }
            } 
            else if (comp instanceof JPanel) 
            {
                JButton nestedButton = findButtonWithText_Excluded((JPanel) comp, text);
                if (nestedButton != null) 
                {
                    return nestedButton;
                }
            }
        }
        return null;
    }


   
}
