/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/
package com.thelocalmarketplace.software.test.GUI_Test;//package name

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.lang.reflect.Field;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.AttendantKeyboard;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import com.thelocalmarketplace.software.GUI.StartScreen;

import powerutility.PowerGrid;
//All imports confirmed

public class StartScreenTest {//public class
	private CentralCheckoutStationLogic logic;
	private AbstractSelfCheckoutStation station;
	private PrimaryGUI gui;
	private StartScreen screen;
	private PowerGrid grid;
	private AttendantStation aStation;
	
	private GuiStub guiStub;//instantiating class variables
	
	@Before//setup method
	public void setUp() {
		station = new SelfCheckoutStationGold();
		grid = PowerGrid.instance();
		station.plugIn(grid);
		station.turnOn();
		aStation = new AttendantStation();
		aStation.plugIn(grid);
		aStation.turnOn();
		//setting up and plugging in the station and attendant station
		
		logic = CentralCheckoutStationLogic.installOn(station, aStation);
		gui = new PrimaryGUI(logic);
		guiStub = new GuiStub(logic);
		//setting up the central checkout logic as well as the gui and gui stub
	}
	
	//This method is to test whether or not the setup method of the start screen works
	@Test
	public void testAddWidgets() {
		screen = new StartScreen(gui, logic);//start screen instantiated
		
		JPanel newPanel = screen.getPanel();//new panel obtained
		
		Component[] components = newPanel.getComponents();
		assertEquals(2, components.length);//Getting the components on the panel, should be 2 of them (2 buttons)
		assertTrue(components[0] instanceof JButton);//First one should be a Jbutton
		
		JButton startSessionButton = (JButton) components[0];
	    assertEquals("Touch to start", startSessionButton.getText());//The Touch to Start should be the first button
	    
	    assertTrue(components[1] instanceof JButton);
        JButton selectLanguageButton = (JButton) components[1];
        assertEquals("Select Language", selectLanguageButton.getText());//The second button should be the select Language Button
	}
	
	//Testing the action listener for start session
	@Test
	public void testActionListener_StartSession() {
		screen = new StartScreen(gui, logic);
		JPanel newPanel = screen.getPanel();//setting up the screen with the gui
		
		Component[] components = newPanel.getComponents();
		JButton startSessionButton = (JButton) components[0];//getting components and the first button
		
		startSessionButton.getActionListeners()[0].actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ""));
	    assertTrue(logic.sessionStarter.hasSessionBeenStarted());
	    //After pushing the button, the session has to have been started
	}
	
	//Testing the action listener for show language selection
	@Test
	public void testActionListener_ShowLanguageSelection() {
		screen = new StartScreen(guiStub, logic);
		JPanel newPanel = screen.getPanel();
		//Getting the screen and JPanel
		
		Component[] components = newPanel.getComponents();
		JButton selectLangButton = (JButton) components[1];//Getting the first button- select language
		
		selectLangButton.getActionListeners()[0].actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, ""));
		assertTrue(guiStub.selectLangOn);
		//Preforming action, and checking if this action was executed through the boolean set up in the stub below
	}
	
	class GuiStub extends PrimaryGUI{//The gui stub that extends PrimaryGUI
		
		public boolean selectLangOn = false;//The boolean that checks whether or not select language has been executed
		
		public GuiStub(CentralCheckoutStationLogic logic) {
			super(logic);
		}//Constructor does nothing but setup super()
		
		@Override//Overrides the showLanguageSelection method
		public void showLanguageSelectionScreen() {
			super.showLanguageSelectionScreen();//preform the method in the above class
			selectLangOn = true;//if preformed successfully, then make this known in the boolean
		}
	}
}