/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.printer.ReceiptPrinterBronze;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.CardIssuer;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.junit.Before;
import org.junit.Test;
import com.thelocalmarketplace.software.GUI.MembershipNumberScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.Controller.MembershipCardReaderController;

import powerutility.PowerGrid;

public class MembershipNumberScreenTest {
    private MembershipNumberScreen screen;
    private CentralCheckoutStationLogic logic;
    private JFrame frame;
    private JTextField numberTextField;
    private JButton returnButton, enterButton;
    private volatile boolean found;
    private JLabel enterMembershipMessage;
    private JButton[] enterNumButtons;
    
    
    private AbstractSelfCheckoutStation station;
    private PrimaryGUI primary;
    private CentralCheckoutStationLogic centralCheckoutStationLogic;

    
    
    @Test
    public void testSwipeWithValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "12345678";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[0].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }
    
    
    @Test
    public void testSwipeWithInValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "123456";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[0].doClick(); 
    	numButtons[0].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Error: Members not found. Please try again.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }
    
    @Test
    public void testInsertWithValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "12345678";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[1].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }
    
    @Test
    public void testInsertWithInValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "12345";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[1].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Error: Members not found. Please try again.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }
    
    @Test
    public void testScanWithValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "1444";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[2].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }
    
    
    @Test
    public void testScanWithInValidMembershipNumber() throws Exception {
    	
    	PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
    	AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
		
    	MembershipNumberScreen newScreen = new MembershipNumberScreen(primary, logic1);
    	
    	JTextField textField = newScreen.getNumberTextField();
    	JButton[] numButtons = newScreen.getEnterNumButtons();
    	
    	// Wait enough time for the asynchronous operations to complete
        Thread.sleep(2000);
        
    	String validMembershipNumber = "1442";
    	
        // Simulate entering a valid membership number and triggering the swipe action.
    	textField.setText(validMembershipNumber);
    	numButtons[2].doClick(); 
    	
    	JLabel message = newScreen.getEnterNumberMessage();
    			
        // Assert that the message indicates the number was successfully entered.
        String expectedMessage = "<html><center>Error: Members not found.<br>Enter Membership Number</center></html>";
        assertEquals(expectedMessage, message.getText());
    }

}
