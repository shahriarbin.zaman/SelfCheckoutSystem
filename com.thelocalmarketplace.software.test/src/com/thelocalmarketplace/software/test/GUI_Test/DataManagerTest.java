/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.software.GUI.DataManager;

public class DataManagerTest {
	DataManager dataManager;
	
	@Before
    public void setup() {
		dataManager = new DataManager();
	}
	
	@Test
	public void getLanguageTest() {
		String language = dataManager.getLanguage();
		String thisLangauge = "English";
		assertEquals(language, thisLangauge);
			
	}
	
	@Test
	public void getInstanceNullTest() {
		dataManager = null;
		assertTrue(dataManager.getInstance() != null);
	}
	
	@Test
	public void setLanguageTest() {
		String language = "French";
		dataManager.setLanguage(language);
		String thisLangauge = dataManager.getLanguage();
		assertEquals(language, thisLangauge);
	}

}