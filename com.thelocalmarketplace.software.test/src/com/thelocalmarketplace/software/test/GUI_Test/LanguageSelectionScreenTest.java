/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JPanel;


import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.screen.TouchScreenBronze;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.DataManager;
import com.thelocalmarketplace.software.GUI.LanguageSelectionScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;

import powerutility.PowerGrid;

public class LanguageSelectionScreenTest {
	private LanguageSelectionScreen languageScreen;
	private PrimaryGUI gui;
	private CentralCheckoutStationLogic logic;
	private SelfCheckoutStationBronze station;
	private TouchScreenBronze touchScreen;
	private AttendantStation aStation;
	private boolean showStartScreen;
	
	@Before
    public void setUp() throws Exception {
		PowerGrid grid = PowerGrid.instance();
		
		station = new SelfCheckoutStationBronze();
		station.plugIn(grid);
		station.turnOn();
		
		aStation = new AttendantStation();
		aStation.plugIn(grid);
		aStation.turnOn();
		
		logic = CentralCheckoutStationLogic.installOn(station, aStation);
		
		touchScreen = new TouchScreenBronze();
		touchScreen.plugIn(grid);
		touchScreen.turnOn();
		touchScreen.enable();
		touchScreen.setVisible(true);
		
		showStartScreen = false;
		
		gui = new PrimaryGUI(logic) {
	            @Override
	            public void showStartScreen() {
	                LanguageSelectionScreenTest.this.showStartScreen = true;
	                super.showStartScreen();
	            }
	        };
		
        languageScreen = new LanguageSelectionScreen(gui, logic);
        languageScreen.addWidgets();
             
    }
	
	
	@Test
	public void testAddWidgets() {
	    JPanel panel = languageScreen.getPanel();
	    assertNotNull("Panel should not be null", panel);

	    Component[] languagePanelComponents = panel.getComponents();
	    assertEquals("languagePanel should have 4 components", 4, languagePanelComponents.length);

	    assertTrue("The first component should be a JPanel", languagePanelComponents[0] instanceof JPanel);
	    JPanel languageButtonPanel = (JPanel) languagePanelComponents[0];

	    Component[] languageButtonPanelComponents = languageButtonPanel.getComponents();
	    assertEquals("languageButtonPanel should have 2 components", 2, languageButtonPanelComponents.length);

	    JButton selectEnglishButton = (JButton) languageButtonPanelComponents[0];
	    JButton selectFrenchButton = (JButton) languageButtonPanelComponents[1];

	    assertEquals("Button text should be English", "English", selectEnglishButton.getText());
	    assertEquals("Button text should be French", "French", selectFrenchButton.getText());

	    assertTrue("The second component should be a JButton", languagePanelComponents[1] instanceof JButton);
	    JButton returnButton = (JButton) languagePanelComponents[1];

	    assertEquals("Button text should be Return", "Return", returnButton.getText());
	}

	@Test
	public void testLanguageSelection_English() {
	    JPanel languageButtonPanel = (JPanel) languageScreen.getPanel().getComponent(0);

	    JButton selectEnglishButton = (JButton) languageButtonPanel.getComponent(0);

	    selectEnglishButton.doClick();

	    assertEquals("Language should be set to English", "English", DataManager.getInstance().getLanguage());
	}

	@Test
	public void testLanguageSelection_French() {
		JPanel languageButtonPanel = (JPanel) languageScreen.getPanel().getComponent(0);

	    JButton selectFrenchButton = (JButton) languageButtonPanel.getComponent(1);

	    selectFrenchButton.doClick();;

	    assertEquals("Language should be set to French", "French", DataManager.getInstance().getLanguage());
	}
	
	@Test
    public void testReturnButtonFunctionality() {
        JPanel panel = languageScreen.getPanel();
        JButton returnButton = (JButton) panel.getComponent(1); 
        
        returnButton.doClick(); 

        assertTrue("showStartScreen should be called on primary GUI", showStartScreen);
    }


}

