/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;


import com.jjjwelectronics.card.Card;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.PayObject;
import com.thelocalmarketplace.software.GUI.SystemListener;

import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import powerutility.PowerGrid;


public class PayObjectTest {
	CentralCheckoutStationLogic logic;
	
	Card card;
	Card membershipCard;
	Card null_card;
	SelfCheckoutStationBronze station = new SelfCheckoutStationBronze();
	
	@Before
	public void setUp() {
		station.plugIn(PowerGrid.instance());
		station.turnOn();
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(station);
		card = new Card("Mastercard", "123456", "Cardholder", "449", "5678", false, true);
		membershipCard = new Card("Membership", "345678", "Member", "990", "2010", false, true);
	}
	
	@Test
	public void testGetCard() {
		PayObject.setUpCard(logic);
		assertNotNull(PayObject.getCard());
	}
	
	@Test (expected = NullPointerException.class)
	public void testNullCard() {
		card = PayObject.getCard();
	}
	
	@Test
	public void testGetMembershipCard() {
		PayObject.setUpMembershipCard(logic);
		assertNotNull(PayObject.getMembershipCard());
	}
	
	@Test (expected = NullPointerException.class)
	public void testNullMembershipCard() {
		card = PayObject.getMembershipCard();
	}
	
}
