/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.swing.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.AttendantStationScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import com.thelocalmarketplace.software.GUI.StartScreen;

import powerutility.PowerGrid;

public class AttendantStationScreenTest {

    private AttendantStationScreen attendantStationScreen;

    private List<CentralCheckoutStationLogic> centralCheckoutStations;

    private CentralCheckoutStationLogic logic1;
    private CentralCheckoutStationLogic logic2;
    private AbstractSelfCheckoutStation station1;
    private AbstractSelfCheckoutStation station2;
    private PrimaryGUI gui;
    private PowerGrid grid;
    private AttendantStation aStation;

    @Before//setup method
    public void setUp() {
        station1 = new SelfCheckoutStationGold();
        grid = PowerGrid.instance();
        station1.plugIn(grid);
        station1.turnOn();
        
        station2 = new SelfCheckoutStationGold();
        station2.plugIn(grid);
        station2.turnOn();
        
        aStation = new AttendantStation();
        aStation.plugIn(grid);
        aStation.turnOn();

        logic1 = CentralCheckoutStationLogic.installOn(station1, aStation);
        logic2 = CentralCheckoutStationLogic.installOn(station2, aStation);
        gui = new PrimaryGUI(logic1, logic2);
        
        attendantStationScreen = new AttendantStationScreen(gui, logic1, logic2);//start screen instantiated
    }
    
    @After
    public void reset() {
    	logic1.sessionStarter.endSession();
    }
    
    @Test
    public void testGUIInitilization() {
    	assertNotNull(attendantStationScreen.getPanel());
    	
    }
    
    @Test
    public void testAddTimerResume() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton sessionStatusButton = attendantStationScreen.getSessionStatusButton();

        assertNotNull("Session status button is not found", sessionStatusButton);
        
        String initialButtonText = sessionStatusButton.getText();
        Timer timer = new Timer(500, e -> {
        	sessionStatusButton.doClick();
        });
        timer.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        timer.stop();
        assertEquals("Session status button text should be toggled", 
                     initialButtonText.equals("Resume Session") ? "Resume Session" : "Halt Session", 
                     sessionStatusButton.getText());
    }
    
    @Test
    public void testSwitchStation() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JComboBox<String> dropdown = attendantStationScreen.getDropdown();

        assertNotNull("Session status button is not found", dropdown);
        
        String initialDropdownText = (String) dropdown.getSelectedItem();
        
        assertEquals("Station1", initialDropdownText);
        
        dropdown.setSelectedIndex(1);
        String changedDropdownText = (String) dropdown.getSelectedItem();
        assertEquals("Station2", changedDropdownText); 
        
        dropdown.setSelectedIndex(0);
    }
    
    @Test
    public void testRemovePaper() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton removePaper = attendantStationScreen.getRemovePaperButton();

        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", removePaper);
        
        int initialPaperBalance = logic1.paperManager.getPaperInventory();
        assertEquals(100 , initialPaperBalance);
        
        removePaper.doClick();
        
        int removedPaperBalance = logic1.paperManager.getPaperInventory();
        assertEquals(95 , removedPaperBalance);
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testResolvePaper(){
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton removePaper = attendantStationScreen.getRemovePaperButton();

        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", removePaper);
        
        int initialPaperBalance = logic1.paperManager.getPaperInventory();
        assertEquals(100 , initialPaperBalance);

        logic1.paperManager.removePaper(95, "receipt");
        removePaper.doClick();
        
        int removedPaperBalance = logic1.paperManager.getPaperInventory();
        assertEquals(0 , removedPaperBalance);
        assertEquals(true, logic1.signalAttendantController.getNeedsAssistance());
        
        JButton resolvePaper = attendantStationScreen.getResolvePaperButton();
        resolvePaper.doClick();
        
        assertEquals(50 , logic1.paperManager.getPaperInventory());
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }

    
    @Test
    public void testRemoveInk() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton removeInk = attendantStationScreen.getRemoveInkButton();

        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", removeInk);
        
        int initialInkBalance = logic1.inkManager.getInkLevel();
        assertEquals(100 , initialInkBalance);
        
        removeInk.doClick();
        
        int removedInkBalance = logic1.inkManager.getInkLevel();
        assertEquals(90 , removedInkBalance);
        
        logic1.sessionStarter.startSession();
    }
 
    
    @Test
    public void testResolveInk() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton removeInk = attendantStationScreen.getRemoveInkButton();

        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", removeInk);
        
        int initialInkBalance = logic1.inkManager.getInkLevel();
        assertEquals(100 , initialInkBalance);

        logic1.inkManager.setInkLevel(51);
        removeInk.doClick();
        
        int removedInkBalance = logic1.inkManager.getInkLevel();
        assertEquals(41 , removedInkBalance);
        assertEquals(true, logic1.signalAttendantController.getNeedsAssistance());
        
        JButton resolveInk = attendantStationScreen.getResolveInkButton();
        resolveInk.doClick();
        
        assertEquals(100 , logic1.inkManager.getInkLevel());
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testEnableDisable() {
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton enable = attendantStationScreen.getEnableStationButton();
        JButton disable = attendantStationScreen.getDisableStationButton();
        
        assertNotNull("Session status button is not found", enable);
 
        disable.doClick();
        
        assertTrue(logic1.stationDisabler.getStationDisabled());
        
        enable.doClick();
        
        assertTrue(!logic1.stationDisabler.getStationDisabled());
    }
    
    @Test
    public void testRemoveCash(){
        JButton remove = attendantStationScreen.getRemoveCash();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", remove);
        
        int initialCash = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(100 , initialCash);
        
        remove.doClick();
        
        int removedCash = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(90 , removedCash); 
        
        logic1.sessionStarter.endSession();
    }
    
    @Test
    public void testAddCash(){
        JButton add = attendantStationScreen.getAddCash();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", add);
        
        int initialCash = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(100 , initialCash);
        
        add.doClick();
        
        int addedCash = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(110 , addedCash); 
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testResolveCashFull(){
        JButton add = attendantStationScreen.getAddCash();
        
        logic1.sessionStarter.endSession();

        int initialCashBalance = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(100 , initialCashBalance);

        logic1.cashAndCoinManager.addCash(90);
        add.doClick();
        
        int addedCashBalance = logic1.cashAndCoinManager.getCashBalance();
        
        assertEquals(true, logic1.signalAttendantController.getNeedsAssistance());
        
        JButton resolveCash = attendantStationScreen.getResolveCashButton();
        resolveCash.doClick();
        
        assertEquals(150 , logic1.cashAndCoinManager.getCashBalance());
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testResolveCashEmpty(){
        JButton remove = attendantStationScreen.getRemoveCash();
        
        logic1.sessionStarter.endSession();

        int initialCashBalance = logic1.cashAndCoinManager.getCashBalance();
        assertEquals(100 , initialCashBalance);

        logic1.cashAndCoinManager.removeCash(90);
        remove.doClick();
        
        int addedCashBalance = logic1.cashAndCoinManager.getCashBalance();
        
        assertEquals(true, logic1.signalAttendantController.getNeedsAssistance());
        
        JButton resolveCash = attendantStationScreen.getResolveCashButton();
        resolveCash.doClick();
        
        assertEquals(50 , logic1.cashAndCoinManager.getCashBalance());
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testSelectCoin(){
    	JComboBox<Integer> coinSelect = attendantStationScreen.getCoinDropdown();
        assertNotNull("Session status button is not found", coinSelect);
        
        int initialCoin = (int) coinSelect.getSelectedItem();
        
        assertEquals(1 , initialCoin);
        
        coinSelect.setSelectedIndex(1);
        
        int newCoin = (int) coinSelect.getSelectedItem();
        assertEquals(50 , newCoin); 
        
    }
    
    @Test
    public void testRemoveCoin(){
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton remove = attendantStationScreen.getRemoveCoin();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", remove);
        
        int initialCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(10 , initialCoins);
        
        remove.doClick();
        
        int removedCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(9 , removedCoins); 
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testAddCoin(){
        JPanel attendantPanel = attendantStationScreen.getPanel();
        JButton add = attendantStationScreen.getAddCoin();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", add);
        
        int initialCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(10 , initialCoins);
        
        add.doClick();
        
        int addedCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(11 , addedCoins); 
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testResolveFullCoin(){
        JButton add = attendantStationScreen.getAddCoin();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", add);
        
        int initialCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(10 , initialCoins);
        
        logic1.cashAndCoinManager.addCoins(1, 9);
        add.doClick();
        
        JButton resolveCoin = attendantStationScreen.getResolveCoinsButton();
        resolveCoin.doClick();
        
        int resolvedCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        
        assertEquals(10 , resolvedCoins);
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void testResolveEmptyCoin(){
    	JButton remove = attendantStationScreen.getRemoveCoin();
        
        logic1.sessionStarter.endSession();
        
        assertNotNull("Session status button is not found", remove);
        
        int initialCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        assertEquals(10 , initialCoins);
        
        logic1.cashAndCoinManager.removeCoins(1, 9);
        remove.doClick();
        
        JButton resolveCoin = attendantStationScreen.getResolveCoinsButton();
        resolveCoin.doClick();
        
        int resolvedCoins = logic1.cashAndCoinManager.getCoinInventory().get(1);
        
        assertEquals(10 , resolvedCoins);
        assertEquals(false, logic1.signalAttendantController.getNeedsAssistance());
        
        logic1.sessionStarter.startSession();
    }
    
    @Test
    public void showAddItems() {
    	JButton show = attendantStationScreen.getAddItemByTextButton();
    	
    	show.doClick();
    	
    }
  
}