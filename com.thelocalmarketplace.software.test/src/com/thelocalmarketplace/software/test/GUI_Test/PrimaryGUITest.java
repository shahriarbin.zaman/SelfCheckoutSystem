/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;//Package name identified

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.CardLayout;
import java.lang.reflect.Field;

import javax.swing.JPanel;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;

import powerutility.PowerGrid;
//Imports declared

public class PrimaryGUITest {
	private PrimaryGUI gui;
	private PowerGrid grid;
	private AbstractSelfCheckoutStation station;
	private AttendantStation aStation;
	private CentralCheckoutStationLogic logic;
	private guiStub stub;
	//Defining variables needed for testing
	
	@Before//Setup method to declare the variables needed
	public void setUp() {
		grid = PowerGrid.instance();
		station = new SelfCheckoutStationGold();
		station.plugIn(grid);
		station.turnOn();
		//Instantiating and plugging in the station
		
		aStation= new AttendantStation();
		aStation.plugIn(grid);
		aStation.turnOn();
		//And doing the same with the attendant station
		
		logic = CentralCheckoutStationLogic.installOn(station, aStation);//Instantiating the logic
	}
	
	@Test//Testing to see if the constructor can be called for primary gui and making sure everything sets up right
	public void TestConstructor() {
		gui = new PrimaryGUI(logic);

		 assertNotNull(gui);
	     assertNotNull(gui.getStartScreen());
	     assertNotNull(gui.getLanguageSelectionScreen());
	     assertNotNull(gui.getAddItemsScreen());
	     assertNotNull(gui.getAttendantStationScreen());
	     assertNotNull(gui.getPaymentScreen());
	     assertNotNull(gui.getMembershipNumberScreen());
	     assertNotNull(gui.getAttendantKeyboardScreen());
	     //makes sure all variables setup by the gui are instantiated properly after the constructor is called
	     //In turn, the constructor is tested as well as all of the getter methods in just one test case
	}
	
	@Test//Testing to see whether or not showing the keyboard works
	public void testShowKeyboardScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.keyboardShown);
		stub.showKeyboard();
		assertTrue(stub.keyboardShown);
		//Testing the methods with the booleans provided by the stubs
	}
	
	//The same exact logic and assertions are used to test the remianing setter methods:
	@Test
	public void testShowStartScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.startScreenShown);
		stub.showStartScreen();
		assertTrue(stub.startScreenShown);
	}
	
	@Test
	public void testshowLanguageSelectionScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.langSelScreenShown);
		stub.showLanguageSelectionScreen();
		assertTrue(stub.langSelScreenShown);
	}
	
	@Test
	public void testshowAddItemsScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.addScreenShown);
		stub.showAddItemsScreen();
		assertTrue(stub.addScreenShown);
	}
	
	@Test
	public void testshowPaymentOptionsScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.payopScreenShown);
		stub.showPaymentOptionsScreen();
		assertTrue(stub.payopScreenShown);
	}
	
	@Test
	public void testshowBanknoteScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.bnScreenShown);
		stub.showBanknoteScreen();
		assertTrue(stub.bnScreenShown);
	}
	
	@Test
	public void testshowCoinScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.coinScreenShown);
		stub.showCoinScreen();
		assertTrue(stub.coinScreenShown);
	}
	
	@Test
	public void testshowCreditScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.creditScreenShown);
		stub.showCreditScreen();
		assertTrue(stub.creditScreenShown);
	}
	
	@Test
	public void testshowDebitScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.debitScreenShown);
		stub.showDebitScreen();
		assertTrue(stub.debitScreenShown);
	}
	
	@Test
	public void testshowFailedPaymentScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.failPaymentScreenShown);
		stub.showFailedPaymentScreen();
		assertTrue(stub.failPaymentScreenShown);
	}
	
	@Test
	public void testshowThankYouScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.tyScreenShown);
		stub.showThankYouScreen();
		assertTrue(stub.tyScreenShown);
	}
	
	@Test
	public void testshowMembershipNumberScreen() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.memNumScreenShown);
		stub.showMembershipNumberScreen();
		assertTrue(stub.memNumScreenShown);
	}
	
	@Test
	public void testshowAttendantKeyboard() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.AKBScreenShown);
		stub.showAttendantKeyboard();
		assertTrue(stub.AKBScreenShown);
	}
	
	@Test
	public void testshowAttendantMain() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		stub = new guiStub(logic);
		assertFalse(stub.ATMScreenShown);
		stub.showAttendantMain();
		assertTrue(stub.ATMScreenShown);
	}
	
	@Test
	public void testPanelShown() {
		stub = new guiStub(logic);
		assertFalse(stub.panelShown);
		stub.showSearchPanel();
		assertTrue(stub.panelShown);
	}
	//The above was testing the setter methods
	
	//Testing the main method to see whether or not execution of main works
	@Test
	public void testMain() {
		String[] args = {""};
		boolean didWork = false;
		try {
			gui.main(args);//calling the main method
			didWork =true;//if it worked, then set this boolean to true
			} catch (Exception e) {
				didWork = false;//or else set it to false
			}
		assertTrue(didWork);//it should be true- verify using assertions
	}
	
	//Gui stub so we can keep track of the exections
	class guiStub extends PrimaryGUI{
		public boolean panelShown = false;
		public boolean keyboardShown = false;
		public boolean startScreenShown = false;
		public boolean langSelScreenShown = false;
		public boolean addScreenShown = false;
		public boolean payopScreenShown = false;
		
		public boolean bnScreenShown = false;
		public boolean coinScreenShown = false;
		
		public boolean creditScreenShown = false;
		public boolean debitScreenShown = false;
		public boolean failPaymentScreenShown = false;
		public boolean tyScreenShown = false;
		public boolean memNumScreenShown = false;
		public boolean AKBScreenShown = false;
		public boolean ATMScreenShown = false;
		//The booleans are declared that determines whether or not the screen show methods worked
		
		public guiStub(CentralCheckoutStationLogic logic) {
			super(logic);
		}//constructor stub
		
		//The following methods call on the same respective methods from the parent class, and set their respective booleans from false to true
		//so that they can be verified during the test cases' assertions
		public void showSearchPanel() {
			super.showSearchPanel();
			panelShown = true;
		}
		public void showKeyboard() {
			super.showKeyboard();
			keyboardShown = true;
		}
		
	    public void showStartScreen() {
	    	super.showStartScreen();
	    	startScreenShown = true;
	    }

	    public void showLanguageSelectionScreen() {
	    	super.showLanguageSelectionScreen();
	    	langSelScreenShown = true;
	    }

	    public void showAddItemsScreen() {
	    	super.showAddItemsScreen();
	    	addScreenShown = true;
	    }

	    public void showPaymentOptionsScreen() {
	    	super.showPaymentOptionsScreen();
	    	payopScreenShown = true;
	    }
	    
	    public void showBanknoteScreen() {
	    	super.showBanknoteScreen();
	    	bnScreenShown = true;
	    }
	    
	    public void showCoinScreen() {
	    	super.showCoinScreen();
	    	coinScreenShown = true;
	    }
	    
	    public void showCreditScreen() {
	    	super.showCreditScreen();
	    	creditScreenShown = true;
	    }
	    
	    public void showDebitScreen() {
	    	super.showDebitScreen();
	    	debitScreenShown = true;
	    }
	    
	    public void showFailedPaymentScreen() {
	    	super.showFailedPaymentScreen();
	    	failPaymentScreenShown = true;
	    }
	    
	    public void showThankYouScreen() {
	    	super.showThankYouScreen();
	    	tyScreenShown = true;
	    }
	    
	    public void showMembershipNumberScreen() {
	    	super.showMembershipNumberScreen();
	    	memNumScreenShown = true;
	    }
	 
	    public void showAttendantKeyboard() {
	    	super.showAttendantKeyboard();
	    	AKBScreenShown = true;
	    }
	    
	    public void showAttendantMain() {
	    	super.showAttendantMain();
	    	ATMScreenShown = true;
	    }
	    
	}
}