/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.software.GUI.ItemDatabase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;

public class ItemDatabaseTest {

    @BeforeClass
    public static void setUpClass() {
        ItemDatabase.loadItems();
        ItemDatabase.initBarcodedItems();
    }

    @Test
    public void testLoadItems() {
        assertFalse("PLUProduct map should not be empty", ItemDatabase.getPLUProducts().isEmpty());
        assertFalse("barcodedProduct map should not be empty", ItemDatabase.getBarcodeItems().isEmpty());
        assertFalse("PLUPros list should not be empty", ItemDatabase.getPLUItems().isEmpty());
        assertFalse("barPros list should not be empty", ItemDatabase.getBarcodeProduct().isEmpty());
    }

    @Test
    public void testGetPLUProducts() {
        Map<String, PLUCodedItem> pluProducts = ItemDatabase.getPLUProducts();
        assertNotNull("PLU products map should not be null", pluProducts);
        // Testing for a specific PLU-coded item - banana
        assertTrue("PLU products should contain key for '0001'", pluProducts.containsKey("0001"));
        PLUCodedItem banana = pluProducts.get("0001");
        assertNotNull("PLUCodedItem for '0001' should not be null", banana);
        assertEquals("The PLU code for '0001' should be '0001'", "0001", banana.getPLUCode().toString());
    }

    @Test
    public void testGetBarcodeItems() {
        Map<String, BarcodedItem> barcodeItems = ItemDatabase.getBarcodeItems();
        assertNotNull("Barcoded items map should not be null", barcodeItems);
        assertTrue("Barcoded items should contain key '1'", barcodeItems.containsKey("1"));
        assertNotNull("BarcodedItem for '1' should not be null", barcodeItems.get("1"));
    }

    @Test
    public void testGetPLUItems() {
        ArrayList<PLUCodedProduct> pluItems = ItemDatabase.getPLUItems();
        assertNotNull("PLU items list should not be null", pluItems);
        assertFalse("PLU items list should not be empty", pluItems.isEmpty());
        // Testing for a specific PLU-coded product - banana
        PLUCodedProduct bananaProduct = pluItems.stream()
                .filter(product -> "BANANA".equals(product.getDescription()))
                .findFirst()
                .orElse(null);
        assertNotNull("PLUCodedProduct for 'BANANA' should not be null", bananaProduct);
    }

    @Test
    public void testGetBarcodeProduct() {
        ArrayList<BarcodedProduct> barcodeProducts = ItemDatabase.getBarcodeProduct();
        assertNotNull("Barcoded products list should not be null", barcodeProducts);
        assertFalse("Barcoded products list should not be empty", barcodeProducts.isEmpty());
        // Testing for a specific barcoded product - apple
        BarcodedProduct appleProduct = barcodeProducts.stream()
                .filter(product -> "apple".equalsIgnoreCase(product.getDescription()))
                .findFirst()
                .orElse(null);
        assertNotNull("BarcodedProduct for 'apple' should not be null", appleProduct);
    }
}