/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.PaymentScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import com.thelocalmarketplace.software.GUI.SystemListener;

import powerutility.PowerGrid;

public class SystemListenerTest {
	private static PaymentScreen paymentScreen;
	//private static PaymentScreenStub paymentScreenStub;
	public static CentralCheckoutStationLogic centralCheckoutStationLogic;
	private SelfCheckoutStationGold station;
	private PrimaryGUI primary;
	private PowerGrid grid;
	private AttendantStation aStation;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	
    @Before
    public void setUp() {
    	System.setOut(new PrintStream(outputStreamCaptor)); // Redirect System.out to capture output
    	grid = PowerGrid.instance();
    	station = new SelfCheckoutStationGold();
    	station.plugIn(grid);
    	station.turnOn();
    	
    	aStation = new AttendantStation();
    	aStation.plugIn(grid);
    	aStation.turnOn();
    	
    	centralCheckoutStationLogic = CentralCheckoutStationLogic.installOn(station, aStation);
    	primary = new PrimaryGUI(centralCheckoutStationLogic);
    	
        paymentScreen = new PaymentScreen(primary, centralCheckoutStationLogic);

    }
    @After
    public void tearDown() {
        System.setOut(System.out); // Reset System.out to its original setting
    }
    
    
    @Test
    public void testConnectSystemListener() {
    	//tests connectSystemListener method
    	TestableSystemListener.connect(paymentScreen, centralCheckoutStationLogic);

        // Check if SystemListener's paymentScreen is the same as the one passed
        assertSame(paymentScreen, SystemListener.getPaymentScreen());

        // Check if SystemListener's centralCheckoutStation is the same as the one passed
        assertSame(centralCheckoutStationLogic, SystemListener.getCentralCheckoutStation());
    }
    @Test
    public void testAPaymentHasBeenMade() {
        SystemListener.aPaymentHasBeenMade();

        String output = outputStreamCaptor.toString().trim();
        assertEquals("Payment recieved.", output);
    }
    


    @Test
    public void testAPaymentHasFailedWithoutMessage() {
        SystemListener.aPaymentHasFailed();
        String output = outputStreamCaptor.toString().trim();
        assertEquals("Payment failed.", output);
    }
    @Test
    public void testAPaymentHasFailed() {
        String expectedErrorMessage = "Card declined";
        SystemListener.aPaymentHasFailed(expectedErrorMessage);

        // Check the console output for "Payment failed."
        String output = outputStreamCaptor.toString().trim();
        assertEquals("Payment failed.", output);

    }
    
    
}

class TestableSystemListener extends SystemListener {
    public static void connect(PaymentScreen paymentScreen, CentralCheckoutStationLogic centralCheckoutStation) {
        connectSystemListener(paymentScreen, centralCheckoutStation, getMembershipNumberScreen() );
        
    }
}