/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Component;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.AddItemsScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import powerutility.PowerGrid;

public class AddItemsScreenTest {
	
	private CentralCheckoutStationLogic logic;
	private SelfCheckoutStationGold stationGold;
	private PrimaryGUI primaryGUI;
	private AddItemsScreen addItemsScreen;
	PriceLookUpCode heldItem;
	private JPanel addItemsPanel;
	
	@Before
	public void setup() {
		
		PowerGrid.engageUninterruptiblePowerSource();
		
		logic = new CentralCheckoutStationLogic(logic);
		stationGold = new SelfCheckoutStationGold();
		stationGold.plugIn(PowerGrid.instance());
		stationGold.turnOn();
		
		logic = CentralCheckoutStationLogic.installOn(stationGold);
		logic.attendantStation.screen.plugIn(PowerGrid.instance());
		logic.attendantStation.screen.turnOn();
		primaryGUI = new PrimaryGUI(logic);
		
		logic.sessionStarter.startSession();
		
		addItemsScreen = primaryGUI.getAddItemsScreen();
		addItemsPanel = addItemsScreen.getPanel();
	}
	
	// Test to see that the addItemsScreen is divided into 4 sections
	@Test
	public void testNumberOfSectionsOnAddItemsScreen() {
		
		Component[] addItemsComponents = addItemsPanel.getComponents();
		int expectedValue = 4;
		assertEquals(expectedValue, addItemsComponents.length);
		
	}
	
	/*
	// Test adding item using PLU code
	@Test
	public void testAddingPLUCodedItem() {
		
		JTextField itemTextField = addItemsScreen.getKeyboard().getTextField();
		DefaultListModel<String> listModel = addItemsScreen.getListModel();
		JButton pluButton = addItemsScreen.getAddPLUButton();
		
		// Pass the PLU code of a banana
		itemTextField.setText("0001"); 
		pluButton.doClick();
		
		// Check if banana and its prices were added to the list model
		assertTrue(!listModel.isEmpty());

	}
	
	// Test adding item using barcode scanner
	@Test
	public void testAddingBarcodedItem() {
		
		JTextField itemTextField = addItemsScreen.getKeyboard().getTextField();
		DefaultListModel<String> listModel = addItemsScreen.getListModel();
		JButton barcodeButton = addItemsScreen.getAddBarcodeButton();
		
		// Pass the barcode of an apple
		itemTextField.setText("1");
		barcodeButton.doClick(); 
		
		// Check if apple and its prices were added to the list model
		assertTrue(!listModel.isEmpty());
		
	}
	
	// Test adding item using handheld scanner
	@Test
	public void testAddingItemWithHandheldScanner() {
		
		JTextField itemTextField = addItemsScreen.getKeyboard().getTextField();
		DefaultListModel<String> listModel = addItemsScreen.getListModel();
		JButton handHeldScanner = addItemsScreen.getHandheldScanner();
		
		// Pass the pen using hand held scanner
		itemTextField.setText("2");
		handHeldScanner.doClick();
		
		// Check if pen and its prices were added to the list model
		assertTrue(!listModel.isEmpty());
		
	}
	
	@Test
	public void testAddingItemWithEmptyBarcode() {
		
		JTextField itemTextField = addItemsScreen.getKeyboard().getTextField();
		DefaultListModel<String> errorModel = addItemsScreen.getErrorModel();
		JButton barcodeButton = addItemsScreen.getAddBarcodeButton();
		
		// Pass an empty barcode
		itemTextField.setText("");
		barcodeButton.doClick();
		
		// Check is error model contains error message
		String expectedErrorMessage = "Barcode was empty";
		assertEquals(expectedErrorMessage, errorModel.get(0));
		
	}
	
	@Test
	public void testAddingItemWithEmptyPLUCode() {
		
		JTextField itemTextField = addItemsScreen.getKeyboard().getTextField();
		DefaultListModel<String> errorModel = addItemsScreen.getErrorModel();
		JButton pluButton = addItemsScreen.getAddPLUButton();
		
		// Pass the PLU code of a banana
		itemTextField.setText("");
		pluButton.doClick();
		String expectedErrorMessage = "Unknown item";
		assertEquals(expectedErrorMessage, errorModel.get(0));
	}
	*/
	
	@Test
	public void testHelpButton() {
		
		JButton helpButton = addItemsScreen.getHelpB();
		
		// Pass the number of bags
		helpButton.doClick();
		assertTrue(logic.signalAttendantController.getNeedsAssistance());
	}
	
	@Test
	public void testSetNumberOfBagsMethods() {
		addItemsScreen.setnumOfBags(5);
		assertEquals(addItemsScreen.getNumOfBags(), 5);
	}
}