/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;

import static org.junit.Assert.assertEquals;

import javax.swing.JFrame;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.software.GUI.ReceiptScreen;


public class ReceiptScreenTest {
	
	
	ReceiptScreen screen;
	private JFrame frame;
	

	
	@Before
	public void setUp() {
		screen = new ReceiptScreen("hello");
	}
	
	@Test
	public void testMain() {
		screen = new ReceiptScreen("text");

		assertEquals(screen,screen);

	}

}
