/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.test.GUI_Test;//Package name identified

import static org.junit.Assert.*;
import org.junit.Test;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.PaymentScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;

import java.awt.Component;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PaymentScreenTest {

    @Test
    public void testButtonLabels() {
        // Create an instance of PaymentScreen
        PrimaryGUI primary = new PrimaryGUI();
        CentralCheckoutStationLogic centralStation = new CentralCheckoutStationLogic(null);
        PaymentScreen paymentScreen = new PaymentScreen(primary, centralStation);
        
        // Get all buttons from the PaymentScreen instance
        List<JButton> buttons = getAllButtons(paymentScreen.getPaymentOptionsPanel());

        // Expected button labels
        String[] expectedLabels = {"Banknote", "Coin", "Credit Card", "Debit Card", "Return"};

        // Assert that each button has the expected label
        for (int i = 0; i < buttons.size(); i++) {
            assertEquals(expectedLabels[i], buttons.get(i).getText());
        }
    }

    @Test
    public void testButtonActions() {
        // Create an instance of PaymentScreen
        PrimaryGUI primary = new PrimaryGUI();
        CentralCheckoutStationLogic centralStation = new CentralCheckoutStationLogic(null);
        PaymentScreen paymentScreen = new PaymentScreen(primary, centralStation);
        
        // Get the buttons from the PaymentScreen instance
        JButton banknoteButton = paymentScreen.getBanknoteB();
        JButton coinButton = paymentScreen.getCoinB();
        JButton creditButton = paymentScreen.getCreditB();
        JButton debitButton = paymentScreen.getDebitB();
        JButton returnButton = paymentScreen.getReturnToItemScreen();

        // Simulate button clicks and verify the expected actions
        banknoteButton.doClick();
        assertTrue(paymentScreen.getBanknotePanel().isVisible());
        assertFalse(paymentScreen.getPaymentOptionsPanel().isVisible()); // Banknote panel should be visible

        coinButton.doClick();
        assertTrue(paymentScreen.getCoinPanel().isVisible());
        assertFalse(paymentScreen.getPaymentOptionsPanel().isVisible()); // Coin panel should be visible

        creditButton.doClick();
        assertTrue(paymentScreen.getCreditPanel().isVisible());
        assertFalse(paymentScreen.getPaymentOptionsPanel().isVisible()); // Credit panel should be visible

        debitButton.doClick();
        assertTrue(paymentScreen.getDebitPanel().isVisible());
        assertFalse(paymentScreen.getPaymentOptionsPanel().isVisible()); // Debit panel should be visible

        returnButton.doClick();
        assertFalse(paymentScreen.getBanknotePanel().isVisible());
        assertTrue(paymentScreen.getPaymentOptionsPanel().isVisible()); // Payment options panel should be visible
    }

    @Test
    public void testFailedPayment() {
        // Create an instance of PaymentScreen
        PrimaryGUI primary = new PrimaryGUI();
        CentralCheckoutStationLogic centralStation = new CentralCheckoutStationLogic(null);
        PaymentScreen paymentScreen = new PaymentScreen(primary, centralStation);
        
        // Simulate a failed payment
        paymentScreen.paymentFailed();
        
        // Check if the failed payment panel is visible
        assertTrue(paymentScreen.getFailedPaymentPanel().isVisible());
    }

    @Test
    public void testThankYouScreen() {
        // Create an instance of PaymentScreen
        PrimaryGUI primary = new PrimaryGUI();
        CentralCheckoutStationLogic centralStation = new CentralCheckoutStationLogic(null);
        PaymentScreen paymentScreen = new PaymentScreen(primary, centralStation);
        
        // Simulate a successful payment
        paymentScreen.paymentRecieved();
        
        // Check if the thank you panel is visible
        assertTrue(paymentScreen.getThankYouPanel().isVisible());
    }

    // Helper method to get all buttons recursively from a container
    private List<JButton> getAllButtons(Container container) {
        List<JButton> buttons = new ArrayList<>();
        Component[] components = container.getComponents();
        for (Component component : components) {
            if (component instanceof JButton) {
                buttons.add((JButton) component);
            } else if (component instanceof Container) {
                buttons.addAll(getAllButtons((Container) component));
            }
        }
        return buttons;
    }

   
}