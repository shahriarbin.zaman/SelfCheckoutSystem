/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import com.thelocalmarketplace.software.PaperManager;

public class PaperManagerTest {
    private PaperManager paperManager;
    private PaperManager paperManager2;

    @Before
    public void setup() {
        paperManager = new PaperManager(100);    // used to add paper types and quantities
        paperManager2 = new PaperManager(5);     // used for low paper tests
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructor_invalidinitialinventory() {
        new PaperManager(-100);
    }

    @Test
    public void testAddPaper_PositiveQuantity() {
        int initial_inventory = paperManager.getPaperInventory();
        String newPaperType = "Type 1";
        paperManager.addPaper(30, newPaperType);
        assertEquals(initial_inventory + 30, paperManager.getPaperInventory());
        assertTrue(paperManager.getPaperTypes().contains(newPaperType));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddPaper_NegativeQuantity() {
        paperManager.addPaper(-100, "Type 2");
    }

    @Test
    public void testRemovePaper_ValidQuantity() {
        String newPaperType = "Type 3";
        paperManager.addPaper(50, newPaperType);
        int initial_inventory = paperManager.getPaperInventory();
        paperManager.removePaper(20, newPaperType);
        assertEquals(initial_inventory - 20, paperManager.getPaperInventory());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemovePaper_NegativeQuantity() {
        paperManager.removePaper(-20, "Type 4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemovePaper_TooBigQuantity() {
        paperManager.removePaper(1000, "Type 5");
    }

    @Test
    public void testIsPaperLow() {
        assertFalse(paperManager.isPaperLow());
        assertTrue(paperManager2.isPaperLow());
    }
}
