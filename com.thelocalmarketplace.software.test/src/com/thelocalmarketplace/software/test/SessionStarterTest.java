package com.thelocalmarketplace.software.test;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.scanner.Barcode;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
//import com.thelocalmarketplace.hardware.SelfCheckoutStation;
//import com.jjjwelectronics.scale.ElectronicScale;
//import com.thelocalmarketplace.software.StartSession;

import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;

/*     Group Members and UCIDS
Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/


public class SessionStarterTest {
	private SessionStarter sessionHasStarted;
	private CentralCheckoutStationLogic logic;

	@Before
	public void setup() {
		sessionHasStarted = SessionStarter.getSessionStarter(logic);
	}

    @Test 
    public void testHasSessionBeenStarted() {
    	sessionHasStarted.startSession();
        assertTrue(sessionHasStarted.hasSessionBeenStarted());
        sessionHasStarted.endSession();
        assertFalse(sessionHasStarted.hasSessionBeenStarted());
    }
    
    @Test (expected = NullPointerException.class)
    public void testWithNull() {
    	sessionHasStarted = null; 
    	sessionHasStarted.startSession();
    }
}