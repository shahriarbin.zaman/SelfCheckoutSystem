/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.ListenerTest;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.SelfCheckoutStationSilver;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Listener.SessionStateListener;

import powerutility.PowerGrid;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;

public class SessionStateListenerTest {
	
        private CentralCheckoutStationLogic logic;
	    private SessionStateListener listener;
	    
	    int totalItems;
		ArrayList<BarcodedProduct> totalProductsList;
		ItemRemover removeItem;
		BarcodedItem item;
		BarcodedItem item2;
		PLUCodedItem pluItem;
		PLUCodedProduct pluProduct;
		Barcode testBarcode;
		Barcode testBarcode2;
		Mass testMass;
		PriceLookUpCode pluCode;
		String pluCodeString;
		String productDescription;
		long price;
		ShoppingCart shoppingCart;
		BarcodedProduct barcodedProduct;
		ElectronicScaleBronze bronze;
		ElectronicScaleSilver silver;
		ElectronicScaleGold gold;
	    	private PowerGrid grid;
	    	SelfCheckoutStationBronze stationBronze;
	   	SelfCheckoutStationSilver stationSilver;
	    	SelfCheckoutStationGold stationGold;
	   	private final PrintStream standardOut = System.out;
	   	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	   	private BigDecimal initialTotalWeight;
	   	private long initialTotalPrice;
		private IElectronicScale eScale;
		TypeOfScanner scanner;

		
		@Before
		public void setup() {
			System.setOut(new PrintStream(outputStreamCaptor));
			stationBronze = new SelfCheckoutStationBronze();
			logic = new CentralCheckoutStationLogic(logic);
			logic = CentralCheckoutStationLogic.installOn(stationBronze);
			this.logic.hardware.getBaggingArea();
			
			Numeral[] numerals = {Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
			Numeral[] numeral2 = {Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.six };
	        
			//creating test barcodes
			pluCodeString = "12345";
			testBarcode = new Barcode(numerals); 
	        testBarcode2 = new Barcode(numeral2);
	        pluCode = new PriceLookUpCode(pluCodeString);
	        productDescription = "apple";
	        price = 67;
			
	        //creating scale objects
			bronze = new ElectronicScaleBronze();
			silver = new ElectronicScaleSilver();
			gold = new ElectronicScaleGold();
			
			this.shoppingCart = ShoppingCart.getShoppingCart(logic);
			totalItems = shoppingCart.getTotalNumberOfItems(); // getting totalItems of shopping cart
			totalProductsList = shoppingCart.getShoppingCartProducts();
			
			removeItem = new ItemRemover(logic); //creating instance of RemoveItem class
			testMass = new Mass(BigDecimal.valueOf(500));  //creating test mass
			
			//creating item instances
			item = new BarcodedItem(testBarcode, testMass);  
			item2 = new BarcodedItem(testBarcode2, testMass);
			barcodedProduct = new BarcodedProduct(testBarcode, "Sample Product", 123,  4.56);
			pluItem = new PLUCodedItem(pluCode, testMass); 
			pluProduct = new PLUCodedProduct(pluCode, productDescription, price );
	       
			grid = PowerGrid.instance();
	        
			initialTotalWeight = shoppingCart.getTotalExpectedWeight(); //initial total wight of shoppingCart
	        initialTotalPrice = shoppingCart.getTotalPrice(); //initial total price of shopping cart
	        
			TypeOfScanner scanner = TypeOfScanner.HANDHELD;
	        listener = SessionStateListener.getSessionStateListener(logic);

	        


		}

	    @Test
	    public void testSingletonInstance() {
	        SessionStateListener listener2 = SessionStateListener.getSessionStateListener(logic);
	        assertSame(listener, listener2);
	    }
	    
	    @Test
	    public void testInitialStateIsNormal() {
	        assertEquals(SessionStateListener.SessionState.NORMAL, listener.getCurrentState());
	    }

	    @Test
	    public void testSetStateToBlocked() {
	        listener.setState(SessionStateListener.SessionState.BLOCKED);
	        assertEquals(SessionStateListener.SessionState.BLOCKED, listener.getCurrentState());
	    }
	    

}