/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/


package com.thelocalmarketplace.software.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.software.InkManager;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class InkManagerTest {
    private InkManager inkManager;
    private int initialInkLevel;
    private String inkType;
    private CentralCheckoutStationLogic logic;
    private int inkAmount;
    private String documentType;

    @Before
    public void setUp() {
        // Initialize logic here; adjust according to your actual constructor
        logic = new CentralCheckoutStationLogic(null); 
        inkType = "black";
        initialInkLevel = 60;
        documentType = "paper";
        inkManager = new InkManager(initialInkLevel, inkType, logic);
    }

    @Test
    public void should_RefillInk_WhenCalled() {
        inkManager.refillInk();
        assertEquals("Ink should be refilled to 100%", 100, inkManager.getInkLevel());
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_ThrowException_WhenInkLevelIsSetBelowZero() {
        new InkManager(-1, inkType, logic);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_ThrowException_WhenInkLevelIsSetAbove100() {
        new InkManager(101, inkType, logic);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_ThrowException_WhenAmountOfInkUsedIsNegative() {
        inkManager.useInk(-1, documentType);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_ThrowException_WhenNotEnoughInkIsAvailable() {
        inkManager.useInk(80, documentType);
    }

    @Test
    public void should_SubtractAmountFromInkLevel_WhenAmountIsUsed() {
        inkManager.useInk(40, documentType);
        assertEquals("Ink level should decrease by the amount used", 20, inkManager.getInkLevel());
    }

}
