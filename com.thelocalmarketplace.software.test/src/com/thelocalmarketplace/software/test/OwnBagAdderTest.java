/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818 
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/


package com.thelocalmarketplace.software.test;

import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.ShoppingCart;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OwnBagAdderTest {

    private OwnBagAdder ownBagAdder;
    private ShoppingCart shoppingCart;
    SelfCheckoutStationGold stationGold;
	CentralCheckoutStationLogic logic;

    @Before
    public void setUp() {
    	stationGold = new SelfCheckoutStationGold();
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(stationGold);
        ownBagAdder = new OwnBagAdder(null); // Pass null since the constructor does not depend on CentralCheckoutStationLogic
        shoppingCart = ShoppingCart.getShoppingCart(logic);
    }

    @Test
    public void shouldNotAddBagsWhenNumberOfBagsIsZero() {
        ownBagAdder.updateWeightWithBags(0);
        assertFalse(ownBagAdder.getAddBagState());
    }

    @Test
    public void shouldStartAddingBags() {
        ownBagAdder.startAddingBags();
        assertTrue(ownBagAdder.getAddBagState());
    }

    @Test
    public void shouldFinishAddingBags() {
        ownBagAdder.startAddingBags(); // Start adding bags
        ownBagAdder.finishAddingBags(); // Finish adding bags
        assertFalse(ownBagAdder.getAddBagState()); // Ensure the state is false after finishing
    }

    @Test
    public void shouldNotAddBagsIfAlreadyAdding() {
        ownBagAdder.startAddingBags();
        assertTrue(ownBagAdder.getAddBagState());

        // Try to start adding bags again
        ownBagAdder.startAddingBags();
        assertTrue(ownBagAdder.getAddBagState()); // State should remain true

        // Finish adding bags
        ownBagAdder.finishAddingBags();
        assertFalse(ownBagAdder.getAddBagState()); // Ensure the state is false after finishing
    }

    @Test
    public void testUpdateWeightWithBags_ZeroNumberOfBags() {
        ownBagAdder.updateWeightWithBags(0); // Assuming no bags are added

        assertFalse(ownBagAdder.getAddBagState()); // Check if addBagState is false
        assertNull(ownBagAdder.getBagMass()); // Ensure bag mass is null as no bags are added
    }

    @Test
    public void testUpdateWeightWithBags_NegativeNumberOfBags() {
        ownBagAdder.updateWeightWithBags(-1); // Assuming negative bags count (invalid input)

        assertFalse(ownBagAdder.getAddBagState()); // Check if addBagState is false
        assertNull(ownBagAdder.getBagMass()); // Ensure bag mass is null as no bags are added
    }
    
    @Test
    public void testStartAddingBags() {
        ownBagAdder.startAddingBags();
        assertTrue(ownBagAdder.getAddBagState());
    }
    
    @Test
    public void testFinishAddingBags() {
        ownBagAdder.finishAddingBags();
        assertFalse(ownBagAdder.getAddBagState());
    }
    
    @Test
    public void testGetAddBagState() {
        assertFalse(ownBagAdder.getAddBagState());
        ownBagAdder.startAddingBags();
        assertTrue(ownBagAdder.getAddBagState());
    }
    
    @Test
    public void testUpdateWeightWithBags_NumberOfBagsZero() {
        OwnBagAdder ownBagAdder = new OwnBagAdder(logic);
        
        ownBagAdder.updateWeightWithBags(0);
        
        assertFalse(ownBagAdder.getAddBagState());
        assertNull(ownBagAdder.getBagMass());
    }

    @Test
    public void testUpdateWeightWithBags_NumberOfBagsNegative() {
        OwnBagAdder ownBagAdder = new OwnBagAdder(logic);
        
        ownBagAdder.updateWeightWithBags(-1);
        
        assertFalse(ownBagAdder.getAddBagState());
        assertNull(ownBagAdder.getBagMass());
    }

 
    @Test
    public void testConstructor() {
        OwnBagAdder ownBagAdder = new OwnBagAdder(logic);
        
        assertFalse(ownBagAdder.getAddBagState());
    }
    @Test
    public void testBagMassCalculation() {
        // Create an instance of OwnBagAdder with the stubbed ShoppingCart
        OwnBagAdder ownBagAdder = new OwnBagAdder(logic);

        // Stubbing the total actual weight and total expected weight
        ownBagAdder.setCurrentMass(new BigDecimal("100")); // Stubbing the current mass
        ownBagAdder.setCurrentMass(new BigDecimal("90")); // Stubbing the current expected mass

        // Call the updateWeightWithBags method with numberOfBags = 2
        ownBagAdder.updateWeightWithBags(2);

        // Calculate the expected bag mass manually
        BigDecimal expectedBagMass = new BigDecimal("10");

        // Verify that the bag mass calculated by the method matches the manually calculated value
    }
}