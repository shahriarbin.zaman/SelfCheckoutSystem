package com.thelocalmarketplace.software.test;

/*     Group Members and UCIDS
Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.card.CardReaderBronze;
import com.jjjwelectronics.card.CardReaderGold;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.SelfCheckoutStationSilver;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;

public class CentralCheckoutStationLogicTest {
	
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	
	CentralCheckoutStationLogic logic;
	SelfCheckoutStationSilver silver;
	public AbstractSelfCheckoutStation hardware; 
	AttendantStation attendant = new AttendantStation(); 

	
	@Before
	public void setUp() {
		
		System.setOut(new PrintStream(outputStreamCaptor));

		silver = new SelfCheckoutStationSilver();
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(silver);
	}
	
	@Test
	public void Should_ReturnMethodOfPayment_WhenCalled(){	
		logic.setMethodsOfPayment(MethodsOfPayment.CARD);

		MethodsOfPayment payment = logic.getMethodsOfPayment();

		
		assertEquals(CentralCheckoutStationLogic.MethodsOfPayment.CARD, payment);
	}
	
	@Test
	public void Should_ReturnTypeOfCard_WhenCalled(){	
		logic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);

		TypeOfCard card = logic.getTypeOfCard();

		
		assertEquals(CentralCheckoutStationLogic.TypeOfCard.MEMBERSHIP_CARD, card);
	}
	
	@Test
	public void Should_ReturnMethodOfCardPayment_WhenCalled(){	
		logic.setMethodsOfCardPayment(MethodsOfCardPayment.INSERT);

		MethodsOfCardPayment paymentType = logic.getMethodsOfCardPayment();

		
		assertEquals(CentralCheckoutStationLogic.MethodsOfCardPayment.INSERT, paymentType);
	}
	
	@Test
	public void Should_ReturnMembershipCardsHashMap_WhenCalled(){	
		String membershipNumber = "12345";
		logic.addMembershipCard(membershipNumber);

		HashMap membershipCard = logic.getMembershipCards();
		
		assertTrue(membershipCard.containsKey(membershipNumber));

	}

	@Test
	public void Should_ReturnMembershipCard_WhenCalled(){	
		String card = ("TestCard");
		logic.setCurrentMemberShipCard(card);

		String membershipCard = logic.getCurrentMemberShipCard();

		
		assertEquals(card, membershipCard);
	} 
	
	@Test
	public void Should_ReturnCardIssuer_WhenCalled(){	
		CardIssuer issuer = new CardIssuer("TestName", 1234);
		logic.setCardIssuer(issuer);

		CardIssuer cardIssuer = logic.getCardIssuer();

		
		assertEquals(issuer, cardIssuer);
	} 
	
	@Test
	public void Should_ReturnCardReaderTypeBronze_WhenCalled(){	
		
		SelfCheckoutStationBronze bronze = new SelfCheckoutStationBronze();
		CentralCheckoutStationLogic logic = null;
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(bronze);
		
		assertEquals("Bronze", logic.getCardReaderType()); 
	} 
	
	
	@Test
	public void Should_ReturnCardReaderTypeSilver_WhenCalled(){	
		
		SelfCheckoutStationBronze bronze = new SelfCheckoutStationBronze();
		CentralCheckoutStationLogic logic = null;
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(bronze);
		
		assertEquals("Bronze", logic.getCardReaderType()); 
	} 
	
	@Test
	public void Should_ReturnCardReaderTypeGold_WhenCalled(){	
		
		SelfCheckoutStationGold gold = new SelfCheckoutStationGold();
		CentralCheckoutStationLogic logic = null;
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(gold);
		
		assertEquals("Gold", logic.getCardReaderType()); 
	} 
	
	
	@Test
	public void Should_ReturnScanningAreaTypeBronze_WhenCalled(){	
		
		SelfCheckoutStationSilver silver = new SelfCheckoutStationSilver();
		CentralCheckoutStationLogic logic = null;
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(silver);
		
		assertEquals("Bronze", logic.getScanningAreaType()); 
	} 
	
	@Test
	public void Should_ReturnScanningAreaTypeSilver_WhenCalled(){	
		
		SelfCheckoutStationGold gold = new SelfCheckoutStationGold();
		CentralCheckoutStationLogic logic = null;
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(gold);
		
		assertEquals("Silver", logic.getScanningAreaType()); 
	} 
	
	@Test
	public void testInstallOnOverloadWithAttendantStation() {
		SelfCheckoutStationBronze bronze = new SelfCheckoutStationBronze();
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(bronze, attendant);
		assertNotNull(logic1);
	}
	

}
