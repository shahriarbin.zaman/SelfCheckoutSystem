package com.thelocalmarketplace.software.test.ControllerTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import org.junit.Before;

import powerutility.PowerGrid;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Currency;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.NoCashAvailableException;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.BanknoteStorageUnit;
import com.tdc.banknote.IBanknoteDispenser;
import com.tdc.coin.Coin;
import com.tdc.coin.ICoinDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Controller.BanknoteDispenserController;
import com.thelocalmarketplace.software.Controller.CoinDispenserController;

import ca.ucalgary.seng300.simulation.SimulationException;
import powerutility.PowerGrid;
public class CoinDispenserControllerTest {
	
	//logic
		private SelfCheckoutStationGold checkout;
		private CentralCheckoutStationLogic logic;
		private CoinDispenserController coinDispenserController;
		private BigDecimal[] denominations;

		private Coin coin;
		private Currency CAD;
		private ICoinDispenser dispenser;
		
		//assertion
		private final PrintStream standardOut = System.out;
		private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
		private String[] lines;
		private String lastLine;

		@Before
		public void setup() {
			System.setOut(new PrintStream(outputStreamCaptor));
			PowerGrid.engageUninterruptiblePowerSource();
			logic = new CentralCheckoutStationLogic(logic);
			
			CAD = Currency.getInstance("CAD");
			denominations = new BigDecimal[] {
	        		new BigDecimal("1.00"),
	        		new BigDecimal("0.10"),
	        		new BigDecimal("0.25"),
	        };

			coin = new Coin(CAD, denominations[0]);
			System.out.println("coin: "+ coin.toString());
			AbstractSelfCheckoutStation.configureCurrency(CAD);
			
			AbstractSelfCheckoutStation.configureCoinDenominations(denominations);
			AbstractSelfCheckoutStation.configureCoinStorageUnitCapacity(5);
			AbstractSelfCheckoutStation.configureCoinDispenserCapacity(1000);
	        
	        checkout = new SelfCheckoutStationGold();
	        checkout.plugIn(PowerGrid.instance());
	        checkout.turnOn();
			logic = CentralCheckoutStationLogic.installOn(checkout);

			coinDispenserController = new CoinDispenserController(logic, denominations[0]);
			coinDispenserController.turnedOn(dispenser);
			coinDispenserController.enabled(dispenser);
			dispenser = logic.hardware.getCoinDispensers().get(denominations[0]);
			
			//if dispenser is 80% full, then it is almost full
			coinDispenserController.setWarningFullPercentage(0.8);
			//if dispenser is 20% full, then it is almost empty, 
			coinDispenserController.setWarningEmptyPercentage(0.2);
		}
		@After
		public void takeDown() {
			System.setOut(standardOut);
		}
		

		@Test public void test_GetMaxCapacity() {
			System.out.println(coinDispenserController.getMaxCapacity());
			assertEquals(coinDispenserController.getMaxCapacity(), dispenser.getCapacity());
		}
		@Test public void test_GetCurrentCapacity() {
			//nothing has been added, should be 0
			assertEquals(coinDispenserController.getCurrentCapacity(), 0);
		}
		

		@Test public void test_CheckIfCoinDispenserIsAlmostFullWhenAlmostFull() throws Exception {		
			for(int i = 0; i < 800; i++) {
				dispenser.load(coin);
			}
			assertEquals(coinDispenserController.getCurrentCapacity(), 800);
			coinDispenserController.checkIfCoinDispenserIsAlmostFull();
		}
		

		@Test public void test_CheckIfBanknoteDispenserIsAlmostFullWhenNotAlmostFull() throws Exception {		
			for(int i = 0; i < 799; i++) {
				dispenser.load(coin);
			}
			assertEquals(coinDispenserController.getCurrentCapacity(),799);
			coinDispenserController.checkIfCoinDispenserIsAlmostFull();
		}	
		

		@Test public void test_CheckIfCoinDispenserIsAlmostEmptyWhenAlmostEmpty() throws Exception {		
			for(int i = 0; i < 200; i++) {
				dispenser.load(coin);
			}
			assertEquals(coinDispenserController.getCurrentCapacity(),200);
			coinDispenserController.checkIfCoinDispenserIsAlmostEmpty();
		}

		@Test public void test_CheckIfBanknoteDispenserIsAlmostEmptyWhenNotAlmostEmpty() throws Exception {		
			for(int i = 0; i < 201; i++) {
				dispenser.load(coin);
			}
			assertEquals(coinDispenserController.getCurrentCapacity(),201);
			coinDispenserController.checkIfCoinDispenserIsAlmostEmpty();
		}

		@Test public void test_AnnounceDispenserIsEmptyWhenCoinsEmpty() throws Exception {		
			dispenser.load(coin);
			dispenser.emit();
			lines = outputStreamCaptor.toString().split("\\r?\\n");
	        lastLine = lines[lines.length-1].trim();
	        Assert.assertEquals(lastLine, "Coin dispenser is empty. Attendant has been alerted");
		}
		
		@Test
		public void test_AnnounceDispenserIsFullWhenCoinsFull() throws Exception {
			coinDispenserController.coinsFull(dispenser);
		    lines = outputStreamCaptor.toString().split("\\r?\\n");
		    lastLine = lines[lines.length-1].trim();
		    Assert.assertEquals(lastLine,"Coin dispenser is full. Attendant has been alerted.");
		}
		
		@Test public void test_AnnounceCoinRemovedWhenCoinsRemoved() throws CashOverloadException, NoCashAvailableException, DisabledException {
			dispenser.load(coin,coin);
			dispenser.emit();
			lines = outputStreamCaptor.toString().split("\\r?\\n");
	        lastLine = lines[lines.length-1].trim();
	        Assert.assertEquals(lastLine, "Coin removed successfully.");
		}
		
		
		
		@Test public void test_AnnounceBanknotesUnloaded() throws CashOverloadException {
			dispenser.load(coin, coin, coin);
			dispenser.unload();
		}
		

		
		@Test public void test_TurnOffAndDisableController() {
			coinDispenserController.disabled(dispenser);
			coinDispenserController.turnedOff(dispenser);
		}
		
		@Test public void test_AnnounceCoinAddedWhenCoinsAdded() throws CashOverloadException {
			coinDispenserController.coinAdded(dispenser, coin);
			lines = outputStreamCaptor.toString().split("\\r?\\n");
	        lastLine = lines[lines.length-1].trim();
	        Assert.assertEquals(lastLine, "Coin added successfully.");
		}
		


		
}
