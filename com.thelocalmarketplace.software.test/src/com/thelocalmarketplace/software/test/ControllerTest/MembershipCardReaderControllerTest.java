/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.card.Card;
import com.jjjwelectronics.card.Card.CardData;
import com.jjjwelectronics.scanner.Barcode;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Controller.CardReaderController;
import com.thelocalmarketplace.software.Controller.MembershipCardReaderController;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import powerutility.PowerGrid;

public class MembershipCardReaderControllerTest {
	private CentralCheckoutStationLogic logic;

	
	private MembershipCardReaderController membershipController;
	private CardIssuer Winston;
	
	private AbstractSelfCheckoutStation station;
	private Card membershipCard;
	private Calendar cardExpiry;
	private SessionStarter sessionStarter;
	

    private TypeOfCard validTypeOfCard;
    private TypeOfCard invalidTypeOfCard;
    private SessionState sessionState;

    private final PrintStream originalOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

	@Before
	public void setUp() {

		System.setOut(new PrintStream(outputStreamCaptor));
		
		PowerGrid grid = PowerGrid.instance();

		Winston = new CardIssuer("Winston", 10);
		
		station = new SelfCheckoutStationGold();
		station.plugIn(grid);
		station.turnOn();
		cardExpiry = Calendar.getInstance();
		cardExpiry.set(2025, 8, 25);
		
		System.setOut(new PrintStream(outputStreamCaptor));
		logic = CentralCheckoutStationLogic.installOn(station);
		logic.setCardIssuer(Winston);
		logic.setMethodsOfPayment(CentralCheckoutStationLogic.MethodsOfPayment.CARD);
	    validTypeOfCard = CentralCheckoutStationLogic.TypeOfCard.MEMBERSHIP_CARD;
	    
	    logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.SWIPE);

		
		membershipController = new MembershipCardReaderController(logic);
		
		membershipCard = new Card("Membership", "1234567890123456", "Harry Truman", "123", "1234", false, false);
		Winston.addCardData("1234567890123456", "Harry Truman", cardExpiry, "123", 10000);
		
	}

	@After
	public void restoreStreams() {
	    System.setOut(originalOut);
	}

	/*
     * this code is unreachable because when you create an instance of a self-checkout
     * it automatically intializes a card reader
     * 
     * this test case is only to ensure error handling works
     */
	@Test(expected = IOException.class)
	public void testPerformAction_CardReaderNotInitialized() throws IOException {
	    	
	        Card validCard = new Card("Membership", "12345", "Steve", null, null, true, false);
	        membershipController.performAction(validTypeOfCard,validCard);
	    }

    @Test (expected = IOException.class)
    public void testPerformAction_InvalidCardKind() throws IOException {
 
    	invalidTypeOfCard = CentralCheckoutStationLogic.TypeOfCard.CREDIT_CARD;
        Card invalidKindCard = new Card("Credit", "12345", "Steve", null, null, true, false);
        membershipController.performAction(invalidTypeOfCard, invalidKindCard);
    }

    @Test (expected = IOException.class)
    public void testPerformAction_MemberNotFound() throws IOException {
    	logic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
        Card testMembershipCard = new Card("Membership", "954535532", "Harry Truman", 
        		"123", "1234", true, true);
        
        membershipController.performAction(validTypeOfCard, testMembershipCard);

    }
    
    @Test (expected = IOException.class)
    public void testPerformAction_NonSwipeMethod() throws IOException {

        logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.INSERT);

        String cardNumber = "1234567890123456";
        Card validMembershipCard = new Card("Membership", cardNumber, "Harry Truman", "123", "1234", true, true);
        logic.addMembershipCard(cardNumber);
        membershipController.addMembershipCard(cardNumber);
        
        assertTrue("The membership card should exist in the system.", 
                logic.getMembershipCards().containsKey(validMembershipCard.number));
        
        membershipController.performAction(validTypeOfCard, validMembershipCard);

    }

    @Test
    public void testPerformAction_ValidSwipe() throws IOException {
    	logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.SWIPE);
    	logic.setTypeOfCard(validTypeOfCard);
        String cardNumber = "1234567890123456";
        Card validMembershipCard = new Card("Membership", cardNumber, "Harry Truman", "123", "1234", true, true);
        logic.addMembershipCard(cardNumber);
        membershipController.addMembershipCard(cardNumber);
        
        assertTrue("The membership card should exist in the system.", 
                logic.getMembershipCards().containsKey(validMembershipCard.number));
        
        membershipController.performAction(validTypeOfCard, validMembershipCard);

    }

    @Test
    public void testBarcodeScanned_ValidCard() throws Exception {

    	Numeral[] numerals = { Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
        Barcode barcode = new Barcode(numerals);

        membershipController.aBarcodeHasBeenScanned(null, barcode); 
        
        String[] lines = outputStreamCaptor.toString().trim().split(System.lineSeparator());
        String lastLine = lines[lines.length - 1];

        assertEquals("Membership card successfully verified", lastLine);
    }
    
    @Test
    public void testSwitchScannerWithNull() {
        outputStreamCaptor.reset();
        TypeOfScanner scannerType = null;

        membershipController.switchScanner(scannerType);

        String expectedOutput = "Scanner type not set, defaulting to MAIN scanner.";
        assertEquals(expectedOutput, outputStreamCaptor.toString().trim());
    }
    
    @Test
    public void testSwitchScannerToMain() {
        outputStreamCaptor.reset();
        TypeOfScanner scannerType = TypeOfScanner.MAIN;

        membershipController.switchScanner(scannerType);

        String expectedOutput = "Using the MAIN scanner.";
        assertEquals(expectedOutput, outputStreamCaptor.toString().trim());

    }
    
    @Test
    public void testSwitchScannerToHandheld() {

        outputStreamCaptor.reset();
        TypeOfScanner scannerType = TypeOfScanner.HANDHELD;

        membershipController.switchScanner(scannerType);

        String expectedOutput = "Using the HANDHELD scanner.";
        assertEquals(expectedOutput, outputStreamCaptor.toString().trim());

    }

    @Test
    public void testSwitchScannerToUnsupportedType() {
    	/*
         * this test case can't be completed as there is no way to set an unsupported type of scanner
         */
    }
    
    @Test
    public void testACardHasBeenInserted() {
        outputStreamCaptor.reset(); 
        membershipController.aCardHasBeenInserted();
        assertEquals("Membership card can not be inserted.", outputStreamCaptor.toString().trim());
    }


    
    @Test
    public void testTheCardHasBeenRemoved() {

        membershipController.logic.setMethodsOfPayment(CentralCheckoutStationLogic.MethodsOfPayment.CARD);
        membershipController.logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.SWIPE);
        membershipController.logic.setTypeOfCard(CentralCheckoutStationLogic.TypeOfCard.MEMBERSHIP_CARD);

        membershipController.theCardHasBeenRemoved();

        assertEquals(CentralCheckoutStationLogic.MethodsOfPayment.DEFAULT, membershipController.logic.getMethodsOfPayment());
        assertEquals(CentralCheckoutStationLogic.MethodsOfCardPayment.DEFAULT, membershipController.logic.getMethodsOfCardPayment());
        assertEquals(CentralCheckoutStationLogic.TypeOfCard.DEFAULT, membershipController.logic.getTypeOfCard());
    }
    
    @Test
    public void testACardHasBeenTapped() {
        membershipController.aCardHasBeenTapped();
        assertEquals("Membership card can not be tapped.", outputStreamCaptor.toString().trim());
    }
    
    @Test
    public void performAction_PrintsUnsupportedCardTypeMessage_WhenCardTypeIsUnsupported() throws IOException {

    	/*
        * the "Only membership card is accepted" error comes first therefore unable to test the print statement
        * when card type is unsupported
        */
    }
    
    @Test
    public void testADeviceHasBeenTurnedOn() {
    	boolean didWork = false;
		try {
			membershipController.aDeviceHasBeenTurnedOn(null);
			didWork = true;
		} catch (Exception e) {
			didWork = false;
		}
		
		assertTrue(didWork);

    }
    
    @Test
    public void testADeviceHasBeenTurnedOff() {
    	boolean didWork = false;
		try {
			membershipController.aDeviceHasBeenTurnedOff(null);
			didWork = true;
		} catch (Exception e) {
			didWork = false;
		}
		
		assertTrue(didWork);
    }
}