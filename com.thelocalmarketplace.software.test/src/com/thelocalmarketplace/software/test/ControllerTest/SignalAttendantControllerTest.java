/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
package com.thelocalmarketplace.software.test.ControllerTest;

	import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.junit.Before;
	import org.junit.Test;

	import com.thelocalmarketplace.hardware.ISelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;

import powerutility.PowerGrid;

import com.jjjwelectronics.bag.IReusableBagDispenser;
import com.jjjwelectronics.card.ICardReader;
import com.jjjwelectronics.printer.IReceiptPrinter;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.IBarcodeScanner;
import com.jjjwelectronics.screen.ITouchScreen;
import com.tdc.banknote.BanknoteDispensationSlot;
import com.tdc.banknote.BanknoteInsertionSlot;
import com.tdc.banknote.BanknoteStorageUnit;
import com.tdc.banknote.BanknoteValidator;
import com.tdc.banknote.IBanknoteDispenser;
import com.tdc.coin.CoinSlot;
import com.tdc.coin.CoinStorageUnit;
import com.tdc.coin.CoinValidator;
import com.tdc.coin.ICoinDispenser;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.CoinTray;

	public class SignalAttendantControllerTest {

	    private SignalAttendantController signalAttendantController;
	    private CentralCheckoutStationLogic logic;
	    private SelfCheckoutStationBronze customerStation;
	    private AttendantStation attendantStation;
	    private PowerGrid powerGrid;

	    @Before
	    public void setUp() {
	    	
	    	powerGrid = PowerGrid.instance();
	    	logic = new CentralCheckoutStationLogic(logic);
	        customerStation = new SelfCheckoutStationBronze();
	        logic = CentralCheckoutStationLogic.installOn(customerStation);
	        customerStation.plugIn(powerGrid);
	        customerStation.turnOn();
	        
	        signalAttendantController = this.logic.signalAttendantController;
	        attendantStation = this.logic.attendantStation;
	  
	    }

	    @Test
	    public void testAlertAttendant() {
	       
	        signalAttendantController.alertAttendant();
	        assertTrue(signalAttendantController.getNeedsAssistance());
	        assertFalse(signalAttendantController.getRequestCleared());
	    }

	    @Test
	    public void testSendAlertToAttendant() throws Exception {
	       
	        signalAttendantController.alertAttendant();
	        signalAttendantController.sendAlertToAttendant(customerStation);
	        assertTrue(signalAttendantController.isHelpSessionActive());
	        assertFalse(signalAttendantController.getRequestCleared());
	    }

	    @Test
	    public void testClearCustomerRequest() throws Exception {
	       
	        signalAttendantController.alertAttendant();
	        signalAttendantController.sendAlertToAttendant(customerStation);
	        signalAttendantController.clearCustomerRequest(customerStation);
	        assertFalse(signalAttendantController.isHelpSessionActive());
	        assertTrue(signalAttendantController.getRequestCleared());
	    }
	   
	   @Test
	   public void testSetRequestCleared() {
		   signalAttendantController.setRequestCleared(false);
		   signalAttendantController.alertAttendant();
		   assertFalse(signalAttendantController.getRequestCleared());
	   }
	   
	   @Test
	   public void testSendAlertWhenAssistanceIsNotNeeded() throws Exception {
		   signalAttendantController.sendAlertToAttendant(customerStation);
		   assertFalse(signalAttendantController.getNeedsAssistance());
	   }
	   
	   @Test(expected = Exception.class)
	   public void testSendTwoConsecutiveAlertsToAttendant() throws Exception {
		   signalAttendantController.alertAttendant();
		   signalAttendantController.sendAlertToAttendant(customerStation);
		   signalAttendantController.sendAlertToAttendant(customerStation);
	   }
	   
	   @Test(expected = Exception.class)
	   public void testClearCustomerRequestAfterAttendantHasClearedRequest() throws Exception {
		   signalAttendantController.alertAttendant();
		   signalAttendantController.sendAlertToAttendant(customerStation);
		   signalAttendantController.clearCustomerRequest(customerStation);
		   signalAttendantController.clearCustomerRequest(customerStation);
	   }
	   
	   @Test(expected = Exception.class)
	   public void testIfCustomerStationIsPartOfSupervisedStation() throws Exception {
		   SelfCheckoutStationBronze customerStation1 = new SelfCheckoutStationBronze();
		   SelfCheckoutStationBronze customerStation2 = new SelfCheckoutStationBronze();
		   SelfCheckoutStationBronze customerStation3 = new SelfCheckoutStationBronze();
		   SelfCheckoutStationBronze customerStation4 = new SelfCheckoutStationBronze();
		   
		   attendantStation.add(customerStation1);
		   attendantStation.add(customerStation2);
		   attendantStation.add(customerStation3);
		   attendantStation.add(customerStation4);
		   attendantStation.remove(customerStation);
		   
		   signalAttendantController.alertAttendant();
		   signalAttendantController.sendAlertToAttendant(customerStation);
	   }
	   
	}
