package com.thelocalmarketplace.software.test.ControllerTest;

/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Currency;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.OverloadedDevice;
import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.NoCashAvailableException;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.BanknoteDispenserGold;
import com.tdc.banknote.IBanknoteDispenser;
import com.tdc.coin.Coin;
import com.tdc.coin.ICoinDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Controller.BanknoteDispenserController;
import com.thelocalmarketplace.software.Controller.ChangeController;
import com.thelocalmarketplace.software.Controller.CoinDispenserController;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import powerutility.PowerGrid;

public class ChangeControllerTest {
	
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	ChangeController changeController;
	CentralCheckoutStationLogic logic;
	BigDecimal currentAmountPaid;
	BigDecimal remainingBalance;
	BigDecimal amountDue;
	BigDecimal changeDue;
	Currency currency;
	PowerGrid powerGrid;
	SelfCheckoutStationBronze bronzeStation;
	SessionStateListener listener;
	SessionState state;
	BigDecimal[] banknoteDenominations;
	BigDecimal[] coinDenominations;
	private IBanknoteDispenser banknoteDispenser;
	private ICoinDispenser coinDispenser;
	 
	Banknote banknote;
	Coin coin;
	
	private BanknoteDispenserController banknoteDispenserController;
	private CoinDispenserController coinDispenserController;
	private BanknoteDispenserGold abstractDispenser;
	
	@Before
	public void setUp() throws CashOverloadException {
		PowerGrid.engageUninterruptiblePowerSource();
        powerGrid = PowerGrid.instance();
		
		currency = Currency.getInstance("CAD");
		banknoteDenominations = new BigDecimal[] {
                new BigDecimal("0.50"), 
                new BigDecimal("0.75"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
         }; 
		
		coinDenominations = new BigDecimal[] {
        		new BigDecimal("1.00"),
        		new BigDecimal("0.10"),
        		new BigDecimal("0.25"),
        };

		coin = new Coin(currency, coinDenominations[0]);
		
		AbstractSelfCheckoutStation.configureCurrency(currency);
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);
		
		bronzeStation = new SelfCheckoutStationBronze();
		bronzeStation.plugIn(powerGrid);
		bronzeStation.turnOn();
		
		logic = CentralCheckoutStationLogic.installOn(bronzeStation);
		changeController = logic.changeController;
		
		banknoteDispenserController = new BanknoteDispenserController(logic, banknoteDenominations[0]);
		banknoteDispenserController.turnedOn(banknoteDispenser);
		banknoteDispenserController.enabled(banknoteDispenser);
		banknoteDispenser = logic.hardware.getBanknoteDispensers().get(banknoteDenominations[0]);
		banknote = new Banknote(currency, banknoteDenominations[0]);
		
		coinDispenserController = new CoinDispenserController(logic, coinDenominations[1]);
		coinDispenserController.turnedOn(banknoteDispenser);
		coinDispenserController.enabled(banknoteDispenser);
		coinDispenser = logic.hardware.getCoinDispensers().get(coinDenominations[1]);
	}
	
	@Test
	public void Should_DisplaySuccessMessage_IfAmountIsPaidInFull() throws EmptyDevice, OverloadedDevice, NoCashAvailableException, DisabledException, CashOverloadException {
		
		logic.hardware.getPrinter().addInk(50);
		
		currentAmountPaid = BigDecimal.valueOf(2.3);
		remainingBalance = BigDecimal.ZERO;
		amountDue = BigDecimal.valueOf(2.3);
		Currency.getInstance("CAD");
		
		changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, amountDue, currency);
		assertEquals(changeController.getChangeDue(), BigDecimal.ZERO);
	}

	@Test
	public void Should_DisplayAmountDue_IfThereIsBalanceTopay() throws EmptyDevice, OverloadedDevice, NoCashAvailableException, DisabledException, CashOverloadException {
		
		currentAmountPaid = BigDecimal.valueOf(1.1);
		remainingBalance = BigDecimal.valueOf(1.2);
		amountDue = BigDecimal.valueOf(2.3);
		Currency.getInstance("CAD");
		
		changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, amountDue, currency);
	}

	@Test
	public void Should_DispenseChangeAsBanknotes_IfAmountPaidIsGreaterThanAmountdue() throws EmptyDevice, OverloadedDevice, NoCashAvailableException, DisabledException, CashOverloadException {
		
		logic.hardware.getPrinter().addInk(50);
		
		banknoteDispenser.load(banknote, banknote, banknote);
		System.out.println("dispenser size: " + banknoteDispenser.size());
		
		currentAmountPaid = BigDecimal.valueOf(3.4);
		amountDue = BigDecimal.valueOf(2.4);
		remainingBalance = amountDue.subtract(currentAmountPaid);
		Currency.getInstance("CAD");
		
		changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, amountDue, currency);
		assertEquals(changeController.getChangeDue(), BigDecimal.valueOf(1.0));		
	}
	
	@Test
	public void Should_DispenseChangeAsBanknotesAndCoins() throws CashOverloadException, EmptyDevice, OverloadedDevice, NoCashAvailableException, DisabledException {
		
		logic.hardware.getPrinter().addInk(50);
		
		banknoteDispenser.load(banknote, banknote, banknote);
		coinDispenser.load(coin, coin, coin);
		System.out.println("dispenser size: " + banknoteDispenser.size());
		
		currentAmountPaid = BigDecimal.valueOf(3.4);
		amountDue = BigDecimal.valueOf(2.3);
		remainingBalance = amountDue.subtract(currentAmountPaid);
		Currency.getInstance("CAD");
		
		changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, amountDue, currency);
		assertEquals(changeController.getChangeDue(), BigDecimal.valueOf(1.1));
	}

	@Test(expected = NoCashAvailableException.class)
	public void should_RunOutOfCash_IfAmountPaidIsGreaterThanAmountDue() throws EmptyDevice, OverloadedDevice, NoCashAvailableException, DisabledException, CashOverloadException {
	
		logic.hardware.getPrinter().addInk(50);

		
	    BigDecimal amountDue = new BigDecimal("5.00");
	    BigDecimal currentAmountPaid = new BigDecimal("10.00");
	     
	    BigDecimal remainingBalance = amountDue.subtract(currentAmountPaid);
	    Currency currency = Currency.getInstance("CAD");

	    changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, amountDue, currency);

	    BigDecimal expectedChangeDue = currentAmountPaid.subtract(amountDue);
	    assertEquals(expectedChangeDue, changeController.getChangeDue());
	}

}