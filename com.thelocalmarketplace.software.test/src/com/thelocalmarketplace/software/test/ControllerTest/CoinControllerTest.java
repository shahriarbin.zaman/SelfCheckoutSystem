package com.thelocalmarketplace.software.test.ControllerTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import static org.junit.Assert.assertEquals;
/*     Group Members and UCIDS

Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.NoCashAvailableException;
import com.tdc.coin.AbstractCoinDispenser;
import com.tdc.coin.Coin;
import com.tdc.coin.CoinDispenserBronze;
import com.tdc.coin.CoinValidator;
import com.tdc.coin.ICoinDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.CoinController;
import com.thelocalmarketplace.software.Controller.CoinDispenserController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;

import ca.ucalgary.seng300.simulation.SimulationException;
import powerutility.PowerGrid;
	
public class CoinControllerTest {
	private CoinController coinController;
	private CentralCheckoutStationLogic centralSystem;
	private CentralCheckoutStationLogic logic;
	private SelfCheckoutStationBronze stationBronze;
	private Currency CAD;
	private BigDecimal[] denominations;
	private CoinValidator validator;
	private Currency coinCurrency;
	private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private PrintStream originalOut = System.out;
    private BarcodedItem testItem;
    private Barcode testBarcode;
    private BarcodedProduct testProduct;
    private List<BigDecimal> coinDenominations;
    private CoinDispenserController coinDispenserController;
    private ICoinDispenser dispenser;
    
    @Before
    public void setUp() {

        outContent = new ByteArrayOutputStream();
        originalOut = System.out;
        System.setOut(new PrintStream(outContent));
        
        PowerGrid.engageUninterruptiblePowerSource();
        logic = new CentralCheckoutStationLogic(logic);
        coinCurrency = Currency.getInstance("CAD");
        
        denominations = new BigDecimal[] {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
            }; 
        
        AbstractSelfCheckoutStation.configureCurrency(coinCurrency);
        AbstractSelfCheckoutStation.configureCoinDenominations(denominations);
        AbstractSelfCheckoutStation stationBronze = new SelfCheckoutStationBronze();
        stationBronze = new SelfCheckoutStationBronze();
        stationBronze.plugIn(PowerGrid.instance());
        stationBronze.turnOn();
        
        centralSystem = CentralCheckoutStationLogic.installOn(stationBronze);

        coinController = new CoinController(centralSystem);
        coinDispenserController = new CoinDispenserController(centralSystem, denominations[0]);
        coinDenominations = Arrays.asList(
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
            );

        validator = new CoinValidator(coinCurrency, coinDenominations);
        validator.connect(PowerGrid.instance());
        validator.activate();
        validator.enable();
        dispenser = new CoinDispenserBronze (10);
        dispenser.connect(PowerGrid.instance());
        dispenser.activate();
        
        AttendantStation attendantStation = new AttendantStation();
        SignalAttendantController signalAttendantController = new SignalAttendantController(centralSystem, attendantStation);
        centralSystem.signalAttendantController = signalAttendantController;
    }



    @Test
    public void testValidCoinDetected_RemainingBalance() {
    	testBarcode = new Barcode(new Numeral[]{Numeral.one});
        testProduct = new BarcodedProduct(testBarcode, "test Product", 1L, 100.0);
        testItem = new BarcodedItem(testBarcode, new Mass(100));

        ShoppingCart shoppingCart = ShoppingCart.getShoppingCart(centralSystem);
        ShoppingCart.getShoppingCart(centralSystem).removeBarcodedItemFromCart(testItem);

        shoppingCart.addBarcodedItemToCart(testItem);
        shoppingCart.addPrice(testProduct.getPrice()); 
        
    	BigDecimal value = new BigDecimal("0.50"); 
        coinController.validCoinDetected(validator, value);
        
        assertTrue("The output should display the remaining amount due", outContent.toString().contains("Amount Due: "));
    }

    @Test (expected = DisabledException.class)
    public void testLoadingCoin_WhenDispenserDisabled() throws SimulationException, CashOverloadException, NoCashAvailableException, DisabledException {
    	BigDecimal paymentAmount = new BigDecimal("5.00"); 
        BigDecimal amountDue = new BigDecimal("3.00"); 
        BigDecimal changeDue = paymentAmount.subtract(amountDue);
        Coin validCoin = new Coin(coinCurrency, amountDue);
        outContent.reset();
        
        coinDispenserController.disabled(dispenser);
        dispenser.enable();
        dispenser.disable();
        coinController.validCoinDetected(validator, paymentAmount);
        
    		dispenser.load(validCoin);
    		dispenser.emit();
    	}
    
    
	@Test
	public void testGetSessionSuspended_WhenSessionIsActive() {

	    boolean sessionSuspended = coinController.getSessionSuspended();

	    assertFalse("Session should not be suspended", sessionSuspended);
	}
	
	@Test
	public void testGetEncounteredException_WhenNoException() {

	    Exception encounteredException = coinController.getEncounteredException();

	    assertNull("No exception should have been encountered", encounteredException);
	}
	
	@Test
	public void testInvalidCoinDetected() {
	    outContent.reset();
	    testBarcode = new Barcode(new Numeral[]{Numeral.one});
        testProduct = new BarcodedProduct(testBarcode, "test Product", 1L, 100.0);
        testItem = new BarcodedItem(testBarcode, new Mass(100));


        ShoppingCart shoppingCart = ShoppingCart.getShoppingCart(centralSystem);
        ShoppingCart.getShoppingCart(centralSystem).removeBarcodedItemFromCart(testItem);
        shoppingCart.addBarcodedItemToCart(testItem);
        shoppingCart.addPrice(testProduct.getPrice()); 
	    
        System.setOut(new PrintStream(outContent));

	    coinController.invalidCoinDetected(validator);

	    String capturedOutput = outContent.toString().trim();

	    assertTrue("The output should confirm that an invalid coin was detected", 
	        capturedOutput.equals("Invalid coin detected"));

	   System.setOut(originalOut);
	}
	
	@Test
	public void testGetAmountDue_ReturnsCorrectAmount() {

	    BigDecimal expectedAmountDue = new BigDecimal("2");
	    assertEquals(expectedAmountDue, coinController.getAmountDue());
	}

	@Test
	public void testGetCurrentAmountPaid_ReturnsCurrentPaidAmount() {

	    BigDecimal value = new BigDecimal("0.50"); 
	    coinController.validCoinDetected(validator, value);
	    assertEquals(value, coinController.getCurrentAmountPaid());
	}

	@Test
	public void testGetRemainingBalance_ReturnsCorrectRemainingBalance() {
	    
		BigDecimal value = new BigDecimal("0.50"); 
        coinController.validCoinDetected(validator, value);
	    assertEquals(value, coinController.getRemainingBalance());
	}


}