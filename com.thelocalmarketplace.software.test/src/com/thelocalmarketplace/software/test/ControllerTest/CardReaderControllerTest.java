package com.thelocalmarketplace.software.test.ControllerTest;//package name

/*     Group Members and UCIDS
Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.card.Card;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.Controller.CardReaderController;

import junit.framework.Assert;
import powerutility.PowerGrid;
//Importing all classes and junit frameworks

public class CardReaderControllerTest {//Test class
	private CentralCheckoutStationLogic checkoutLogic;
	private CardReaderController readerController;
	
	private CardIssuer Winston;
	
	private AbstractSelfCheckoutStation station;
	private Card card;
	private Calendar cardExpiry;
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    //Setting up the variables needed to successfully test
    
	@Before//Method before the test cases
	public void setUp() {
		//checkoutLogic = new CentralCheckoutStationLogic(null);
		PowerGrid grid = PowerGrid.instance();
		PowerGrid grid2 = PowerGrid.instance();//instantiating some power grids to be used for vars
		Winston = new CardIssuer("Winston", 10);//Making a card issuer
		
		station = new SelfCheckoutStationGold();
		station.plugIn(grid);
		station.turnOn();
		cardExpiry = Calendar.getInstance();
		cardExpiry.set(2025, 8, 25);//Setting up the self checkout station and setting a card expiry date
		
		System.setOut(new PrintStream(outContent));//setting up printstream so we can assert values from the output stream
		checkoutLogic = CentralCheckoutStationLogic.installOn(station);
		checkoutLogic.setCardIssuer(Winston);//attatching logic with station and setting up issuer
		//
		
		readerController = new CardReaderController(checkoutLogic);//setting up the card reader controller
		
		card = new Card("Debit", "1234567890123456", "Harry Truman", "123", "1234", true, true);
		Winston.addCardData("1234567890123456", "Harry Truman", cardExpiry, "123", 10000);
	}
	
	@After//After the testing...
    public void restoreStreams() {
        System.setOut(originalOut);//we need to restore the output stream
    }
	
	@Test//testing swipe action
	public void testActionSWIPE() throws IOException {
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.SWIPE);// manually setting method to swipe
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);//manually setting card type to membership type
		Assert.assertEquals(readerController.performAction(card, "1234").getNumber(), "1234567890123456");
		//making sure we can successfully perform action and the card data output is correct
	}
	
	@Test//testing tap action
	public void testActionTAP() throws IOException {
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.TAP);
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);//setting up the checkout logic
		Assert.assertEquals(readerController.performAction(card, "1234").getNumber(), "1234567890123456");
		//making sure we can successfully perform action and card data output is correct
	}
	
	@Test//testing insert action
	public void testActionInsert() throws IOException {
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.INSERT);
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);//setting up the checkout logic
		Assert.assertEquals(readerController.performAction(card, "1234").getNumber(), "1234567890123456");
	}//making sure we can successfully perform action and card data output is correct
	
	@Test (expected = IllegalArgumentException.class)//checking to see if we can test without a proper payment method
	public void testActionDefault() throws IOException {
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.DEFAULT);
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.performAction(card, "1234");
		
	}//This should throw an IllegalArgumentException
	
	@Test//Testing to see if we can automatically insert card
	public void testCardInsertionMethod() throws IOException {
		
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.aCardHasBeenInserted();
		Assert.assertEquals(checkoutLogic.getMethodsOfCardPayment(), MethodsOfCardPayment.INSERT);
	}
	
	@Test//Testing to see if we can automatically insert and remove card (asserting with output stream)
	public void testCardDeletionMethod() throws IOException {
		
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.aCardHasBeenInserted();
		readerController.performAction(card, "1234");
		readerController.removeCard();
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "Card has been removed.");
	}
	
	@Test//testing card removal method
	public void testCardRemovalMethod() throws IOException {
		
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.aCardHasBeenInserted();
		readerController.performAction(card, "1234");
		readerController.theCardHasBeenRemoved();
		
		Assert.assertEquals(checkoutLogic.getMethodsOfPayment(), MethodsOfPayment.DEFAULT);
		Assert.assertEquals(checkoutLogic.getMethodsOfCardPayment(), MethodsOfCardPayment.DEFAULT);
		Assert.assertEquals(checkoutLogic.getTypeOfCard(), TypeOfCard.DEFAULT);
	}//checking to see if all card related enums are reset after removal using assertions
	
	@Test
	public void testCardTappingMethod() throws IOException {//testing auto tap
		
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.aCardHasBeenTapped();
		
		Assert.assertEquals(checkoutLogic.getMethodsOfCardPayment(), MethodsOfCardPayment.TAP);
		
	}
	
	@Test
	public void testCardSwipingMethod() throws IOException {//testing auto swipe
		
		checkoutLogic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
		readerController.aCardHasBeenSwiped();
		
		Assert.assertEquals(checkoutLogic.getMethodsOfCardPayment(), MethodsOfCardPayment.SWIPE);
		
	}
	
	@Test
	public void testCreditCollectData() throws IOException {//testing to see if we can correctly collect data from a credit card
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.TAP);
		checkoutLogic.setTypeOfCard(TypeOfCard.CREDIT_CARD);
		
		readerController.performAction(card, "1234");
		
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
        String first23Chars = lastLine.substring(0, Math.min(23, lastLine.length()));
        
        Assert.assertEquals(first23Chars, "Remaining balance due: ");    //asserting using output stream   
	}
	
	@Test
	public void testDebitCollectData() throws IOException{//testing to see if we can correctly collect data from a debit card
		checkoutLogic.setMethodsOfCardPayment(MethodsOfCardPayment.TAP);
		checkoutLogic.setTypeOfCard(TypeOfCard.DEBIT_CARD);
		
		readerController.performAction(card, "1234");
		
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
        String first23Chars = lastLine.substring(0, Math.min(23, lastLine.length()));
        
        Assert.assertEquals(first23Chars, "Remaining balance due: ");   //asserting using output stream
	}
	
	//Some overwritten methods were implemented due to stubbing, but are empty (no code to execute)
	//For these, we are just testing whether or not they can be called in the following 4 methods
	//The assert checks whether or not the method was successfully executed without throwing an error, which both checks to see if we can execute, and gives us extra coverage
	
	@Test
	public void testOverWrittenStub1() {//Checks to see if aDeviceHasBeenEnabled can be called
		boolean didWork = false;//boolean that checks if we can execute method without error
		try {
			readerController.aDeviceHasBeenEnabled(null);
			didWork = true;//if we execute without throwing an error, didWork = true
		} catch (Exception e) {
			didWork = false;//otherwise its false
		}
		
		assertTrue(didWork);//it should be true, check with assert
	}
	
	@Test
	public void testOverWrittenStub2() {
		boolean didWork = false;
		try {
			readerController.aDeviceHasBeenDisabled(null);
			didWork = true;
		} catch (Exception e) {
			didWork = false;
		}
		
		assertTrue(didWork);
	}
	
	@Test
	public void testOverWrittenStub3() {
		boolean didWork = false;
		try {
			readerController.aDeviceHasBeenTurnedOn(null);
			didWork = true;
		} catch (Exception e) {
			didWork = false;
		}
		
		assertTrue(didWork);
	}
	
	@Test
	public void testOverWrittenStub4() {
		boolean didWork = false;
		try {
			readerController.aDeviceHasBeenTurnedOff(null);
			didWork = true;
		} catch (Exception e) {
			didWork = false;
		}
		
		assertTrue(didWork);
	}
}
