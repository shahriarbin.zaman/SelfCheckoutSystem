/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.*;

import com.thelocalmarketplace.software.*;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Controller.CoinDispenserController;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Item;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.card.CardReaderBronze;
import com.jjjwelectronics.card.CardReaderGold;
import com.jjjwelectronics.card.CardReaderSilver;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleListener;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodeScannerListener;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.jjjwelectronics.scanner.IBarcodeScanner;
import com.thelocalmarketplace.hardware.*;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.hardware.external.ProductDatabases;

import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;
import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import powerutility.NoPowerException;
import powerutility.PowerGrid;

public class ScannerControllerTest {

	ScannerController scannerController;
	IBarcodeScanner mainScanner;
	IBarcodeScanner handheldScanner;
	Barcode testBarcode;
	CentralCheckoutStationLogicStub24 bronzeLogic;
	AbstractSelfCheckoutStation station;
	 
	SelfCheckoutStationBronze stationBronze; 
	SelfCheckoutStationGold stationGold; 
	SelfCheckoutStationSilver stationSilver;
	CentralCheckoutStationLogic logic;
	PowerGrid grid;
	private BarcodedProduct testProduct;
	ShoppingCart shoppingCart;
	
	
	@Before
    public void setup() {
		station = new SelfCheckoutStationBronze();
		grid = PowerGrid.instance();
    	
    	station.plugIn(grid);
    	station.turnOn();
        
    	
        bronzeLogic = new CentralCheckoutStationLogicStub24(bronzeLogic);
        bronzeLogic = new CentralCheckoutStationLogicStub24(station);      
        bronzeLogic.hardware = station;
        bronzeLogic.setTypeOfScanner(TypeOfScanner.MAIN);     
        
        // barcode item
        Numeral[] numerals = { Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
        testBarcode = new Barcode(numerals);
        testProduct = new BarcodedProduct(testBarcode, "Test Product", 1000, 500.0); 
        ProductDatabases.BARCODED_PRODUCT_DATABASE.put(testBarcode, testProduct);
        
        grid = PowerGrid.instance();
        
        scannerController = new ScannerController(bronzeLogic);
        
        ItemDatabase.loadItems();
    }
	
	
	@Test
    public void scanningInvalidBarcode() {
	    testBarcode = null;
	    assertThrows(NullPointerException.class, () -> {
	    scannerController.aBarcodeHasBeenScanned(handheldScanner, testBarcode);
	    });
	}
	
	@Test
    public void addItemViaBarcodeTest() {
		shoppingCart = ShoppingCart.getShoppingCart(bronzeLogic);
		
		Numeral[] numerals = { Numeral.one };
		Barcode code = new Barcode(numerals);
		
		ItemDatabase.initBarcodedItems();
		
		scannerController.addItemViaBarcode(code);
	    BarcodedItem temp = null;
	    ArrayList<BarcodedItem> items = shoppingCart.getShoppingCartItems();
	    for (int i =0; i < items.size(); i++) {
	    	if ((items.get(i).getBarcode()).equals(code)) {
	    		temp = items.get(i);
	    	}
	    }
	    assertNotNull(temp);
	}
	
	
	@Test 
    public void switchScannerMain() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	    scannerController.switchScanner(TypeOfScanner.MAIN);
	    assertEquals("Using the MAIN scanner.\n", outContent.toString());
    }
	
	@Test
    public void switchScannerHandheld() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	    scannerController.switchScanner(TypeOfScanner.HANDHELD);
	    assertEquals("Using the HANDHELD scanner.\n", outContent.toString());
    }
	
	@Test
    public void switchScannerNull() {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	    scannerController.switchScanner(null);
	    assertEquals("Scanner type not set, defaulting to MAIN scanner.\n", outContent.toString());
    }
	
	@Test
	public void testGetScannerType() {
		scannerController.switchScanner(TypeOfScanner.HANDHELD);
		assertEquals(scannerController.getScannerType(), TypeOfScanner.HANDHELD);
	}
	
	@Test (expected = InvalidStateSimulationException.class)
	public void testThatSessionStartCheckingIsCovered() {
		SessionStarter sessionManager = SessionStarter.getSessionStarter(logic);
	    	
		sessionManager.endSession();
	    	
	    Numeral[] numerals = { Numeral.one };
		Barcode code = new Barcode(numerals);
	    scannerController.addItemViaBarcode(code);
	    	    
	    sessionManager.startSession();
	}

}




// ---- STUBS FOR TESTING

class CentralCheckoutStationLogicStub24 extends CentralCheckoutStationLogic {
	
    public CentralCheckoutStationLogic logic;
		
    public MethodsOfPayment methodsOfPayment;
    public MethodsOfCardPayment methodsOfCardPayment;
    public TypeOfCard typeOfCard;
	public CardIssuer cardIssuer;
	public TypeOfScanner scannerType; 
	
	public String currentMemberShipCard;
	public HashMap<String, Boolean> membershipCards = new HashMap<>();
	
	public CentralCheckoutStationLogicStub24(CentralCheckoutStationLogic logic) 
	{
		super(logic);
	}
	PowerGrid grid;
		
	
    public CentralCheckoutStationLogicStub24(AbstractSelfCheckoutStation station) {
    	
    	super(null); // Pass null for the logic parameter, since we're not using it in the stub
    	
    	this.hardware = station;
    	grid = PowerGrid.instance();
        this.scaleController = new ScaleController(this);
        ElectronicScaleBronzeStub1 scale = new ElectronicScaleBronzeStub1();
        Mass mass = Mass.ONE_GRAM;
        scale.setSimulatedMass(mass);
        
        scale.plugIn(grid);
        scale.turnOn();
        this.scaleController.setScaleScanningArea(scale);
        
        OwnBagAdder ownBags;
		ownBags = new OwnBagAdder(this);
		this.scaleController.setOwnBagAdder(ownBags);
		
		SessionStarter.getSessionStarter(logic).startSession();
       
    
    }

    public AbstractSelfCheckoutStation getHardware() {
        return hardware;
    }

    @Override
    public void setMethodsOfPayment(MethodsOfPayment method) {
    	methodsOfPayment = method;
    }

    @Override
    public void setTypeOfCard(TypeOfCard type) {
    	typeOfCard = type;
    }

    @Override
    public void setMethodsOfCardPayment(MethodsOfCardPayment method) {
    	methodsOfCardPayment = method;
    }

    
    @Override
    public MethodsOfPayment getMethodsOfPayment() 
	{
		return methodsOfPayment;
	}
    @Override
	public TypeOfCard getTypeOfCard() 
	{
		return typeOfCard;
	}
    @Override
	public MethodsOfCardPayment getMethodsOfCardPayment() 
	{
		return methodsOfCardPayment;
	}

    public void addMembershipCard(String membershipNumber) {
		this.membershipCards.put(membershipNumber, true);
	}
	
	public HashMap getMembershipCards() {
        return membershipCards;
    }
	
	public void setCurrentMemberShipCard(String card) {
		this.currentMemberShipCard = card;
	}
	
	public String getCurrentMemberShipCard() {
		return this.currentMemberShipCard;
	}
	
	public String getCardReaderType() 
	{
		return "Bronze";
    }
	
	public CardIssuer getCardIssuer() 
	{
		return cardIssuer;
	}

	public void setCardIssuer(CardIssuer cardIssuer) 
	{
		this.cardIssuer = cardIssuer;
	}
	
	public TypeOfScanner getTypeOfScanner() {
		return scannerType;
	}
	
	public void setTypeOfScanner(TypeOfScanner scannerType) {
		this.scannerType = scannerType;
	}
	
	
	public String getScanningAreaType() 
	{
		return "Bronze";
    }
	
   
}

class IBarcodeScannerStub2 implements IBarcodeScanner {
    private boolean isEnabled = true;
    private boolean simulateNullPointer = false;
    private boolean simulateNoPower = false;

    @Override
    public void enable() {
        isEnabled = true;
    }

    @Override
    public void disable() {
        isEnabled = false;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

	@Override
	public boolean isPluggedIn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPoweredUp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void plugIn(PowerGrid grid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unplug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnOn() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnOff() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean deregister(BarcodeScannerListener listener) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void deregisterAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void register(BarcodeScannerListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDisabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<BarcodeScannerListener> listeners() {
		// TODO Auto-generated method stub
		return null;
	}

    public void setSimulateNullPointer(boolean simulate) {
        this.simulateNullPointer = simulate;
    }

    public void setSimulateNoPower(boolean simulate) {
        this.simulateNoPower = simulate;
    }

    @Override
    public void scan(BarcodedItem item) {
        if (simulateNullPointer) {
            throw new NullPointerSimulationException("Null Pointer Exception simulated.");
        }
        if (simulateNoPower) {
            throw new NoPowerException();
        }
        
    }
    
}
class ElectronicScaleBronzeStub1 extends ElectronicScaleBronze {
    private Mass simulatedMass;
    private boolean simulateOverload = false;

    public ElectronicScaleBronzeStub1() {
        super();
        
        simulatedMass = new Mass(BigInteger.ZERO);
    }

    public void setSimulatedMass(Mass mass) {
        this.simulatedMass = mass;
    }

    public void setSimulateOverload(boolean simulateOverload) {
        this.simulateOverload = simulateOverload;
    }

    @Override
    public synchronized Mass getCurrentMassOnTheScale() throws OverloadedDevice {

        if (simulateOverload || (simulatedMass != null && simulatedMass.compareTo(massLimit) > 0)) {
            throw new OverloadedDevice();
        }

        // Return the simulated mass, or a default if not set.
        return simulatedMass != null ? simulatedMass : new Mass(BigInteger.ZERO);
    }
}

