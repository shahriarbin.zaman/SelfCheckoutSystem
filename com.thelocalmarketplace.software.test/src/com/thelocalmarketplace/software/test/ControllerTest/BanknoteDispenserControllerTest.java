package com.thelocalmarketplace.software.test.ControllerTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Currency;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.IComponent;
import com.tdc.IComponentObserver;
import com.tdc.NoCashAvailableException;
import com.tdc.banknote.AbstractBanknoteDispenser;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.BanknoteDispenserGold;
import com.tdc.banknote.BanknoteDispenserObserver;
import com.tdc.banknote.BanknoteStorageUnit;
import com.tdc.banknote.IBanknoteDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Controller.BanknoteDispenserController;

import ca.ucalgary.seng300.simulation.SimulationException;
import powerutility.PowerGrid;

public class BanknoteDispenserControllerTest {
	private SelfCheckoutStationGold checkout;
	private CentralCheckoutStationLogic logic;
	private BanknoteDispenserController banknoteDispenserController;
	private BigDecimal[] denominations;

	private Banknote banknote;
	private Currency CAD;
	private IBanknoteDispenser dispenser;
	private BanknoteDispenserGold abstractDispenser;
	
	//assertion
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	private String[] lines;
	private String lastLine;

	@Before
	public void setup() {
		System.setOut(new PrintStream(outputStreamCaptor));
		PowerGrid.engageUninterruptiblePowerSource();
		logic = new CentralCheckoutStationLogic(logic);
		
		CAD = Currency.getInstance("CAD");
		denominations = new BigDecimal[] {
        		new BigDecimal("1.00"),
        		new BigDecimal("5.00"),
        		new BigDecimal("10.00"),
        };
		//Canadian $1 bill
		banknote = new Banknote(CAD, denominations[0]);
		AbstractSelfCheckoutStation.configureCurrency(CAD);
		AbstractSelfCheckoutStation.configureBanknoteDenominations(denominations);
        
        checkout = new SelfCheckoutStationGold();
        checkout.plugIn(PowerGrid.instance());
        checkout.turnOn();
		logic = CentralCheckoutStationLogic.installOn(checkout);

		//banknote dispenser of $1 bills
		banknoteDispenserController = new BanknoteDispenserController(logic, denominations[0]);
		banknoteDispenserController.turnedOn(dispenser);
		banknoteDispenserController.enabled(dispenser);
		dispenser = logic.hardware.getBanknoteDispensers().get(denominations[0]);
		
		//if dispenser is 80% full, then it is almost full
		banknoteDispenserController.setWarningFullPercentage(0.8);
		//if dispenser is 20% full, then it is almost empty, 
		banknoteDispenserController.setWarningEmptyPercentage(0.2);
		
		abstractDispenser = new BanknoteDispenserGold();
		abstractDispenser.attach(banknoteDispenserController);
		abstractDispenser.connect(PowerGrid.instance());
	}
	@After
	public void takeDown() {
		System.setOut(standardOut);
	}
	@Test public void shouldGetMaxCapacityTest() {
		assertEquals(banknoteDispenserController.getMaxCapacity(), dispenser.getCapacity());
	}
	@Test public void shouldGetCurrentCapacityTest() {
		assertEquals(banknoteDispenserController.getCurrentCapacity(), 0);
	}

	@Test public void shouldCheckIfBanknoteDispenserIsAlmostFullWhenAlmostFullTest() throws Exception {		
		for(int i = 0; i < 800; i++) {
			dispenser.load(banknote);
		}
		assertEquals(banknoteDispenserController.getCurrentCapacity(),800);
		banknoteDispenserController.checkIfBanknoteDispenserIsAlmostFull();
	}	
	@Test public void shouldCheckIfBanknoteDispenserIsAlmostFullWhenNotAlmostFullTest() throws Exception {		
		for(int i = 0; i < 799; i++) {
			dispenser.load(banknote);
		}
		assertEquals(banknoteDispenserController.getCurrentCapacity(),799);
		banknoteDispenserController.checkIfBanknoteDispenserIsAlmostFull();
	}	
	@Test public void shouldCheckIfBanknoteDispenserIsAlmostEmptyWhenAlmostEmptyTest() throws Exception {		
		for(int i = 0; i < 200; i++) {
			dispenser.load(banknote);
		}
		assertEquals(banknoteDispenserController.getCurrentCapacity(),200);
		banknoteDispenserController.checkIfBanknoteDispenserIsAlmostEmpty();
	}
	@Test public void shouldCheckIfBanknoteDispenserIsAlmostEmptyWhenNotAlmostEmptyTest() throws Exception {		
		for(int i = 0; i < 201; i++) {
			dispenser.load(banknote);
		}
		assertEquals(banknoteDispenserController.getCurrentCapacity(),201);
		banknoteDispenserController.checkIfBanknoteDispenserIsAlmostEmpty();
	}
	
	@Test public void shouldAnnounceDispenserIsEmptyWhenBanknotesEmptyTest() throws Exception {		
		dispenser.load(banknote);
		dispenser.emit();
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length-1].trim();
        Assert.assertEquals(lastLine, "Bank note dispenser is empty. Attendant has been alerted.");
	}
	@Test public void shouldAnnounceBanknoteRemovedWhenBanknoteRemovedTest() throws CashOverloadException, NoCashAvailableException, DisabledException {
		dispenser.load(banknote,banknote);
		dispenser.emit();
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length-1].trim();
        Assert.assertEquals(lastLine, "Bank note removed successfully from dispenser.");
	}
	@Test public void shouldCallBanknotesUnloadedWhenBanknotesUnloadedTest() throws CashOverloadException {
		dispenser.load(banknote, banknote, banknote);
		dispenser.unload();
	}
	@Test public void shouldAnnounceBanknoteAddedWhenBanknoteAddedTest() throws CashOverloadException, DisabledException {
		abstractDispenser.receive(banknote);
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length-1].trim();
        Assert.assertEquals(lastLine, "Bank note added successfully to dispenser.");
	}	
	@Test public void shouldAnnounceMoneyFullWhenDispenserIsFullTest() throws Exception {		
		for(int i = 0; i < banknoteDispenserController.getMaxCapacity(); i++) {
			abstractDispenser.receive(banknote);
		}
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length-1].trim();
        Assert.assertEquals(lastLine, "Bank note dispenser is full. Attendant has been alerted.");
	}
	@Test public void shouldTurnOffAndDisableControllerTest() {
		banknoteDispenserController.disabled(dispenser);
		banknoteDispenserController.turnedOff(dispenser);
	}
}
