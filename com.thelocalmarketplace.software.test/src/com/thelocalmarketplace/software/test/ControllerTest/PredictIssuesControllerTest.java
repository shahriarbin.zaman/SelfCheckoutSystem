package com.thelocalmarketplace.software.test.ControllerTest;

/*     Group Members and UCIDS
Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.Controller.PredictIssuesController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;

import powerutility.PowerGrid;

//Imports Defined

public class PredictIssuesControllerTest {
	
    private PredictIssuesController controller;
    private SessionStarter sessionStarter;
    private CentralCheckoutStationLogic logic;
    
    private AbstractSelfCheckoutStation station;
    private PowerGrid grid;
    
    //Defining private variables
    
    //To be done before the test cases= setUp() method
    @Before
    public void setUp() {
    	grid = PowerGrid.instance();
        station = new SelfCheckoutStationGold();
        //instantiating powergrid and station
        
        station.plugIn(grid);
        station.turnOn();
        //plugging in and turning on the station
        
        logic= CentralCheckoutStationLogic.installOn(station);//setting up centralcheckoutstaitonlogic
        
        sessionStarter = SessionStarter.getSessionStarter(logic);
        //setting up the session starter and hooking it up to the logic
        
        controller = new PredictIssuesController(logic, sessionStarter);
        //instantiating the predict issues controller, and hooking it up to both the starter and the logic
    }
    
    
    @Test (expected = Exception.class)//Test case to see if we can predict low paper while session is active
    public void checkLowPaperEngineOn() throws Exception {
    	sessionStarter.startSession();
    	controller.predictLowOnPaper();//Should throw an exception- session must be inactive for predict engine
    }
    
    @Test//Testing to see if we can successfully predict low on paper when session is off
    public void checkLowPaperEngineOff() throws Exception {
    	sessionStarter.endSession();//session is turned off so predict engine works
    	
    	logic.paperManager.removePaper(95, "Letter");//removing 95 sheets of paper of the initial 100
    	
    	controller.predictLowOnPaper();//predict low on paper- (95% of paper stock is now gone)
    	
    	assertTrue(controller.getLowOnPaper());//should be true that it's low on paper
    	
    	//one other command is tested when predictLowOnPaper() and other such methods are called, tested in last test case
    }
    
    @Test
    public void checkNotLowPaperEngineOff() throws Exception {
    	
    	sessionStarter.endSession();//Ending the session
    	logic.paperManager.removePaper(89, "Letter");
    	//removing 89 sheets of paper (over 10% of stock still remaining)- low on paper shouldn't be an issue
    	
    	controller.predictLowOnPaper();
    	//predict low on paper, should be false (from assertion)
    	assertFalse(controller.getLowOnPaper());
    }
    
    @Test (expected = Exception.class)//Testing to see if we can predict low on coins with session starter on
    public void checkLowCoinsEngineOn() throws Exception {
    	sessionStarter.startSession();
    	controller.predictLowOnCoins();
    }
    
    @Test
    public void checkLowCoinsEngineOff() throws Exception {
    	sessionStarter.endSession();
    	controller.predictLowOnCoins();
    	assertTrue(controller.getLowOnCoins());
    }
    
    @Test(expected = Exception.class)//Testing to see if we can predict banknotes full with engine on
    public void checkFullBankNotesEngineOn() throws Exception {
    	sessionStarter.startSession();
    	controller.predictBanknotesFull();
    }
    
    @Test//Testing to see if we can successfully predict if bank notes are almost full with engine off
    public void checkFullBankNotesEngineOff() throws Exception {
    	sessionStarter.endSession();
    	assertFalse(controller.getBanknotesAlmostFull());
    	controller.predictBanknotesFull();
    	assertTrue(controller.getBanknotesAlmostFull());
    }
    
    @Test (expected = Exception.class)//Testing to see if we can predict low on banknotes with engine on
    public void checkLowBankNotesEngineOn() throws Exception {
    	sessionStarter.startSession();
    	controller.predictLowOnBanknotes();
    }
    
    @Test//Testing to see if we can predict low on banknotes with engine off
    public void checkLowBankNotesEngineOff() throws Exception {
    	sessionStarter.endSession();
    	assertFalse(controller.getLowOnBanknotes());
    	controller.predictLowOnBanknotes();
    	assertTrue(controller.getLowOnBanknotes());
    }
    
    @Test (expected = Exception.class)//Testing to see if we can predict coins full with engine off
    public void checkFullCoinsEngineOn() throws Exception{
    	sessionStarter.startSession();
    	controller.predictCoinsFull();
    }
    
    @Test//Testing to see if we can predict coins almost full with engine off
    public void checkFullCoinsEngineOff() throws Exception {
    	sessionStarter.endSession();
    	assertFalse(controller.getCoinsAlmostFull());
    	controller.predictCoinsFull();
    	assertTrue(controller.getCoinsAlmostFull());
    }
    
    @Test (expected = Exception.class)///Testing to see if we can predict low ink with engine on
    public void checkLowInkEngineOn() throws Exception {
    	sessionStarter.startSession();
    	controller.predictLowOnInk();
    }
    
    @Test
    public void checkLowInkEngineOff() throws Exception {//Testing to see if we can predict low ink with engine on
    	sessionStarter.endSession();
    	controller.setWarningLowInkPercentage(1.10);
    	assertFalse(controller.getLowOnInk());
    	controller.predictLowOnInk();
    	assertTrue(controller.getLowOnInk());
    }
   
    @Test//Testing full disabled station on approval:
    public void testFullDisabledStationOnApproval() throws Exception {
    	sessionStarter.endSession();
    	//Turn off session so predict engine works
    	
    	logic.signalAttendantController.alertAttendant();
    	logic.signalAttendantController.sendAlertToAttendant(logic.hardware);
    	//send the alert to attendant to confirm the attendant passes the request
    	//These 2 lines automatically confirm that if a request is sent to the attendant to disable the station, it will be passed automatically
    	
    	assertTrue(logic.signalAttendantController.isHelpSessionActive());//help session should be active by this point
    	
    	controller.predictCoinsFull(); //predict coins full- a request will then be sent to the attendant to disable the station
    	//from lines 181-182 we garuntee that if a request is sent, the attendant will automatically disable the station
    	
    	assertTrue(logic.stationDisabler.getStationDisabled());
    	//because of this, the station should now be disabled
    }
}