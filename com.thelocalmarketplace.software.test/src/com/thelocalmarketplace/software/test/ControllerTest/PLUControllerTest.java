/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodeScannerListener;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.jjjwelectronics.scanner.IBarcodeScanner;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.SelfCheckoutStationSilver;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.software.Controller.PLUController;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Listener.SessionStateListener;

import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;
import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import powerutility.NoPowerException;
import powerutility.PowerGrid;

public class PLUControllerTest {

	private CentralCheckoutStationLogic logic;
    private SessionStateListener listener;
    
    int totalItems;
	ArrayList<BarcodedProduct> totalProductsList;
	ItemRemover removeItem;
	BarcodedItem item;
	BarcodedItem item2;
	PLUCodedItem pluItem;
	PLUCodedProduct pluProduct;
	Barcode testBarcode;
	Barcode testBarcode2;
	Mass testMass;
	PriceLookUpCode pluCode;
	String pluCodeString;
	String productDescription;
	long price;
	ShoppingCart shoppingCart;
	BarcodedProduct barcodedProduct;
	ElectronicScaleBronze bronze;
	ElectronicScaleSilver silver;
	ElectronicScaleGold gold;
    private PowerGrid grid;
    SelfCheckoutStationBronze stationBronze;
   	SelfCheckoutStationSilver stationSilver;
    SelfCheckoutStationGold stationGold;
   	private final PrintStream standardOut = System.out;
   	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
   	private BigDecimal initialTotalWeight;
   	private long initialTotalPrice;
	private IElectronicScale eScale;
	TypeOfScanner scanner;
    private PLUController pluController;
    SessionStarter sessionStarter;


	
	@Before
	public void setup() {
		stationBronze = new SelfCheckoutStationBronze();
		grid = PowerGrid.instance();
    	
		stationBronze.plugIn(grid);
		stationBronze.turnOn();
		
		logic = new CentralCheckoutStationLogicStub2(logic);
		logic = new CentralCheckoutStationLogicStub2(stationBronze);      
		logic.hardware = stationBronze;
		logic.setTypeOfCard(TypeOfCard.CREDIT_CARD); 
		
		CentralCheckoutStationLogic.installOn(stationBronze);

        Numeral[] numerals = { Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
        pluCodeString = "12345";
		testBarcode = new Barcode(numerals); 
        pluCode = new PriceLookUpCode(pluCodeString);
        productDescription = "apple";
        price = 67;
		
        
		testMass = new Mass(BigDecimal.valueOf(500));  //creating test mass
		pluItem = new PLUCodedItem(pluCode, testMass); 
		pluProduct = new PLUCodedProduct(pluCode, productDescription, price );
       
		ProductDatabases.PLU_PRODUCT_DATABASE.put(pluCode, pluProduct);        
		
        grid = PowerGrid.instance();
        pluController = new PLUController(logic);
        
    	shoppingCart = ShoppingCart.getShoppingCart(logic);

        ItemDatabase.loadItems();
        
        PowerGrid.engageUninterruptiblePowerSource();
	}
	
	
    @Test(expected = NullPointerException.class)
    public void testAddItemByPLU_NullCode() {
        // Call the method with null code, it should throw a NullPointerException
        pluController.addItemByPLU(null);
    }
    

    @Test 
    public void testThatItemIsAddedIntoCartByPLU() {
        PriceLookUpCode code = new PriceLookUpCode("0001");
        pluController.addItemByPLU(code);

        PLUCodedItem temp = null;
        ArrayList<PLUCodedItem> items = shoppingCart.getShoppingCartPLUItems();
        for (int i =0; i < items.size(); i++) {
        	if ((items.get(i).getPLUCode()).equals(code)) {
        		temp = items.get(i);
        	}
        }
        assertNotNull(temp);
    }
    
    @Test
    public void testThatSessionStartCheckingIsCovered() {
    	SessionStarter sessionManager = SessionStarter.getSessionStarter(logic);
    	
    	sessionManager.endSession();
    	
        PriceLookUpCode code = new PriceLookUpCode("0005");
        pluController.addItemByPLU(code);
                        
        sessionManager.startSession();
    }

}






// ---------------- STUBS FOR TESTING

class CentralCheckoutStationLogicStub2 extends CentralCheckoutStationLogic {
	

    public CentralCheckoutStationLogic logic;
		
    public MethodsOfPayment methodsOfPayment;
    public MethodsOfCardPayment methodsOfCardPayment;
    public TypeOfCard typeOfCard;
	public CardIssuer cardIssuer;
	public TypeOfScanner scannerType; 
	
	public String currentMemberShipCard;
	public HashMap<String, Boolean> membershipCards = new HashMap<>();
	
	public CentralCheckoutStationLogicStub2(CentralCheckoutStationLogic logic) 
	{
		super(logic);
	}
	PowerGrid grid;
		
	
    public CentralCheckoutStationLogicStub2(AbstractSelfCheckoutStation station) {
    	
    	super(null); // Pass null for the logic parameter, since we're not using it in the stub
    	
    	this.hardware = station;
    	grid = PowerGrid.instance();
        this.scaleController = new ScaleController(this);
        this.priceLookupCodeController = new PLUController(this);
        ElectronicScaleBronzeStub1 scale = new ElectronicScaleBronzeStub1();
        Mass mass = Mass.ONE_GRAM;
        scale.setSimulatedMass(mass);
        
        scale.plugIn(grid);
        scale.turnOn();
        this.scaleController.setScaleScanningArea(scale);
        
        this.sessionStarter = SessionStarter.getSessionStarter(logic);
        this.sessionStarter.startSession();
        
        this.attendantStation = new AttendantStation();
        SignalAttendantController attendantController = new SignalAttendantController(logic, attendantStation);
        
        OwnBagAdder ownBags;
		ownBags = new OwnBagAdder(this);
		this.scaleController.setOwnBagAdder(ownBags);
       
    
    }

    
    
    
    public AbstractSelfCheckoutStation getHardware() {
        return hardware;
    }

    @Override
    public void setMethodsOfPayment(MethodsOfPayment method) {
    	methodsOfPayment = method;
    }

    @Override
    public void setTypeOfCard(TypeOfCard type) {
    	typeOfCard = type;
    }

    @Override
    public void setMethodsOfCardPayment(MethodsOfCardPayment method) {
    	methodsOfCardPayment = method;
    }

    
    @Override
    public MethodsOfPayment getMethodsOfPayment() 
	{
		return methodsOfPayment;
	}
    @Override
	public TypeOfCard getTypeOfCard() 
	{
		return typeOfCard;
	}
    @Override
	public MethodsOfCardPayment getMethodsOfCardPayment() 
	{
		return methodsOfCardPayment;
	}

    public void addMembershipCard(String membershipNumber) {
		this.membershipCards.put(membershipNumber, true);
	}
	
	public HashMap getMembershipCards() {
        return membershipCards;
    }
	
	public void setCurrentMemberShipCard(String card) {
		this.currentMemberShipCard = card;
	}
	
	public String getCurrentMemberShipCard() {
		return this.currentMemberShipCard;
	}
	
	public String getCardReaderType() 
	{
		return "Bronze";
    }
	
	public CardIssuer getCardIssuer() 
	{
		return cardIssuer;
	}

	public void setCardIssuer(CardIssuer cardIssuer) 
	{
		this.cardIssuer = cardIssuer;
	}
	
	public TypeOfScanner getTypeOfScanner() {
		return scannerType;
	}
	
	public void setTypeOfScanner(TypeOfScanner scannerType) {
		this.scannerType = scannerType;
	}
	
	
	public String getScanningAreaType() 
	{
		return "Bronze";
    }
	
   
}

class IBarcodeScannerStub3 implements IBarcodeScanner {
    private boolean isEnabled = true;
    private boolean simulateNullPointer = false;
    private boolean simulateNoPower = false;

    @Override
    public void enable() {
        isEnabled = true;
    }

    @Override
    public void disable() {
        isEnabled = false;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

	@Override
	public boolean isPluggedIn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPoweredUp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void plugIn(PowerGrid grid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unplug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnOn() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnOff() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean deregister(BarcodeScannerListener listener) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void deregisterAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void register(BarcodeScannerListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDisabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<BarcodeScannerListener> listeners() {
		// TODO Auto-generated method stub
		return null;
	}

    public void setSimulateNullPointer(boolean simulate) {
        this.simulateNullPointer = simulate;
    }

    public void setSimulateNoPower(boolean simulate) {
        this.simulateNoPower = simulate;
    }

    @Override
    public void scan(BarcodedItem item) {
        if (simulateNullPointer) {
            throw new NullPointerSimulationException("Null Pointer Exception simulated.");
        }
        if (simulateNoPower) {
            throw new NoPowerException();
        }
        
    }
    
}




class ElectronicScaleBronzeStub2 extends ElectronicScaleBronze {
    private Mass simulatedMass;
    private boolean simulateOverload = false;

    public ElectronicScaleBronzeStub2() {
        super();
        
        simulatedMass = new Mass(BigInteger.ZERO);
    }

    public void setSimulatedMass(Mass mass) {
        this.simulatedMass = mass;
    }

    public void setSimulateOverload(boolean simulateOverload) {
        this.simulateOverload = simulateOverload;
    }

    @Override
    public synchronized Mass getCurrentMassOnTheScale() throws OverloadedDevice {

        if (simulateOverload || (simulatedMass != null && simulatedMass.compareTo(massLimit) > 0)) {
            throw new OverloadedDevice();
        }

        // Return the simulated mass, or a default if not set.
        return simulatedMass != null ? simulatedMass : new Mass(BigInteger.ZERO);
    }
}