//**Note; a PowerSurge may occassionally be thrown, Please rerun test cases in this case

/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.printer.ReceiptPrinterBronze;
import com.jjjwelectronics.printer.ReceiptPrinterGold;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.ReceiptPrinterController;

import junit.framework.Assert;
import powerutility.PowerGrid;
//iniitalizing imports

public class ReceiptPrinterControllerTest {

	private ReceiptPrinterController controller;
	private CentralCheckoutStationLogic logic;
	private AbstractSelfCheckoutStation station;
	private PowerGrid grid;
	private Barcode appleCode;
	private BarcodedProduct apple;
	private Mass appleMass;
	private ShoppingCart cart;
	//instantiating private variables needed for test cases
	
	private BarcodedItem bcodeItem;
	
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    //instantiating output stream variables required for assertions
	
	@Before//the setup method before the test cases
	public void setUp() {
		grid = PowerGrid.instance();
		station = new SelfCheckoutStationGold();
		station.plugIn(grid);
		station.turnOn();
		//Setting up the powergrid and the station
		
		logic = CentralCheckoutStationLogic.installOn(station);
		controller = new ReceiptPrinterController(logic);
		//Setting up the receipt printer controller and the Central logic unit
		
		appleMass = new Mass(2.95);
		Numeral[] code = {Numeral.one, Numeral.two, Numeral.three};
		appleCode = new Barcode(code);
		bcodeItem = new BarcodedItem(appleCode, appleMass);
		apple = new BarcodedProduct(appleCode, "Apple", 100, 2.95);
		cart = ShoppingCart.getShoppingCart(logic);
		//Setting up the barcoded item and product for an apple object, so it can be used with the shopping cart
		
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(appleCode, apple);
		cart.addBarcodedItemToCart(bcodeItem);
		//item added to product databases, and later to the cart
		
		System.setOut(new PrintStream(outContent));//output stream initialized
	}
	
	@After//After the testing...
    public void restoreStreams() {
        System.setOut(originalOut);//we need to restore the output stream
    }
	
	@Test//Basic test printing method, tests to see if we can print an item from start to finish from the shopping cart
	public void testPrintBasic() throws EmptyDevice, OverloadedDevice {
		logic.inkManager.setInkLevel(ReceiptPrinterGold.MAXIMUM_INK);
		logic.hardware.getPrinter().addInk(ReceiptPrinterGold.MAXIMUM_INK);
		logic.hardware.getPrinter().addPaper(ReceiptPrinterGold.MAXIMUM_PAPER);
		//setting up the ink and paper managers, as well as setting up the ink and paper levels in the printer
		
		logic.sessionStarter.startSession();//starting up the session
		
		controller.print();//printing
		
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
        //After printing, "Receipt Printed" will be printed to the console, next assert line checks to see if this is true
		Assert.assertEquals(lastLine, "Receipt printed");
	}
	
	@Test//Testing to print with no paper
	public void testPrintNoPaper() throws EmptyDevice, OverloadedDevice {
		
		logic.inkManager.setInkLevel(ReceiptPrinterGold.MAXIMUM_INK);
		logic.hardware.getPrinter().addInk(ReceiptPrinterGold.MAXIMUM_INK);
		logic.hardware.getPrinter().addPaper(100);
		//setting up paper and ink levels in the printer
		
		logic.paperManager.removePaper(95, "Letter");//removing 95 sheets of paper from ink manager
		//The printer controller uses the paper manager to get data on paper, not the numbers from the printer sicne the printer is a bronze printer and doesn't have those capabilities
		//We have only 5 papers left, so the paper manager will give a warning
		
		logic.sessionStarter.startSession();
		controller.print();//starting and printing
		
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "The printer is low on paper");
		//the output isn't printed, instead what's printed is "The printer is low on paper"
	}
	
	@Test//Testing printing with no ink
	public void testPrintNoInk() throws EmptyDevice, OverloadedDevice {
		logic.inkManager.setInkLevel(10);//setting the ink level to only 10
		logic.hardware.getPrinter().addInk(1000);//adding ink level 10 to the printer
		logic.hardware.getPrinter().addPaper(1000);
		logic.sessionStarter.startSession();
		controller.print();//starting session and printing
		//if ink levels are under 20, warning/error message will print
		
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "The printer is low on ink");
		//the message above should be printed, it is then compared using assertions and output stream
	}
	
	@Test//testing the override method of out of paper
	public void testOverRideOutOfPaper() {
		controller.thePrinterIsOutOfPaper();
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "The printer is out of paper please refill");
		//the only thing this method does is print the above line
	}
	
	@Test//testing the override method of out of ink
	public void testOverRideOutOfInk() {
		controller.thePrinterIsOutOfInk();
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "The printer is out of ink please refill");
		//the only thing this method does is print the above ink
	}
	
	@Test//testing the override method of refilled paper
	public void paperAddedtoPrinter() {
		controller.paperHasBeenAddedToThePrinter();
		String[] lines = outContent.toString().split("\\r?\\n");

        String lastLine = lines[lines.length - 1].trim();
		Assert.assertEquals(lastLine, "The paper has been refilled");
		//The only thing this method does is print that the paper has been refilled
	}
	
	//The next 5 test cases check to see if the overwritten stub methods can be called/accessed without throwing an exception
	
	@Test
	public void testOverWrittenStub1() {
		boolean successfullyCalled = false;//this boolean checks to see if the method was executed without throwing an exception
		try {
			controller.inkHasBeenAddedToThePrinter();
			successfullyCalled = true;//if executed, the bool is true
		} catch (Exception e) {
			successfullyCalled = false;//otherwise its false
		}
		
		assertTrue(successfullyCalled);//it should be true- verify by assertion
	}
	
	//The next 4 stub test cases are written with the exact same format
	
	@Test
	public void testOverWrittenStub2() {
		boolean successfullyCalled = false;
		try {
			controller.aDeviceHasBeenEnabled(null);
			successfullyCalled = true;
		} catch (Exception e) {
			successfullyCalled = false;
		}
		
		assertTrue(successfullyCalled);
	}
	
	@Test
	public void testOverWrittenStub3() {
		boolean successfullyCalled = false;
		try {
			controller.aDeviceHasBeenDisabled(null);
			successfullyCalled = true;
		} catch (Exception e) {
			successfullyCalled = false;
		}
		
		assertTrue(successfullyCalled);
	}
	
	@Test
	public void testOverWrittenStub4() {
		boolean successfullyCalled = false;
		try {
			controller.aDeviceHasBeenTurnedOn(null);
			successfullyCalled = true;
		} catch (Exception e) {
			successfullyCalled = false;
		}
		
		assertTrue(successfullyCalled);
	}
	
	@Test
	public void testOverWrittenStub5() {
		boolean successfullyCalled = false;
		try {
			controller.aDeviceHasBeenTurnedOff(null);
			successfullyCalled = true;
		} catch (Exception e) {
			successfullyCalled = false;
		}
		
		assertTrue(successfullyCalled);
	}
	
}