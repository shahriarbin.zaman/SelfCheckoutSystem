package com.thelocalmarketplace.software.test.ControllerTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Currency;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;

import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.banknote.Banknote;

import com.tdc.banknote.BanknoteValidator;
import com.tdc.banknote.IBanknoteDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.BanknoteController;

import powerutility.PowerGrid;


public class BanknoteControllerTest {
	//logic
	private SelfCheckoutStationGold checkout;
	private CentralCheckoutStationLogic logic;
	private BanknoteController banknoteController;
	private BigDecimal[] denominations;

	private Banknote banknote;
	private Currency CAD;
	private BanknoteValidator validator;
	private ShoppingCart cart;
	private IBanknoteDispenser dispenser;
	
	private Numeral[] code;
	private Barcode barCode;
	private BarcodedProduct product;
	private BarcodedItem item;

	//assertion
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	private String[] lines;
	private String lastLine;

	@Before
	public void setup() throws CashOverloadException {
		PowerGrid.engageUninterruptiblePowerSource();
		logic = new CentralCheckoutStationLogic(logic);
		
		CAD = Currency.getInstance("CAD");
		denominations = new BigDecimal[] {
        		new BigDecimal("1.00"),
        		new BigDecimal("5.00"),
        		new BigDecimal("10.00"),
        };
		banknote = new Banknote(CAD, denominations[0]);
		AbstractSelfCheckoutStation.configureCurrency(CAD);
		AbstractSelfCheckoutStation.configureBanknoteDenominations(denominations);
        checkout = new SelfCheckoutStationGold();
        checkout.plugIn(PowerGrid.instance());
        checkout.turnOn();
        logic = CentralCheckoutStationLogic.installOn(checkout);
        cart = ShoppingCart.getShoppingCart(logic);

        code = new Numeral[1];
		code[0] = Numeral.one;;
		barCode = new Barcode(code);
		product = new BarcodedProduct(barCode, "banana", 7, 100); 
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(barCode, product);
		item = new BarcodedItem(barCode, new Mass(100));
		cart.addBarcodedItemToCart(item);
		cart.addPrice(product.getPrice());
		
		banknoteController = new BanknoteController(logic);
		validator = logic.hardware.getBanknoteValidator();
		validator.detachAll();
		validator.attach(banknoteController);
		banknoteController.turnedOn(validator);
		banknoteController.enabled(validator);
	}
	@After
	public void takeDown() {
		cart.removePrice(product.getPrice());
	}
	//may fail depending on when executed
	@Test public void shouldGetAmountDueAfterInitializingControllerTest() {
		assertEquals(banknoteController.getAmountDue(), new BigDecimal(cart.getTotalPrice()));
	}
	@Test public void shouldGetRemainingBalanceAfterInitializingControllerTest() {
		assertEquals(banknoteController.getRemainingBalance(),new BigDecimal(cart.getTotalPrice()));	
	}
	@Test public void shouldGetCurrentAmountPaidAfterInitializingControllerTest() {
		assertEquals(banknoteController.getCurrentAmountPaid(), new BigDecimal(0));	
	}
	@Test public void shouldAnnounceGoodBanknoteWhenNoExceptionsCaughtTest() throws Exception {
		validator.receive(banknote);
		validator.receive(banknote);
		validator.receive(banknote);
		assertFalse(banknoteController.getSessionSuspended());
		assertNull(banknoteController.getEncounteredException());
	}
	//banknote dispenser is empty so NoCashException will be thrown in goodBanknote() 
	@Test public void shouldAnnounceGoodBanknoteWhenExceptionsCaughtTest() throws DisabledException, CashOverloadException {
		validator.receive(banknote);
		validator.receive(banknote);
		validator.receive(banknote);
		banknote = new Banknote(CAD, denominations[1]);
		validator.receive(banknote);
		assertTrue(banknoteController.getSessionSuspended());
		assertEquals(banknoteController.getEncounteredException().toString(), "com.tdc.NoCashAvailableException");
	}
	@Test public void shouldAnnounceControllerIsDisabledAndTurnedOffTest() {
		banknoteController.disabled(dispenser);
		banknoteController.turnedOff(dispenser);
	}
	@Test public void shouldAnnounceBadBanknoteWhenBanknoteIsDifferentCurrencyTest() throws DisabledException, CashOverloadException {
		System.setOut(new PrintStream(outputStreamCaptor));
		//is a bad banknote because it is a different type of currency
		validator.receive(new Banknote(Currency.getInstance("USD"),denominations[0]));
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length - 1].trim();
        Assert.assertEquals(lastLine, "Invalid bank note detected");
		System.setOut(standardOut);
	}
}
