/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.bag.ReusableBag;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.BagPurchaserController;

import powerutility.PowerGrid;


public class BagPurchaserControllerTest {
	 private BagPurchaserController bagPurchaserController;

		CentralCheckoutStationLogic logic;
		SelfCheckoutStationBronze stationBronze;
    	private PowerGrid grid;
    	private ShoppingCart cart;
    	
    	private ReusableBag reusableBag;
    	private Mass reusableBagMass; 
    	private Barcode reusableBagBarcode;
    	private BarcodedItem barcodedReusableBag;
    	
    	private final PrintStream standardOut = System.out;
       	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	 
	    @Before
	    public void setUp() {
			System.setOut(new PrintStream(outputStreamCaptor));
	    	
	        // Set up the BagPurchaserController instance for testing
	    	stationBronze = new SelfCheckoutStationBronze();
			logic = new CentralCheckoutStationLogic(logic);
			logic = CentralCheckoutStationLogic.installOn(stationBronze);
			this.logic.hardware.getBaggingArea();
	        bagPurchaserController = new BagPurchaserController(logic);
	        
			Numeral[] numerals = {Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five, Numeral.seven};
	        
	        reusableBag = new ReusableBag();
			reusableBagMass = reusableBag.getMass();
			reusableBagBarcode = new Barcode(numerals);	
			barcodedReusableBag = new BarcodedItem(reusableBagBarcode, reusableBagMass);     // generate a barcoded item from reusable bag
	        
	        grid = PowerGrid.instance();
	        cart = ShoppingCart.getShoppingCart(logic);
	    }

	    @Test
	    public void testConstructorInitialization() {
	        // Test constructor initialization of instance variables

	        // Check if reusableBagDispenser is not null
	        assertNotNull(logic.hardware.getReusableBagDispenser());

	        // Add more assertions to check other instance variables if needed
	    }

	    @Test
	    public void testPurchaseReusableBags() throws EmptyDevice, OverloadedDevice {
	    	this.logic.hardware.getReusableBagDispenser().plugIn(grid);
	    	this.logic.hardware.getReusableBagDispenser().turnOn();
	    	
	    	this.logic.hardware.getReusableBagDispenser().load(reusableBag);
	    	this.logic.hardware.getReusableBagDispenser().load(reusableBag);
	    	this.logic.hardware.getReusableBagDispenser().load(reusableBag);
	    	this.logic.hardware.getReusableBagDispenser().load(reusableBag);

	    	cart.addPrice(50);
	    	
	    	bagPurchaserController.purchaseReusableBags(3);
	    	assertTrue(bagPurchaserController.getOperationComplete());
	    }

	    @Test
	    public void testPurchaseReusableBagsNegativeInput() {
	    	this.logic.hardware.getReusableBagDispenser().plugIn(grid);
	    	this.logic.hardware.getReusableBagDispenser().turnOn();
	        try {
	            bagPurchaserController.purchaseReusableBags(-1);
	            assertFalse(bagPurchaserController.getOperationComplete());
	        } catch (EmptyDevice e) {
	            fail("No EmptyDevice exception expected");
	        }
	    }

	    @Test(expected = EmptyDevice.class)
	    public void testPurchaseReusableBagsEmptyDeviceException() throws EmptyDevice {
	    	this.logic.hardware.getReusableBagDispenser().plugIn(grid);
	    	this.logic.hardware.getReusableBagDispenser().turnOn();
	    	
	    	bagPurchaserController.purchaseReusableBags(10);
	    }
	    
	    @Test
	    public void testOutOfBags() {
	    	bagPurchaserController.theDispenserIsOutOfBags();
	    	
	    	// it should be true that the output stream starts with the given message 
	    	assertEquals(true, outputStreamCaptor.toString().startsWith("Dispenser has run out of bags. Attendant has been alerted"));
	    }
	

}
