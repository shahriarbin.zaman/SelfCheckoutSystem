/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test.ControllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.Barcode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;
import powerutility.PowerGrid;

public class ScaleControllerTest {
    private CentralCheckoutStationLogic logic;
    private CentralCheckoutStationLogic silverScaleLogic;
    
    private SelfCheckoutStationBronze checkout;
    private ScaleController scaleController;
    private IElectronicScale scale;

    private Barcode barCode;
    private OwnBagAdder ownBags;
    
	private Numeral[] code;
	private ShoppingCart cart;
	
	private SignalAttendantController signalAttendant;
	private ScannerController bScanner;
    
    
    @Before public void setup() {
    	PowerGrid.engageUninterruptiblePowerSource();
		logic = new CentralCheckoutStationLogic(logic);
		checkout = new SelfCheckoutStationBronze();
		logic = CentralCheckoutStationLogic.installOn(checkout);
		
		cart = ShoppingCart.getShoppingCart(logic);
		scaleController = new ScaleController(logic);
		ownBags = new OwnBagAdder(logic);
		
		signalAttendant = logic.signalAttendantController;
		bScanner = logic.scannerController;
		ItemDatabase.loadItems();
        code = new Numeral[1];
		code[0] = Numeral.one;
		//hardcoded barcode for apple - weight 1g
		barCode = new Barcode(code);
		
		SessionStarter.getSessionStarter(logic).startSession();
		ItemDatabase.loadItems();
		ItemDatabase.initBarcodedItems();
	}
    
    @Test
    public void testInitialization() {
        assertNotNull(scaleController);
        assertNotNull(scaleController.getScaleScanningArea());
        assertFalse(scaleController.weightDiscrepancyDetected());
    }
    
    @After public void takeDown() {
    	signalAttendant.setRequestCleared(false);
    }
   
    @Test
    public void testInitializeScanningAreaScaleWhenScaleBronze() {
        assertTrue(scaleController.getScaleScanningArea() instanceof ElectronicScaleBronze);
    }
    @Test
    public void testSetOwnBagAdder() {
		scaleController.setOwnBagAdder(ownBags);
    }
    @Test 
    public void testInitializeScanningAreaScaleWhenScaleSilver() {
    	silverScaleLogic = new CentralCheckoutStationLogic(silverScaleLogic);
    	SelfCheckoutStationGold goldCheckout = new SelfCheckoutStationGold(); 
    	silverScaleLogic = CentralCheckoutStationLogic.installOn(goldCheckout);
    	scaleController = new ScaleController(silverScaleLogic);
    	assertTrue(scaleController.getScaleScanningArea() instanceof ElectronicScaleSilver);
    }
    @Test 
    public void testGetScaleScanningAreaWhenScaleGold() {
    	scaleController.setScaleScanningArea(new ElectronicScaleGold());
    	assertTrue(scaleController.getScaleScanningArea() instanceof ElectronicScaleGold);
    }
    
    //no gold scale - keep in mind

    @Test
    public void testCheckWeightDiscrepancy_WithoutDiscrepancy() throws OverloadedDevice, Exception {
    	cart.balanceWeights();
        scaleController.checkWeightDiscrepancy();
        assertFalse(scaleController.weightDiscrepancyDetected());
    }
    @Test
    public void testCheckWeightDiscrepancy_WithDiscrepancy() throws OverloadedDevice, Exception {
    	cart.addExpectedWeight(new BigDecimal(50));
    	cart.addActualWeight(new BigDecimal(45));
        scaleController.checkWeightDiscrepancy();
        assertTrue(scaleController.weightDiscrepancyDetected());
        cart.removeActualWeight(new BigDecimal(45));
        cart.removeExpectedWeight(new BigDecimal(50));
    }
    // check output string
    @Test
    public void testCheckMassLimit_WhenOverLimit() throws Exception {
    	//limit is 1000g
    	scaleController.checkMassLimit(new Mass(new BigDecimal(1001)));
    	assertTrue(scaleController.hasExceededLimit());
    }
    @Test
    public void testCheckMassLimit_WhenAtLimit() throws Exception {
    	//limit is 1000g
    	scaleController.checkMassLimit(new Mass(new BigDecimal(1000)));
    	assertFalse(scaleController.hasExceededLimit());
    }
    @Test
    public void testCheckClearRequest_AfterWeightDiscrepancyDetected() throws Exception {
       	cart.addExpectedWeight(new BigDecimal(50));
    	cart.addActualWeight(new BigDecimal(45));
        scaleController.checkWeightDiscrepancy();
        assertTrue(scaleController.weightDiscrepancyDetected());
       	signalAttendant.setRequestCleared(true);
    	scaleController.checkClearRequest();
    	cart.removeActualWeight(new BigDecimal(45));
    	cart.removeExpectedWeight(new BigDecimal(50));
    }   
    @Test 
    public void testTheMassOnTheScaleHasChanged_NoDiscrepancyDetected() {
    	cart.balanceWeights();			// balance weights for testing purposes
    	
        bScanner.addItemViaBarcode(barCode);
        
        assertFalse(scaleController.weightDiscrepancyDetected());
    }
    
    @Test (expected = InvalidStateSimulationException.class) 
    public void testTheMassOnTheScaleHasChanged_DiscrepancyDetected() {
    	//alters expected weight so that weight discrepancy is detected
    	cart.addExpectedWeight(new BigDecimal(10));
        bScanner.addItemViaBarcode(barCode);
        cart.removeExpectedWeight(new BigDecimal(10));
    }
    
    @Test
    public void testCheckRequestCleared_False() throws OverloadedDevice, Exception {    	    	    	
    	cart.addExpectedWeight(new BigDecimal(7770));
    	cart.addActualWeight(new BigDecimal(45));
    	
    	scaleController.checkWeightDiscrepancy();
    	logic.signalAttendantController.setRequestCleared(false); 		// attendant does not approve
    	    	
    	scaleController.checkClearRequest();
    	
        assertTrue(scaleController.weightDiscrepancyDetected());
        
        cart.removeActualWeight(new BigDecimal(7770));
        cart.removeExpectedWeight(new BigDecimal(50));
    }
    
    @Test
    public void testTheMassOnTheScaleHasChanged() {
    	scaleController.theMassOnTheScaleHasChanged(scale, new Mass(777));
    	assertTrue(scaleController.weightDiscrepancyDetected());
    }
    
}
