package com.thelocalmarketplace.software.test.PaymentTest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.card.Card;
import com.jjjwelectronics.card.Card.CardData;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Payment.CreditCardPayment;
import com.thelocalmarketplace.software.Payment.DebitCardPayment;

import powerutility.PowerGrid;

/*     Group Members and UCIDS
Suiba Sultana Swapnil			30174800
Khushi Choksi				30168167
Doug Strueby				30122048
Sukhpreet Kaur Sandhu			30174163
Soila Pertet				30102008
Chetas Patel				30173271
Lev Sinaga				30191948
Ayesha Saeed				30174420
Sadeeb Hossain				30145821
Nolan Ruzicki				30132405
Shahriar Bin Zaman			30111988
Anshi Khatri				30172820
Kazi Mysha Moontaha			30179818
Nabihah Hussaini			30175407
Hareem Arif				30169729
Mahnoor Hameed				30076366
Maria Manosalva Rojas			30146319
Jui Das					30176206
Wasif Ud Dowlah				30130706
Navid Rahman				30186149
Shahriar Khan				30185337
*/

public class DebitCardPaymentTest {
	private CardIssuer cardIssuer;
	private CentralCheckoutStationLogic logic;
	private DebitCardPayment debitCardPayment;	
	private Card card;
	private Calendar expiry;
	private CardData cardData;
	private double transactionAmount;
	private ShoppingCart cart;
	
	//add to cart
	private Numeral[] code;
	private Barcode barCode;
	private BarcodedProduct product;
	private BarcodedItem item;
	private SelfCheckoutStationGold checkout;
	
	//assertion
	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
	private String[] lines;
	private String lastLine;
	
	@Before
	public void setUp() throws OverloadedDevice {
		System.setOut(new PrintStream(outputStreamCaptor));
		logic = new CentralCheckoutStationLogic(logic); 
		checkout = new SelfCheckoutStationGold();
		checkout.plugIn(PowerGrid.instance());
		checkout.turnOn();
		logic = CentralCheckoutStationLogic.installOn(checkout);
		logic.sessionStarter.startSession();

		cart = ShoppingCart.getShoppingCart(logic);
		cardIssuer = new CardIssuer("Scotiabank", 1);
		logic.setCardIssuer(cardIssuer);
		debitCardPayment = new DebitCardPayment(logic);
		//setup card
		expiry = Calendar.getInstance();
	    expiry.set(2024, Calendar.JULY, 1);
	    card = new Card("Debit", "1838373772740583", "Bob Duncan", "736", null, false, false);
		cardIssuer.addCardData("1838373772740583", "Bob Duncan", expiry, "736", 15.0);
		//item
		code = new Numeral[1];
		code[0] = Numeral.one;;
		barCode = new Barcode(code);
		product = new BarcodedProduct(barCode, "apple", 10, 100); 
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(barCode, product);
		item = new BarcodedItem(barCode, new Mass(100));
		
	}
	@After
	public void takeDown() {
		System.setOut(standardOut);
		cart.removePrice((long)transactionAmount);
		while(cart.getShoppingCartItems().size()!=0) {
			cart.removeBarcodedItemFromCart(item);
		}
	}
	@Test public void shouldAnnounceDataIsNullWhenCardDataIsNullTest() {
		cart.addBarcodedItemToCart(item);
		cart.addPrice(product.getPrice());
		transactionAmount = (double)cart.getTotalPrice();
		debitCardPayment.processTransaction(cardData, transactionAmount);
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length - 2].trim();
        Assert.assertEquals(lastLine, "The debit card is not compatible with the payment method used.");
	}	
	@Test public void shouldProcessTransactionWhenPostTransactionProcessedTest() throws IOException {
		cart.addBarcodedItemToCart(item);
		cart.addPrice(product.getPrice());	
		transactionAmount = (double)cart.getTotalPrice();
		cardData = card.swipe();
		try {
			debitCardPayment.processTransaction(cardData, transactionAmount);
		}catch(Exception e) {
			e.toString();      
		}
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length - 1].trim();
        Assert.assertEquals(lastLine, "Remaining balance due: 0");
	}
	@Test public void shouldNotProcessTransactionWhenHoldNotAuthorizedTest() throws IOException {
		cart.addBarcodedItemToCart(item);
		cart.addPrice(product.getPrice());
		cart.addBarcodedItemToCart(item);
		cart.addPrice(product.getPrice());
		transactionAmount = (double)cart.getTotalPrice();
		cardData = card.swipe();
		debitCardPayment.processTransaction(cardData, transactionAmount);
		lines = outputStreamCaptor.toString().split("\\r?\\n");
        lastLine = lines[lines.length - 2].trim();
        Assert.assertEquals(lastLine, "Failed to authorize hold on the debit card.");
	}
}
