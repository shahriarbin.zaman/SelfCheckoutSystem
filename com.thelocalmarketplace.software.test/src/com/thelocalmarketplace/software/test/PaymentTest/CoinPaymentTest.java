package com.thelocalmarketplace.software.test.PaymentTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import static org.junit.Assert.assertNotNull;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tdc.NoCashAvailableException;
import com.tdc.coin.Coin;
import com.tdc.coin.CoinDispenserBronze;
import com.tdc.coin.CoinStorageUnit;
import com.tdc.coin.CoinValidator;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.ChangeController;
import com.thelocalmarketplace.software.Controller.CoinController;
import com.thelocalmarketplace.software.Payment.CoinPayment;

import powerutility.PowerGrid;

import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;

public class CoinPaymentTest {
	

    private CoinPayment coinPayment;
    private CentralCheckoutStationLogic logic;
    private Currency currency;
    private PowerGrid grid;
    private SelfCheckoutStationBronze stationBronze;
	private ShoppingCart cart;
	private CoinController coinController;
	private Currency CAD;
    private List<BigDecimal> denominations;
    private BigDecimal[] denominations2;
    private CoinValidator coinValidator;
    private CoinStorageUnit coinStorageUnit;
    private CoinDispenserBronze bronzeDispenser;
    private ChangeController changeController;


	private final PrintStream standardOut = System.out;
	private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
    	
    	cart = ShoppingCart.getShoppingCart(logic);
    	cart.addPrice(1);
    	
    	PowerGrid.engageUninterruptiblePowerSource();
    	grid = PowerGrid.instance();
    	
    	CAD = currency.getInstance("CAD");
    	
    	AbstractSelfCheckoutStation.configureCurrency(CAD);
    	

    	denominations = new ArrayList<>();
    	denominations.add(BigDecimal.ONE);
    	denominations.add(BigDecimal.valueOf(0.10));
    	denominations.add(BigDecimal.valueOf(0.50));
    	denominations.add(BigDecimal.valueOf(0.25));
    	denominations.add(BigDecimal.valueOf(5));

        denominations2 = new BigDecimal[]{
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
    	
    	AbstractSelfCheckoutStation.configureCoinDenominations(denominations2);
        AbstractSelfCheckoutStation.configureCoinStorageUnitCapacity(5);
        AbstractSelfCheckoutStation.configureCoinTrayCapacity(5);
        AbstractSelfCheckoutStation.configureCoinDispenserCapacity(5);
        
        stationBronze = new SelfCheckoutStationBronze();

        stationBronze.plugIn(grid);
        stationBronze.turnOn();
        
		logic = new CentralCheckoutStationLogic(logic);

		logic.hardware = stationBronze;

		logic = CentralCheckoutStationLogic.installOn(stationBronze);		
		logic.hardware.getBaggingArea();
        
        coinValidator = new CoinValidator(CAD, denominations);
        coinValidator.connect(grid);
        coinValidator.activate();
        coinValidator.enable();
        
        coinStorageUnit = new CoinStorageUnit(5);
        coinStorageUnit.connect(grid);
        coinStorageUnit.activate();
        
        bronzeDispenser = new CoinDispenserBronze(1);
        bronzeDispenser.connect(grid);
        bronzeDispenser.activate();
        
        coinPayment = new CoinPayment(logic);
        changeController = new ChangeController(logic);
    	
    }
	
    @After
    public void takeDown() {
    	cart.removePrice(1);
    }
    
    @Test(expected = NoCashAvailableException.class)
    public void testReceiveCoinWithSessionSuspended() throws Exception {
		logic.hardware.getPrinter().addInk(50);
    	
        Coin coin = new Coin(CAD, new BigDecimal("2.00"));     
        coinPayment.receiveCoin(coin);
        coinPayment.checkIfSessionIsSuspended();
    }
    
    
    @Test
    public void testReceiveCoinValid() throws Exception {
		logic.hardware.getPrinter().addInk(50);
    	
		currency = Currency.getInstance("CAD");
        Coin validCoin = new Coin(currency, new BigDecimal("1.00"));
        coinPayment.receiveCoin(validCoin);
        assertNotNull(validCoin);

    }
     
    
}