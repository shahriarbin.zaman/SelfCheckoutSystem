package com.thelocalmarketplace.software.test.PaymentTest;
/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/
import static org.junit.Assert.assertNotNull;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Currency;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.tdc.CashOverloadException;
import com.tdc.NoCashAvailableException;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.BanknoteDispensationSlot;
import com.tdc.banknote.BanknoteDispenserBronze;
import com.tdc.banknote.BanknoteInsertionSlot;
import com.tdc.banknote.BanknoteStorageUnit;
import com.tdc.banknote.BanknoteValidator;
import com.tdc.banknote.IBanknoteDispenser;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.BanknoteController;
import com.thelocalmarketplace.software.Controller.ChangeController;
import com.thelocalmarketplace.software.Controller.CoinController;
import com.thelocalmarketplace.software.Payment.BanknotePayment;
import com.thelocalmarketplace.software.Payment.CoinPayment;

import powerutility.PowerGrid;

public class BanknotePaymentTest {
 	private CentralCheckoutStationLogic logic;
    private Currency currency;
    private PowerGrid grid;
    private SelfCheckoutStationBronze stationBronze;
	private ShoppingCart cart;
	private Currency CAD;
    private BigDecimal[] denominations;
    private BanknoteValidator banknoteValidator;
    private BanknoteStorageUnit banknoteStorageUnit;
    private BanknoteDispenserBronze bronzeDispenser;
    private ChangeController changeController;
    private BanknoteInsertionSlot insertionSlot;
    private BanknoteDispensationSlot banknoteDispensationSlot;
    private BanknotePayment banknotePayment;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() throws CashOverloadException {
    	
    	cart = ShoppingCart.getShoppingCart(logic);
        cart.addPrice(2);
    	
    	PowerGrid.engageUninterruptiblePowerSource();
    	grid = PowerGrid.instance();
    	
    	CAD = currency.getInstance("CAD");
    	
    	AbstractSelfCheckoutStation.configureCurrency(CAD);
    	

    	denominations = new BigDecimal[]{
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
        AbstractSelfCheckoutStation.configureBanknoteDenominations(denominations);
        AbstractSelfCheckoutStation.configureBanknoteStorageUnitCapacity(5);
        AbstractSelfCheckoutStation.configureCoinDenominations(denominations);

        
        stationBronze = new SelfCheckoutStationBronze();
        insertionSlot = new BanknoteInsertionSlot();

        stationBronze.plugIn(grid);
        stationBronze.turnOn();
        
		logic = new CentralCheckoutStationLogic(logic);

		logic.hardware = stationBronze;

		logic = CentralCheckoutStationLogic.installOn(stationBronze);		
		logic.hardware.getBaggingArea();
        
		banknoteValidator = new BanknoteValidator(CAD, denominations);
		banknoteValidator.connect(grid);
		banknoteValidator.activate();
		banknoteValidator.enable();
        
		insertionSlot.connect(grid);
        insertionSlot.activate();
        
        banknoteStorageUnit = new BanknoteStorageUnit(5);
        banknoteStorageUnit.connect(grid);
        banknoteStorageUnit.activate();
        
        bronzeDispenser = new BanknoteDispenserBronze();
        bronzeDispenser.connect(grid);
        bronzeDispenser.activate();
        bronzeDispenser.load(new Banknote(CAD, new BigDecimal("2.00")));

        
        banknoteDispensationSlot = new BanknoteDispensationSlot();
        banknoteDispensationSlot.connect(grid);
        banknoteDispensationSlot.activate();
        
      
        banknotePayment = new BanknotePayment(logic);
        changeController = new ChangeController(logic);
 
    }

    @After
    public void takeDown() {
    	cart.removePrice(2);
    }
    
    @Test 
    public void testReceiveBanknoteValid() throws Exception {

		currency = Currency.getInstance("CAD");
        Banknote banknote1 = new Banknote(currency, new BigDecimal("2.00"));
        
        banknotePayment.receiveBanknote(banknote1);
        assertNotNull(banknote1);

    }
    
    @Test(expected = NoCashAvailableException.class)
    public void testReceiveBanknoteWithSessionSuspended() throws Exception {
        
        Banknote banknote = new Banknote(CAD, new BigDecimal("5.00"));
        banknotePayment.receiveBanknote(banknote);
        banknotePayment.checkIfSessionIsSuspended();
    }
}