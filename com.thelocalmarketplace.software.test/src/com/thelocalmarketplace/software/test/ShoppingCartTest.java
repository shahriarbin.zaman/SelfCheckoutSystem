package com.thelocalmarketplace.software.test;

/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.GUI.ItemDatabase;

import powerutility.PowerGrid;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ShoppingCartTest {
	private SelfCheckoutStationBronze stationBronze;
	private CentralCheckoutStationLogic logic;
	
    private ShoppingCart shoppingCart;
    private BarcodedItem testItem;
    private BarcodedProduct testProduct;
    private Barcode testBarcode;
    
    PowerGrid grid;
    
    PLUCodedItem testPluItem;
    PLUCodedProduct pluProduct;

    
    
    @Before
    public void setUp() {
    	stationBronze = new SelfCheckoutStationBronze();
		logic = new CentralCheckoutStationLogic(logic);
		logic = CentralCheckoutStationLogic.installOn(stationBronze);
        shoppingCart = ShoppingCart.getShoppingCart(logic);
        
        grid = PowerGrid.instance();
        PowerGrid.engageUninterruptiblePowerSource();
        stationBronze.plugIn(grid);
    	stationBronze.turnOn();
        
        // Create a Barcode object with Numeral array equivalent to "12345"
        Numeral[] numerals = { Numeral.one, Numeral.two, Numeral.three, Numeral.four, Numeral.five };
        testBarcode = new Barcode(numerals);
        testProduct = new BarcodedProduct(testBarcode, "Test Product", 1000, 500.0);
        testItem = new BarcodedItem(testBarcode, new Mass(BigDecimal.valueOf(testProduct.getExpectedWeight())));
        // Add the thang to the db
        ProductDatabases.BARCODED_PRODUCT_DATABASE.put(testBarcode, testProduct);
        
        //creating test barcodes
      	String pluCodeString = "12345";
        PriceLookUpCode pluCode = new PriceLookUpCode(pluCodeString);
        String productDescription = "apple";
        long price = 67;
		Mass testMass = new Mass(BigDecimal.valueOf(500));  //creating test mass

      		
      	//creating item instances
      	testPluItem = new PLUCodedItem(pluCode, testMass); 
      	pluProduct = ProductDatabases.PLU_PRODUCT_DATABASE.get(testPluItem.getPLUCode());
      	ProductDatabases.PLU_PRODUCT_DATABASE.put(testPluItem.getPLUCode(), pluProduct);
      	
      	SessionStarter.getSessionStarter(logic).startSession();
      	
      	ItemDatabase.loadItems();
    }
    
    @After
    public void tearDown() {
        // Clean up the database to its original state
        ProductDatabases.BARCODED_PRODUCT_DATABASE.remove(testBarcode, testProduct);
        ProductDatabases.PLU_PRODUCT_DATABASE.remove(testPluItem.getPLUCode(), pluProduct);
        shoppingCart.clearItems();
    }
    
    // ----- BARCODED ITEM METHODS TESTING
    @Test
    public void addBarcodedItemToCart_ShouldAddItem() {	
        shoppingCart.addBarcodedItemToCart(testItem);
        assertFalse("Cart should not be empty after adding item", shoppingCart.getShoppingCartItems().isEmpty());
        assertEquals("Added item should be in the cart", testItem, shoppingCart.getShoppingCartItems().get(shoppingCart.getShoppingCartItems().size()-1));
    }

    @Test
    public void removeBarcodedItemFromCart_ShouldRemoveItem() {        
        shoppingCart.addBarcodedItemToCart(testItem);
        int previousNumberOfItems = shoppingCart.getShoppingCartItems().size();
        shoppingCart.removeBarcodedItemFromCart(testItem);
        assertEquals(previousNumberOfItems - 1, shoppingCart.getShoppingCartItems().size());
    }
    // ------------------------------------
    
    
    
    // ------- PLU CODED ITEM METHODS TESTING
    @Test
    public void addPLUCodedItemToCart_ShouldAddItem() {
    	shoppingCart.addPLUItemToCart(testPluItem);
    	assertFalse("Cart should not be empty after adding item", shoppingCart.getShoppingCartPLUItems().isEmpty());
        assertEquals("Added item should be in the cart", testPluItem, shoppingCart.getShoppingCartPLUItems().get(shoppingCart.getShoppingCartPLUItems().size() - 1));
    }
    
    @Test
    public void removePLUCodedItemFromCart_ShouldRemoveItem() {
    	shoppingCart.addPLUItemToCart(testPluItem);
    	shoppingCart.addPLUItemToCart(testPluItem);
    	shoppingCart.addPLUItemToCart(testPluItem);

    	int previousNumberOfItems = shoppingCart.getShoppingCartPLUItems().size();
    	
    	shoppingCart.removePLUItemFromCart(testPluItem);
    	shoppingCart.removePLUItemFromCart(testPluItem);

    	assertEquals(previousNumberOfItems - 2, shoppingCart.getShoppingCartPLUItems().size());
    }
    
    // --------------------------------------

    @Test
    public void addExpectedWeight_ShouldIncreaseTotalExpectedWeight() {
        BigDecimal weightToAdd = new BigDecimal("500");
        BigDecimal previousWeight = shoppingCart.getTotalExpectedWeight();
        shoppingCart.addExpectedWeight(weightToAdd);
        assertEquals("Total expected weight should be updated", previousWeight.add(weightToAdd), shoppingCart.getTotalExpectedWeight());
    }

    @Test
    public void removeExpectedWeight_ShouldDecreaseTotalExpectedWeight() {
        BigDecimal weightToAdd = new BigDecimal("500");
        BigDecimal weightToRemove = new BigDecimal("200");
        shoppingCart.addExpectedWeight(weightToAdd);
        BigDecimal previousWeight = shoppingCart.getTotalExpectedWeight();
        shoppingCart.removeExpectedWeight(weightToRemove);
        assertEquals("Total expected weight should be decreased", previousWeight.subtract(weightToRemove), shoppingCart.getTotalExpectedWeight());
    }
    
    @Test 
    public void addActualWeight_ShouldIncreaseTotalActualWeight() {
    	 BigDecimal weightToAdd = new BigDecimal("6000");
         BigDecimal previousWeight = shoppingCart.getTotalActualWeight();
         shoppingCart.addActualWeight(weightToAdd);
         assertEquals("Total actual weight should be updated", previousWeight.add(weightToAdd), shoppingCart.getTotalActualWeight());
    }
    
    @Test
    public void removeActualWeight_ShouldDecreaseTotalActualWeight() {
        BigDecimal weightToAdd = new BigDecimal("505");
        BigDecimal weightToRemove = new BigDecimal("201");
        shoppingCart.addActualWeight(weightToAdd);
        BigDecimal previousWeight = shoppingCart.getTotalActualWeight();
        shoppingCart.removeActualWeight(weightToRemove);
        assertEquals("Total actual weight should be decreased", previousWeight.subtract(weightToRemove), shoppingCart.getTotalActualWeight());
    }

    @Test
    public void addPrice_ShouldIncreaseTotalPrice() {
        long priceToAdd = 200;
        long previousPrice = shoppingCart.getTotalPrice();
        shoppingCart.addPrice(priceToAdd);
        assertEquals("Total price should be increased", previousPrice + priceToAdd, shoppingCart.getTotalPrice());
        shoppingCart.removePrice(priceToAdd);
    }

    @Test
    public void removePrice_ShouldDecreaseTotalPrice() {
        long priceToAdd = 500;
        long priceToRemove = 300;
        shoppingCart.addPrice(priceToAdd);
        shoppingCart.removePrice(priceToRemove);
        assertEquals("Total price should be decreased", priceToAdd - priceToRemove, shoppingCart.getTotalPrice());
    }
    
    @Test
    public void getTotalNumberOfItems_ShouldReturnCorrectItemCount() {
    	int previousNumberOfItems = shoppingCart.getTotalNumberOfItems();
    	
        shoppingCart.addBarcodedItemToCart(testItem);
        shoppingCart.addPLUItemToCart(testPluItem);

        int itemCount = shoppingCart.getTotalNumberOfItems();
        assertEquals("Cart should contain two items", previousNumberOfItems + 2, itemCount);
    }
    
    @Test
    public void getShoppingCartBarcodedProducts_ShouldReturnCorrectProducts() {
        shoppingCart.addBarcodedItemToCart(testItem);

        ArrayList<BarcodedProduct> products = shoppingCart.getShoppingCartProducts();
        assertEquals("Cart should contain two products", 1, products.size());
        assertTrue("Cart should contain product", products.contains(testProduct));
    }
    
    @Test
    public void getShoppingCartPLUCodedProducts_ShouldReturnCorrectProducts() {
        shoppingCart.addPLUItemToCart(testPluItem);
        
        ArrayList<PLUCodedProduct> products = shoppingCart.getShoppingCartPLUProducts();
        assertEquals("Cart should contain two products", 1, products.size());
        assertTrue("Cart should contain product", products.contains(pluProduct));
    }
    
    @Test
    public void testBalanceWeights_Wrong() {    	
        shoppingCart.balanceWeights();
        assertEquals(shoppingCart.getTotalActualWeight(), shoppingCart.getTotalExpectedWeight());
    }
    
    @Test
    public void testClearItems() {
    	shoppingCart.clearItems();
    	
    	assertTrue(shoppingCart.getTotalExpectedWeight() == BigDecimal.ZERO);
    	assertTrue(shoppingCart.getTotalActualWeight() == BigDecimal.ZERO);
    	assertTrue(shoppingCart.getTotalPrice() == 0);
    	assertTrue(shoppingCart.getTotalNumberOfItems() == 0);
    	assertTrue(shoppingCart.getShoppingCartItems().isEmpty());
    	assertTrue(shoppingCart.getShoppingCartProducts().isEmpty());
    	assertTrue(shoppingCart.getShoppingCartPLUItems().isEmpty());
    	assertTrue(shoppingCart.getShoppingCartPLUProducts().isEmpty());
    }
    
    @Test
    public void testGetShoppingCartMethodOverload() {
    	// should still return same instance due to singleton pattern
    	ShoppingCart shoppingCart2 = ShoppingCart.getShoppingCart();
    	assertEquals(shoppingCart, shoppingCart2);
    }
    
    
}