/*
Suiba Sultana Swapnil		30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu		30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    		30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas		30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.StationEnabler;

public class StationEnablerTest {
    private CentralCheckoutStationLogic logic;
    private StationEnabler stationEnabler;

    @Before
    public void setup() {
        logic = CentralCheckoutStationLogic.installOn(new SelfCheckoutStationGold());
        stationEnabler = new StationEnabler();
    }

    @Test
    public void shouldAssertStationEnablerFalse() {
        assertFalse(stationEnabler.getStationEnabledState());
    }

    @Test
    public void shouldGetStationEnabled() {
        assertFalse(stationEnabler.getStationEnabledState());
        stationEnabler.enableStation(logic);
        assertTrue(stationEnabler.getStationEnabledState());
    }
}
