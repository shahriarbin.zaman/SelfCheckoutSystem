/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software;

import java.util.HashMap;
import java.util.Map;

public class InkManager extends CentralCheckoutStationLogic{
	

    private int inkLevel; // Current ink level in percentage
    private String inkType; // Type of ink cartridge
    private Map<String, Integer> inkUsage; // Track ink usage by document type

    public InkManager(int initialInkLevel, String inkType, CentralCheckoutStationLogic logic) {
    	super(logic);
    	
    	if (initialInkLevel < 0 || initialInkLevel > 100) {
            throw new IllegalArgumentException("Initial ink level must be between 0 and 100");
        }
        this.inkLevel = initialInkLevel;
        this.inkType = inkType;
        this.inkUsage = new HashMap<>();
    }

    // Function to refill the ink cartridge
    public void refillInk() {
        this.inkLevel = 100;
    }

    // Function to simulate using ink for printing
    public void useInk(int amount, String documentType) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Amount of ink used must be positive");
        }
        if (amount > this.inkLevel) {
            throw new IllegalArgumentException("Not enough ink available");
        }
        this.inkLevel -= amount;
        
        // Update ink usage for the document type
        inkUsage.put(documentType, inkUsage.getOrDefault(documentType, 0) + amount);
    }

    // Function to automatically reorder ink cartridges when ink level is low
    public void reorderInk() {
        if (inkLevel < 20) {
            
        	// Logic to reorder ink cartridges from supplier
            System.out.println("Low ink level. Reordering ink cartridges...");
            logic.reorderInkCartridges();
        }
    }

    // Function to track ink usage by document type
    public int getInkUsage(String documentType) {
        return inkUsage.getOrDefault(documentType, 0);
    }

    // Function to replace ink cartridge when empty or low on ink
    public void replaceInkCartridge() {
        System.out.println("Replace ink cartridge with a new one.");
        logic.replaceInkCartridge();
    }

    // Function to calculate total ink usage
    public int calculateTotalInkUsage() {
        int totalUsage = 0;
        for (int usage : inkUsage.values()) {
            totalUsage += usage;
        }
        return totalUsage;
    }

    // Getters and setters for ink type and ink level
    public String getInkType() {
        return inkType;
    }

    public void setInkType(String inkType) {
        this.inkType = inkType;
    }

    public int getInkLevel() {
        return inkLevel;
    }

    public void setInkLevel(int inkLevel) {
        this.inkLevel = inkLevel;
    }
}