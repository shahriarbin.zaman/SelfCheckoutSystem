/*
 * <h1>StartSession</h1>
 * The StartSession program implements an application that
 * starts a session for the user to begin scanning groceries.
 * Also checks if the system is in session and ends the session.
 * <p>
 * 
 */

/*
Suiba Sultana Swapnil	30174800
Khushi Choksi	30168167
Doug Strueby	30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet	30102008
Chetas Patel	30173271
Lev Sinaga	30191948
Ayesha Saeed	30174420
Sadeeb Hossain	30145821
Nolan Ruzicki	30132405
Shahriar Bin Zaman	30111988
Anshi Khatri	30172820
Kazi Mysha Moontaha	30179818
Nabihah Hussaini	30175407
Hareem Arif	30169729
Mahnoor Hameed	30076366
Maria Manosalva Rojas	30146319
Jui Das	30176206
Wasif Ud Dowlah	30130706
Navid Rahman	30186149
Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software;

import com.thelocalmarketplace.software.GUI.SystemListener;

public class SessionStarter {
	
	private static SessionStarter sessionStarter;
	
	public boolean session = false;
	
	public static SessionStarter getSessionStarter(CentralCheckoutStationLogic logic) {
        if (sessionStarter == null) {
        	sessionStarter = new SessionStarter();
        }
        return sessionStarter;
	}
	
	private SessionStarter( ) {
		
	}
	
	/*
	 * This method is used to start a session.
	 */
	public void startSession() {
		session = true;
	} 
	 
	/*
	 * This method is used to end a session.
	 */
	public void endSession() {
		session = false;
		SystemListener.theSessionHasEnded();
		// reset shopping cart and other info.
	}
	
	
	/*
	 * This method is used to get the value of session in order to determine if
	 * a the system is in a session or not.
	 * @Return session
	 */
	public boolean hasSessionBeenStarted() {
		return session;
	}
}