/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Payment;

import com.jjjwelectronics.card.Card.CardData;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.ReceiptPrinterController;
import com.thelocalmarketplace.software.GUI.SystemListener;

public class DebitCardPayment 
{
    private CardIssuer cardIssuer;
	private CentralCheckoutStationLogic centralSystem; 
	private ReceiptPrinterController receiptPrinterControl;

    public DebitCardPayment(CentralCheckoutStationLogic logic) 
    {
        this.cardIssuer = logic.getCardIssuer();
        this.centralSystem = logic;
        this.receiptPrinterControl = centralSystem.receiptPrinterController;
    }

    public void processTransaction(CardData data, double transactionAmount) 
    {
    	//Card.tap() returns null CardTapData(implements CardData) object if tap is not enabled on card
    	// same with Card.insert() if card does not have chip
    	if(data == null) {
    		System.out.println("The debit card is not compatible with the payment method used.");
            SystemListener.aPaymentHasFailed("The debit card is not compatible with the payment method used."); 
            
    	} else {
    		String cardNumber = data.getNumber();
	        
	        long holdNumber = cardIssuer.authorizeHold(cardNumber, transactionAmount);
	        
	        if (holdNumber != -1) {
	            System.out.println("Debit card hold authorized successfully.");
	            
	            boolean postSuccess = cardIssuer.postTransaction(cardNumber, holdNumber, transactionAmount);
	            
	            if (postSuccess) {
	                System.out.println("Debit card transaction posted successfully.");
	                ShoppingCart.getShoppingCart(centralSystem).removePrice((long) transactionAmount);
	                SystemListener.aPaymentHasBeenMade(); 
	                
	                try {
	                    receiptPrinterControl.print();
	                    System.out.println("Receipt has been printed successfully.");
	                    
	                } catch (Exception e) {
	                    System.out.println("Failed to print receipt: " + e.getMessage());
	                }
	                
	            } else {
	                System.out.println("Debit card transaction failed to post.");
	                SystemListener.aPaymentHasFailed("Debit card transaction failed to post.");
	            }
	            
	        } else {
	            System.out.println("Failed to authorize hold on the debit card.");
	            SystemListener.aPaymentHasFailed("Failed to authorize hold on the debit card."); 
	        }
    	}
        // Display the remaining balance due
        long remainingBalance = ShoppingCart.getShoppingCart(centralSystem).getTotalPrice();
        System.out.println("Remaining balance due: " + remainingBalance);
    }
}