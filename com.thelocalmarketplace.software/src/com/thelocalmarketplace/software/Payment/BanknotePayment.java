/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Payment;

import com.tdc.banknote.Banknote;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class BanknotePayment extends CentralCheckoutStationLogic {

	public BanknotePayment(CentralCheckoutStationLogic logic) {
		
		super(logic);
		
	}
	
	public void checkIfSessionIsSuspended() throws Exception {
		if(this.logic.coinController.getSessionSuspended()) {
			System.out.println("Issue occured during transaction. Session suspended");
			System.out.println(this.logic.banknoteController.getEncounteredException());
			throw this.logic.banknoteController.getEncounteredException();
		} 
	}
	
	/** 
	 * Calculate the amount paid by the customer
	 * @param Banknote banknote
	 * 		the bank note inserted by the customer
	 * @throws Exception 
	 * */
	public void receiveBanknote(Banknote banknote) throws Exception {
		
		// accept and validate bank note inserted by customer
		this.logic.hardware.getBanknoteInput().receive(banknote);
		
		// check if session was suspended after bank note validator received bank note
		checkIfSessionIsSuspended();
		
		// store inserted bank note if bank note is valid
		this.logic.hardware.getBanknoteStorage().receive(banknote);
		
	}

}