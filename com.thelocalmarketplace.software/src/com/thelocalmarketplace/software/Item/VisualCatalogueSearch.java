/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Item;

import java.util.Map.Entry;

import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.screen.TouchScreenListener;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.Product;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;

/**
 * Handles item selection from the visual catalog, updating cart upon selection.
 */
public class VisualCatalogueSearch extends CentralCheckoutStationLogic implements TouchScreenListener 
{
    public VisualCatalogueSearch(CentralCheckoutStationLogic logic) 
    {
		super(logic);
	}

    /**
     * Called when a product is selected from the visual catalog.
     * Adds the product to the current shopping session.
     * @param product The selected product from the screen.
     */
    public void addProductFromCatalogue(Product product) 
    {
        // Determine if the product is identified by a barcode or PLU.
    	
    	/*	For a barcode product, it gets added to the cart using the barcode,
    	 *  but it is not scanned so the argument passed for the barcode scanner is null.
    	 */
        if (product instanceof BarcodedProduct) 
        {
            Barcode barcode = ((BarcodedProduct) product).getBarcode();
            
            logic.scannerController.addItemViaBarcode(barcode);
            
        } 
        
        else if (product instanceof PLUCodedProduct) 
        {
            PriceLookUpCode pluCode = ((PLUCodedProduct) product).getPLUCode();
            logic.priceLookupCodeController.addItemByPLU(pluCode);
        } 
        
        /* Theoretically, this condition should not be reached since the inventory is allowed to have products 
         * that have a barcode or a PLU option. 
         */
        else 
        {
            System.out.println("Invalid product. Cannot add to cart.");
        }
    }


    @Override
    public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
    }
        

    @Override
    public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
    }

    @Override
    public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
    }

    @Override
    public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
    }
}