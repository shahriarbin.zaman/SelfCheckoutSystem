/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Item;


import java.util.Map.Entry;

import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class TextSearch extends CentralCheckoutStationLogic {
		/**
	 * Constructor for the TextSearch class.
	 * @param logic The central logic.
	 */
	public TextSearch(CentralCheckoutStationLogic logic) {
		super(logic);
	}
	
	public PriceLookUpCode findItemByName(String productName) {
		if (productName == null) {
			return null;		
		}
		for (Entry<PriceLookUpCode, PLUCodedProduct> entry : ProductDatabases.PLU_PRODUCT_DATABASE.entrySet()) {
			PLUCodedProduct product = entry.getValue();
			if (product.getDescription().equals(productName)) {
				return product.getPLUCode();
			}
		}
		return null;
	} 
	
	/**
	 * Finds the searched product in the database. If it is found and the customer chooses to add it, the product is added by PLU code. 
	 * @param productName The name that the user searches. 
	 */
	public void addItemByName(String productName) {
		PriceLookUpCode itemCode = findItemByName(productName);
		if(itemCode == null) {
			System.out.println("Item not found");
			return;
		}
		logic.priceLookupCodeController.addItemByPLU(itemCode);   
	}
}
