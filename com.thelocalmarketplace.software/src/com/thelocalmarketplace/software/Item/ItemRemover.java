/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Item;


import java.util.ArrayList;
import java.math.BigDecimal;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;

public class ItemRemover extends CentralCheckoutStationLogic{
	
	private IElectronicScale eScale;
	private ShoppingCart shoppingCart;
	private int totalItems;
	private ArrayList<BarcodedItem> itemShoppingCart;
	private ArrayList<PLUCodedItem> PLUitemShoppingCart;
	private BigDecimal actualWeight;
	PriceLookUpCode pluCode;
	Mass mass = new Mass(BigDecimal.valueOf(10));

	// constructor for RemoveItem
	public ItemRemover(CentralCheckoutStationLogic logic) {
		super(logic);
		eScale = this.logic.hardware.getBaggingArea();
		this.shoppingCart = ShoppingCart.getShoppingCart(logic);
	}

	// removeItem method can be used outside the class, (tester must test all three scale types)
	public void removeBarcodedItem(BarcodedItem item, IElectronicScale eScale) {
		
		ProductDatabaseInitializer productDB = new ProductDatabaseInitializer();
		productDB.initialize();
		
		totalItems = this.shoppingCart.getShoppingCartItems().size();
		itemShoppingCart = this.shoppingCart.getShoppingCartItems();
		
		// if there are no items in the cart, notify customer
		if(totalItems == 0) {
			System.out.println("There are no items to be removed from your shopping cart.");
		} 
		else {
			// if the shopping cart contains the item
			if(itemShoppingCart.contains(item)) {
				eScale.addAnItem(item); 
				eScale.removeAnItem(item); 
				
				shoppingCart.removeBarcodedItemFromCart(item);
				

				this.actualWeight = item.getMass().inGrams();
				this.shoppingCart.removeActualWeight(this.actualWeight);
				
				// remove barcoded attributes
				BarcodedProduct product = ProductDatabases.BARCODED_PRODUCT_DATABASE.get(item.getBarcode());
	        	removeBarcodedProductAttributes(product);
				System.out.println("Item removed from shopping cart successfully.");
			} 
			else { 
				System.out.println("This item is not in your shopping cart."); 
			}
		}
	}
	
	public void removePLUItem(PLUCodedItem item) {

		PLUProductDatabaseInitializer productDB = new PLUProductDatabaseInitializer();
		productDB.initializePLU();
		
		totalItems = this.shoppingCart.getShoppingCartPLUItems().size();
		PLUitemShoppingCart = this.shoppingCart.getShoppingCartPLUItems();
		
		// if there are no items in the cart, notify customer
		if(totalItems == 0) {
			System.out.println("There are no items to be removed from your shopping cart.");
		} 
		else {
			// if the shopping cart contains the item
			if(PLUitemShoppingCart.contains(item)) {
				
				shoppingCart.removePLUItemFromCart(item); 
				
				this.shoppingCart.removeExpectedWeight(item.getMass().inGrams());
				
				// remove PLU coded attributes
				this.shoppingCart.removeActualWeight(item.getMass().inGrams());
				PLUCodedProduct product = ProductDatabases.PLU_PRODUCT_DATABASE.get(item.getPLUCode()); 
				shoppingCart.removePrice(product.getPrice());
				
				System.out.println("Item removed from shopping cart successfully.");
			} 
			else { // if the shopping cart does not contain the item
				System.out.println("This item is not in your shopping cart."); 
			}
		} 
	}

	// public method called in removeBarcodedItem
	public void removeBarcodedProductAttributes(BarcodedProduct product) {
		
		long productPrice = product.getPrice(); 
		this.shoppingCart.removePrice(productPrice);	
		
    	BigDecimal expectedWeightInGrams = BigDecimal.valueOf(product.getExpectedWeight()); 
    	this.shoppingCart.removeExpectedWeight(expectedWeightInGrams);
	}
	

	public class ProductDatabaseInitializer {
	    public void initialize() {
		
		Barcode barcode = new Barcode(new Numeral[]{Numeral.one, Numeral.two, Numeral.three,Numeral.four,Numeral.five});
		BarcodedProduct product = new BarcodedProduct(barcode, "Sample Product", 123, 4.56);
		
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(barcode, product);
	    }
	}
	    
	    public class PLUProductDatabaseInitializer {
		    public void initializePLU() {
			
		        pluCode = new PriceLookUpCode("12345");
		    	PLUCodedProduct product = new PLUCodedProduct(pluCode, "Sample PLU Product", 23 );
			
		    	ProductDatabases.PLU_PRODUCT_DATABASE.put(pluCode, product);
		    }
	}
	    
	 
}