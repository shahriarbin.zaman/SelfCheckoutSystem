/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software;

import java.util.ArrayList;
import java.util.List;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.OverloadedDevice;

public class PaperManager {
    private int paperInventory; // Inventory of paper in sheets
    private List<String> paperTypes; // List of supported paper types

    public PaperManager(int initialPaperInventory) {
        if (initialPaperInventory < 0) {
            throw new IllegalArgumentException("Initial paper inventory cannot be negative");
        }
        this.paperInventory = initialPaperInventory;
        this.paperTypes = new ArrayList<>();
    }

    // Function to add paper of a specific type to the inventory
    public void addPaper(int quantity, String paperType) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity of paper must be positive");
        }
        this.paperInventory += quantity;
        if (!paperTypes.contains(paperType)) {
            paperTypes.add(paperType);
        }
    }

    // Function to remove paper of a specific type from the inventory
    public void removePaper(int quantity, String paperType) {
        if (quantity <= 0 || quantity > this.paperInventory) {
            throw new IllegalArgumentException("Invalid quantity of paper to remove");
        }
        this.paperInventory -= quantity;
    }

    // Function to check if paper inventory is low
    public boolean isPaperLow() {
        return paperInventory < 10;
    }

    // Function to automatically reorder paper when inventory is low
    public void reorderPaper() {
        if (isPaperLow()) {
            System.out.println("Low paper inventory. Reordering paper...");
        }
    }

    // Function to track paper usage for a specific task
    public void trackPaperUsage(String taskName, int sheetsUsed) {
        System.out.println("Paper used for task '" + taskName + "': " + sheetsUsed + " sheets");
    }

    // Function to retrieve a list of supported paper types
    public List<String> getPaperTypes() {
        return paperTypes;
    }

    public int getPaperInventory() {
    	return paperInventory;
    }
}