
/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/


package com.thelocalmarketplace.software.GUI;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jjjwelectronics.Item;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.external.ProductDatabases;

public class ItemDatabase {
	static Map<String, PLUCodedItem> PLUProduct = new HashMap<>();
	static Map<String, BarcodedItem> barcodedProduct = new HashMap<>();
	static ArrayList<BarcodedProduct> barPros = new ArrayList<>();
	static ArrayList<PLUCodedProduct> PLUPros = new ArrayList<>();
	
	public static void loadItems() {
		initPLUItems();
	}
	
	@SuppressWarnings("unused")
	private static void initPLUItems() {
		PriceLookUpCode bananaPLU = new PriceLookUpCode("0001");
		PriceLookUpCode hamPLU = new PriceLookUpCode("0002");
		PriceLookUpCode cheesePLU = new PriceLookUpCode("0003");
		PriceLookUpCode cookiePLU = new PriceLookUpCode("0004");
		PriceLookUpCode chickenPLU = new PriceLookUpCode("0005");
		PriceLookUpCode steakPLU = new PriceLookUpCode("0006");
		PriceLookUpCode pizzaPLU = new PriceLookUpCode("0007");
		PriceLookUpCode peanutsPLU = new PriceLookUpCode("0008");
		PriceLookUpCode cerealPLU = new PriceLookUpCode("0009");
		PriceLookUpCode ricePLU = new PriceLookUpCode("0010");
		PriceLookUpCode chairPLU = new PriceLookUpCode("0011");
		
		PLUCodedItem banana = new PLUCodedItem(bananaPLU, new Mass(new BigDecimal("12")));
		PLUCodedItem ham = new PLUCodedItem(hamPLU, new Mass(new BigDecimal("150")));
		PLUCodedItem cheese = new PLUCodedItem(cheesePLU, new Mass(new BigDecimal("20")));
		PLUCodedItem cookie = new PLUCodedItem(cookiePLU, new Mass(new BigDecimal("20")));
		PLUCodedItem chicken = new PLUCodedItem(chickenPLU, new Mass(new BigDecimal("150")));
		PLUCodedItem steak = new PLUCodedItem(steakPLU, new Mass(new BigDecimal("200")));
		PLUCodedItem pizza = new PLUCodedItem(pizzaPLU, new Mass(new BigDecimal("100")));
		PLUCodedItem peanuts = new PLUCodedItem(peanutsPLU, new Mass(new BigDecimal("30")));
		PLUCodedItem cereal = new PLUCodedItem(cerealPLU, new Mass(new BigDecimal("40")));
		PLUCodedItem rice = new PLUCodedItem(ricePLU, new Mass(new BigDecimal("50")));
		PLUCodedItem chair = new PLUCodedItem(chairPLU, new Mass(new BigDecimal("10000")));
		
		PLUProduct.put("0001", banana);
		PLUProduct.put("0002", ham);
		PLUProduct.put("0003", cheese);
		PLUProduct.put("0004", cookie);
		PLUProduct.put("0005", chicken);
		PLUProduct.put("0006", steak);
		PLUProduct.put("0007", pizza);
		PLUProduct.put("0008", peanuts);
		PLUProduct.put("0009", cereal);
		PLUProduct.put("0010", rice);
		PLUProduct.put("0011", chair);
		
		
		PLUCodedProduct bananaProduct = new PLUCodedProduct(bananaPLU, "BANANA", 2);
		PLUPros.add(bananaProduct);
		PLUCodedProduct hamProduct = new PLUCodedProduct(hamPLU, "HAM", 20);
		PLUPros.add(hamProduct);
		PLUCodedProduct cheeseProduct = new PLUCodedProduct(cheesePLU, "CHEESE", 7);
		PLUPros.add(cheeseProduct);
		PLUCodedProduct cookieProduct = new PLUCodedProduct(cookiePLU, "COOKIE", 8);
		PLUPros.add(cookieProduct);
		PLUCodedProduct chickenProduct = new PLUCodedProduct(chickenPLU, "CHICKEN", 18);
		PLUPros.add(chickenProduct);
		PLUCodedProduct steakProduct = new PLUCodedProduct(steakPLU, "STEAK", 40);
		PLUPros.add(steakProduct);
		PLUCodedProduct pizzaProduct = new PLUCodedProduct(pizzaPLU, "PIZZA", 10);
		PLUPros.add(pizzaProduct);
		PLUCodedProduct peanutsProduct = new PLUCodedProduct(peanutsPLU, "PEANUTS", 3);
		PLUPros.add(peanutsProduct);
		PLUCodedProduct cerealProduct = new PLUCodedProduct(cerealPLU, "CEREAL", 5);
		PLUPros.add(cerealProduct);
		PLUCodedProduct riceProduct = new PLUCodedProduct(ricePLU, "RICE", 5);
		PLUPros.add(riceProduct);
		PLUCodedProduct chairProduct = new PLUCodedProduct(chairPLU, "CHAIR", 90);
		PLUPros.add(chairProduct);
		
		
		ProductDatabases.PLU_PRODUCT_DATABASE.put(bananaPLU, bananaProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(hamPLU, hamProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(cheesePLU, cheeseProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(cookiePLU, cookieProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(chickenPLU, chickenProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(steakPLU, steakProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(pizzaPLU, pizzaProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(peanutsPLU, peanutsProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(cerealPLU, cerealProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(ricePLU, riceProduct);
		ProductDatabases.PLU_PRODUCT_DATABASE.put(chairPLU, chairProduct);
		
	}
	
	public static void initBarcodedItems() {
		Numeral[] code = new Numeral[1];
		Numeral num = Numeral.one;
		code[0] = num;
		Barcode bCode = new Barcode(code);
		
		BarcodedItem item = new BarcodedItem(bCode, new Mass(new BigDecimal("1")));
		BarcodedProduct product = new BarcodedProduct(bCode, "apple", 1, 1);
		barcodedProduct.put("1", item);
		barPros.add(product);
		
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(bCode, product);
		
		
		
		code = new Numeral[1];
		num = Numeral.two;
		code[0] = num;
		bCode = new Barcode(code);
		
		item = new BarcodedItem(bCode, new Mass(new BigDecimal("1")));
		product = new BarcodedProduct(bCode, "pen", 1, 1);
		barcodedProduct.put("2", item);
		barPros.add(product);
		
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(bCode, product);
		
		code = new Numeral[1];
		num = Numeral.three;
		code[0] = num;
		bCode = new Barcode(code);
		
		item = new BarcodedItem(bCode, new Mass(new BigDecimal("10000")));
		product = new BarcodedProduct(bCode, "Dog Food", 15, 10000);
		barcodedProduct.put("3", item);
		barPros.add(product);
		
		ProductDatabases.BARCODED_PRODUCT_DATABASE.put(bCode, product);
	}
	public static Map<String, PLUCodedItem> getPLUProducts() {
		return PLUProduct;
	}
	public static Map<String, BarcodedItem> getBarcodeItems(){
		return barcodedProduct;
	}
	public static ArrayList<PLUCodedProduct> getPLUItems() {
		return PLUPros;
	}
	public static ArrayList<BarcodedProduct> getBarcodeProduct(){
		return barPros;
	}
}
