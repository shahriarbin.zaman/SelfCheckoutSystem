/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.Product;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Item.VisualCatalogueSearch;

public class VisualSearchScreen {
	
	private JFrame buttonFrame;
	private JPanel searchPanel;
	private JButton returnButton;
	private JButton[][] buttons;
	
	int PLUPros;
	int barcodePros;
	String text;
	int bind;
	int row;
	int col;
	PrimaryGUI primary;
	CentralCheckoutStationLogic centralStation;
	ShoppingCart cart;
	
	public VisualSearchScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation){
		this.centralStation = centralStation;
		this.primary = primary;
		this.cart = ShoppingCart.getShoppingCart(centralStation);
		
		buttonFrame = new JFrame();
		
		visualSearchSetUp();
		
		buttonFrame.setContentPane(searchPanel);
	}
	
	
	
	public void visualSearchSetUp() {
        barcodePros = ItemDatabase.getBarcodeProduct().size();
        PLUPros = ItemDatabase.getPLUItems().size();
        int rows = barcodePros + PLUPros;
        rows = (int) Math.ceil(rows / 2);
        
		searchPanel = new JPanel();
		searchPanel.setLayout(new GridLayout(rows, 2));
        
        buttons = new JButton[rows][2];
        
        bind = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < 2; j++) {
            	if(bind < barcodePros) {
            		buttons[i][j] = new JButton(bind + ". Photo of " + ItemDatabase.getBarcodeProduct().get(bind).getDescription());
            		buttons[i][j].addActionListener(new ButtonListener(i, j));
            	}
            	else {
            		buttons[i][j] = new JButton(bind + ". Photo of "+ ItemDatabase.getPLUItems().get(bind - 3).getDescription());
            		buttons[i][j].addActionListener(new ButtonListener(i, j));
            	}
                searchPanel.add(buttons[i][j]);
                bind++;
            }
        }
        returnButton = new JButton("Return");
        returnButton.addActionListener(e -> {
        	setItem();
        	primary.showAddItemsScreen();
        }); 
        
        searchPanel.add(returnButton);
        
	}
	
	private void setItem() {
        // Find the selected tile and perform action based on it
		VisualCatalogueSearch vis = new VisualCatalogueSearch(centralStation);
        for (int i = 0; i < buttons.length; i++) {
            for (int j = 0; j < buttons[i].length; j++) {
                if (!buttons[i][j].isEnabled()) {
                	if(ButtonListener.getIndex() < barcodePros) {
                		primary.getAddItemsScreen().findAndAddBarcode(ItemDatabase.getBarcodeProduct().get(ButtonListener.getIndex()).getBarcode());
                		primary.getAddItemsScreen().isBarcode(true);
                	}else {
                		primary.getAddItemsScreen().findAndAddPlu(ItemDatabase.getPLUItems().get(ButtonListener.getIndex()-(barcodePros)).getPLUCode());
                		primary.getAddItemsScreen().isBarcode(false);
                	}
                }
            }
        }
    }
	
	public JPanel getSearchPanel() {
		return searchPanel;
	}
	
	class ButtonListener implements ActionListener {
	    private int row;
	    private int col;
	    private static int index;
	    
	    public ButtonListener(int row, int col) {
	        this.row = row;
	        this.col = col;
	    }

	    @Override
	    public void actionPerformed(ActionEvent e) {
	        // Deselect all buttons
	        for (int i = 0; i < buttons.length; i++) {
	            for (int j = 0; j < buttons[i].length; j++) {
	                buttons[i][j].setEnabled(true);
	            }
	        }
	        // Select the clicked button
	        buttons[row][col].setEnabled(false);
	        if(row == 0) {
	        	index = row + col;
	        }else {
	        	index = row * 2 + col;
	        }
	    }

	    public static int getIndex() {
	    	return index;
	    }
	}
	
}
