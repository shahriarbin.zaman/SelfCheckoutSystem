/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import com.jjjwelectronics.keyboard.USKeyboardQWERTY;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.ISelfCheckoutStation;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.StationEnabler;


public class AttendantStationScreen {
	private JFrame attendantFrame;
	private JPanel attendantPanel;
	private JPanel stationSelectPanel;
	private JPanel stationOverviewPanel;
	private JPanel stationInteractionPanel;
	
	private JButton resolveInkIssueButton;
	private JButton resolvePaperIssueButton;
	private JButton resolveWeightDiscrepancyButton;
	private JButton resolveCashButton;
	private JButton resolveCoinsButton;
	private JButton sessionStatusButton;
	private JButton resolveIssueButton;
	private JButton removeInkButton;
	private JButton removePaperButton;
	private JButton enableStationButton;
	private JButton disableStationButton;
	private JButton removeCash;
	private JButton addCash;
	private JButton addCoin;
	private JButton removeCoin;
	private JButton addItemByTextButton;
	
	private StationEnabler stationEnabler;
	private CentralCheckoutStationLogic logic;
	
	private PrimaryGUI primary;
	
	private AttendantStation attendantStation;
	private int numberOfStations;
	private JComboBox<String> dropdown;
	private JComboBox<Integer> coinDropdown;
	private HashMap<String, JLabel> attendantLabels;
	private List<String> stationLabels;
	@SuppressWarnings("unused")
	private ISelfCheckoutStation selectedStation;
	private Timer assistanceCheckTimer;
	private int selectedIndex;
	private int selectedCoinIndex;
	private int selectedCoin;
	private Integer[] coinValuesArray;
	private Font smallerFont;
	
	private List<CentralCheckoutStationLogic> centralCheckoutStations;
	
	//Constructor
	public AttendantStationScreen(PrimaryGUI primary, CentralCheckoutStationLogic... centralCheckoutStations) {
		selectedIndex = 0;
		selectedCoinIndex = 0;
		
		this.centralCheckoutStations = new ArrayList<>(Arrays.asList(centralCheckoutStations));
		this.logic = centralCheckoutStations[selectedIndex];
		this.primary = primary;

		Set<Integer> coinValues = logic.cashAndCoinManager.getCoinInventory().keySet();
		
		coinValuesArray = coinValues.toArray(new Integer[0]);
		selectedCoin = coinValuesArray[selectedCoinIndex];
		
		stationEnabler = new StationEnabler();
		attendantStation = logic.attendantStation;
		numberOfStations = attendantStation.supervisedStationCount();
		
		for(CentralCheckoutStationLogic centralCheckoutStation : centralCheckoutStations) {
			centralCheckoutStation.predictIssuesController.setWarningLowInkPercentage(0.51);
			centralCheckoutStation.cashAndCoinManager.addCash(50);
		}
		
		smallerFont = new Font("Dialog", Font.BOLD, 10);
		
		createGUI();
		
		new BoxLayout(attendantPanel, BoxLayout.Y_AXIS);
		Box rows[] = new Box[3];
		rows[0] = Box.createHorizontalBox();
		rows[1] = Box.createHorizontalBox();
		rows[2] = Box.createHorizontalBox();
		
		attendantPanel.add(rows[0]);
		attendantPanel.add(rows[1]);  
		attendantPanel.add(rows[2]);

		stationOverviewPanel.setPreferredSize(new Dimension(750,500));
		stationSelectPanel.setPreferredSize(new Dimension(750,50));
		stationInteractionPanel.setPreferredSize(new Dimension(750,60));
		
		rows[0].add(stationOverviewPanel);	
		rows[1].add(stationSelectPanel);
		rows[2].add(stationInteractionPanel);
		
		addTimer();
	}
	
	//Method to create GUI components
	private void createGUI() {
		attendantFrame = new JFrame("Attendant Station Screen");
		attendantPanel = new JPanel();
		stationSelectPanel = new JPanel(new GridLayout(1, 2));
		stationOverviewPanel = new JPanel(new GridLayout(1, 3, 5, 0));
		stationInteractionPanel = new JPanel(new GridLayout(3, 1));
		
		addWidgetsOverview();
        addWidgetsSelected();
        addInteractionWidgets();
        
        attendantPanel.add(stationOverviewPanel, BorderLayout.NORTH);
        attendantPanel.add(stationSelectPanel, BorderLayout.CENTER);
        attendantPanel.add(stationInteractionPanel, BorderLayout.SOUTH);
        
        attendantFrame.setContentPane(attendantPanel);
        attendantFrame.setLocationRelativeTo(null);
        attendantFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//Method to add overview widgets
	private void addWidgetsOverview() {
	    attendantLabels = new HashMap<>();

	    for (int i = 0; i < numberOfStations; i++) {
	        JPanel stationPanel = new JPanel(new GridBagLayout());
	        GridBagConstraints gbc = new GridBagConstraints();
	        gbc.anchor = GridBagConstraints.WEST;
	        gbc.gridx = 0;
	        gbc.gridy = 0;
	        gbc.insets = new Insets(5, 5, 5, 5); // Adjust vertical spacing here
	        
	        JLabel stationLabel = new JLabel("Station" + (i + 1));
	        Font boldFont = new Font(stationLabel.getFont().getName(), Font.BOLD, stationLabel.getFont().getSize() + 5);
	        stationLabel.setFont(boldFont);
	        stationPanel.add(stationLabel, gbc);
	        
	        gbc.gridy++;
	        JLabel enabledStatus = new JLabel("Enabled: " + stationEnabler.getStationEnabledState());
	        stationPanel.add(enabledStatus, gbc);

	        gbc.gridy++;
	        JLabel inkAmount = new JLabel("Ink Level: " + logic.inkManager.getInkLevel());
	        stationPanel.add(inkAmount, gbc);

	        gbc.gridy++;
	        JLabel cashAmount = new JLabel("Cash Balance: " + logic.cashAndCoinManager.getCashBalance());
	        stationPanel.add(cashAmount, gbc);

	        gbc.gridy++;
	        JLabel paperAmount = new JLabel("Paper Level: " + logic.paperManager.getPaperInventory());
	        stationPanel.add(paperAmount, gbc);

	        for (Map.Entry<Integer, Integer> entry : logic.cashAndCoinManager.getCoinInventory().entrySet()) {
	            gbc.gridy++;
	            int coinValue = entry.getKey();
	            int quantity = entry.getValue();
	            JLabel coinLabel = new JLabel("Coin Balance " + coinValue + ": " + quantity);
	            stationPanel.add(coinLabel, gbc);
	            
	            attendantLabels.put("coinLabel" + coinValue + (i + 1), coinLabel);
	        }
	        
	        gbc.gridy++;
	        JLabel assitanceCheck = new JLabel("Assistance needed: " + logic.signalAttendantController.getNeedsAssistance());
	        stationPanel.add(assitanceCheck, gbc);

	        attendantLabels.put("stationLabel" + (i + 1), stationLabel);
	        attendantLabels.put("enabledStatus" + (i + 1), enabledStatus);
	        attendantLabels.put("inkAmount" + (i + 1), inkAmount);
	        attendantLabels.put("cashAmount" + (i + 1), cashAmount);
	        attendantLabels.put("paperAmount" + (i + 1), paperAmount);
	        attendantLabels.put("assitanceCheck" + (i + 1), assitanceCheck);

	        stationOverviewPanel.add(stationPanel);
	    }
	}

	
	//Method to add overview widgets
	private void addWidgetsSelected() {
		JLabel selectedStationLabel = new JLabel("Selected Station: ");
		
		selectedStation = attendantStation.supervisedStations().get(0);
	    stationLabels = new ArrayList<>();
	    
	    for (String key : attendantLabels.keySet()) {
	        if (key.startsWith("stationLabel")) {
	            stationLabels.add(attendantLabels.get(key).getText());
	        }
	    }
	    
	    dropdown = new JComboBox<>(stationLabels.toArray(new String[0]));
	    dropdown.addActionListener(e -> {
	    	selectedIndex = dropdown.getSelectedIndex();
	    	selectedStation = attendantStation.supervisedStations().get(selectedIndex);
	    	logic = centralCheckoutStations.get(selectedIndex);
	    });
	    
		stationSelectPanel.add(selectedStationLabel);
		stationSelectPanel.add(dropdown);
	}
	
	//Method to add interaction widgets
	private void addInteractionWidgets() {
        addResolveInkIssueWidget();
        addResolvePaperIssueWidget();
        addStationStatusWidget();
        addResolveCoinIssueWidget();
		addItemByTextWidget();
		addApproveWeightWidget();
		addResolveCashIssueWidget();
		addSessionStatusWidget();
		addRemoveInkManagerWidget();
		addRemovePaperWidget();
		addRemoveCashWidget();
		addCashCashWidget();
		removeCoinWidget();
		addCoinWidget();
		selectCoinWidget();
		addClearRequest();
	}
	
	//Method for remove ink widget
	private void addRemoveInkManagerWidget() {
		removeInkButton = new JButton("Remove Ink");
		removeInkButton.setFont(smallerFont);
		removeInkButton.addActionListener(e -> {
			try {
				if(!logic.predictIssuesController.getLowOnInk()) {
					logic.predictIssuesController.predictLowOnInk();
					if(logic.predictIssuesController.getLowOnInk()) {
						resolveInkIssueButton.setEnabled(true);
					}
				}
				logic.inkManager.useInk(10, null);
				logic.inkManager.setInkLevel(logic.inkManager.getInkLevel());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		
		stationInteractionPanel.add(removeInkButton);
	}
	
	//Method to resolve issues regarding ink
	private void addResolveInkIssueWidget() {
		resolveInkIssueButton = new JButton("Resolve Ink");
		resolveInkIssueButton.setFont(smallerFont);
		resolveInkIssueButton.setFont(smallerFont);
		resolveInkIssueButton.setEnabled(false);
		
		resolveInkIssueButton.addActionListener(e -> {
			try {
				if(logic.predictIssuesController.getLowOnInk()){
					logic.predictIssuesController.predictLowOnInk();
					logic.inkManager.refillInk();
					logic.inkManager.setInkLevel(logic.inkManager.getInkLevel());
					resolveInkIssueButton.setEnabled(false);
					logic.signalAttendantController.setRequestCleared(false);
					logic.predictIssuesController.setLowOnInk(false);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		
		stationInteractionPanel.add(resolveInkIssueButton);
	}
	
	//Method for remove paper widget
	private void addRemovePaperWidget() {
		removePaperButton = new JButton("Remove Paper");
		removePaperButton.setFont(smallerFont);
		removePaperButton.setFont(smallerFont);
		removePaperButton.addActionListener(e -> {
			try {
				if(!logic.predictIssuesController.getLowOnPaper()) {
					logic.predictIssuesController.predictLowOnPaper();
					if(logic.predictIssuesController.getLowOnPaper()) {
						resolvePaperIssueButton.setEnabled(true);
					}
				}
				logic.paperManager.removePaper(5, "Receipt");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		
		stationInteractionPanel.add(removePaperButton);
	}
	
	//Method to resolve issues regarding paper
	private void addResolvePaperIssueWidget() {
		resolvePaperIssueButton = new JButton("Resolve Paper");
		resolvePaperIssueButton.setFont(smallerFont);
		resolvePaperIssueButton.setEnabled(false);
		
		resolvePaperIssueButton.addActionListener(e -> {

			logic.signalAttendantController.setRequestCleared(false);
				try {
					if(logic.predictIssuesController.getLowOnPaper()) {
						logic.predictIssuesController.predictLowOnPaper();
						logic.paperManager.addPaper(50, "Receipt");
						resolvePaperIssueButton.setEnabled(true);
						logic.predictIssuesController.setLowOnPaper(false);
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
		});
		
		stationInteractionPanel.add(resolvePaperIssueButton);
	}
	
	//Method to add station status widget
	@SuppressWarnings("unchecked")
	private void addStationStatusWidget() {
		enableStationButton = new JButton("Enable Station");
		disableStationButton = new JButton("Disable Station");
		enableStationButton.setFont(smallerFont);
		disableStationButton.setFont(smallerFont);
		
		enableStationButton.addActionListener(e -> {
			if(logic.stationDisabler.getStationDisabled()) {
				stationEnabler.enableStation(logic);
				logic.stationDisabler.stationDisabled = false;
			}
		});
		
		disableStationButton.addActionListener(e -> {
			if(!logic.stationDisabler.getStationDisabled()) {
				logic.stationDisabler.disableStation(logic);
			}
		});
		
		stationInteractionPanel.add(enableStationButton);
		stationInteractionPanel.add(disableStationButton);
	}

	//Method to add various weight issue widgets
	private void addApproveWeightWidget() {
		resolveWeightDiscrepancyButton = new JButton("Resolve Weight");
		resolveWeightDiscrepancyButton.setFont(smallerFont);
		
		resolveWeightDiscrepancyButton.addActionListener(e -> {
			logic.signalAttendantController.setRequestCleared(false);
			try {
				if(logic.signalAttendantController.isHelpSessionActive()) {
					logic.signalAttendantController.clearCustomerRequest(selectedStation);
				}
				logic.signalAttendantController.sendAlertToAttendant(selectedStation);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logic.scaleController.checkClearRequest();
			primary.getAddItemsScreen().WeightIssueResolved();
			logic.signalAttendantController.setRequestCleared(false);
		});

		stationInteractionPanel.add(resolveWeightDiscrepancyButton);
	}
	
	//Approve alert general
	private void addClearRequest() {
		resolveIssueButton = new JButton("Resolve Issue");
		resolveIssueButton.setFont(smallerFont);
		resolveIssueButton.setEnabled(false);
		
		resolveIssueButton.addActionListener(e -> {
			try {
				logic.signalAttendantController.clearCustomerRequest(selectedStation);
				resolveIssueButton.setEnabled(false);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		stationInteractionPanel.add(resolveIssueButton);
	}
	
	//Method to add item by text widget
	private void addItemByTextWidget() {
		addItemByTextButton = new JButton("Add Item");
		addItemByTextButton.setFont(smallerFont);
		
		addItemByTextButton.addActionListener(e -> {
			USKeyboardQWERTY attendentKeyboard = logic.attendantStation.keyboard;
			@SuppressWarnings("unused")
			VirtualKeyboard keyboard = new VirtualKeyboard(primary, logic, attendentKeyboard);
			primary.showAttendantKeyboard();
		});
		
		stationInteractionPanel.add(addItemByTextButton);
	}
	
	//Method for the session status widget
	private void addSessionStatusWidget() {
		sessionStatusButton = new JButton("Resume Session");
		sessionStatusButton.setFont(smallerFont);
		
		sessionStatusButton.addActionListener(e1 -> {
			if(logic.sessionStarter.hasSessionBeenStarted()) {
				logic.sessionStarter.endSession();
			}else {
				logic.sessionStarter.startSession();
			}
		});

		stationInteractionPanel.add(sessionStatusButton);
	}
	
	//Method for the remove cash widget
	private void addRemoveCashWidget() {
		removeCash = new JButton("Remove cash");
		removeCash.setFont(smallerFont);
		removeCash.addActionListener(e ->{
			try {
				if(!logic.predictIssuesController.getLowOnBanknotes()) {
					logic.predictIssuesController.predictLowOnBanknotes();
					if(logic.predictIssuesController.getLowOnBanknotes()) {
						resolveCashButton.setEnabled(true);
					}
				}
				logic.cashAndCoinManager.removeCash(10);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		stationInteractionPanel.add(removeCash);
	}
	
	//Method for the add cash widget
	private void addCashCashWidget() {
		addCash = new JButton("Add cash");
		addCash.setFont(smallerFont);
		addCash.addActionListener(e ->{
			try {
				if(!logic.predictIssuesController.getBanknotesAlmostFull()) {
					logic.predictIssuesController.predictBanknotesFull();
					if(logic.predictIssuesController.getBanknotesAlmostFull()) {
						resolveCashButton.setEnabled(true);
					}
				}
				logic.cashAndCoinManager.addCash(10);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		stationInteractionPanel.add(addCash);
	}

	//Method to add a widget to resolve cash related issues
	private void addResolveCashIssueWidget() {
		resolveCashButton = new JButton("Resolve Cash");
		resolveCashButton.setFont(smallerFont);
		resolveCashButton.setEnabled(false);
		
		resolveCashButton.addActionListener(e -> {
				try {
					if(logic.predictIssuesController.getLowOnBanknotes()) {
						logic.predictIssuesController.predictLowOnBanknotes();
						logic.cashAndCoinManager.addCash(50);
						resolveCashButton.setEnabled(false);
						logic.signalAttendantController.setRequestCleared(false);
						logic.predictIssuesController.setLowOnBanknotes(false);
					}else if(logic.predictIssuesController.getBanknotesAlmostFull()) {
						logic.predictIssuesController.predictBanknotesFull();
						logic.cashAndCoinManager.removeCash(50);
						resolveCashButton.setEnabled(false);
						logic.signalAttendantController.setRequestCleared(false);
						logic.predictIssuesController.setBanknotesAlmostFull(false);
						
					}
				} catch (Exception e1) {
					e1.printStackTrace();
				}
		});
		stationInteractionPanel.add(resolveCashButton);
	}
	
	private void selectCoinWidget() {
	    coinDropdown = new JComboBox<>(coinValuesArray);
	    coinDropdown.addActionListener(e -> {
	    	selectedCoinIndex = coinDropdown.getSelectedIndex();
	    	selectedCoin = coinValuesArray[selectedCoinIndex];
	    });
	    
	    stationInteractionPanel.add(coinDropdown);
	}
	
	//Method for the remove coin widget
	private void removeCoinWidget() {
		removeCoin = new JButton("Remove Coin");
		removeCoin.setFont(smallerFont);
		removeCoin.addActionListener(e ->{
			try {
				if(!logic.predictIssuesController.getLowOnCoins()) {
					logic.predictIssuesController.predictLowOnCoins();
					if(logic.predictIssuesController.getLowOnCoins()) {
						resolveCoinsButton.setEnabled(true);
					}
				}
				logic.cashAndCoinManager.removeCoins(selectedCoin, 1);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		stationInteractionPanel.add(removeCoin);
	}
	
	//Method for the add coin widget
	private void addCoinWidget() {
		addCoin = new JButton("Add Coin");
		addCoin.setFont(smallerFont);
		addCoin.addActionListener(e ->{
			try {
				if(!logic.predictIssuesController.getCoinsAlmostFull()) {
					logic.predictIssuesController.predictCoinsFull();
					if(logic.predictIssuesController.getCoinsAlmostFull()) {
						resolveCoinsButton.setEnabled(true);
					}
				}
				logic.cashAndCoinManager.addCoins(selectedCoin, 1);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		stationInteractionPanel.add(addCoin);
	}
	
	//Method to add a widget to resolve coin related issues
	private void addResolveCoinIssueWidget() {
		resolveCoinsButton = new JButton("Coin Issue");
		resolveCoinsButton.setFont(smallerFont);
		resolveCoinsButton.setEnabled(false);
		
		resolveCoinsButton.addActionListener(e -> {
			try {
				if(logic.predictIssuesController.getLowOnCoins()) {
					logic.predictIssuesController.predictLowOnCoins();
					logic.cashAndCoinManager.addCoins(selectedCoin, 10);
					resolveCoinsButton.setEnabled(false);
					logic.signalAttendantController.setRequestCleared(false);
					logic.predictIssuesController.setLowOnCoins(false);
				}else if(logic.predictIssuesController.getCoinsAlmostFull()) {
					logic.predictIssuesController.predictCoinsFull();
					logic.cashAndCoinManager.removeCoins(selectedCoin, 10);
					resolveCoinsButton.setEnabled(false);
					logic.signalAttendantController.setRequestCleared(false);
					logic.predictIssuesController.setCoinsAlmostFull(false);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		
		stationInteractionPanel.add(resolveCoinsButton);
	}
	
	//Method to add timer that monitors updates from checkout station GUI
	private void addTimer() {
		assistanceCheckTimer = new Timer(500, e -> {
			attendantLabels.get("assitanceCheck" + (selectedIndex + 1)).setText("Assistance needed: " + logic.signalAttendantController.getNeedsAssistance());
            attendantLabels.get("enabledStatus" + (selectedIndex + 1)).setText("Enabled: " + !(logic.stationDisabler.getStationDisabled()));
            attendantLabels.get("inkAmount" + (selectedIndex + 1)).setText("Ink Level: " + logic.inkManager.getInkLevel());
            attendantLabels.get("paperAmount" + (selectedIndex + 1)).setText("Paper Level: " + logic.paperManager.getPaperInventory());
           
            for (Map.Entry<Integer, Integer> entry : logic.cashAndCoinManager.getCoinInventory().entrySet()) {
                int coinValue = entry.getKey();
                int quantity = entry.getValue();
                attendantLabels.get("coinLabel" + coinValue + (selectedIndex + 1)).setText("Coin Balance " + coinValue + ": " + quantity);
            }
            
            attendantLabels.get("cashAmount" + (selectedIndex + 1)).setText("Cash Balance: " + logic.cashAndCoinManager.getCashBalance());
            
    		if(logic.sessionStarter.hasSessionBeenStarted()) {
    			sessionStatusButton.setText("Halt Session");
    		}else {
    			sessionStatusButton.setText("Resume Session");
    		} 
        });
		
        assistanceCheckTimer.start();
	}

	//Getters
	public JButton getResolveInkButton() {
		return resolveInkIssueButton;
	}
	
	public JButton getResolvePaperButton() {
		return resolvePaperIssueButton;
	}
	
	public JButton getResolveWeightDiscrepancyButton() {
		return resolveWeightDiscrepancyButton;
	}
	
	public JButton getResolveCashButton() {
		return resolveCashButton;
	}
	
	public JButton getResolveCoinsButton() {
		return resolveCoinsButton;
	}
	
	public JPanel getPanel() {
		return attendantPanel;
	}
	
	public ISelfCheckoutStation getSelectedStation() {
		return selectedStation;
	}
	
	public JButton getSessionStatusButton() {
		return sessionStatusButton;
	}
	
	public JComboBox<String> getDropdown() {
		return dropdown;
	}
	public JComboBox<Integer> getCoinDropdown(){
		return coinDropdown;
	}
	
	public JButton getRemoveInkButton() {
		return removeInkButton;
	}
	
	public JButton getRemovePaperButton() {
		return removePaperButton;
	}
	
	public JButton getEnableStationButton() {
		return enableStationButton;
	}
	
	public JButton getDisableStationButton() {
		return disableStationButton;
	}
	
	public JButton getRemoveCash() {
		return removeCash;
	}
	
	public JButton getAddCash() {
		return addCash;
	}
	
	public JButton getAddCoin() {
		return addCoin;
	}
	
	public JButton getRemoveCoin() {
		return removeCoin;
	}
	
	public JButton getAddItemByTextButton() {
		return addItemByTextButton;
	}
	
	public JButton getResolveIssueButton() {
		return resolveIssueButton;
	}
	//Setters
	public void setButtonEnabled(JButton button) {
		button.setEnabled(true);
	}
	
	public void setResolveIssueButtonEnabled() {
		resolveIssueButton.setEnabled(true);
	}
	
	
	
}