
/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.jjjwelectronics.IllegalDigitException;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.card.Card;
import com.jjjwelectronics.card.MagneticStripeFailureException;
import com.jjjwelectronics.scanner.Barcode;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;

import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;

public class MembershipNumberScreen {
	private JFrame frame;

	private JPanel panel, subPanel, typeNumPanel;
	private JButton[] enterNumButtons;
	private JLabel enterMembershipMessage;
	private JTextField numberTextField;
	private JButton returnButton;
	
	private PrimaryGUI primary;
	private CentralCheckoutStationLogic logic;

	public MembershipNumberScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation) {
		this.primary = primary;
		logic = centralStation;
		
		frame = new JFrame();
		panel = new JPanel();
		
		returnButton = new JButton("Return");
		returnButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 primary.showAddItemsScreen();} // go back to add items screen
	    });
		
		setUpMembershipScreen();
		
		// set up frame
		frame.setContentPane(panel);
		frame.setBounds(100, 100, 700, 700);
	}
	
	private void setUpMembershipScreen() {
		panel.setLayout(new GridLayout(3,1));
	
		enterMembershipMessage = new JLabel("Enter Membership Number");
		enterMembershipMessage.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(enterMembershipMessage);
   	 	
		enterNumButtons = new JButton[3];
		for(int i = 0; i < enterNumButtons.length; i++) {
			enterNumButtons[i] = new JButton();
		}
		subPanel = new JPanel();
		subPanel.setLayout(new GridLayout(1,3));
		
		typeNumPanel = new JPanel();
		typeNumPanel.setLayout(new GridLayout(3,1));
		numberTextField = new JTextField(); 
		
		subPanel.add(enterNumButtons[0]);
		subPanel.add(typeNumPanel);
		subPanel.add(enterNumButtons[2]);
		
		typeNumPanel.add(new JLabel("Type Membership Number",SwingConstants.CENTER));
		typeNumPanel.add(numberTextField);
		typeNumPanel.add(enterNumButtons[1]);
		
		enterNumButtons[0].setText("Swipe");
		enterNumButtons[1].setText("Insert");
		enterNumButtons[2].setText("Scan");
		
		panel.add(subPanel);
		panel.add(returnButton);	
		
   	 	setUpEnterNumButtons();
   	 	
   	 	logic.addMembershipCard("12345678");
   	 	logic.addMembershipCard("1444");
	}
	
	// Getter method for testing
    public JButton[] getEnterNumButtons() {
        return enterNumButtons;
    }
    
    public JTextField getNumberTextField() {
    	return numberTextField;
    }
    
    public JLabel getEnterNumberMessage() {
    	return enterMembershipMessage;
    }
	
	private void setUpEnterNumButtons() { 
		// clicking these buttons represents/simulates swiping/inserting/scanning membership number
		enterNumButtons[0].addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent e) { // swipe
	        	String membershipNumber = numberTextField.getText();
	        	Card membershipCard = new Card("Membership",membershipNumber,"Cardholder","N/A","N/A",false,false); // is a membership card
	     		logic.setMethodsOfCardPayment(MethodsOfCardPayment.SWIPE);
	     		logic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
	        	
	        	try {
					logic.membershipCardReaderController.performAction(TypeOfCard.MEMBERSHIP_CARD, membershipCard);
					enterMembershipMessage.setText("<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>");
					
	        	} catch (MagneticStripeFailureException e1) {
	        		enterMembershipMessage.setText("<html><center>Swipe failed. Please try again.<br>Enter Membership Number</center></html>");
	        		 
				} catch (IOException e1) {
	        		enterMembershipMessage.setText("<html><center>Error: "+e1.getMessage()+". Please try again.<br>Enter Membership Number</center></html>");
				}
	        	 
	         }
	    });
		enterNumButtons[1].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // type
	        	String membershipNumber = numberTextField.getText();
	        	Card membershipCard = new Card("Membership",membershipNumber,"Cardholder","N/A","N/A",false,false); // is a membership card
	     		logic.setMethodsOfCardPayment(MethodsOfCardPayment.SWIPE);
	     		logic.setTypeOfCard(TypeOfCard.MEMBERSHIP_CARD);
	     		
	     		
	        	
	     		try {
					logic.membershipCardReaderController.performAction(TypeOfCard.MEMBERSHIP_CARD, membershipCard);
					enterMembershipMessage.setText("<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>");
					
	        	} catch (MagneticStripeFailureException e1) {
	        		enterMembershipMessage.setText("<html><center>An error occured in processing your membership number. "
	        				+ "Please try again.<br>Enter Membership Number</center></html>");
	        		 
				} catch (IOException e1) {
	        		enterMembershipMessage.setText("<html><center>Error: "+e1.getMessage()+". Please try again.<br>Enter Membership Number</center></html>");
				}
	        	
	        	numberTextField.setText(""); // clear textfield
	         }
	    });
		enterNumButtons[2].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // scan
	        	String membershipNumber = numberTextField.getText();
	        	Card membershipCard = new Card("Membership",membershipNumber,"Cardholder","N/A","N/A",false,false);
	        	char[] charArray = membershipCard.number.toCharArray();
				Numeral[] numerals = new Numeral[charArray.length];
				for(int i = 0; i < charArray.length; i++) {
					try {
						numerals[i] = Numeral.valueOf((byte)Character.digit(charArray[i], 10));
					}
					catch(IllegalDigitException e1) {
						throw new InvalidArgumentSimulationException("The code must be a string of numerals.");
					}
				}
				
				if(logic.getMembershipCards().containsKey(membershipNumber)) {
					logic.membershipCardReaderController.aBarcodeHasBeenScanned(logic.hardware.getMainScanner(),new Barcode(numerals));
					enterMembershipMessage.setText("<html><center>Membership Number successfully entered.<br>Enter Membership Number</center></html>");
	     		}
				else {
					enterMembershipMessage.setText("<html><center>Error: Members not found.<br>Enter Membership Number</center></html>");
				}
				
	        	
	        	numberTextField.setText(""); // clear textfield
	         }
	    });
	}
	
	public JPanel getMembershipPanel() {
		return panel;
	}
	
	// for end of session
	public void clearMembership() {
		enterMembershipMessage.setText("Enter Membership Number");
	}

}