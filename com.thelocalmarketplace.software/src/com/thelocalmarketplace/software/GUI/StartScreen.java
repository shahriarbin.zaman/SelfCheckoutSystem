/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;

public class StartScreen{

	private JFrame startFrame;
	private JPanel startPanel;
	private JButton startSession;
	private JButton selectLanguage;
	
	private CentralCheckoutStationLogic centralCheckoutStation;
	private PrimaryGUI primary;
	
	//constructor
	public StartScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation){
		this.centralCheckoutStation = centralStation;
		this.primary = primary;
		
		startFrame = new JFrame();
		startPanel = new JPanel();
		startPanel.setLayout(new GridLayout(2,2));
		
		addWidgets();
		
		startFrame.setContentPane(startPanel);
		startFrame.setBounds(100, 100, 600, 600);
		
		startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startFrame.pack();
		startFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
	
	//method for adding widgets
	private void addWidgets() {
		startSession = new JButton("Touch to start");
		startSession.addActionListener(e -> {
			centralCheckoutStation.sessionStarter.startSession();

			primary.showAddItemsScreen();
		});
		
		selectLanguage = new JButton("Select Language");
		selectLanguage.addActionListener(e -> {

			primary.showLanguageSelectionScreen();
		});
		
		startPanel.add(startSession);
		startPanel.add(selectLanguage);
	}
	
	//getters
	public JPanel getPanel() {
		return startPanel;
	}
	
	public JButton getStartSessionButton() {
		return startSession;
	}
	
	public JButton getSelectLanguageButton() {
		return selectLanguage;
	}
}
