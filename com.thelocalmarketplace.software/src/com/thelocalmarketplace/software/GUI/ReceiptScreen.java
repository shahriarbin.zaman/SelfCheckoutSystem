/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.Color;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane; 

public class ReceiptScreen {
	
	private JFrame frame;
	private JPanel panel;
	private JPanel subPanel;
	private JTextPane receipt;
	private JButton exit;
	
	public ReceiptScreen(String text) {
		frame = new JFrame(); 
		frame.setUndecorated(true);

		panel = new JPanel();
		panel.setBackground(Color.white);
		panel.setBorder(BorderFactory.createLineBorder(Color.black));
		
		receipt =  new JTextPane();
		receipt.setText(text);
		receipt.setEditable(false);
		
		exit = new JButton("Take Receipt");
		exit.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 frame.dispose();
	         }
	    });
		
		new BoxLayout(panel,BoxLayout.Y_AXIS);
		Box rows[] = new Box[2];
		rows[0] = Box.createHorizontalBox();
		rows[1] = Box.createHorizontalBox();

		panel.add(rows[0]);
		panel.add(rows[1]);  
		
		subPanel = new JPanel();
		subPanel.setBackground(Color.white);
		subPanel.setPreferredSize(new Dimension(497,400));
		
		rows[0].add(subPanel);	
		rows[1].add(exit);
		
		subPanel.add(receipt);
		receipt.setAlignmentX(JTextPane.CENTER_ALIGNMENT);

		frame.setBounds(100, 200, 500, 500);
		frame.setContentPane(panel);

		frame.setVisible(true);
	}

}
