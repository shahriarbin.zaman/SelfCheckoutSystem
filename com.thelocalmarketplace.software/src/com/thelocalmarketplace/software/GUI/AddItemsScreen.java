/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.Dimension;
import java.awt.GridLayout;

import java.util.ArrayList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import com.jjjwelectronics.IllegalDigitException;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.PLUController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;

public class AddItemsScreen{
	private JFrame itemFrame;
	private JFrame buttonFrame;
	private JPanel searchPanel;
	private JPanel buttonsPanel;
	private JPanel keyBoardPanel;
	private JPanel addItemsPanel;
	private JPanel itemScreenPanel;
	private JScrollPane itemScreenPane; 
	
	private JList<String> itemList;
	private DefaultListModel<String> listModel;
	private ItemRemover remove;
	private ArrayList<ArrayList<String>> itemType = new ArrayList<>();
	private int numOfBags;
	private OwnBagAdder oba;
	
	private JButton checkoutButton;

	private JButton addItemByVisualCatalogue;
	private JButton bags;
	private JButton helpB;
	private JButton handHeldScanner;
	private JButton addItemByTextButton;
	private JButton payButton;
	private JButton addPLUButton;
	private JButton addBarcodeButton;
	private JButton enterMembershipButton;
	private JTextField itemBox;
	
	private VirtualKeyboard keyboard;
	
	private PriceLookUpCode heldPLUItem;
	private Barcode heldBarcodeItem;
	private CentralCheckoutStationLogic centralCheckoutStation;
	private PrimaryGUI primary;
	private ShoppingCart cart;
	private PriceLookUpCode heldItem;
	private Boolean isItBarcode;
	private DefaultListModel<String> errorModel = new DefaultListModel<>();
	private JList<String> errorList = new JList<String>(errorModel);
	private JScrollPane errorPane = new JScrollPane(errorList);

	public void addLogMessage(String error) {
		this.errorModel.addElement(error);
	}

	public void clearErrorMessages() {
		this.errorModel.clear();
	}

	public void WeightIssueResolved() {
		if(heldPLUItem != null) {
			PLUCodedItem item = ItemDatabase.getPLUProducts().get(heldPLUItem.toString());
			PLUCodedProduct product = ProductDatabases.PLU_PRODUCT_DATABASE.get(item.getPLUCode());
			centralCheckoutStation.scaleController.getScaleScanningArea().removeAnItem(item);
			cart.addPLUItemToCart(item);
			cart.addPrice(product.getPrice());
			
			this.addLogMessage("Adding item after weight issues resolved");
			System.out.println("Item added successfully. Total items: " + cart.getTotalNumberOfItems());
			ArrayList<PLUCodedProduct> pluProducts = cart.getShoppingCartPLUProducts();
			String pluProduct = pluProducts.get(pluProducts.size()-1).getDescription();
			String price = Long.toString(pluProducts.get(pluProducts.size()-1).getPrice());
				
			listModel.addElement(pluProduct + "              " + "$ " + price);
			ArrayList<String> temp = new ArrayList<>();
			temp.add(heldPLUItem.toString());
			temp.add("PLU");
			itemType.add(temp);
			keyboard.clearText();
			heldPLUItem = null;
		}
		if(heldBarcodeItem != null) {
			BarcodedItem item = ItemDatabase.getBarcodeItems().get(heldBarcodeItem.toString());
			BarcodedProduct product = ProductDatabases.BARCODED_PRODUCT_DATABASE.get(item.getBarcode());
			centralCheckoutStation.scaleController.getScaleScanningArea().removeAnItem(item);
			cart.addBarcodedItemToCart(item);
			cart.addPrice(product.getPrice());
			
			this.addLogMessage("Adding item after weight issues resolved");
			System.out.println("Item added successfully. Total items: " + cart.getTotalNumberOfItems());
			ArrayList<BarcodedProduct> barcodedProducts = cart.getShoppingCartProducts();
			String barcodedProduct = barcodedProducts.get(barcodedProducts.size()-1).getDescription();
			String price = Long.toString(barcodedProducts.get(barcodedProducts.size()-1).getPrice());
			
			listModel.addElement(barcodedProduct + "              " + "$ " + price);
			ArrayList<String> temp = new ArrayList<>();
			temp.add(heldBarcodeItem.toString());
			temp.add("BCode");
			itemType.add(temp);
			keyboard.clearText();
			heldBarcodeItem = null;
		}
	}
	
	public AddItemsScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation){
		this.centralCheckoutStation = centralStation;
		this.primary = primary;
		this.cart = ShoppingCart.getShoppingCart(centralStation);
		
		itemFrame = new JFrame();
		
		addItemsPanel = new JPanel();
		
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new GridLayout(2,5));

		keyBoardPanel = new JPanel();
		keyBoardPanel.setLayout(new GridLayout(1,1));
		
		listModel = new DefaultListModel<>();
		itemList = new JList<>(listModel);
        itemScreenPane = new JScrollPane(itemList);
        
		addWidgets();
		
		new BoxLayout(addItemsPanel,BoxLayout.Y_AXIS);
		Box rows[] = new Box[4];
		rows[0] = Box.createHorizontalBox();
		rows[1] = Box.createHorizontalBox();
		rows[2] = Box.createHorizontalBox();
		rows[3] = Box.createHorizontalBox();
		
		addItemsPanel.add(rows[0]);
		addItemsPanel.add(rows[1]);
		addItemsPanel.add(rows[2]);
		addItemsPanel.add(rows[3]);

		keyBoardPanel.setPreferredSize(new Dimension(700,100));
		buttonsPanel.setPreferredSize(new Dimension(600,50));
		itemScreenPane.setPreferredSize(new Dimension(700,200));
		errorPane.setPreferredSize(new Dimension(700, 200));
		
		rows[0].add(itemScreenPane);
		rows[1].add(keyBoardPanel);	
		rows[2].add(buttonsPanel);
		rows[3].add(errorPane);
		
		itemFrame.setContentPane(addItemsPanel);
		itemFrame.setBounds(200, 200, 800, 1100);
		
	}
	
	public void findAndAddPlu(PriceLookUpCode code) {
		PLUController PLUcon = new PLUController(centralCheckoutStation);
		try {
			if(!PLUcon.addItemByPLU(code)) {
				if(PLUcon.logic.scaleController.hasExceededLimit()) {
					// Bulky item
					if(heldPLUItem != null) {
						System.out.println("heldItem is not null");
					}
					heldPLUItem = code;
					this.addLogMessage("Bulky item, please wait for assistance");
				}
				return;
			}
		} catch (Exception ex) {
			this.addLogMessage(ex.getMessage());
			return;
		}
		ArrayList<PLUCodedProduct> pluProducts = cart.getShoppingCartPLUProducts();
		String pluProduct = pluProducts.get(pluProducts.size()-1).getDescription();
		String price = Long.toString(pluProducts.get(pluProducts.size()-1).getPrice());
		
		listModel.addElement(pluProduct + "              " + "$ " + price);
		ArrayList<String> temp = new ArrayList<>();
		temp.add(code.toString());
		temp.add("PLU");
		itemType.add(temp);
	}
	
	public void findAndAddPlu(String name) {
		PriceLookUpCode itemCode;
		try {
			itemCode = new PriceLookUpCode(name);
		} catch(NullPointerSimulationException ex) {
			this.addLogMessage("Unknown item");
			return;
		}
		findAndAddPlu(itemCode);
		keyboard.clearText();
	}
	
	public void addWidgets() {
		checkoutButton = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			checkoutButton.setText("Checkout");
		}else {
			checkoutButton.setText("Vérifier");
		}
		checkoutButton.addActionListener(e -> {
			primary.getPaymentScreen().updateTotal();
			if(cart.getTotalPrice()!=0) {
				primary.showPaymentOptionsScreen();
			}
		});
		
		addPLUButton = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			addPLUButton.setText("PLU search");
		}else {
			addPLUButton.setText("Recherche de PLU"); // used to be Press
		}
		addPLUButton.addActionListener(e -> {
			this.findAndAddPlu(keyboard.getText());
		});
		
		addBarcodeButton = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			addBarcodeButton.setText("Barcode Scan");
		}else {
			addBarcodeButton.setText("Press");
		}
		addBarcodeButton.addActionListener(e -> {
			if(keyboard.getText() == null || keyboard.getText().isEmpty()) {
				this.addLogMessage("Barcode was empty");
				return;
			}
			char[] charArray = keyboard.getText().toCharArray();
			Numeral[] numerals = new Numeral[charArray.length];

			for(int i = 0; i < charArray.length; i++) {
				try {
					numerals[i] = Numeral.valueOf((byte)Character.digit(charArray[i], 10));
				}
				catch(IllegalDigitException e1) {
					this.addLogMessage("The code must be a string of numerals.");
					return;
				}
			}
			
			findAndAddBarcode(numerals);
		});
		

        JButton removeButton = new JButton("Remove Item");
        removeButton.addActionListener(e -> {
                int selectedIndex = itemList.getSelectedIndex();
                remove = new ItemRemover(centralCheckoutStation);
                if (selectedIndex != -1) {
                	if(itemType.get(selectedIndex).get(1) == "PLU") {
                    	listModel.remove(selectedIndex);
                    	PLUCodedItem item = ItemDatabase.getPLUProducts().get(itemType.get(selectedIndex).get(0));
                    	remove.removePLUItem(item);
                    	try { 
                    		centralCheckoutStation.scaleController.getScaleScanningArea().removeAnItem(item);
                    	} catch(InvalidArgumentSimulationException ex) {
                    		// Not on scale
                    	}
                	}else {
                		listModel.remove(selectedIndex);
                		BarcodedItem item = ItemDatabase.getBarcodeItems().get(itemType.get(selectedIndex).get(0));
                		remove.removeBarcodedItem(item, centralCheckoutStation.hardware.getBaggingArea());
                		try {
                			centralCheckoutStation.scaleController.getScaleScanningArea().removeAnItem(item);
                		} catch(InvalidArgumentSimulationException ex) {
                			// Not on scale
                		}
                	}
                	itemType.remove(selectedIndex);
                }
        });

		enterMembershipButton = new JButton("<html><center>Enter Membership<br>Number</center></html>");
		enterMembershipButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	primary.showMembershipNumberScreen();
	         }
	    });
		
		addItemByTextButton = new JButton();
		setAddItemByTextButtonDisabled();
		if(DataManager.getInstance().getLanguage() == "English") {
			addItemByTextButton.setText("Confirm Add");
		}else {
			addItemByTextButton.setText("Confirmer l'ajout"); // used to be Press
		}
		addItemByTextButton.addActionListener(e -> {
			PLUCodedProduct selectedProduct = primary.getAttendantKeyboardScreen().getSelectedProduct();
			
			ArrayList<PLUCodedProduct> pluProducts = cart.getShoppingCartPLUProducts();
			String pluProduct = selectedProduct.getDescription();
			String price = Long.toString(selectedProduct.getPrice());
			cart.addPrice(selectedProduct.getPrice());
			
			listModel.addElement(pluProduct + "              " + "$ " + price);
			ArrayList<String> temp = new ArrayList<>();
			temp.add(selectedProduct.getPLUCode().toString());
			temp.add("PLU");
			itemType.add(temp);
			setAddItemByTextButtonDisabled();
		});
		
		handHeldScanner = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			handHeldScanner.setText("Handheld Scan");
		}else {
			handHeldScanner.setText("Numérisation portable"); // used to be Press
		}
		handHeldScanner.addActionListener(e -> {
			if(keyboard.getText() == null || keyboard.getText().isEmpty()) {
				this.addLogMessage("Barcode was empty");
				return;
			}
			char[] charArray = keyboard.getText().toCharArray();
			Numeral[] numerals = new Numeral[charArray.length];

			for(int i = 0; i < charArray.length; i++) {
				try {
					numerals[i] = Numeral.valueOf((byte)Character.digit(charArray[i], 10));
				}
				catch(IllegalDigitException e1) {
					this.addLogMessage("The code must be a string of numerals.");
					return;
				}
			}
			
			findAndAddBarcode(numerals);
		});
		
		helpB = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			helpB.setText("Alert Attendent");
		}else {
			helpB.setText("Préposé aux alertes");
		}
		helpB.addActionListener(e -> { 
	        	 centralCheckoutStation.signalAttendantController.alertAttendant();
	        	 centralCheckoutStation.signalAttendantController.activateHelpSession();
	        	 primary.getAttendantStationScreen().setResolveIssueButtonEnabled();
	         });
		
		bags = new JButton();
		if(DataManager.getInstance().getLanguage() == "English") {
			bags.setText("Add Own Bags");
		}else {
			bags.setText("Ajouter vos propres sacs");
		}
		bags.addActionListener(e -> {
			try {
				numOfBags = Integer.parseInt(keyboard.getText());
			} catch(NumberFormatException ex) {
				this.addLogMessage("Invalid number of bags added.");
				return;
			}
			keyboard.clearText();
			OwnBagAdder oba = new OwnBagAdder(centralCheckoutStation);
			oba.updateWeightWithBags(numOfBags);
		});
		
		JButton searchButton = new JButton("Search Visual Catalog");
		searchButton.addActionListener(e -> {
			primary.showSearchPanel();
		});		
		
		itemBox = new JTextField();
		
		keyboard = new VirtualKeyboard(primary, centralCheckoutStation);
		keyBoardPanel.add(keyboard.getPanel());
		buttonsPanel.add(enterMembershipButton);
		buttonsPanel.add(addBarcodeButton);
		buttonsPanel.add(addPLUButton);
		buttonsPanel.add(checkoutButton);
		buttonsPanel.add(removeButton);
		buttonsPanel.add(addItemByTextButton);
		buttonsPanel.add(handHeldScanner);
		buttonsPanel.add(helpB);
		buttonsPanel.add(bags);
		buttonsPanel.add(searchButton);
	}

	public void findAndAddBarcode(Barcode code) {
		ScannerController scan = new ScannerController(centralCheckoutStation);
		try {
			scan.aBarcodeHasBeenScanned(centralCheckoutStation.hardware.getMainScanner(), code);
		} catch (NullPointerException ex) {
			this.addLogMessage("Invalid item barcode");
			return;
		} catch (Exception ex) {
			if(centralCheckoutStation.scaleController.hasExceededLimit()) {
				// Bulky item
				if(heldBarcodeItem != null) {
					System.out.println("heldBarcodeItem is not null");
				}
				heldBarcodeItem = code;
			}
			this.addLogMessage(ex.getMessage());
			return;
		}
		
		ArrayList<BarcodedProduct> barcodedProducts = cart.getShoppingCartProducts();
		String barcodedProduct = barcodedProducts.get(barcodedProducts.size()-1).getDescription();
		String price = Long.toString(barcodedProducts.get(barcodedProducts.size()-1).getPrice());
		
		listModel.addElement(barcodedProduct + "              " + "$ " + price);
		ArrayList<String> temp = new ArrayList<>();
		temp.add(code.toString());
		temp.add("BCode");
		itemType.add(temp);
		keyboard.clearText();;
	}
	
	public void findAndAddBarcode(Numeral[] numerals) {
		Barcode code;
		try {
			code = new Barcode(numerals);
		} catch(Exception ex) {
			this.addLogMessage(ex.getMessage());
			return;
		}
		this.findAndAddBarcode(code);
	}
	
	public void setAddItemByTextButtonDisabled() {
		addItemByTextButton.setEnabled(false);
	}
	
	public void setAddItemByTextButtonEnabled() {
		addItemByTextButton.setEnabled(true);
	}
	
	public JPanel getPanel() {
		return addItemsPanel;
	}
	
	public void setnumOfBags(int num) {
		numOfBags = num;
	}
	
	public void isBarcode(Boolean code) {
		this.isItBarcode = code;
	}
	
	public void clearItems() {
        this.itemList.removeAll();
        this.listModel.clear();
    }

	public Object getKeyboard() {
		// TODO Auto-generated method stub
		return null;
	}

	public DefaultListModel<String> getListModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public JButton getAddPLUButton() {
		// TODO Auto-generated method stub
		return null;
	}

	public Object getNumOfBags() {
		// TODO Auto-generated method stub
		return null;
	}

	public JButton getHelpB() {
		// TODO Auto-generated method stub
		return null;
	}

	public DefaultListModel<String> getErrorModel() {
		// TODO Auto-generated method stub
		return null;
	}

	public JButton getAddBarcodeButton() {
		// TODO Auto-generated method stub
		return null;
	}
}
