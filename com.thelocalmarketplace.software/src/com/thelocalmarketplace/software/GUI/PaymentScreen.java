/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import com.jjjwelectronics.card.BlockedCardException;
import com.jjjwelectronics.card.Card;
import com.jjjwelectronics.card.Card.CardInsertData;
import com.jjjwelectronics.card.Card.CardSwipeData;
import com.jjjwelectronics.card.Card.CardTapData;
import com.jjjwelectronics.card.ChipFailureException;
import com.jjjwelectronics.card.InvalidPINException;
import com.jjjwelectronics.card.MagneticStripeFailureException;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.tdc.banknote.Banknote;
import com.tdc.coin.Coin;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.Item.ItemRemover;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Controller.ChangeController;
import com.thelocalmarketplace.software.Payment.BanknotePayment;
import com.thelocalmarketplace.software.Payment.CoinPayment;
import com.thelocalmarketplace.software.Payment.CreditCardPayment;
import com.thelocalmarketplace.software.Payment.DebitCardPayment;

import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;

public class PaymentScreen {
	private JFrame frame;
	
	private JPanel paymentOptionsPanel, optionButtonsPanel;
	
	private JPanel banknotePanel, banknoteSubPanel; 
	
	private JPanel coinPanel, coinSubPanel;
	
	private JPanel creditPanel, creditSubPanel, creditInsertPanel;
	private JTextField creditPinTextField;
	
	private JPanel debitPanel, debitSubPanel, debitInsertPanel;
	private JTextField debitPinTextField;
	
	private JPanel titlePanel;
	private JPanel returnButtonPanel;
	
	private JPanel failedPaymentPanel, thankYouPanel;

	private JButton banknoteB, coinB, creditB, debitB, membershipB, helpB;
	private JButton[] debitPayButtons, creditPayButtons, coinPayButtons, banknotePayButtons;
	
	private JButton[] returnButtons;
	
	private JButton failedPaymentButton;
	private JButton returnToItemScreen;
	
	private JLabel title, total, banknoteMessage, coinMessage, creditMessage, debitMessage, paymentFailedMessage, thankYouMessage;
	
	private boolean hasPartiallyPaid;
	
	private BigDecimal[] banknoteDenominations;
	private List<BigDecimal> coinDenominations;
	private CentralCheckoutStationLogic logic;
	private PrimaryGUI primary;
	public ShoppingCart shoppingCart;
	private CoinPayment coinPayment;
	private BanknotePayment banknotePayment;
	
	public PaymentScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation) {
		this.logic = centralStation;
		this.primary = primary;
		shoppingCart = ShoppingCart.getShoppingCart(centralStation);
		coinPayment = new CoinPayment(logic);
		banknotePayment = new BanknotePayment(logic);
				
		frame = new JFrame();
		// init and set up panels
		paymentOptionsPanel = new JPanel();
		
		titlePanel = new JPanel();
		titlePanel.setLayout(new GridLayout(2,1));
		
		title = new JLabel("Select Method of Payment");
		title.setHorizontalAlignment(SwingConstants.CENTER);
		titlePanel.add(title);
		
		total = new JLabel("Total: $"+shoppingCart.getTotalPrice()); 
		
		total.setHorizontalAlignment(SwingConstants.CENTER);
		titlePanel.add(total);
		
		optionButtonsPanel = new JPanel();
		optionButtonsPanel.setLayout(new GridLayout(2,2));
		
		returnButtonPanel = new JPanel();
		returnButtonPanel.setLayout(new GridLayout(1,3));
		helpB = new JButton("Request Help");
		returnButtonPanel.add(helpB);
		membershipB = new JButton("Enter Membership Number");
		returnButtonPanel.add(membershipB);
		
		// Setup. buttons should be set up before screens(panels)
		hasPartiallyPaid = false;
		setUpReturnButtons();
		setUpButtons();
		setUpBanknoteScreen();
		setUpCoinScreen();
		setUpCreditScreen();
		setUpDebitScreen();
		refreshRemainingAmountDue();
		setUpFailedPaymentScreen();
		setUpThankYouScreen();
		
		// set up main (payment option) screen layout
		new BoxLayout(paymentOptionsPanel,BoxLayout.Y_AXIS);
		Box rows[] = new Box[3];
		rows[0] = Box.createHorizontalBox();
		rows[1] = Box.createHorizontalBox();
		rows[2] = Box.createHorizontalBox();

		paymentOptionsPanel.add(rows[0]);
		paymentOptionsPanel.add(rows[1]);  
		paymentOptionsPanel.add(rows[2]);  

		titlePanel.setPreferredSize(new Dimension(700,85));
		optionButtonsPanel.setPreferredSize(new Dimension(700,450));
		returnButtonPanel.setPreferredSize(new Dimension(700,150));
		
		rows[0].add(titlePanel);	
		rows[1].add(optionButtonsPanel);
		rows[2].add(returnButtonPanel);

		// set up frame
		frame.setContentPane(paymentOptionsPanel);
		frame.setBounds(100, 100, 700, 700);
	}
	
	private void setUpReturnButtons() {
		returnButtons = new JButton[4];
		for(int i = 0; i < returnButtons.length; i++) {
			returnButtons[i] = new JButton("Back to Payment Options");  
			// for banknote and coin payment, ensure user cannot leave screen if partial payment has been made
			if(i == 0|| i == 1) {
				returnButtons[i].addActionListener(new ActionListener() {
			         public void actionPerformed(ActionEvent e) { 
			        	 if(!hasPartiallyPaid) { // block user from leaving screen
			        		 primary.showPaymentOptionsScreen(); 
			        	 } 
			         }
			    });
				
			} else {
				returnButtons[i].addActionListener(new ActionListener() {
			         public void actionPerformed(ActionEvent e) { 
			        	 primary.showPaymentOptionsScreen();}
			    });
			}
		}
	}
	
	private void setUpBanknoteScreen() {
		banknotePanel = new JPanel();
		banknotePanel.setLayout(new GridLayout(3,1));
		banknoteMessage = new JLabel();
   	 	banknoteMessage.setHorizontalAlignment(SwingConstants.CENTER);
   	 	banknotePanel.add(banknoteMessage);

   	 	banknoteDenominations = logic.hardware.getBanknoteDenominations();
   	 	
   	 	banknoteSubPanel = new JPanel();
   	 	banknoteSubPanel.setLayout(new GridLayout(1,banknoteDenominations.length));
   	 	
   	 	banknotePayButtons = new JButton[banknoteDenominations.length];
		for(int i = banknotePayButtons.length-1; i >=0; i--) {
			banknotePayButtons[i] = new JButton();
			banknotePayButtons[i].setText(""+banknoteDenominations[i]);
			banknoteSubPanel.add(banknotePayButtons[i]);
		}

		banknotePanel.add(banknoteSubPanel);
   	 	banknotePanel.add(returnButtons[0]);	
   	 	setUpBanknotePayButtons();
	}
	
	private void setUpCoinScreen() {
		coinPanel = new JPanel();
		coinPanel.setLayout(new GridLayout(3,1));
		coinMessage = new JLabel(); 
		coinMessage.setHorizontalAlignment(SwingConstants.CENTER);
   	 	coinPanel.add(coinMessage);
   	 	
   	 	coinDenominations = logic.hardware.getCoinDenominations();
   	 	
   	 	coinSubPanel = new JPanel();
   	 	coinSubPanel.setLayout(new GridLayout(1,coinDenominations.size()));
   	 	
   	 	coinPayButtons = new JButton[coinDenominations.size()];
		for(int i = coinPayButtons.length-1; i >=0; i--) {
			coinPayButtons[i] = new JButton();
			coinPayButtons[i].setText(""+coinDenominations.get(i));
			coinSubPanel.add(coinPayButtons[i]);
		}
		
		coinPanel.add(coinSubPanel);
   	 	coinPanel.add(returnButtons[1]);	
   	 	setUpCoinPayButtons();
   	 	
	}
	
	private void setUpCreditScreen() {
		creditPanel = new JPanel();
		creditPanel.setLayout(new GridLayout(3,1));
	
		creditMessage = new JLabel();
		creditMessage.setHorizontalAlignment(SwingConstants.CENTER);
		creditPanel.add(creditMessage);
   	 	
		creditPayButtons = new JButton[3];
		for(int i = 0; i < creditPayButtons.length; i++) {
			creditPayButtons[i] = new JButton();
		}
		creditSubPanel = new JPanel();
		creditSubPanel.setLayout(new GridLayout(1,3));
		
		creditInsertPanel = new JPanel();
		creditInsertPanel.setLayout(new GridLayout(3,1));
		creditPinTextField = new JTextField(); 
		
		creditSubPanel.add(creditPayButtons[0]);
		creditSubPanel.add(creditInsertPanel);
		creditSubPanel.add(creditPayButtons[2]);
		
		creditInsertPanel.add(new JLabel("Enter PIN",SwingConstants.CENTER));
		creditInsertPanel.add(creditPinTextField);
		creditInsertPanel.add(creditPayButtons[1]);
		
		creditPayButtons[0].setText("Swipe");
		creditPayButtons[1].setText("Insert");
		creditPayButtons[2].setText("Tap");
		
		creditPanel.add(creditSubPanel);
		creditPanel.add(returnButtons[2]);	
		
   	 	setUpCreditPayButtons();
	}
	
	private void setUpDebitScreen() {
		debitPanel = new JPanel();
		debitPanel.setLayout(new GridLayout(3,1));
		debitMessage = new JLabel(); 
		debitMessage.setHorizontalAlignment(SwingConstants.CENTER);
		debitPanel.add(debitMessage);
		
		debitPayButtons = new JButton[3];
		for(int i = 0; i < debitPayButtons.length; i++) {
			debitPayButtons[i] = new JButton();
		}
   	 	
		debitSubPanel = new JPanel();
		debitSubPanel.setLayout(new GridLayout(1,3));
		
		debitInsertPanel = new JPanel();
		debitInsertPanel.setLayout(new GridLayout(3,1));
		debitPinTextField = new JTextField(); 
		
		debitSubPanel.add(debitPayButtons[0]);
		debitSubPanel.add(debitInsertPanel);
		debitSubPanel.add(debitPayButtons[2]);
		
		debitInsertPanel.add(new JLabel("Enter PIN",SwingConstants.CENTER));
		debitInsertPanel.add(debitPinTextField);
		debitInsertPanel.add(debitPayButtons[1]);
		
		debitPayButtons[0].setText("Swipe");
		debitPayButtons[1].setText("Insert");
		debitPayButtons[2].setText("Tap");
		
		debitPanel.add(debitSubPanel);
   	 	debitPanel.add(returnButtons[3]);	
   	 	setUpDebitPayButtons();
	}
	
	private void setUpFailedPaymentScreen() {
		failedPaymentPanel = new JPanel();
		failedPaymentPanel.setLayout(new GridLayout(2,1));
		paymentFailedMessage = new JLabel("Payment failed. Please try again."); // general failed payment message
		paymentFailedMessage.setHorizontalAlignment(SwingConstants.CENTER);
		failedPaymentPanel.add(paymentFailedMessage);
		
		failedPaymentPanel.add(failedPaymentButton);	
   	}
	
	private void setUpThankYouScreen() {
		thankYouPanel = new JPanel();
		thankYouPanel.setLayout(new GridLayout(1,1));
		thankYouMessage = new JLabel("Thank you for shopping with us."); // general failed payment message
		thankYouMessage.setHorizontalAlignment(SwingConstants.CENTER);
		thankYouPanel.add(thankYouMessage);
   	}
	
	private void setUpBanknotePayButtons() {
		// clicking these buttons represents/simulates inserting the corresponding banknote into corressponding banknote slot
		for(int i = 0; i < banknotePayButtons.length; i++) {
			int index = i;
			banknotePayButtons[index].addActionListener(new ActionListener() {
		         public void actionPerformed(ActionEvent e) { 
		        	Banknote validBanknote = new Banknote(Currency.getInstance("CAD"), banknoteDenominations[index]);
		            try {
		            	System.out.println("thisworks");
		            	banknotePayment.receiveBanknote(validBanknote);
		            	hasPartiallyPaid = true;
		            	returnButtons[0].setText("Continue adding banknotes to complete payment.");
		            		
					} catch (Exception e1) { // means error with coin payment system
						// future implementation: determine what error occured (EmptyDevice, OverloadedDevice, NoCashAvailableException,
						// DisabledException, CashOverloadException) and signal to attendant. 
						e1.printStackTrace();
						// show error screen/message
						paymentFailed("An issue occured with this station's banknote payment system. Please use a different payment method.");
					}
		            // future implementation: CoinController field "remainingBalance" should not change if an error has occured 
		            // with receiving a coin (receiveCoin() method).
		            refreshRemainingAmountDue();
		         }
		    });
		}
	}
	
	private void setUpCoinPayButtons() {
		// clicking these buttons represents/simulates inserting the corresponding coin into the corresponding coinslot
		for(int i = 0; i < coinPayButtons.length; i++) {
			int index = i;
			coinPayButtons[index].addActionListener(new ActionListener() {
		         public void actionPerformed(ActionEvent e) { 
		            Coin validCoin = new Coin(Currency.getInstance("CAD"), coinDenominations.get(index));
		            try {
						coinPayment.receiveCoin(validCoin);
						hasPartiallyPaid = true;
						returnButtons[1].setText("Continue adding coins to complete payment.");
					} catch (Exception e1) { // means error with coin payment system
						// future implementation: determine what error occured (EmptyDevice, OverloadedDevice, NoCashAvailableException,
						// DisabledException, CashOverloadException) and signal to attendant. 
						//e1.printStackTrace();
						// show error screen/message
						paymentFailed("An issue occured with this station's coin payment system. Please use a different payment method.");
						
					}
		            // future implementation: CoinController field "remainingBalance" should not change if an error has occured 
		            // with recieving a coin (receiveCoin() method).
		            refreshRemainingAmountDue();
		         }
		    });
		}
	}
	
	private void setUpDebitPayButtons() {
		// clicking these buttons represents/simulates swiping/inserting/tapping card
		debitPayButtons[0].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by swipe
	        	 Card card = PayObject.getCard(); 
	        	 DebitCardPayment payment = new DebitCardPayment(logic);	// will be paying with debit card        	 
	        	 try {
	        		CardSwipeData cardData = card.swipe();// returns CardData object
	        		// NOTE: 2nd argument (double transactionAmount) should be: (double)shoppingCart.getTotalPrice()  ,
	        		// as of now, add items functionality is not yet added to GUI, so total price is always $0,
	        		//  and would always lead to a failed payment.
	        		payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment
					// if transaction goes through, receipt printed and paymentRecieved() called

				// if payment simply fails, does not go to catch block
				} catch (BlockedCardException|MagneticStripeFailureException e1) { 
					paymentFailed("Card is blocked/faulty."); // show error screen/message
					
				} catch (IOException e1) { // other error
					//e1.printStackTrace();
					paymentFailed();
				}
	        	refreshRemainingAmountDue();
	         }
	    });
		debitPayButtons[1].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by Insert
	        	Card card = PayObject.getCard(); 
	        	DebitCardPayment payment = new DebitCardPayment(logic);	// will be paying with debit card        	 
	        	 
	        	String pin = debitPinTextField.getText();
	        	try {
					CardInsertData cardData = card.insert(pin);// returns CardData object
					payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment
					
	        	} catch(InvalidPINException e1) {
	        		System.out.println("Invalid PIN");
	        		paymentFailed("Invalid PIN"); // show error screen/message
	        		 
				} catch (BlockedCardException|ChipFailureException e1) { 
					paymentFailed("Card is blocked/Chip failure"); // show error screen/message
					
				} catch (IOException e1) { // other error
					//e1.printStackTrace();
					paymentFailed();
				} 
	        	debitPinTextField.setText(""); // clear textfield
	        	refreshRemainingAmountDue();
	         }
	    });
		debitPayButtons[2].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by Tap
	        	 Card card = PayObject.getCard(); 
	        	 DebitCardPayment payment = new DebitCardPayment(logic);	// will be paying with debit card 
					try {
						CardTapData cardData = card.tap(); // returns null if tap is not enabled for the card
						payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment

					} catch (BlockedCardException|ChipFailureException e1) { 
						paymentFailed("Card is blocked/Chip failure"); // show error screen/message
						
					} catch (IOException e1) { // other error
						paymentFailed();
					}
					refreshRemainingAmountDue();
	         }
	    });
	}
	
	private void setUpCreditPayButtons() { 
		// clicking these buttons represents/simulates swiping/inserting/tapping card
		creditPayButtons[0].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by swipe
	        	 Card card = PayObject.getCard(); 
	        	 CreditCardPayment payment = new CreditCardPayment(logic);	// will be paying with credit card        	 
	        	 try {
	        		CardSwipeData cardData = card.swipe();// returns CardData object
	        		// NOTE: 2nd argument (double transactionAmount) should be: (double)shoppingCart.getTotalPrice()  ,
	        		// as of now, add items functionality is not yet added to GUI, so total price is always $0,
	        		//  and would always lead to a failed payment.
	        		payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment
					// if transaction goes through, receipt printed and paymentRecieved() called

				// if payment simply fails, does not go to catch block
				} catch (BlockedCardException|MagneticStripeFailureException e1) { 
					paymentFailed("Card is blocked/faulty."); // show error screen/message
					
				} catch (IOException e1) { // other error
					paymentFailed();
				} 
	        	refreshRemainingAmountDue();
	         }
	    });
		creditPayButtons[1].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by Insert
	        	Card card = PayObject.getCard(); 
	        	CreditCardPayment payment = new CreditCardPayment(logic);	// will be paying with credit card        	 
	        	 
	        	String pin = creditPinTextField.getText();
	        	try {
					CardInsertData cardData = card.insert(pin);// returns CardData object
					payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment
					
	        	} catch(InvalidPINException e1) {
	        		System.out.println("Invalid PIN");
	        		paymentFailed("Invalid PIN"); // show error screen/message
	        		 
				} catch (BlockedCardException|ChipFailureException e1) { 
					paymentFailed("Card is blocked/Chip failure"); // show error screen/message
					
				} catch (IOException e1) { // other error
					paymentFailed();
				} 
	        	creditPinTextField.setText(""); // clear textfield
	        	refreshRemainingAmountDue();
	         }
	    });
		creditPayButtons[2].addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // pay by Tap
	        	 Card card = PayObject.getCard(); 
	        	 CreditCardPayment payment = new CreditCardPayment(logic);	// will be paying with credit card 
					try {
						CardTapData cardData = card.tap(); // returns null if tap is not enabled for the card
						payment.processTransaction(cardData, (double)shoppingCart.getTotalPrice() ); // set double to 0 to fail payment

					} catch (BlockedCardException|ChipFailureException e1) { 
						paymentFailed("Card is blocked/Chip failure"); // show error screen/message
						
					} catch (IOException e1) { // other error
						paymentFailed();
					} 
					refreshRemainingAmountDue();
	         }
	    });
	}
	
	// set up buttons on the "Select Payment" screen, and failed payment button
	private void setUpButtons() {
		// pay by banknote button
		setBanknoteB(new JButton("Banknote"));     
		optionButtonsPanel.add(getBanknoteB());
		getBanknoteB().addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { // go to banknote payment screen
	        	 logic.banknoteController.updateAmountDue(); // set up payment using banknotes
	        	 refreshRemainingAmountDue(); // refresh label
	        	 primary.showBanknoteScreen();
	         }
	    });
		
		// pay by coin button
		setCoinB(new JButton("Coin"));     
		optionButtonsPanel.add(getCoinB());
		getCoinB().addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 logic.coinController.updateAmountDue(); // set up payment using coins
	        	 refreshRemainingAmountDue(); // refresh label
	        	 primary.showCoinScreen();
	         }
	    });
		
		// pay by credit card button
		setCreditB(new JButton("Credit Card"));     
		optionButtonsPanel.add(getCreditB());
		getCreditB().addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 primary.showCreditScreen();
	         }
	    });
		
		// pay by debit card button
		setDebitB(new JButton("Debit Card"));     
		optionButtonsPanel.add(getDebitB());
		getDebitB().addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {  	 
	        	 primary.showDebitScreen();
	         }
	    });
		
		setReturnToItemScreen(new JButton("Return"));  
		returnButtonPanel.add(getReturnToItemScreen());
		getReturnToItemScreen().addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 primary.showAddItemsScreen(); // back to items screen
	         }
	    });
		
		// return button on failed payment
		failedPaymentButton = new JButton("Back");  
		failedPaymentButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	     		paymentFailedMessage.setText("Payment failed. Please try again."); // set back to default message
	        	primary.showPaymentOptionsScreen();// go back to payment options
	         }
	    });
		
		membershipB.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	primary.showMembershipNumberScreen();
	         }
	    });
		
		helpB.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) { 
	        	 logic.signalAttendantController.alertAttendant();
	        	 logic.signalAttendantController.activateHelpSession();
	        	 primary.getAttendantStationScreen().setResolveIssueButtonEnabled();
	         }
	    });
	}

	// refreshes the labels displaying the remaining amount due
	private void refreshRemainingAmountDue() {
		banknoteMessage.setText("<html><center>Insert banknote(s) into corresponding banknote insertion slot(s).<br>"
        		+ "Remaining amount due: $"+getRemainingBanknotesDue()+" CAD</center></html>");
		coinMessage.setText("<html><center>Insert coin(s) into corresponding coin insertion slot(s).<br>"
        		+ "Remaining amount due: $"+getRemainingCoinsDue()+" CAD</center></html>");
		debitMessage.setText("<html><center>Swipe/Insert/Tap Debit card.<br>Remaining amount due: $"
				+shoppingCart.getTotalPrice()+" CAD</center></html>");
		creditMessage.setText("<html><center>Swipe/Insert/Tap Credit card.<br>Remaining amount due: $"
				+shoppingCart.getTotalPrice()+" CAD</center></html>");
		
	}
	
	public void updateTotal() {
		total.setText("Total: $"+shoppingCart.getTotalPrice()); 
		refreshRemainingAmountDue();
	}
	
	private double getRemainingCoinsDue(){
		return logic.coinController.getRemainingBalance().doubleValue();
	}
	
	private double getRemainingBanknotesDue(){
		return logic.banknoteController.getRemainingBalance().doubleValue();
	}
	
	public JPanel getPaymentOptionsPanel() {
		return paymentOptionsPanel;
	}
	
	public JPanel getBanknotePanel() {
		return banknotePanel;
	}
	
	public JPanel getCoinPanel() {
		return coinPanel;
	}
	
	public JPanel getCreditPanel() {
		return creditPanel;
	}
	
	public JPanel getDebitPanel() {
		return debitPanel;
	}
	
	public JPanel getFailedPaymentPanel() {
		return failedPaymentPanel;
	}
	
	public JPanel getThankYouPanel() {
		return thankYouPanel;
	}
	
	
	// the following methods are called by system listener only
	public void paymentRecieved(){
		System.out.println("Payment recieved."); // temporary
		primary.showThankYouScreen();
	}

	// Clear items from the shopping cart & scale.
	private void clearItems() {
		// First clear items from the scale
		AbstractElectronicScale scale = logic.scaleController.getScaleScanningArea();
		// First we need to clear the cart
		for (PLUCodedItem i : shoppingCart.getShoppingCartPLUItems()) {
			try {
				scale.removeAnItem(i);
			}catch(InvalidArgumentSimulationException e){
				//Bulky items not on scale
			}
			
		}
		for(BarcodedItem i : shoppingCart.getShoppingCartItems()) {
			try {
				scale.removeAnItem(i);
			}catch(InvalidArgumentSimulationException e){
				//Bulky items not on scale
			}
		}
		// Reset the shopping cart.
		shoppingCart.clearItems();
		// Reset the item screen
		primary.getAddItemsScreen().clearErrorMessages();
		primary.getAddItemsScreen().clearItems();
	}
	
	protected void endSession(){		
		// clear textfields
    	debitPinTextField.setText(""); 
    	creditPinTextField.setText("");
		// Clear the items in the cart
		clearItems();
		// clear change controller
		logic.changeController = new ChangeController(logic);
		// clear membership number
		
		
		ActionListener endThankYouScreen = new ActionListener() {
		      public void actionPerformed(ActionEvent evt) {
		    	  primary.showStartScreen(); // go back to start session screen after 2 seconds
		      }
		 };
		 Timer timer = new Timer(2000, endThankYouScreen);
		 timer.setRepeats(false);
		 timer.start();
		  
		 // Ending session needs to implement resetting shopping cart, expected weight, etc, to initial values
	}
	
	public void paymentFailed(){
		System.out.println("Payment failed."); // temporary
		primary.showFailedPaymentScreen(); // show error screen/message
	}
	
	protected void paymentFailed(String errorMessage){
		System.out.println("Payment failed."); // temporary
		paymentFailedMessage.setText("<html><center>"+errorMessage+"<br>Payment failed.</center></html>");
		
		primary.showFailedPaymentScreen(); // show error screen/message
	}
	
	protected void showReceiptScreen(String receipt) {
		
		try {
				logic.sessionStarter.endSession();
				logic.inkManager.useInk(80, "HP Catridge");
				logic.predictIssuesController.predictLowOnInk();
				if(logic.predictIssuesController.getLowOnInk()) {
					JButton inkButton = primary.getAttendantStationScreen().getResolveInkButton();
					primary.getAttendantStationScreen().setButtonEnabled(inkButton);
				}
				
				logic.paperManager.removePaper(1, "Receipt Paper");
				logic.predictIssuesController.predictLowOnPaper();
				if(logic.predictIssuesController.getLowOnPaper()) {
					JButton paperButton = primary.getAttendantStationScreen().getResolvePaperButton();
					primary.getAttendantStationScreen().setButtonEnabled(paperButton);
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new ReceiptScreen("Receipt:\n\n"+receipt);
		
	}

	public JButton getReturnToItemScreen() {
		return returnToItemScreen;
	}

	public void setReturnToItemScreen(JButton returnToItemScreen) {
		this.returnToItemScreen = returnToItemScreen;
	}

	public JButton getDebitB() {
		return debitB;
	}

	public void setDebitB(JButton debitB) {
		this.debitB = debitB;
	}

	public JButton getCreditB() {
		return creditB;
	}

	public void setCreditB(JButton creditB) {
		this.creditB = creditB;
	}

	public JButton getCoinB() {
		return coinB;
	}

	public void setCoinB(JButton coinB) {
		this.coinB = coinB;
	}

	public JButton getBanknoteB() {
		return banknoteB;
	}

	public void setBanknoteB(JButton banknoteB) {
		this.banknoteB = banknoteB;
	}
	
}
