/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.math.BigDecimal;
import java.util.Currency;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.printer.ReceiptPrinterBronze;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.screen.ITouchScreen;
import com.tdc.CashOverloadException;
import com.tdc.banknote.Banknote;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

import powerutility.PowerGrid;

public class PrimaryGUI {
	private AbstractSelfCheckoutStation selfCheckoutStation;
	private CentralCheckoutStationLogic centralCheckoutStationLogic;
	
	private StartScreen startScreen;
	private LanguageSelectionScreen languageSelectionScreen;
	private AddItemsScreen addItemsScreen;
	private AttendantStationScreen attendantStationScreen;
	private PaymentScreen paymentScreen;
	private MembershipNumberScreen membershipNumberScreen;

	private ITouchScreen primaryTouchScreen;
	private AttendantStation attendentStation;
	private VirtualKeyboard keyboardScreen;
	private AttendantKeyboard attendantKeyboardScreen;
	private VisualSearchScreen visualSearchScreen;
	
	private JFrame primaryFrame;
	private JPanel primaryPanel;
	private CardLayout primaryCardLayout;
	private CardLayout attendantCardLayout;
	
	private JFrame attendantFrame;
	private JPanel attendantPanel;
	
	//Constuctor
	public PrimaryGUI(CentralCheckoutStationLogic... logic) {   
		centralCheckoutStationLogic = logic[0];
		
		this.selfCheckoutStation = centralCheckoutStationLogic.hardware;
		this.attendentStation = centralCheckoutStationLogic.attendantStation;
		
		attendentStation = centralCheckoutStationLogic.attendantStation;
		
		primaryFrame = selfCheckoutStation.getScreen().getFrame();
		primaryFrame.setBounds(100, 100, 700, 700);
		primaryCardLayout = new CardLayout();
		primaryPanel = new JPanel(primaryCardLayout);
		
		startScreen = new StartScreen(this, centralCheckoutStationLogic);
		languageSelectionScreen = new LanguageSelectionScreen(this, centralCheckoutStationLogic);
		addItemsScreen = new AddItemsScreen(this, centralCheckoutStationLogic);
		keyboardScreen = new VirtualKeyboard(this, centralCheckoutStationLogic);
		paymentScreen = new PaymentScreen(this, centralCheckoutStationLogic);
		membershipNumberScreen = new MembershipNumberScreen(this, centralCheckoutStationLogic);
		visualSearchScreen = new VisualSearchScreen(this, centralCheckoutStationLogic);
		
		SystemListener.connectSystemListener(paymentScreen,centralCheckoutStationLogic, membershipNumberScreen); // connect system listener to GUI
		centralCheckoutStationLogic.predictIssuesController.setWarningLowInkPercentage(0.2);
		
		primaryPanel.add(startScreen.getPanel(), "Start");
		primaryPanel.add(languageSelectionScreen.getPanel(), "Language");
		primaryPanel.add(addItemsScreen.getPanel(), "AddItems");
		primaryPanel.add(visualSearchScreen.getSearchPanel(), "SearchItem");
		primaryPanel.add(keyboardScreen.getPanel(), "keyboard");
		primaryPanel.add(paymentScreen.getPaymentOptionsPanel(), "PaymentOptions");
		primaryPanel.add(paymentScreen.getBanknotePanel(), "Banknote");
		primaryPanel.add(paymentScreen.getCoinPanel(), "Coin");
		primaryPanel.add(paymentScreen.getCreditPanel(), "Credit");
		primaryPanel.add(paymentScreen.getDebitPanel(), "Debit");
		primaryPanel.add(paymentScreen.getFailedPaymentPanel(), "FailedPayment");
		primaryPanel.add(paymentScreen.getThankYouPanel(), "ThankYou");
		primaryPanel.add(membershipNumberScreen.getMembershipPanel(), "Membership");

		
		primaryFrame.getContentPane().add(primaryPanel, BorderLayout.CENTER);
		
		attendantFrame = attendentStation.screen.getFrame();
		attendantFrame.setBounds(100, 100, 750, 800);
		attendantCardLayout = new CardLayout();
		attendantPanel = new JPanel(attendantCardLayout);
		
		attendantStationScreen = new AttendantStationScreen(this, logic);
		attendantKeyboardScreen = new AttendantKeyboard(this, centralCheckoutStationLogic);
		
		attendantPanel.add(attendantStationScreen.getPanel(), "Attendant");
		attendantPanel.add(attendantKeyboardScreen.getPanel(), "Keyboard");
		
		attendantFrame.getContentPane().add(attendantPanel, BorderLayout.CENTER);
		
		primaryFrame.setLocation(0, 100);
		attendantFrame.setLocation(725, 100);
		
		selfCheckoutStation.getScreen().setVisible(true);
		attendentStation.screen.setVisible(true);
		
		showStartScreen();
		showAttendantMain();
	}
	
	//show methods
	public void showSearchPanel() {
		primaryCardLayout.show(primaryPanel, "SearchItem");
	}

	public void showKeyboard() {
		primaryCardLayout.show(primaryPanel, "keyboard");
	}
	
    public void showStartScreen() {
    	primaryCardLayout.show(primaryPanel, "Start");
    }

    public void showLanguageSelectionScreen() {
    	primaryCardLayout.show(primaryPanel, "Language");
    }

    public void showAddItemsScreen() {
    	primaryCardLayout.show(primaryPanel, "AddItems");
    }

    public void showPaymentOptionsScreen() {
    	primaryCardLayout.show(primaryPanel, "PaymentOptions");
    }
    
    public void showBanknoteScreen() {
    	primaryCardLayout.show(primaryPanel, "Banknote");
    }
    
    public void showCoinScreen() {
    	primaryCardLayout.show(primaryPanel, "Coin");
    }
    
    public void showCreditScreen() {
    	primaryCardLayout.show(primaryPanel, "Credit");
    }
    
    public void showDebitScreen() {
    	primaryCardLayout.show(primaryPanel, "Debit");
    }
    
    public void showFailedPaymentScreen() {
    	primaryCardLayout.show(primaryPanel, "FailedPayment");
    }
    
    public void showThankYouScreen() {
    	primaryCardLayout.show(primaryPanel, "ThankYou");
    }
    
    public void showMembershipNumberScreen() {
    	primaryCardLayout.show(primaryPanel, "Membership");
    }
 
    public void showAttendantKeyboard() {
    	attendantCardLayout.show(attendantPanel, "Keyboard");
    }
    
    public void showAttendantMain() {
    	attendantCardLayout.show(attendantPanel, "Attendant");
    }
    
    //getters
	public StartScreen getStartScreen() {
		return startScreen;
	}
	public LanguageSelectionScreen getLanguageSelectionScreen() {
		return languageSelectionScreen;
	}
	public AddItemsScreen getAddItemsScreen() {
		return addItemsScreen;
	}
	public AttendantStationScreen getAttendantStationScreen() {
		return attendantStationScreen;
	}
	public AttendantKeyboard getAttendantKeyboardScreen() {
		return attendantKeyboardScreen;
	}
	public PaymentScreen getPaymentScreen() {
		return paymentScreen;
	}
	public MembershipNumberScreen getMembershipNumberScreen() {
		return membershipNumberScreen;
	}
	
	//closes screen
	public void close() {
		primaryTouchScreen.setVisible(false);
	}
	
	public static void main(String[] args) {
		ItemDatabase.loadItems();
		ItemDatabase.initBarcodedItems();
		
		PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
        
		BigDecimal coinDenominations[] = {
                new BigDecimal("0.50"), 
                new BigDecimal("0.10"), 
                new BigDecimal("0.25"), 
                new BigDecimal("1.00"), 
                new BigDecimal("2.00")
        };
        
        BigDecimal banknoteDenominations[] = {
        		new BigDecimal("1.00"),
        		new BigDecimal("2.00"),
        		new BigDecimal("3.00"),
        		new BigDecimal("4.00"),
        		new BigDecimal("5.00"),
        };
        
		AbstractSelfCheckoutStation.resetConfigurationToDefaults();
		AbstractSelfCheckoutStation.configureCoinDenominations(coinDenominations);	
		AbstractSelfCheckoutStation.configureBanknoteDenominations(banknoteDenominations);
		
		AttendantStation attendantStation = new AttendantStation();
		attendantStation.plugIn(grid);
		attendantStation.turnOn();
		
		SelfCheckoutStationBronze stationOne = new SelfCheckoutStationBronze();
		stationOne.plugIn(grid);
		stationOne.turnOn();
		
		SelfCheckoutStationBronze stationTwo = new SelfCheckoutStationBronze();
		stationTwo.plugIn(grid);
		stationTwo.turnOn();
		
		SelfCheckoutStationBronze stationThree = new SelfCheckoutStationBronze(); 
		stationThree.plugIn(grid);
		stationThree.turnOn();
				
		
		try { // Set up receipt printers
			stationOne.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationOne.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationTwo.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationTwo.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
			stationThree.getPrinter().addInk(ReceiptPrinterBronze.MAXIMUM_INK);
			stationThree.getPrinter().addPaper(ReceiptPrinterBronze.MAXIMUM_PAPER);
		} catch (OverloadedDevice e) {
			e.printStackTrace();}
		
		CentralCheckoutStationLogic logic1 = CentralCheckoutStationLogic.installOn(stationOne, attendantStation);
		CentralCheckoutStationLogic logic2 = CentralCheckoutStationLogic.installOn(stationTwo, attendantStation);
		CentralCheckoutStationLogic logic3 = CentralCheckoutStationLogic.installOn(stationThree, attendantStation);

		PrimaryGUI primary = new PrimaryGUI(logic1, logic2, logic3);
	}
}
