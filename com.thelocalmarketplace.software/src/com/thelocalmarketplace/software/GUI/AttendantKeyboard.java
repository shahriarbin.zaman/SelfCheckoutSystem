/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jjjwelectronics.Mass;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Item.TextSearch;

import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;

public class AttendantKeyboard {

	private CentralCheckoutStationLogic logic;
	private PrimaryGUI primary;
	
	private JFrame mainFrame;
	private JPanel mainPanel;
	private JPanel keyboardPanel;
	private JPanel buttonPanel;
	private JPanel returnPanel;
	
	private JButton continueButton;
	
	private VirtualKeyboard keyboard;
	private TextSearch textSearch;
	
	
	private PLUCodedProduct selectedProduct;
	
	public AttendantKeyboard(PrimaryGUI primary, CentralCheckoutStationLogic logic) {
		this.primary = primary;
		this.logic = logic;
		textSearch = new TextSearch(logic);
		
		mainFrame = new JFrame();
		mainPanel = new JPanel();
		keyboardPanel = new JPanel(new GridLayout(1,1));
		buttonPanel = new JPanel();
		returnPanel = new JPanel();
		
		addWidgets();
		addApproveWeightWidget();
		
		new BoxLayout(mainPanel, BoxLayout.Y_AXIS);
		Box rows[] = new Box[3];
		rows[0] = Box.createHorizontalBox();
		rows[1] = Box.createHorizontalBox();
		rows[2] = Box.createHorizontalBox();
		
		mainPanel.add(rows[0]);
		mainPanel.add(rows[1]);  
		mainPanel.add(rows[2]);

		keyboardPanel.setPreferredSize(new Dimension(750,100));
		buttonPanel.setPreferredSize(new Dimension(750,50));
		returnPanel.setPreferredSize(new Dimension(500,50));
		
		rows[0].add(keyboardPanel);	
		rows[1].add(buttonPanel);
		rows[2].add(returnPanel);
		
		mainFrame.setContentPane(mainPanel);
		mainFrame.setBounds(200, 200, 750, 800);
	}
	
		private void addApproveWeightWidget() {
			JButton resolveWeightDiscrepancyButton = new JButton("Resolve Issue");
			
			resolveWeightDiscrepancyButton.addActionListener(e -> { 
				logic.signalAttendantController.setRequestCleared(false);
				try {
					if(logic.signalAttendantController.isHelpSessionActive()) {
						logic.signalAttendantController.clearCustomerRequest(primary.getAttendantStationScreen().getSelectedStation());
					}
					logic.signalAttendantController.clearCustomerRequest(primary.getAttendantStationScreen().getSelectedStation());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logic.scaleController.checkClearRequest();
				primary.getAddItemsScreen().WeightIssueResolved();
				logic.signalAttendantController.setRequestCleared(false);
			});

			buttonPanel.add(resolveWeightDiscrepancyButton);
		}
	
	public void addWidgets() {
		continueButton = new JButton("Add");
		continueButton.addActionListener(e -> {
			if(logic.scaleController.hasExceededLimit()) {
				primary.getAddItemsScreen().addLogMessage("Can't add more items, weight exceeded");
				keyboard.clearText();
				selectedProduct = null;
				return;
			}
			PriceLookUpCode itemCode;
			try {
				String productName = keyboard.getText().trim();
				itemCode = textSearch.findItemByName(productName);
			} catch (Exception ex) {
				primary.getAddItemsScreen().addLogMessage("Unknown item from attendant");
				return;
			}
			if(itemCode == null) {
				primary.getAddItemsScreen().addLogMessage("Invalid item description");
				selectedProduct = null;
				return;
			}
			ShoppingCart cart = ShoppingCart.getShoppingCart(logic);
			selectedProduct = ProductDatabases.PLU_PRODUCT_DATABASE.get(itemCode);
			PLUCodedItem item = ItemDatabase.getPLUProducts().get(itemCode.toString());
			if(cart.getShoppingCartPLUItems().contains(item)) {
				try {
					logic.scaleController.getScaleScanningArea().removeAnItem(item);					
				} catch (InvalidArgumentSimulationException ex) {
					// Ignore the exception - The item was not on the scale because the attendant added it.
				}
				selectedProduct = null;
				primary.getAddItemsScreen().addLogMessage("The same item cannot be added more than once to the scale.");
			}
			cart.addPLUItemToCart(item);
			primary.getAddItemsScreen().setAddItemByTextButtonEnabled();
			keyboard.clearText();
		});
		
		JButton returnButton = new JButton("Return");
		returnButton.addActionListener(e -> {
			primary.showAttendantMain();
		});
		
		keyboard = new VirtualKeyboard(primary, logic);
		keyboardPanel.add(keyboard.getPanel());
		buttonPanel.add(continueButton);
		returnPanel.add(returnButton, BorderLayout.CENTER);
	}
	
	public JPanel getPanel() {
		return mainPanel;
	}
	
	public PLUCodedProduct getSelectedProduct() {
		return selectedProduct;
	}
	
}
