/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.jjjwelectronics.screen.TouchScreenBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class LanguageSelectionScreen{
	JFrame languageFrame;
	JPanel languagePanel;
	JButton selectEnglish;
	JButton selectFrench;
	JButton returnButton;
	
	private StartScreen startScreen;

	private CentralCheckoutStationLogic centralCheckoutStation;
	private PrimaryGUI primary;
	
	public LanguageSelectionScreen(PrimaryGUI primary, CentralCheckoutStationLogic centralStation) {
		this.centralCheckoutStation = centralStation;
		this.primary = primary;
		
		languageFrame =  new JFrame();
		
		languagePanel = new JPanel();
		languagePanel.setLayout(new GridLayout(2,2));
		
		addWidgets();
		loadText();
		
		languageFrame.setContentPane(languagePanel);
		languageFrame.setBounds(100, 100, 400, 150);
		
		startScreen = primary.getStartScreen();
	}
	
	public void addWidgets() {
		JPanel languageButtonPanel = new JPanel(new GridLayout(1, 2));
		
		selectEnglish = new JButton();
		selectEnglish.addActionListener(e -> {
			DataManager.getInstance().setLanguage("English");
			loadText();
            
			primary.showStartScreen();
		});
		
		selectFrench = new JButton();
			
		selectFrench.addActionListener(e -> {
			DataManager.getInstance().setLanguage("French");
			loadText();

			primary.showStartScreen();
		});
		
		returnButton = new JButton();
		returnButton.addActionListener(e -> {

			primary.showStartScreen();
		});
		
		languageButtonPanel.add(selectEnglish);
		languageButtonPanel.add(selectFrench);
		languagePanel.add(languageButtonPanel);
		languagePanel.add(returnButton);
	}

	public void loadText() {
		if(DataManager.getInstance().getLanguage() == "French") {
			selectFrench.setText("Française");
			selectEnglish.setText("Anglaise");
			returnButton.setText("Retour");
			

			primary.getStartScreen().getStartSessionButton().setText("Touchez pour commencer");
			primary.getStartScreen().getSelectLanguageButton().setText("Choisir la langue");
		}else {
			selectFrench.setText("French");
			selectEnglish.setText("English");
			returnButton.setText("Return");
			

			primary.getStartScreen().getStartSessionButton().setText("Touch to start");
			primary.getStartScreen().getSelectLanguageButton().setText("Select Language");
			}
		}
	
	public JPanel getPanel() {
		return languagePanel;
	}
}

