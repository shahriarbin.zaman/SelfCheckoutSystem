/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

// for GUI to know when a payment has when through 
// also for GUI to know when Weight discrepancy has occured

// A listener of the Self-Checkout system, connected to the GUI
// Listens for key events for GUI to respond to, such as successful/failed payments,
//  weight discrepancies, etc
public class SystemListener {
	private static PaymentScreen paymentScreen;
	private static CentralCheckoutStationLogic centralCheckoutStation;
	private static MembershipNumberScreen membershipNumberScreen;
	
	// is called only in constructor of Primary GUI
	protected static void connectSystemListener(PaymentScreen paymentScreen,CentralCheckoutStationLogic centralCheckoutStation,MembershipNumberScreen membershipNumberScreen) { 
		SystemListener.paymentScreen = paymentScreen;
		SystemListener.centralCheckoutStation = centralCheckoutStation;
		SystemListener.setMembershipNumberScreen(membershipNumberScreen);
		
	}
	
	// called in _Payment classes
	public static void aPaymentHasBeenMade() {
		if(paymentScreen != null) { 
			paymentScreen.paymentRecieved();
		}
	}
	
	// for when a payment has failed for reasons other than a thrown exception
	// eg. if a card has failed to be authorized, payment failed to post
	public static void aPaymentHasFailed() {
		if(paymentScreen != null) { // error handling
			paymentScreen.paymentFailed();
		}
	}
	
	public static void aPaymentHasFailed(String errorMessage) {
		if(paymentScreen != null) { // error handling
			paymentScreen.paymentFailed(errorMessage);
		}
	}
	
	public static void theSessionHasEnded() {
		if(paymentScreen != null) { // error handling
			paymentScreen.endSession();
		}
		if(getMembershipNumberScreen() != null) { // error handling
			getMembershipNumberScreen().clearMembership();
		}
	}
	
	// called in ReceiptPrinterController
	public static void aReceiptHasBeenPrinted(String receipt) {
		if(paymentScreen != null) { // error handling
			paymentScreen.showReceiptScreen(receipt);
		}
	}

	// used in pay object
	public static CentralCheckoutStationLogic getCentralCheckoutStation() {
		return centralCheckoutStation;
	}

	public static Object getPaymentScreen() {
		// TODO Auto-generated method stub
		return null;
	}

	public static MembershipNumberScreen getMembershipNumberScreen() {
		return membershipNumberScreen;
	}

	public static void setMembershipNumberScreen(MembershipNumberScreen membershipNumberScreen) {
		SystemListener.membershipNumberScreen = membershipNumberScreen;
	}
	
}

