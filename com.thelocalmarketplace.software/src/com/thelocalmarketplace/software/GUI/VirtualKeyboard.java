/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
*/

package com.thelocalmarketplace.software.GUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.jjjwelectronics.DisabledDevice;
import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.keyboard.Key;
import com.jjjwelectronics.keyboard.KeyboardListener;
import com.jjjwelectronics.keyboard.USKeyboardQWERTY;
import com.jjjwelectronics.screen.TouchScreenBronze;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

import powerutility.NoPowerException;
import powerutility.PowerGrid;

public class VirtualKeyboard implements KeyboardListener{

    private JTextField textField;
    private USKeyboardQWERTY keyboard;
    private JPanel keyboardPanel;
    private JFrame keyboardFrame;
    private PowerGrid mockGrid;
    private List<String> excludedKeys;
    
    private JPanel mainPanel;
    
    private CentralCheckoutStationLogic centralCheckoutStation;
	private PrimaryGUI primary;
	
	//Constructor
    public VirtualKeyboard(PrimaryGUI primary, CentralCheckoutStationLogic centralCheckoutStation) {
    	this.centralCheckoutStation = centralCheckoutStation;
		this.primary = primary;
		
        keyboardFrame = new JFrame();
        textField = new JTextField(20);
        textField.setEditable(false);
        keyboardPanel = new JPanel(new GridLayout(4, 18));
        keyboard = new USKeyboardQWERTY();
        
        //since its a GUI and not an actual keyboard setting power on as it doesnt apply
        mockGrid = PowerGrid.instance();
        PowerGrid.engageUninterruptiblePowerSource();
        
		keyboard.plugIn(mockGrid);
		keyboard.turnOn();
		keyboard.enable();
		keyboard.register(this);
		
        exclusions();
        keyMapping();
        initializeGUI();
    }
    
    //Constructor
    public VirtualKeyboard(PrimaryGUI primary, CentralCheckoutStationLogic centralCheckoutStation, USKeyboardQWERTY keyboard) {
    	this.centralCheckoutStation = centralCheckoutStation;
		this.primary = primary;
    	
        keyboardFrame = new JFrame();
        textField = new JTextField(20);
        textField.setEditable(false);
        keyboardPanel = new JPanel(new GridLayout(4, 18));
        this.keyboard = keyboard;
        
        //since its a GUI and not an actual keyboard setting power on as it doesnt apply
        mockGrid = PowerGrid.instance();
        PowerGrid.engageUninterruptiblePowerSource();
        
		keyboard.plugIn(mockGrid);
		keyboard.turnOn();
		keyboard.enable();
		keyboard.register(this);
		
        exclusions();
        keyMapping();
        initializeGUI();
    }

    //Method to assign keys to buttons
    private void keyMapping() {
        for (String label : USKeyboardQWERTY.WINDOWS_QWERTY) {
        	if(!excludedKeys.contains(label)){
        		JButton keyButton = new JButton(label);
        		
        		Key key = keyboard.getKey(label);
        		key.plugIn(mockGrid);
                key.turnOn();
                key.enable();
        		
 	            keyButton.addActionListener(e -> {
 	            	try {
 	                    key.press();
 	                    Thread.sleep(100);
 	                    key.release();
 	                } catch (DisabledDevice disabled) {
 	                    disabled.printStackTrace();
 	                } catch (InterruptedException | NoPowerException ex) {
 	                    ex.printStackTrace();
 	                }
 	               keyFunctions(label);
 	            });
 	            keyboardPanel.add(keyButton);
 	        }
        }
	           
    }

    //GUI setup
    private void initializeGUI() {
        mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(textField, BorderLayout.NORTH);
        mainPanel.add(keyboardPanel, BorderLayout.CENTER);
        keyboardFrame.setContentPane(mainPanel);
        keyboardFrame.pack();
    }
    
  //getter
    public JPanel getPanel() {
    	return mainPanel;
    }
    
  //Keys to exclude from keypad
    private void exclusions() {
    	excludedKeys = List.of(
    			"FnLock Esc", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10",
    			"F11", "F12", "Home", "End", "Insert", "Delete", "` ~", "- _", "= +", "Tab",
    			"[ {", "] }", "\\ |", "CapsLock","; :", "' \"", "Shift (Left)",", <", ". >", 
    			"/ ?", "Shift (Right)", "Fn", "Ctrl (Left)", "Windows", "Alt (Left)","Alt (Right)", 
    			"PrtSc", "Ctrl (Right)", "PgUp", "Up Arrow", "PgDn", "Left Arrow", "Down Arrow", "Right Arrow", "Enter"
    			);
    }
    
    
    //assigns special function to specific keys
    //@param label The label of the key to be changed
    private void keyFunctions(String label) {
    	if(label.contains("Spacebar")) {
    		textField.setText(textField.getText() + " ");
    	}else if(label.contains("Backspace")) {
    		String currentText = textField.getText();
    		
    		if(!currentText.isEmpty()) {
    			textField.setText(currentText.substring(0, currentText.length() - 1));
    		}
    	}else if(label.contains("1")){
    		textField.setText(textField.getText() + "1");
    	}else if(label.contains("2")){
    		textField.setText(textField.getText() + "2");
    	}else if(label.contains("3")){
    		textField.setText(textField.getText() + "3");
    	}else if(label.contains("4")){
    		textField.setText(textField.getText() + "4");
    	}else if(label.contains("5")){
    		textField.setText(textField.getText() + "5");
    	}else if(label.contains("6")){
    		textField.setText(textField.getText() + "6");
    	}else if(label.contains("7")){
    		textField.setText(textField.getText() + "7");
    	}else if(label.contains("8")){
    		textField.setText(textField.getText() + "8");
    	}else if(label.contains("9")){
    		textField.setText(textField.getText() + "9");
    	}else if(label.contains("0")){
    		textField.setText(textField.getText() + "0");
    	}else {
    		textField.setText(textField.getText() + label);
    	}
    }
    
    public String getText() {
    	if(textField.getText().trim().isEmpty()) {
    		return null;
    	}
    	return textField.getText();
    }
    
    public void clearText() {
    	textField.setText("");
    }
    
    // Added this for the purpose of testing.
    public JTextField getTextField() {
        return this.textField;
    }
    
    public List getExclusions()
    {
    	return excludedKeys;
    }
    
	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aKeyHasBeenPressed(String label) {
		// TODO Auto-generated method stub
	}

	@Override
	public void aKeyHasBeenReleased(String label) {
		// TODO Auto-generated method stub
	}
    
}