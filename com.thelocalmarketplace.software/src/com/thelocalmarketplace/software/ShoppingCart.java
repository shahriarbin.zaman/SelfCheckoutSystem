/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.Listener.SessionStateListener;

// This class represents the state of a customer's order, when a session is taking place.
// In particular, the state of the items in the order, and the weight that is expected to be in the bagging area.
// 	NOTE FOR GROUPMATES: this class was created so that when items are added by different methods(eg. via Barcode Scan, 
//  Handheld Barcode Scan), there is a single "shopping cart" or list of items being updated, 
//  instead of each "AddItem" class having their own private shopping cart.  
public class ShoppingCart {

	private static ShoppingCart shoppingCart;
	
	private ArrayList<BarcodedItem> cartBarcodedItems;
	private ArrayList<BarcodedProduct> cartBarcodedProducts;
	
	private ArrayList<PLUCodedItem> cartPLUItems;
	private ArrayList<PLUCodedProduct> cartPLUProducts;
	
	private int numberOfItems; 
	private BigDecimal totalExpectedWeight;
	private BigDecimal totalActualWeight;
	private long totalPrice;

	public static ShoppingCart getShoppingCart() {
		if (shoppingCart == null) {
			shoppingCart = new ShoppingCart();
		}
		return shoppingCart;
	}
	
	public static ShoppingCart getShoppingCart(CentralCheckoutStationLogic logic) {
        if (shoppingCart == null) {
        	shoppingCart = new ShoppingCart();
        }
        return shoppingCart;
    }
	
	private ShoppingCart() {
		cartBarcodedItems = new ArrayList<BarcodedItem>();
		cartBarcodedProducts = new ArrayList<BarcodedProduct>();
		
		cartPLUItems = new ArrayList<PLUCodedItem>(); 
		cartPLUProducts = new ArrayList<PLUCodedProduct>();
		
		totalExpectedWeight = BigDecimal.ZERO;
		totalActualWeight = BigDecimal.ZERO;
		totalPrice = 0;
		numberOfItems = 0;
	}

	// Resets the shopping cart to its initial state.
	public void clearItems() {
		totalExpectedWeight = BigDecimal.ZERO;
		totalActualWeight = BigDecimal.ZERO;
		totalPrice = 0;
		numberOfItems = 0;
		cartBarcodedItems.clear();
		cartBarcodedProducts.clear();
		cartPLUItems.clear();
		cartPLUProducts.clear();
	}

	// BARCODED PRODUCTS ------------------------------------------------------------------------------------------------------
	// adds BarcodedItem to cartItems, then determines what product the item is, and adds the product to cartProducts
	public void addBarcodedItemToCart(BarcodedItem barcodeditem) {
		cartBarcodedItems.add(barcodeditem); // add item to cart		
		BarcodedProduct product = ProductDatabases.BARCODED_PRODUCT_DATABASE.get(barcodeditem.getBarcode());
		addBarcodedProductToCart(product);
		numberOfItems++;
	}
	
	// adds BarcodedProduct to cartProducts
	// only for use within ShoppingCart class
	private void addBarcodedProductToCart(BarcodedProduct barcodedProduct) {
		cartBarcodedProducts.add(barcodedProduct); // add product to cart		
	}

	// removes BarcodedItem from cartItems
	public void removeBarcodedItemFromCart(BarcodedItem barcodedItem) {
		cartBarcodedItems.remove(barcodedItem); // remove item from cart
		BarcodedProduct product = ProductDatabases.BARCODED_PRODUCT_DATABASE.get(barcodedItem.getBarcode());
		removeBarcodedProductFromCart(product);
		numberOfItems--;
	}
	
	// removes BarcodedProduct to cartProducts
	// use only within ShoppingCart class
	private void removeBarcodedProductFromCart(BarcodedProduct barcodedProduct) {
		cartBarcodedProducts.remove(barcodedProduct); // remove product from cart		
	}
	// -------------------------------------------------------------------------------------------------------------------------
	
	
	
	
	// PLU CODED PRODUCTS ------------------------------------------------------------------------------------------------------

	public void addPLUItemToCart(PLUCodedItem pluCodedItem) {
		cartPLUItems.add(pluCodedItem); // add item to cart		
		PLUCodedProduct product = ProductDatabases.PLU_PRODUCT_DATABASE.get(pluCodedItem.getPLUCode());
		addPLUProductToCart(product);
		totalActualWeight.add(pluCodedItem.getMass().inGrams());
		numberOfItems++;
	}
	
	private void addPLUProductToCart(PLUCodedProduct pluCodedProduct) {
		cartPLUProducts.add(pluCodedProduct); // add product to cart
	}
	
	// removes BarcodedItem from cartItems
	public void removePLUItemFromCart(PLUCodedItem pluCodedItem) {
		cartPLUItems.remove(pluCodedItem); // remove item from cart
		PLUCodedProduct product = ProductDatabases.PLU_PRODUCT_DATABASE.get(pluCodedItem.getPLUCode());
		removePLUProductFromCart(product);
		numberOfItems--;
	}
	
	// removes BarcodedProduct to cartProducts
	// use only within ShoppingCart class
	private void removePLUProductFromCart(PLUCodedProduct pluCodedProduct) {
		cartPLUProducts.remove(pluCodedProduct); // remove product from cart
	}
	
	// -------------------------------------------------------------------------------------------------------------------------
	
	
	
	// updates the weight that is expected to be in the bagging area
	public void addExpectedWeight(BigDecimal addedWeightInGrams) {
    	totalExpectedWeight = totalExpectedWeight.add(addedWeightInGrams);
	}

	// updates the weight that is expected to be in the bagging area
	public void removeExpectedWeight(BigDecimal removedWeightInGrams) {
    	totalExpectedWeight = totalExpectedWeight.subtract(removedWeightInGrams);
	}
	
	public void addActualWeight(BigDecimal addedWeightInGrams) {
        totalActualWeight = totalActualWeight.add(addedWeightInGrams);

    }
	
	public void removeActualWeight(BigDecimal removedWeightInGrams) {
		totalActualWeight = totalActualWeight.subtract(removedWeightInGrams);
    }
	
	public void balanceWeights() {
		 totalExpectedWeight = totalActualWeight;
	}

	// add the price of the item to the total
	public void addPrice(long addedPrice) {
		totalPrice = totalPrice + addedPrice;
	}

	// remove the price of the item from the total
	public void removePrice(long removedPrice) {
		totalPrice = totalPrice - removedPrice;
	}
		
	public BigDecimal getTotalExpectedWeight() {
        return totalExpectedWeight;
    }
	
	public BigDecimal getTotalActualWeight() {
        return totalActualWeight;
    }

	public long getTotalPrice() {
		return totalPrice;
	}
	
	public int getTotalNumberOfItems() {
		return numberOfItems;
	}
	
	public ArrayList<BarcodedItem> getShoppingCartItems() {
	    	// Returns all the items in the shopping cart; 
	    	// this is a copy of the list, ensuring security of the original list
	    	return new ArrayList<BarcodedItem>(cartBarcodedItems); 
    	}
	
	public ArrayList<BarcodedProduct> getShoppingCartProducts() {
	    	// Returns all the products in the shopping cart; 
	    	// this is a copy of the list, ensuring security of the original list
	    	return new ArrayList<BarcodedProduct>(cartBarcodedProducts); 
    	}
	
	public ArrayList<PLUCodedItem> getShoppingCartPLUItems() {
    	// Returns all the PLU items in the shopping cart; 
    	// this is a copy of the list, ensuring security of the original list
    	return new ArrayList<PLUCodedItem>(cartPLUItems); 
	}
	
	public ArrayList<PLUCodedProduct> getShoppingCartPLUProducts() {
    	// Returns all the PLU items in the shopping cart; 
    	// this is a copy of the list, ensuring security of the original list
    	return new ArrayList<PLUCodedProduct>(cartPLUProducts); 
	}
}
