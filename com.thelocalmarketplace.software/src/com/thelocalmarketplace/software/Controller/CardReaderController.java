/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.io.IOException;

import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.card.*;
import com.jjjwelectronics.card.Card.CardData;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.Payment.CreditCardPayment;
import com.thelocalmarketplace.software.Payment.DebitCardPayment;
import com.thelocalmarketplace.software.*;

public class CardReaderController extends CentralCheckoutStationLogic implements CardReaderListener
{
	private ShoppingCart shoppingCart;
	private final CreditCardPayment creditCardPayment;
	private final DebitCardPayment debitCardPayment;

	public CardReaderController(CentralCheckoutStationLogic logic)
	{
		super(logic);
		
    	this.creditCardPayment = new CreditCardPayment(logic);
   	 	this.debitCardPayment = new DebitCardPayment(logic);
   	 
    	if (this.logic.hardware.getCardReader() != null)
    	{
        	this.logic.hardware.getCardReader() .register(this);
    	}
    	
    	this.shoppingCart = ShoppingCart.getShoppingCart(logic);
	}

	public CardData performAction(Card card, String pin) throws IOException
	{
		if (this.logic.hardware.getCardReader() == null) 
		{
            throw new IllegalStateException("Card reader is not initialized.");
        }
   	 
		MethodsOfCardPayment methodOfCardPayment = logic.getMethodsOfCardPayment();
  	 
    	CardData cardData = null;
    	
    	switch (methodOfCardPayment)
    	{
        	case TAP:
        		cardData = this.logic.hardware.getCardReader().tap(card);
            	break;
        	case SWIPE:
            	cardData = this.logic.hardware.getCardReader().swipe(card);
            	break;
        	case INSERT:
            	cardData = this.logic.hardware.getCardReader().insert(card, pin);
            	break;
        	default:
            	throw new IllegalArgumentException("Unsupported method of card payment: " + methodOfCardPayment);
    	}
    	return cardData;
	}
    
	// Method to removing a card from the reader.
	public void removeCard() 
	{
        if (this.logic.hardware.getCardReader() != null) 
        {
            this.logic.hardware.getCardReader().remove();
            System.out.println("Card has been removed.");
        }
    }
    
	@Override
	public void aCardHasBeenInserted() 
	{
        System.out.println("A card has been inserted.");
        logic.setMethodsOfCardPayment(MethodsOfCardPayment.INSERT);
    }


	@Override
	public void theCardHasBeenRemoved()
	{
	   	 logic.setMethodsOfPayment(CentralCheckoutStationLogic.MethodsOfPayment.DEFAULT);
	   	 logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.DEFAULT);
	   	 logic.setTypeOfCard(CentralCheckoutStationLogic.TypeOfCard.DEFAULT);
	}

	@Override
	public void aCardHasBeenTapped()
	{
    	System.out.println("A card has been tapped.");
    	logic.setMethodsOfCardPayment(MethodsOfCardPayment.TAP);
	}

	@Override
	public void aCardHasBeenSwiped()
	{
    	System.out.println("A card has been swiped.");
    	logic.setMethodsOfCardPayment(MethodsOfCardPayment.SWIPE);
	}
    
	@Override
	public void theDataFromACardHasBeenRead(CardData data)
	{
   	 System.out.println("The data from the card is being read.");
   	 
   	 double transactionAmount = shoppingCart.getTotalPrice();
   	 
   	 //The data can be corrupt therefore we will be validating it.
   	 
    	TypeOfCard cardType = logic.getTypeOfCard();
    	switch (cardType)
    	{
        	case CREDIT_CARD:
            	creditCardPayment.processTransaction(data, transactionAmount);
            	break;
        	case DEBIT_CARD:
            	debitCardPayment.processTransaction(data,transactionAmount);
            	break;
        	default:
            	System.out.println("Unsupported card type.");
            	break;
    	}
	}

	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device)
	{
   	 
	}
}