/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;
import com.tdc.IComponent;
import com.tdc.IComponentObserver;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.BanknoteDispenserObserver;
import com.tdc.banknote.IBanknoteDispenser;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class BanknoteDispenserController extends CentralCheckoutStationLogic implements BanknoteDispenserObserver {
	
	// Instance variables
	private IBanknoteDispenser banknoteDispenser;
	private int maxCapacity;
	private int currentCapacity;
	private double warningEmptyPercentage;    
	private double warningFullPercentage;	  
	
	// Constructor method
	public BanknoteDispenserController(CentralCheckoutStationLogic logic, BigDecimal denomination) {
		
		super(logic);
		
		// access the bank note dispenser of the indicated denomination
		this.banknoteDispenser = this.logic.hardware.getBanknoteDispensers().get(denomination); 
		
		// register this controller to the bank note dispenser
		banknoteDispenser.attach(this);
		
		// get the maximum capacity of the bank note dispenser
		maxCapacity = banknoteDispenser.getCapacity();
		
		// keeps track of the current capacity of the bank note dispenser
		currentCapacity = banknoteDispenser.size();
		
	}
	
		private boolean isBanknoteDispenserAlmostFull() {
			return currentCapacity >= (maxCapacity * warningFullPercentage);
		}
		
		private boolean isBanknoteDispenserAlmostEmpty() {
			return currentCapacity <= (maxCapacity * warningEmptyPercentage);
		}
		
		/**
		 * Check if bank note dispenser is almost full
		 **/
		public void checkIfBanknoteDispenserIsAlmostFull() throws Exception {
			
			if(isBanknoteDispenserAlmostFull()) {
				this.logic.predictIssuesController.predictBanknotesFull();
			}
		}
		
		/**
		 * Check if bank note dispenser is almost empty
		 **/
		public void checkIfBanknoteDispenserIsAlmostEmpty() throws Exception {
			if(isBanknoteDispenserAlmostEmpty()) {
				this.logic.predictIssuesController.predictLowOnBanknotes();
			}
		}

		/**
		 * Alert attendant when bank note dispenser is full
		*/
		@Override
		public void moneyFull(IBanknoteDispenser dispenser) {
			// TODO Auto-generated method stub
			System.out.println("Bank note dispenser is full. Attendant has been alerted.");
			
			// Signal attendant
			this.logic.signalAttendantController.alertAttendant();
		}

		/**
		 * Alert attendant when bank note dispenser is empty
		*/
		@Override
		public void banknotesEmpty(IBanknoteDispenser dispenser) {
			// TODO Auto-generated method stub
			System.out.println("Bank note dispenser is empty. Attendant has been alerted.");
			
			// Signal attendant
			this.logic.signalAttendantController.alertAttendant();
		}

		
		@Override
		public void banknoteAdded(IBanknoteDispenser dispenser, Banknote banknote) {
			// TODO Auto-generated method stub
			currentCapacity = banknoteDispenser.size();
			System.out.println("Bank note added successfully to dispenser.");
		}

		@Override
		public void banknoteRemoved(IBanknoteDispenser dispenser, Banknote banknote) {
			// TODO Auto-generated method stub
			currentCapacity = banknoteDispenser.size();
			System.out.println("Bank note removed successfully from dispenser.");
		}
		
		@Override
		public void banknotesLoaded(IBanknoteDispenser dispenser, Banknote... banknotes) {
			// TODO Auto-generated method stub
			currentCapacity = banknoteDispenser.size();
		}

		@Override
		public void banknotesUnloaded(IBanknoteDispenser dispenser, Banknote... banknotes) {
			// TODO Auto-generated method stub
			currentCapacity = banknoteDispenser.size();
		}
		
		// Setter methods
		/**
		 * Sets the warning percentage which will be used to detect when dispenser is almost empty
		 * 
		 * @param percent 
		 * 	     a double value representing warning percentage. E.g. 0.2 -> 20% low warning percentage
		 **/
		public void setWarningEmptyPercentage(double percent) {
			this.warningEmptyPercentage = percent;
		}
		
		/**
		 * Sets the warning percentage which will be used to detect when dispenser is almost full
		 * 
		 * @param percent 
		 * 		double value representing warning percentage. E.g. 0.8 -> 80% high warning percentage
		 **/
		public void setWarningFullPercentage(double percent) {
			this.warningFullPercentage = percent;
		}
		
		// Getter methods
		public int getMaxCapacity() {
			return maxCapacity;
		}
		
		public int getCurrentCapacity() {
			return currentCapacity;
		}

		// -------------------- No implementation --------------------- //
		
		@Override
		public void enabled(IComponent<? extends IComponentObserver> component) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void disabled(IComponent<? extends IComponentObserver> component) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void turnedOn(IComponent<? extends IComponentObserver> component) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void turnedOff(IComponent<? extends IComponentObserver> component) {
			// TODO Auto-generated method stub
			
		}
}