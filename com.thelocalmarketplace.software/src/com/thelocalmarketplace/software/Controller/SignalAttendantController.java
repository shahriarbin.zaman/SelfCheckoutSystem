/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.util.ArrayList;
import java.util.List;

import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.ISelfCheckoutStation;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class SignalAttendantController extends CentralCheckoutStationLogic {
	
	// Define instance variables
	private boolean activeHelpSession;
	private boolean needsAssistance;
	private boolean requestCleared;
	private AttendantStation attendantStation;
	private List<ISelfCheckoutStation> supportStations;   
	
	public SignalAttendantController(CentralCheckoutStationLogic logic, AttendantStation attendantStation) {
		
		super(logic);
		
		this.activeHelpSession = false;   
		this.needsAssistance = false;
		this.requestCleared = false;
		this.attendantStation = attendantStation;
		this.supportStations = new ArrayList<>();
	}
	
	// Allows customer to signal to system that they would like that attendant's assistance
	public void alertAttendant() {
		needsAssistance = true;
		requestCleared = false;
	}
	
	//**** NEW CHANGES ****//
	// remove isAssistanceNeeded() method and use getNeedsAssistance() getter method
	
	// Check if help session is active
	public boolean isHelpSessionActive() {
		return activeHelpSession;
	}
	
	// Alert attendant only when customer needs help
	public void activateHelpSession() {
		//**** NEW CHANGES ****//
		// use getNeedsAssistance() getter method instead of isAssistanceNeeded() method
		if(getNeedsAssistance()) {
			activeHelpSession = true;
		}
	}
	
	// Signal to attendant station that help is needed
	public void sendAlertToAttendant(ISelfCheckoutStation customerStation) throws Exception {
		
		// Check if customer station is part of supervised stations
		if(attendantStation.supervisedStations().contains(customerStation)) {
			
			// Check if help session is currently active
			if(isHelpSessionActive()) {
				throw new Exception("Help session is currently active. Kindly wait for assistance.");
			}
			
			// add customer station to list of stations that need help
			supportStations.add(customerStation);
			
			// send alert to attendant
			activateHelpSession();
		} else {
			throw new Exception("Customer station is not part of supervised stations");
		}
	}
	
	// Clear request once attendant has attended to the customer
	public void clearCustomerRequest(ISelfCheckoutStation customerStation) throws Exception {
		
		// Check if help station is currently active; 
		// if yes:
		//	  1. Clear request by setting request cleared to true
		//    2. Set needsAssistance and activeHelpSession to false
		if(isHelpSessionActive()) {
			//**** NEW CHANGES ****//
			requestCleared = true;
			needsAssistance = false;
			activeHelpSession = false;
			supportStations.remove(customerStation);
		} else {
			throw new Exception("Customer has already been assisted");
		}
	}
	
	// Add getter method
	public boolean getRequestCleared() {
		return requestCleared;
	}
	
	//**** NEW CHANGES ****//
	// add a getter method for the instance variable needsAssistance
	public boolean getNeedsAssistance() {
		return needsAssistance;
	}
	
	// Add setter method
	public void setRequestCleared(boolean value) {
		this.requestCleared = value;
	}
}