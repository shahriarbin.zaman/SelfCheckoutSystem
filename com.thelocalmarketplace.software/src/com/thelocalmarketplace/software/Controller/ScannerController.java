/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;

import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.card.CardReaderBronze;
import com.jjjwelectronics.card.CardReaderGold;
import com.jjjwelectronics.card.CardReaderSilver;
import com.jjjwelectronics.card.Card.CardData;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodeScannerListener;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.jjjwelectronics.scanner.IBarcodeScanner;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfScanner;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;
import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import powerutility.NoPowerException;

public class ScannerController extends CentralCheckoutStationLogic implements BarcodeScannerListener {

	private ShoppingCart shoppingCart;
	private SessionStateListener state; 
	private SessionStarter sessionManager;
	private IBarcodeScanner bScanner;
	private Mass itemMass;
	private TypeOfScanner scannerUsed = TypeOfScanner.MAIN;
	
	/**
	 * Initializes the ScannerController. 
	 * @param logic The central logic. 
	 */
	public ScannerController(CentralCheckoutStationLogic logic) {
		super(logic);
		
		this.logic.hardware.getMainScanner().register(this);
		this.logic.hardware.getHandheldScanner().register(this);

		bScanner = this.logic.hardware.getHandheldScanner();
		
		shoppingCart = ShoppingCart.getShoppingCart(logic);
		state = SessionStateListener.getSessionStateListener(logic); 
		sessionManager = SessionStarter.getSessionStarter(logic);
	}
	
	/** 
	 * Adds item via barcode. Method implemented so scanner input is not needed. 
	 * @param barcode
	 */
	public void addItemViaBarcode(Barcode barcode) {
		this.aBarcodeHasBeenScanned(bScanner, barcode);
	}
	
	/**
	 * Scans the item and adds the item if verified.
	 * @param barcodeScanner The barcode scanner being used.
	 * @param barcode The barcode that is scanned. 
	 */
	@Override
	public void aBarcodeHasBeenScanned(IBarcodeScanner barcodeScanner, Barcode barcode) {    	
        if(!this.checkSessionStatus()) {
        	throw new InvalidStateSimulationException("Session not started");
        }
        
       
        
        BarcodedItem item = ItemDatabase.getBarcodeItems().get(barcode.toString());
        BarcodedProduct product = ProductDatabases.BARCODED_PRODUCT_DATABASE.get(barcode);
        this.logic.scaleController.getScaleScanningArea().addAnItem(item);
        shoppingCart.addExpectedWeight(item.getMass().inGrams());
        state.setState(SessionState.BLOCKED); 				// BLOCK STATE

        
        if(logic.scaleController.hasExceededLimit()) {
        	shoppingCart.removeExpectedWeight(item.getMass().inGrams());
        	throw new InvalidStateSimulationException("Bulky item, please wait for assistance");
        }
        
        this.itemMass = Mass.ZERO;
        try {
			this.verifyItem(item);
		} catch (Exception e) {
			System.out.println("Item could not be verified. Please wait for assistance.");
			logic.signalAttendantController.alertAttendant();
			
			//throw new InvalidStateSimulationException("Item could not be verified. Please wait for assistance.");
			throw new InvalidStateSimulationException(e.getMessage());
		}
        if(this.logic.scaleController.weightDiscrepancyDetected()) {
        	shoppingCart.removeBarcodedItemFromCart(item);
        	shoppingCart.removeExpectedWeight(item.getMass().inGrams());
        	this.logic.scaleController.getScaleScanningArea().removeAnItem(item);
        	throw new InvalidStateSimulationException("Weight discrepancy detected");
        }
        shoppingCart.addPrice(product.getPrice());
    	System.out.println("Item added successfully. Total items: " + shoppingCart.getTotalNumberOfItems());
       	state.setState(SessionState.NORMAL); 		// UNBLOCK
	}

	/**
	 * Checks if the session is active or not. 
	 */
	private boolean checkSessionStatus() {
        if (sessionManager.hasSessionBeenStarted() == false) {
            System.out.println("No active session. Scanned information is ignored.");
            return false; 
        }
        return true;
	}
	
 
	
	/**
	 * Verifies that there is no weight discrepancy when item is scanned.  
	 * @param barcodedItem
	 * @throws Exception
	 */
	private void verifyItem(BarcodedItem barcodedItem) throws Exception {
		shoppingCart.addActualWeight(barcodedItem.getMass().inGrams()); 
    	if (logic.scaleController.weightDiscrepancyDetected()) {
    		throw new InvalidStateSimulationException("weight diffrence");
    	}
    	shoppingCart.addBarcodedItemToCart(barcodedItem);
    	logic.scaleController.checkWeightDiscrepancy();
    	return;
	}
	
	/**
	 * Switches the scanner, depending on whether the main or handheld scanner is to be used.
	 * The default is the main scanner. 
	 */
	public void switchScanner(TypeOfScanner scannerType) {
	    if (scannerType == null) {
	        System.out.println("Scanner type not set, defaulting to MAIN scanner.");
	        bScanner = this.logic.hardware.getMainScanner(); 	// Default to main scanner
	        this.scannerUsed = TypeOfScanner.MAIN;
	        return;
	    }
	    
	    // Switch on the scannerType if it's not null
	    switch (scannerType) {
	        case MAIN:
	            bScanner = this.logic.hardware.getMainScanner();
	            System.out.println("Using the MAIN scanner.");
		        this.scannerUsed = TypeOfScanner.MAIN;
	            break;
	        case HANDHELD:
	            bScanner = this.logic.hardware.getHandheldScanner();
	            System.out.println("Using the HANDHELD scanner.");
		        this.scannerUsed = TypeOfScanner.HANDHELD;
	            break;
	        default:
	            System.out.println("Unsupported type of scanner: " + scannerType + ". Default set to MAIN scanner.");
	            bScanner = this.logic.hardware.getMainScanner(); 
		        this.scannerUsed = TypeOfScanner.MAIN;
	            break;
	    }
	}
	
	public TypeOfScanner getScannerType() {
		return this.scannerUsed;
	}

	
	
	// -- UNUSED
	
	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

}