
/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;
import java.util.Map;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.Numeral;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.bag.IReusableBagDispenser;
import com.jjjwelectronics.bag.ReusableBag;
import com.jjjwelectronics.bag.ReusableBagDispenserListener;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;

public class BagPurchaserController extends CentralCheckoutStationLogic implements ReusableBagDispenserListener {

	// Instance variables
	private IReusableBagDispenser reusableBagDispenser;
	private ShoppingCart shoppingCart;
	private ReusableBag reusableBag;
	private Mass reusableBagMass;
	private BarcodedItem barcodedReusableBag;
	private Barcode reusableBagBarcode;
	private BigDecimal currentBagPrice;
	private int currentNumberOfReusableBags;
	private BigDecimal reusableBagPrice;
	private BigDecimal cartPrice = BigDecimal.ZERO;
	private boolean operationComplete;
	
	/**
	 * Constructor for BagPurchaserController.
	 * @param logic The central logic
	 */
	public BagPurchaserController(CentralCheckoutStationLogic logic) {
		super(logic);
		this.reusableBagDispenser = logic.hardware.getReusableBagDispenser(); 
		this.shoppingCart = ShoppingCart.getShoppingCart(logic);
		this.cartPrice = new BigDecimal(shoppingCart.getTotalPrice());				  
		
		this.operationComplete = false;
		
		this.reusableBagPrice = new BigDecimal(0.15);
		this.currentBagPrice = BigDecimal.ZERO;
	}

	// Dummy method to create barcode for reusable bag (to be deleted later); just for testing purposes.
	private Numeral[] generateReusableBagBarcode() {
		byte[] byteArr = {5, 2, 7, 1, 0, 9};
		int k = byteArr.length;
		Numeral[] numeral = new Numeral[k];
		
		for(int i = 0; i < k; i++) {
			numeral[i] = Numeral.valueOf(byteArr[i]);
		}
		
		return numeral;
	}
	
	/**
	 * Signals to customer that reusable bags were purchased successfully.
	 */
	private void completeOperation() {
		this.operationComplete = true;
		System.out.println("Reusable Bags added successfully");
	}
	
	/**
	 * Getter method for operation complete status.
	 * @return true if the operation is complete, false otherwise.
	 */
	public boolean getOperationComplete() {
		return operationComplete;
	}
	
	/**
	 * Allows the customer to purchase a number of reusable bags.
	 * @param numberOfBags The number of bags to be purchased. 
	 * @throws EmptyDevice The empty device exception may be thrown if an attempt is made to remove something from an empty device.
	 */
	public void purchaseReusableBags(int numberOfBags) throws EmptyDevice {
		
		if (numberOfBags <= 0) {
			return; 				
		}
		
		try {			
			for(int i = 0; i < numberOfBags; i++) {
				reusableBag = new ReusableBag();
				reusableBagMass = reusableBag.getMass();
				reusableBagBarcode = new Barcode(generateReusableBagBarcode());					 
				barcodedReusableBag = new BarcodedItem(reusableBagBarcode, reusableBagMass);    				
				reusableBagDispenser.dispense();  												 
				shoppingCart.addBarcodedItemToCart(barcodedReusableBag); 
				
				// update expected weight by the expected weight of each bag
				shoppingCart.addExpectedWeight(reusableBagMass.inGrams()); 	
				
				// update the total price based on the price of each reusable bag
				currentBagPrice = currentBagPrice.add(reusableBagPrice);
				cartPrice = cartPrice.add(currentBagPrice);
				
				currentNumberOfReusableBags++; 
			}
			completeOperation();			
		
		} catch(EmptyDevice e) {
			
			// Update cartPrice to original cart price 
			cartPrice = cartPrice.subtract(currentBagPrice);
			
			// Remove bags from cart
			for(int i = 0; i < currentNumberOfReusableBags; i++) {
				shoppingCart.removeBarcodedItemFromCart(barcodedReusableBag);
				shoppingCart.removeExpectedWeight(reusableBagMass.inGrams());
			}
			
			throw e;
		}
		shoppingCart.addPrice(cartPrice.longValue());
	}

	@Override
	public void theDispenserIsOutOfBags() {
		System.out.println("Dispenser has run out of bags. Attendant has been alerted");
	
		logic.signalAttendantController.alertAttendant();
		try {
			logic.signalAttendantController.sendAlertToAttendant(logic.hardware);
		} catch (Exception e) {
			System.out.println("An unexpected error occurred and the alert could not be send to the attendant.");
		}
	}

	
	
	//------------------- No implementation -------------------//
	
	@Override
	public void aBagHasBeenDispensedByTheDispenser() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void bagsHaveBeenLoadedIntoTheDispenser(int count) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

}