/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/


package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;

import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.card.CardReaderBronze;
import com.jjjwelectronics.card.CardReaderGold;
import com.jjjwelectronics.card.CardReaderSilver;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleListener;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.jjjwelectronics.scale.IElectronicScale;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.SelfCheckoutStationBronze;
import com.thelocalmarketplace.hardware.SelfCheckoutStationGold;
import com.thelocalmarketplace.hardware.SelfCheckoutStationSilver;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.OwnBagAdder;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import powerutility.PowerGrid;

import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;
import ca.ucalgary.seng300.simulation.SimulationException;


/* A public class implementing the electronic scale listener. Implements the feature of the scale listener 
 * that determines if a weight discrepancy has occurred. For Iteration 1, the system checks for a 
 * weight discrepancy whenever the "theMassOnTheScaleHasChanged" event occurs.
 */
public class ScaleController extends CentralCheckoutStationLogic implements ElectronicScaleListener {
	
    private SessionStateListener sessionStateListener;
	private ShoppingCart shoppingCart;
	
	private AbstractElectronicScale scale;
	
	private BigDecimal sensitivity; 
	private BigDecimal massLimit;
	private boolean weightDiscrepancy = false;
	private boolean exceededLimit = false;
	private OwnBagAdder ownBags;
	
	private Mass actualMassOfItem;
	private BigDecimal itemWeight;
	PowerGrid grid = PowerGrid.instance();

	public ScaleController(CentralCheckoutStationLogic logic) {
		super(logic);
		
		this.logic.hardware.getBaggingArea().register(this);
		
		this.sensitivity = logic.hardware.getBaggingArea().getSensitivityLimit().inGrams();
		
		this.massLimit = logic.hardware.getBaggingArea().getMassLimit().inGrams();
		
		this.sessionStateListener = SessionStateListener.getSessionStateListener(logic);
		
		this.shoppingCart = ShoppingCart.getShoppingCart(logic);
		
		this.actualMassOfItem = new Mass(BigDecimal.ZERO);
		
		this.ownBags = new OwnBagAdder(logic);
		
		this.initializeScanningAreaScale();
		if (this.scale != null) {
			this.scale.register(this);
			this.scale.plugIn(grid);
			this.scale.turnOn();
		}
	}
	
	/**
	 * Initializes the scale to match the colour of checkout station being used.  
	 */
	private void initializeScanningAreaScale() {
	    String scaleType = logic.getScanningAreaType();
	    
	    if (scaleType == null) {
	        System.out.println("Scanning area scale colour not set, defaulting to Bronze scale.");
	        this.scale = new ElectronicScaleBronze(); 	
	        return;
	    }
	    
	    switch (scaleType) {
	        case "Bronze":
	            this.scale = new ElectronicScaleBronze();
	            break;
	        case "Silver":
	            this.scale = new ElectronicScaleSilver();
	            break;
	        case "Gold":
	            this.scale = new ElectronicScaleGold();
	            break;
	        default:
	            System.out.println("Unsupported scanning area scale type: " + scaleType + ". Default set to Bronze type.");
	            this.scale = new ElectronicScaleBronze(); 
	            break;
	    }
	}

	
	public AbstractElectronicScale getScaleScanningArea() {
		return this.scale;
	}
	
	/**
	 * Announces that the event that some item as been added or removed to/from the electronic scale has occurred.
	 * At the occurrence of the event, the system must check for a weight discrepancy, except for when the user places bags. 
	 */
	@Override
	public void theMassOnTheScaleHasChanged(IElectronicScale scale, Mass mass) {
		if (this.ownBags.getAddBagState() == false) {
			try {
				checkWeightDiscrepancy();
				checkMassLimit(mass);
				if (weightDiscrepancy == true) {
					shoppingCart.addActualWeight(mass.inGrams());		
				}
			} catch (OverloadedDevice e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();				
			}
		}
	}	
	
	/**
	 * Evaluates whether there is a discrepancy between the actual weight on electronic scale 
	 * and the weight that is expected to be on the electronic scale. The result of the evaluation 
	 * sets weightDiscrepancy to false is there is no discrepancy, sets weightDiscrepancy to true otherwise.
	 * Upon a discrepancy, the session will be blocked. 
	 * @throws OverloadedDevice
	 * @throws Exception
	 */
	public void checkWeightDiscrepancy() throws OverloadedDevice, Exception {
		BigDecimal actualWeight = this.shoppingCart.getTotalActualWeight();
		BigDecimal expectedWeight = this.shoppingCart.getTotalExpectedWeight(); 
		if(Math.abs(expectedWeight.subtract(actualWeight).intValue()) >= this.sensitivity.intValue()) {
			this.weightDiscrepancy = true; 
			this.sessionStateListener.setState(SessionState.BLOCKED); // block session
			
			this.getAttendantApproval(logic.signalAttendantController);
		}
		else {
			this.weightDiscrepancy = false;
			this.sessionStateListener.setState(SessionState.NORMAL); // unblock session
		}
	}
	
	// Testing
	public void addWeight(BigDecimal weight) {
		this.shoppingCart.addActualWeight(weight);
	}
	
	
	// -- BAGS TOO HEAVY
	/**
	 * Checks for whether the added mass will exceed the allowed mass limit in the bagging area.
	 * @param mass The added mass. 
	 * @throws Exception Exception can occur when system tries to send alert to attendant. 
	 */
	public void checkMassLimit(Mass mass) throws Exception {
		if (mass.inGrams().compareTo(this.massLimit) > 0) {
			this.exceededLimit = true;
			this.sessionStateListener.setState(SessionState.BLOCKED); // block session
			System.out.println("The mass exceeds the allowed limit. Please wait for assistance to handle bulky item.");
			this.handleBulkyItem();
			return;
		}
		this.exceededLimit = false;
	}
	// -----------------
	
	public boolean hasExceededLimit() {
		return this.exceededLimit;
	}

	/**
	 * Returns a boolean value that is true if a weight discrepancy is detected, and false otherwise.
	 * @return true if there is a weight discrepancy, and false otherwise.
	 */
	public boolean weightDiscrepancyDetected() { 
		return this.weightDiscrepancy; 
	}
	
	/**
	 * Notifies the attendant and if approval is given, session is unblocked. 
	 * @param signalAttendantController The controller that signals the attendant.
	 * @throws Exception May be thrown while sending alert to attendant. 
	 */
	private void getAttendantApproval(SignalAttendantController signalAttendantController) throws Exception {
		if (logic.signalAttendantController.getRequestCleared()) {
			this.sessionStateListener.setState(SessionState.NORMAL); // unblock session
			return;
		}
		signalAttendantController.alertAttendant();
		signalAttendantController.sendAlertToAttendant(logic.hardware);
	}
	
	public void checkClearRequest() {
		if (logic.signalAttendantController.getRequestCleared()) {
			this.weightDiscrepancy = false;
			this.exceededLimit = false;
			this.sessionStateListener.setState(SessionState.NORMAL); // unblock session
			this.shoppingCart.balanceWeights();
		}
	}	
	
	
	
	// -- HANDLE BULKY ITEM
	/**
	 * Handles the situation when a bulky item is being added. 
	 * @throws Exception May be thrown while sending alert to attendant.
	 */
	public void handleBulkyItem() throws Exception {
		this.sessionStateListener.setState(SessionState.BLOCKED); // block session
		this.getAttendantApproval(logic.signalAttendantController);
		this.sessionStateListener.setState(SessionState.NORMAL); // unblock session
	}
	
	// ---------------------
	
	/**
	 * Set the scanning area scale.
	 * @param scale
	 */
	public void setScaleScanningArea(AbstractElectronicScale scale) {
        this.scale = scale;
    }
	
	/**
	 * Initialize the own bag adder. 
	 * @param ownBags
	 */
	public void setOwnBagAdder(OwnBagAdder ownBags) {
        this.ownBags = ownBags;
    }
	
	
	// -- UNUSED
	
	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void theMassOnTheScaleHasExceededItsLimit(IElectronicScale scale) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void theMassOnTheScaleNoLongerExceedsItsLimit(IElectronicScale scale) {
		// TODO Auto-generated method stub
		
	}
	
}