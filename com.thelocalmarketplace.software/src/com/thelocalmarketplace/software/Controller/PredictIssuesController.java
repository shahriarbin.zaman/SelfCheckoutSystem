/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;

public class PredictIssuesController extends CentralCheckoutStationLogic {
	
	// Instance variable 
	private SessionStarter session;
	private boolean lowOnPaper;
	private boolean lowOnInk;
	private boolean lowOnBanknotes;
	private boolean lowOnCoins;
	private boolean coinsAlmostFull;
	private boolean banknotesAlmostFull;
	private double warningLowInkPercentage;
	private int printerInkCapacity;
	

	public PredictIssuesController(CentralCheckoutStationLogic logic, SessionStarter session) {
		
		super(logic);
		
		this.session = session;
		this.lowOnPaper = false;
		this.lowOnPaper = false;
		this.lowOnCoins = false;
		this.coinsAlmostFull = false;
		this.banknotesAlmostFull = false;
		this.printerInkCapacity = 100;
	}
	
	// Check if low on paper
	public void predictLowOnPaper() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only run when session is inactive.");
		}
		
		if(this.logic.paperManager.isPaperLow()) {
			lowOnPaper = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}
	}
	
	// Predict if low on coins
	public void predictLowOnCoins() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only when session is inactive.");
		}
		
		// disable station for maintenance if the station is low on coins
		if(logic.cashAndCoinManager.isCoinOneLow() || logic.cashAndCoinManager.isCoinFiveLow() ||
				logic.cashAndCoinManager.isCoinTenLow() || logic.cashAndCoinManager.isCoinTwentyFiveLow() ||
				logic.cashAndCoinManager.isCoinFiftyLow()) {
			lowOnCoins = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}
		
	}
	
	// Predict if low on bank notes
	public void predictLowOnBanknotes() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only run when session is inactive.");
		}
		
		// signal to attendant to disable station for maintenance if the station is low on bank notes
		if(logic.cashAndCoinManager.isCashLow()) {
			lowOnBanknotes = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}
	}
	
	// Predict if coin dispenser will be full
	public void predictCoinsFull() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only run when session is inactive.");
		}
		
		// signal to attendant to disable station for maintenance if station is almost full of coins
		if(logic.cashAndCoinManager.isCoinOneAlmostFull() || logic.cashAndCoinManager.isCoinFiveAlmostFull() ||
				logic.cashAndCoinManager.isCoinTenAlmostFull()|| logic.cashAndCoinManager.isCoinTwentyFiveAlmostFull() ||
				logic.cashAndCoinManager.isCoinFiftyAlmostFull()) {
			coinsAlmostFull = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}	
	}
	
	// Predict if coin dispenser will be full
	public void predictBanknotesFull() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only run when session is inactive.");
		}
		
		// signal to attendant to disable station for maintenance if station is almost full of bank notes
		if(this.logic.cashAndCoinManager.isCashAlmostFull()) {
			banknotesAlmostFull = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}
	}
	
	// Predict if station is low on ink
	public void predictLowOnInk() throws Exception {
		
		// Check if session is active
		if(session.hasSessionBeenStarted()) {
			throw new Exception("Prediction engine should only run when session is inactive.");
		}
		
		if(this.logic.inkManager.getInkLevel() <= (printerInkCapacity * warningLowInkPercentage)) {
			lowOnInk = true;
			disableStationOnApproval(this.logic.signalAttendantController);
		}
	}
	
	//**** NEW CHANGES ****//
	// Credits: @Khushi Choksi and @Doug Strueby
	// I reference @Khushi's code and used @Doug's changes to implement disabling the station after getting attendant's approval
	// Checks if the help session is active
	//		if yes: it will disable the self-checkout station and clear the request on the station
	//		else: it will alert and send an alert to the attendant
	private void disableStationOnApproval(SignalAttendantController signalAttendantController) throws Exception {
		if (this.logic.signalAttendantController.isHelpSessionActive()) {
			this.logic.stationDisabler.disableStation(this.logic);
			this.logic.signalAttendantController.clearCustomerRequest(this.logic.hardware);
			return;
		}
		
		signalAttendantController.alertAttendant();
		signalAttendantController.sendAlertToAttendant(this.logic.hardware);
	}
	
	// Setter methods
	/**
	 * @param percent
	 * 		a double representing the warning percentage e.g. percent = 0.2 -> 20% warning percentage
	 * */
	public void setWarningLowInkPercentage(double percent) {
		this.warningLowInkPercentage = percent;
	}
	
	// Getter methods
	public boolean getLowOnPaper() {
		return lowOnPaper;
	}
	
	public boolean getLowOnCoins() {
		return lowOnCoins;
	}
	
	public boolean getLowOnBanknotes() {
		return lowOnBanknotes;
	}
	
	public boolean getCoinsAlmostFull() {
		return coinsAlmostFull;
	}
	
	public boolean getBanknotesAlmostFull() {
		return banknotesAlmostFull;
	}
	
	public boolean getLowOnInk() {
		return lowOnInk;
	}
	
	// Setter methods
	public void setLowOnPaper(boolean lowOnPaper) {
		this.lowOnPaper = lowOnPaper;
	}
	
	public void setLowOnCoins(boolean lowOnCoins) {
		this.lowOnCoins = lowOnCoins;
	}
	
	public void setLowOnBanknotes(boolean lowOnBanknotes) {
		this.lowOnBanknotes = lowOnBanknotes;
	}
	
	public void setCoinsAlmostFull(boolean coinsAlmostFull) {
		this.coinsAlmostFull = coinsAlmostFull;
	}
	
	public void setBanknotesAlmostFull(boolean banknotesAlmostFull) {
		this.banknotesAlmostFull = banknotesAlmostFull;
	}
	
	public void setLowOnInk(boolean lowOnInk) {
		this.lowOnInk = lowOnInk;
	}
}