/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;
import java.util.Currency;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.OverloadedDevice;
import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.IComponent;
import com.tdc.IComponentObserver;
import com.tdc.NoCashAvailableException;
import com.tdc.coin.CoinValidator;
import com.tdc.coin.CoinValidatorObserver;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;

public class CoinController extends CentralCheckoutStationLogic implements CoinValidatorObserver {

	// Instance variables
	private BigDecimal amountDue;
	private BigDecimal currentAmountPaid;
	private BigDecimal remainingBalance;
	private Currency currency;
	private boolean sessionSuspended;
	private Exception encounteredException;

	// Constructor method
	public CoinController(CentralCheckoutStationLogic logic) {

		super(logic);
		
		// set the total amount due to the cart's total price
		amountDue = new BigDecimal(ShoppingCart.getShoppingCart(this.logic).getTotalPrice());
		
		// initialise the current amount paid to zero
		currentAmountPaid = BigDecimal.ZERO;
		
		//**** NEW CHANGES ****//
		// initialise remaining balance to the cart's total price
		remainingBalance = amountDue.abs();
		
		//**** NEW CHANGES ****//
		// initialise the currency of the transaction
		currency = Currency.getInstance("CAD");
		
		// Registers the CoinController to receive event notifications from coin validator
		this.logic.hardware.getCoinValidator().attach(this);
	}
	
	public void updateAmountDue() {
		amountDue = new BigDecimal(ShoppingCart.getShoppingCart(this.logic).getTotalPrice());
		remainingBalance = amountDue.abs();
	}
	
	// Getter methods
	//**** NEW CHANGES ****//
	public BigDecimal getAmountDue() {
		return amountDue;
	}
	
	public BigDecimal getCurrentAmountPaid() {
		return currentAmountPaid;
	}
	
	public BigDecimal getRemainingBalance() {
		return remainingBalance;
	}
	
	public boolean getSessionSuspended() {
		return sessionSuspended;
	}
	
	public Exception getEncounteredException() {
		return encounteredException;
	}

	@Override
	public void validCoinDetected(CoinValidator validator, BigDecimal value) {
		
		// TODO Auto-generated method stub
		System.out.print("Valid coin detected");
		
		// update the current amount paid
		currentAmountPaid = currentAmountPaid.add(value);
		
		// update the amount due from the customer
		remainingBalance = remainingBalance.subtract(value);
		
		// check if the customer paid the amount due in full
		// if an error occurs, transaction session is suspended and an exception is thrown
		try {
			this.logic.changeController.checkIfAmountPaidInFull(currentAmountPaid, remainingBalance, 
					amountDue, currency);
		} catch (EmptyDevice | OverloadedDevice | NoCashAvailableException | DisabledException | CashOverloadException e) {
			// TODO Auto-generated catch block
			sessionSuspended = true;
			encounteredException = e;
		}
		
	}

	@Override
	public void invalidCoinDetected(CoinValidator validator) {
		// TODO Auto-generated method stub
		System.out.println("Invalid coin detected");
		
	}
	
// ---------------------------- No implementation ----------------------------- //
	@Override
	public void enabled(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disabled(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnedOn(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnedOff(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}
}