/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.OverloadedDevice;
import com.tdc.CashOverloadException;
import com.tdc.DisabledException;
import com.tdc.NoCashAvailableException;
import com.tdc.banknote.Banknote;
import com.tdc.banknote.IBanknoteDispenser;
import com.tdc.coin.Coin;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.GUI.SystemListener;

/* Note: logic.hardware.getBanknoteDispensers() will cause a power exception
 *  pluggin in, activating. and enabling the BanknoteDispensationSlot object in the main GUI function
 *  does not solve this issue 
 */
public class ChangeController extends CentralCheckoutStationLogic {
	
	// Instance variables for CashController class
	BigDecimal max;
	int currentLength;
	BigDecimal changeDue;
	
	// Constructor method for CashController class
	public ChangeController(CentralCheckoutStationLogic logic) {
		
		super(logic);
		
		this.changeDue = BigDecimal.ZERO;
	}
	
	private void printReceipt() throws EmptyDevice, OverloadedDevice {
		System.out.println("Change due dispensed successfully."); // if change is dispensed in full, print "success" message
		this.logic.receiptPrinterController.print();
	}
	private void printReceipt(String totalMoneyGiven,String changeDue) throws EmptyDevice, OverloadedDevice {
		System.out.println("Change due dispensed successfully."); // if change is dispensed in full, print "success" message
		this.logic.receiptPrinterController.print(totalMoneyGiven,changeDue);
	}
	
	// Method to check if amount due is paid in full
	public void checkIfAmountPaidInFull(BigDecimal currentAmountPaid, BigDecimal remainingBalance, 
			BigDecimal amountDue, Currency currency) throws EmptyDevice, OverloadedDevice, 
	 NoCashAvailableException, DisabledException, CashOverloadException {
		
		// if amount is paid in full, display "success" message
		if(remainingBalance.compareTo(BigDecimal.ZERO) == 0) {
			System.out.print("Amount paid in full. Payment successful.");
			SystemListener.aPaymentHasBeenMade(); // payment successful
			// print out receipt
			printReceipt();
		}  
		
		// if there is a balance to pay, display amount due
		else if (remainingBalance.compareTo(BigDecimal.ZERO) > 0) {
			System.out.println("Amount Due: " + currency + " " + remainingBalance); // Print out current amount due 
		}
	
		// if the amount paid is greater than the amount due, dispense change and print out receipt
		else {
			changeDue = currentAmountPaid.subtract(amountDue);
			System.out.println("Change Due: " + currency + " " + changeDue);
			SystemListener.aPaymentHasBeenMade(); // payment successful
			//dispenseChange(changeDue, currency); causes errors
			printReceipt(currency + " " + currentAmountPaid,currency + " " + changeDue);
		}	
	}
	
	// Dispense change due 
	public void dispenseChange(BigDecimal changeDue, Currency currency) throws NoCashAvailableException, DisabledException, CashOverloadException, EmptyDevice, OverloadedDevice {
		
		BigDecimal[] banknoteDenominations = this.logic.hardware.getBanknoteDenominations(); // get bank note denominations
		Arrays.sort(banknoteDenominations);
		currentLength =  banknoteDenominations.length;
		
		// Dispense change as bank notes if the change due >= minimum bank note denomination
		while(changeDue.compareTo(banknoteDenominations[0]) >= 0) {
			
			max = banknoteDenominations[currentLength - 1]; 	// get the maximum denomination
			
			// if changeDue >= maxDenomination, emit bank note as change
			if(changeDue.compareTo(max) >= 0) {
				
				// emit a single bank note of specific denomination from the dispenser
				this.logic.hardware.getBanknoteDispensers().get(max).emit();
				
				// checks to see if banknote dispenser is empty; if yes, attendant will be alerted 
				// checkout station will be disabled for maintenance
				// clears request once station is disabled
				if(this.logic.signalAttendantController.getRequestCleared() == false) {
					 this.logic.stationDisabler.disableStation(logic);
					 this.logic.signalAttendantController.setRequestCleared(true);
				 }
				
				this.logic.hardware.getBanknoteOutput().receive(new Banknote(currency, max)); 	
						
				// update change due
				changeDue.subtract(max);	 					
			} else {
				currentLength--;
			}
		}
				
		this.logic.hardware.getBanknoteOutput().dispense(); 		// dispense bank notes as change
	
		if(changeDue.compareTo(BigDecimal.ZERO) == 0) {
			System.out.println("Change due dispensed successfully."); // if change is dispensed in full, print "success" message
			this.logic.receiptPrinterController.print();
		} else {
			// dispense remaining change as coins
			dispenseChangeAsCoins(changeDue);
		}
	}
	
	// Method to dispense change as coins
	public void dispenseChangeAsCoins(BigDecimal changeDue) throws CashOverloadException, NoCashAvailableException, DisabledException, EmptyDevice, OverloadedDevice {

		List<BigDecimal> denominations = this.logic.hardware.getCoinDenominations();
		Collections.sort(denominations, Collections.reverseOrder()); // sort coin denominations
		
		int counter = 0;
		
		while(changeDue.compareTo(BigDecimal.ZERO) > 0) {
			 
			 max = denominations.get(counter);								// get the maximum denomination
			 
			 if(changeDue.compareTo(max) >= 0) {
				
				 this.logic.hardware.getCoinDispensers().get(max).emit(); // emit single coin of specific denomination
				 
				 // if coin dispenser is empty, attendant is alerted
				 // checkout station will be disabled for maintenance
				 // clears request once station is disable
				 if(this.logic.signalAttendantController.getRequestCleared() == false) {
					 this.logic.stationDisabler.disableStation(logic);
					 this.logic.signalAttendantController.setRequestCleared(true);
				 }
				 
				 changeDue = changeDue.subtract(max);						 // update the change due
			
			 } else {
				 counter++;													 // get the next maximum denomination
			 }
		 }
		
		printReceipt();
	}	
	
	// Getter methods
	public BigDecimal getChangeDue() {
		return changeDue;
	}
	
}	