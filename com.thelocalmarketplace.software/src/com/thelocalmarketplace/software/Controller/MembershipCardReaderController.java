/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.io.IOException;
import java.math.BigDecimal;

import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.card.Card;
import com.jjjwelectronics.card.CardReaderListener;
import com.jjjwelectronics.card.Card.CardData;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.MethodsOfCardPayment;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic.TypeOfCard;
import com.thelocalmarketplace.software.Controller.*;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;
import com.thelocalmarketplace.software.Payment.CreditCardPayment;
import com.thelocalmarketplace.software.Payment.DebitCardPayment;


import ca.ucalgary.seng300.simulation.NullPointerSimulationException;
import powerutility.NoPowerException;

import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodeScannerListener;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.jjjwelectronics.scanner.IBarcodeScanner;


public class MembershipCardReaderController extends CentralCheckoutStationLogic implements CardReaderListener, BarcodeScannerListener {
	
	private SessionStarter sessionManager;
	private SessionStateListener state;
	private ShoppingCart shoppingCart;
	private Mass itemMass;
	private IBarcodeScanner bScanner;
	
	public MembershipCardReaderController(CentralCheckoutStationLogic logic)
	{
		super(logic);
		
		this.logic.hardware.getMainScanner().register(this);
		this.logic.hardware.getHandheldScanner().register(this);
    	
    	if (this.logic.hardware.getCardReader() != null)
    	{
        	this.logic.hardware.getCardReader().register(this);
    	}
    	
    	shoppingCart = ShoppingCart.getShoppingCart(logic);
    	state = SessionStateListener.getSessionStateListener(logic); 
    	sessionManager = SessionStarter.getSessionStarter(logic);
	}
	
	public CardData performAction(TypeOfCard typeOfCard, Card card) throws IOException
	{
		if (this.logic.hardware.getCardReader() == null) 
		{
            throw new IllegalStateException("Card reader is not initialized.");
            
        }

		if(typeOfCard != TypeOfCard.MEMBERSHIP_CARD){
			throw new IOException("Only membership card is accepted");
		}
		
		if(!logic.getMembershipCards().containsKey(card.number)) {
			throw new IOException("Members not found");
		}
		
		if(logic.getMethodsOfCardPayment().toString() != "SWIPE") {
			throw new IOException("Only swipe is allowed for the membership card");
		}
		
		logic.setCurrentMemberShipCard(card.number);
		
    	return this.logic.hardware.getCardReader().swipe(card);
	}
	
	/**
	 * Scans the card and adds the card if verified.
	 * @param barcodeScanner The barcode scanner being used.
	 * @param barcode The barcode that is scanned. 
	 */
	@Override
	public void aBarcodeHasBeenScanned(IBarcodeScanner barcodeScanner, Barcode barcode) {    	
        this.checkSessionStatus();
        
        state.setState(SessionState.BLOCKED); 				// BLOCK STATE
        
        if(!logic.getMembershipCards().containsKey(barcode.toString())) {
        	System.out.println("Membership card could not be verified.");
        }
        
        logic.setCurrentMemberShipCard(barcode.toString());
    	
    	System.out.println("Membership card successfully verified");
  
       	state.setState(SessionState.NORMAL); 				// UNBLOCK
	}
	
	/**
	 * Checks if the session is active or not. 
	 */
	private void checkSessionStatus() {
        if (sessionManager.hasSessionBeenStarted() == false) {
            System.out.println("No active session. Scanned information is ignored.");
            return; 
        }
	}
	
	
	/**
	 * Switches the scanner, depending on whether the main or handheld scanner is to be used.
	 * Defaults to the main scanner if scannerType is not set or is null.
	 */
	public void switchScanner(TypeOfScanner scannerType) {
	    if (scannerType == null) {
	        System.out.println("Scanner type not set, defaulting to MAIN scanner.");
	        bScanner = this.logic.hardware.getMainScanner(); 	
	        return;
	    }
	    
	    // Switch on the scannerType if it's not null
	    switch (scannerType) {
	        case MAIN:
	            bScanner = this.logic.hardware.getMainScanner();
	            System.out.println("Using the MAIN scanner.");
	            break;
	        case HANDHELD:
	            bScanner = this.logic.hardware.getHandheldScanner();
	            System.out.println("Using the HANDHELD scanner.");
	            break;
	        default:
	            System.out.println("Unsupported type of scanner: " + scannerType + ". Default set to MAIN scanner.");
	            bScanner = this.logic.hardware.getMainScanner(); 
	            break;
	            
	    }
	}

	
	
	@Override
	public void aCardHasBeenInserted()
	{
		System.out.println("Membership card can not be inserted.");
    }


	@Override
	public void theCardHasBeenRemoved()
	{
	   	 logic.setMethodsOfPayment(CentralCheckoutStationLogic.MethodsOfPayment.DEFAULT);
	   	 logic.setMethodsOfCardPayment(CentralCheckoutStationLogic.MethodsOfCardPayment.DEFAULT);
	   	 logic.setTypeOfCard(CentralCheckoutStationLogic.TypeOfCard.DEFAULT);
	}

	@Override
	public void aCardHasBeenTapped()
	{
    	System.out.println("Membership card can not be tapped.");
	}

	@Override
	public void aCardHasBeenSwiped()
	{
    	System.out.println("A card has been swiped.");
    	logic.setMethodsOfCardPayment(MethodsOfCardPayment.SWIPE);
	}
    
	@Override
	public void theDataFromACardHasBeenRead(CardData data)
	{
   	 System.out.println("The data from the card is being read.");
   	 
   	 
   	 
   	 	//The data can be corrupt therefore we will be validating it.
   	 
		TypeOfCard cardType = logic.getTypeOfCard();
		switch (cardType)
		{
	    	case MEMBERSHIP_CARD:
	    		System.out.println("Membership card read successfully.");
	        	break;
	    	default:
	        	System.out.println("Unsupported card type.");
	        	break;
		}
	}

	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device)
	{
   	 
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device)
	{
   	 
	}
}