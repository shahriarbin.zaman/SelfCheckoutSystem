/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;

import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scanner.Barcode;
import com.jjjwelectronics.scanner.BarcodedItem;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedItem;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.hardware.PriceLookUpCode;
import com.thelocalmarketplace.hardware.external.ProductDatabases;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.SessionStarter;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.GUI.ItemDatabase;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import ca.ucalgary.seng300.simulation.InvalidArgumentSimulationException;
import ca.ucalgary.seng300.simulation.InvalidStateSimulationException;

public class PLUController extends CentralCheckoutStationLogic {

	private SessionStarter sessionManager;
	private ShoppingCart shoppingCart;
	private SessionStateListener state; 

	
	PriceLookUpCode priceLookUpCode;


	public PLUController(CentralCheckoutStationLogic logic) {
		super(logic);
		
		sessionManager = SessionStarter.getSessionStarter(logic);
		
		shoppingCart = ShoppingCart.getShoppingCart(logic);
		
		state = SessionStateListener.getSessionStateListener(logic); 

	}
	
	
	public boolean addItemByPLU(PriceLookUpCode code) {
		if(!this.checkSessionStatus()) {
			return false;
		}
		
		if (code == null) {
			throw new NullPointerException("PLU code is null. Please wait for assistance.");
		}
		
		
		
		this.priceLookUpCode = code;
		
		PLUCodedItem item = ItemDatabase.getPLUProducts().get(code.toString());
		PLUCodedProduct product = ProductDatabases.PLU_PRODUCT_DATABASE.get(code);
		
		this.logic.scaleController.getScaleScanningArea().addAnItem(item);
		shoppingCart.addExpectedWeight(item.getMass().inGrams());
		state.setState(SessionState.BLOCKED); 				// BLOCK STATE
        
        if(logic.scaleController.hasExceededLimit()) {
        	shoppingCart.removeExpectedWeight(item.getMass().inGrams());
			return false;
		}
        
        try {
			this.verifyItem(item);
		} catch (Exception e) {
			System.out.println("Item could not be verified. Please wait for assistance.");
			logic.signalAttendantController.alertAttendant();
			this.logic.scaleController.getScaleScanningArea().removeAnItem(item);
			return false;
		}
        if(this.logic.scaleController.weightDiscrepancyDetected()) {
        	shoppingCart.removePLUItemFromCart(item);
        	shoppingCart.removeExpectedWeight(item.getMass().inGrams());
        	this.logic.scaleController.getScaleScanningArea().removeAnItem(item);
        	return false;
        }
        shoppingCart.addPrice(product.getPrice());
        System.out.println("Item added successfully. Total items: " + shoppingCart.getTotalNumberOfItems());
       	state.setState(SessionState.NORMAL); 					// UNBLOCK
       	return true;
	}
	
	private boolean checkSessionStatus() throws InvalidStateSimulationException{
        if (sessionManager.hasSessionBeenStarted() == false) {
            System.out.println("No active session. Scanned information is ignored.");
            return false; 
        }
        return true;
	}
		
	private void verifyItem(PLUCodedItem pluCodedItem) throws Exception {
		shoppingCart.addActualWeight(pluCodedItem.getMass().inGrams()); 
		if(logic.scaleController.weightDiscrepancyDetected()) {
			throw new InvalidStateSimulationException("weight diffrence");
		}
		shoppingCart.addPLUItemToCart(pluCodedItem);
		logic.scaleController.checkWeightDiscrepancy();
		
	}

}