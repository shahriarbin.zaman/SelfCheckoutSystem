/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.math.BigDecimal;

import com.tdc.IComponent;
import com.tdc.IComponentObserver;
import com.tdc.coin.Coin;
import com.tdc.coin.CoinDispenserObserver;
import com.tdc.coin.ICoinDispenser;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

public class CoinDispenserController extends CentralCheckoutStationLogic implements CoinDispenserObserver {
	
	// Instance variables
	private ICoinDispenser coinDispenser;
	private int maxCapacity;
	private int currentCapacity;
	private double warningEmptyPercentage;     
	private double warningFullPercentage;	

	// Constructor method
	public CoinDispenserController(CentralCheckoutStationLogic logic, BigDecimal denomination) {
		
		super(logic);
		
		// access the coin note dispenser of the indicated denomination
		this.coinDispenser = this.logic.hardware.getCoinDispensers().get(denomination);
		
		// register this controller to the coin dispenser
		coinDispenser.attach(this);
		
		// get the maximum capacity of the coin dispenser
		maxCapacity = coinDispenser.getCapacity();
		
		// get the current capacity of the coin dispenser
		currentCapacity = coinDispenser.size();
		
	}
	
	public boolean isCoinDispenserAlmostFull() {
		return currentCapacity >= (maxCapacity * warningFullPercentage);
	}
	
	public boolean isCoinDispenserAlmostEmpty() {
		return currentCapacity <= (maxCapacity * warningEmptyPercentage);
	}
	
	/**
	 * Check if coin dispenser is almost full
	 **/
	public void checkIfCoinDispenserIsAlmostFull() throws Exception {
		
		if(isCoinDispenserAlmostFull()) {
			this.logic.predictIssuesController.predictCoinsFull();
		}
	}
	
	/**
	 * Check if coin dispenser is almost empty
	 **/
	public void checkIfCoinDispenserIsAlmostEmpty() throws Exception {
		if(isCoinDispenserAlmostEmpty()) {
			this.logic.predictIssuesController.predictLowOnCoins();
		}
	}
	
	// Alert attendant when coin dispenser is full
	@Override
	public void coinsFull(ICoinDispenser dispenser) {
		// TODO Auto-generated method stub
		System.out.println("Coin dispenser is full. Attendant has been alerted.");
		 
		// Signal attendant
		this.logic.signalAttendantController.alertAttendant();
	}

	// Alert attendant when coin dispenser is empty
	@Override
	public void coinsEmpty(ICoinDispenser dispenser) {
		// TODO Auto-generated method stub
		System.out.println("Coin dispenser is empty. Attendant has been alerted");
		
		// Signal attendant
		this.logic.signalAttendantController.alertAttendant();
	}
	
	@Override
	public void coinAdded(ICoinDispenser dispenser, Coin coin) {
		// TODO Auto-generated method stub
		currentCapacity = coinDispenser.size();
		System.out.println("Coin added successfully.");
	}

	@Override
	public void coinRemoved(ICoinDispenser dispenser, Coin coin) {
		// TODO Auto-generated method stub
		currentCapacity = coinDispenser.size();
		System.out.println("Coin removed successfully.");
	}
	@Override
	public void coinsLoaded(ICoinDispenser dispenser, Coin... coins) {
		// TODO Auto-generated method stub
		currentCapacity = coinDispenser.size();
	}

	@Override
	public void coinsUnloaded(ICoinDispenser dispenser, Coin... coins) {
		// TODO Auto-generated method stub
		currentCapacity = coinDispenser.size();
	}
	
	// Setter methods
	/**
	 * Sets the warning percentage which will be used to detect when dispenser is almost empty
	 * 
	 * @param percent 
	 * 	     a double value representing warning percentage. E.g. 0.2 -> 20% low warning percentage
	 **/
	public void setWarningEmptyPercentage(double percent) {
		this.warningEmptyPercentage = percent;
	}
	
	/**
	 * Sets the warning percentage which will be used to detect when dispenser is almost full
	 * 
	 * @param percent 
	 * 		double value representing warning percentage. E.g. 0.8 -> 80% high warning percentage
	 **/
	public void setWarningFullPercentage(double percent) {
		this.warningFullPercentage = percent;
	}
	
	// Getter methods
	public int getMaxCapacity() {
		return this.maxCapacity;
	}
	
	public int getCurrentCapacity() {
		return this.currentCapacity;
	}

	
// ------------------------- No implementation ------------------------------ //
	
	@Override
	public void enabled(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disabled(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnedOn(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void turnedOff(IComponent<? extends IComponentObserver> component) {
		// TODO Auto-generated method stub
		
	}

}