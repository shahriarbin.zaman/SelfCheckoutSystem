/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Controller;

import java.util.ArrayList;

import com.jjjwelectronics.EmptyDevice;
import com.jjjwelectronics.IDevice;
import com.jjjwelectronics.IDeviceListener;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.printer.IReceiptPrinter;
import com.jjjwelectronics.printer.ReceiptPrinterListener;
import com.thelocalmarketplace.hardware.BarcodedProduct;
import com.thelocalmarketplace.hardware.PLUCodedProduct;
import com.thelocalmarketplace.software.CentralCheckoutStationLogic;
import com.thelocalmarketplace.software.ShoppingCart;
import com.thelocalmarketplace.software.GUI.SystemListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener;

public class ReceiptPrinterController implements ReceiptPrinterListener {

	private IReceiptPrinter printer;
	private ShoppingCart cart;
	private SessionStateListener session;

	public ReceiptPrinterController(CentralCheckoutStationLogic logic) {
		this.printer = logic.hardware.getPrinter();
		this.session = SessionStateListener.getSessionStateListener(logic);
		cart = ShoppingCart.getShoppingCart(logic);
	}
	
	//NOTE: this original version of this class always causes errors because 
	// SelfCheckoutStationBronze does not support printer.paperRemaining() nor
	// printer.inkRemaining();
	
	public void print() throws EmptyDevice, OverloadedDevice {
		System.out.println(cart.getTotalPrice());
		ArrayList<BarcodedProduct> barcodedItems = cart.getShoppingCartProducts();
		ArrayList<PLUCodedProduct> PLUitems = cart.getShoppingCartPLUProducts();
		String name;
		String price;
		long total = 0;
		
		for(int i = 0; i < barcodedItems.size(); i++) {			
			name = barcodedItems.get(i).getDescription();
			price = Long.toString(barcodedItems.get(i).getPrice());
			total += barcodedItems.get(i).getPrice();
			
			printItem(name);
			printer.print(' ');
			printPrice(price);
			printer.print('\n');
		}
		for(int i = 0; i < PLUitems.size(); i++) {			
			name = PLUitems.get(i).getDescription();
			price = Long.toString(PLUitems.get(i).getPrice());
			total += PLUitems.get(i).getPrice();
			
			printItem(name);
			printer.print(' ');
			printPrice(price);
			printer.print('\n');
		}
		
		printer.print('\n');
		printTotal(total);
		
		printer.cutPaper();
		String receipt = printer.removeReceipt();
		System.out.println("Receipt: "+receipt);
		SystemListener.aReceiptHasBeenPrinted(receipt);
	}
	
	// print receipt including change due
	public void print(String totalMoney,String changeDue) throws EmptyDevice, OverloadedDevice {
		System.out.println(cart.getTotalPrice());
		ArrayList<BarcodedProduct> barcodedItems = cart.getShoppingCartProducts();
		ArrayList<PLUCodedProduct> PLUitems = cart.getShoppingCartPLUProducts();
		String name;
		String price;
		long total = 0;
		
		for(int i = 0; i < barcodedItems.size(); i++) {			
			name = barcodedItems.get(i).getDescription();
			price = Long.toString(barcodedItems.get(i).getPrice());
			total += barcodedItems.get(i).getPrice();
			
			printItem(name);
			printer.print(' ');
			printPrice(price);
			printer.print('\n');
		}
		for(int i = 0; i < PLUitems.size(); i++) {			
			name = PLUitems.get(i).getDescription();
			price = Long.toString(PLUitems.get(i).getPrice());
			total += PLUitems.get(i).getPrice();
			
			printItem(name);
			printer.print(' ');
			printPrice(price);
			printer.print('\n');
		}
		
		printer.print('\n');
		printTotal(total);
		printer.print('\n');
		
		
		String totalMoneyGiven = "Total Amount Paid: "+totalMoney;
		String change = "Change: "+changeDue;
		for(int i = 0; i < totalMoneyGiven.length(); i++) {
			printer.print(totalMoneyGiven.charAt(i));
		}
		printer.print('\n');
		for(int i = 0; i < change.length(); i++) {
			printer.print(change.charAt(i));
		}
		
		printer.cutPaper();
		String receipt = printer.removeReceipt();
		System.out.println("Receipt: "+receipt);
		SystemListener.aReceiptHasBeenPrinted(receipt);
	}

	private void printTotal(long totalPrice) throws EmptyDevice, OverloadedDevice {
		
		String price = totalPrice+"";
		String total = "Total: ";
		
		for(int i = 0; i < total.length(); i++) {
			printer.print(total.charAt(i));
		}
		
		printer.print('$');
		for(int i = 0; i < price.length(); i++) {
			printer.print(price.charAt(i));
		}
	}

	private void printItem(String name) throws EmptyDevice, OverloadedDevice {
		for(int i = 0; i < name.length(); i++) {
			printer.print(name.charAt(i));
		}
	}

	private void printPrice(String price) throws EmptyDevice, OverloadedDevice {
		printer.print('$');
		for(int i = 0; i < price.length(); i++) {
			printer.print(price.charAt(i));
		}
	}

	@Override
	public void thePrinterIsOutOfPaper() {
		System.out.println("The printer is out of paper please refill");
	}

	@Override
	public void thePrinterIsOutOfInk() {
		System.out.println("The printer is out of ink please refill");	
		
	}

	@Override
	public void thePrinterHasLowInk() {
		System.out.println("The printer in low on ink");
		
	}

	@Override
	public void thePrinterHasLowPaper() {
		System.out.println("The printer is low on paper");
		
	}

	@Override
	public void paperHasBeenAddedToThePrinter() {
		System.out.println("The paper has been refilled");
		
	}

	@Override
	public void inkHasBeenAddedToThePrinter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenEnabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenDisabled(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOn(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void aDeviceHasBeenTurnedOff(IDevice<? extends IDeviceListener> device) {
		// TODO Auto-generated method stub
		
	}


}