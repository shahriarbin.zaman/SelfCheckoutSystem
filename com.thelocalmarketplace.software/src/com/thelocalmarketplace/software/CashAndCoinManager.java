/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software;

import java.util.HashMap;
import java.util.Map;

public class CashAndCoinManager extends CentralCheckoutStationLogic {
    private Map<Integer, Integer> coinInventory; // Inventory of coins (value, quantity)
    private int cashBalance; // Total cash balance in cents

    public CashAndCoinManager(CentralCheckoutStationLogic logic) {
        super(logic); 
        this.coinInventory = new HashMap<>();
        this.cashBalance = 0;
        initializeCoinInventory(); 
    }

    // Initialize coin inventory with default values
    private void initializeCoinInventory() {

        int[] coinValues = {1, 5, 10, 25, 50}; 
        for (int value : coinValues) {
            coinInventory.put(value, 10); 
            int currentQuantity = 10;
        }
    }

    // Function to add coins to the inventory
    public void addCoins(int coinValue, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity of coins must be positive");
        }
        int currentQuantity = coinInventory.getOrDefault(coinValue, 0);
        coinInventory.put(coinValue, currentQuantity + quantity);
    }

    // Function to remove coins from the inventory
    public void removeCoins(int coinValue, int quantity) {
    	int currentQuantity = coinInventory.getOrDefault(coinValue, 0);
        if (quantity <= 0 || quantity > currentQuantity) {
            throw new IllegalArgumentException("Invalid quantity of coins to remove");
        }
        coinInventory.put(coinValue, currentQuantity - quantity);
    }

    // Function to calculate total cash balance in cents
    public int calculateCashBalance() {
        int total = 0;
        for (Map.Entry<Integer, Integer> entry : coinInventory.entrySet()) {
            total += entry.getKey() * entry.getValue();
        }
        return total + cashBalance; 
    }

    // Function to add cash to the total cash balance
    public void addCash(int amountInCents) {
        if (amountInCents <= 0) {
            throw new IllegalArgumentException("Amount must be positive");
        }
        cashBalance += amountInCents;
    }

    // Function to remove cash from the total cash balance
    public void removeCash(int amountInCents) {
        if (amountInCents <= 0 || amountInCents > cashBalance) {
            throw new IllegalArgumentException("Invalid amount to remove");
        }
        cashBalance -= amountInCents;
    }
    
    //Getters
    public int getCashBalance() {
    	return cashBalance;
    }
    
    public Map<Integer, Integer> getCoinInventory(){
    	return coinInventory;
    }
    
    //Checks
    public boolean isCashLow() {
    	return(cashBalance < 20);
    }
    
    public boolean isCashAlmostFull() {
    	return(cashBalance > 80);
    }
    
    public boolean isCoinOneLow() {
    	return(coinInventory.get(1) < 2);
    }
    
    public boolean isCoinOneAlmostFull() {
    	return(coinInventory.get(1) > 18);
    }
    
    public boolean isCoinFiveLow() {
    	return(coinInventory.get(5) < 2);
    }
    
    public boolean isCoinFiveAlmostFull() {
    	return(coinInventory.get(5) > 18);
    }
    
    public boolean isCoinTenLow() {
    	return(coinInventory.get(10) < 2);
    }
    
    public boolean isCoinTenAlmostFull() {
    	return(coinInventory.get(10) > 18);
    }
    
    public boolean isCoinTwentyFiveLow() {
    	return(coinInventory.get(25) < 2);
    }
    
    public boolean isCoinTwentyFiveAlmostFull() {
    	return(coinInventory.get(25) > 18);
    }
    
    public boolean isCoinFiftyLow() {
    	return(coinInventory.get(50) < 2);
    }
    
    public boolean isCoinFiftyAlmostFull() {
    	return(coinInventory.get(50) > 18);
    }
    
}