/*
Suiba Sultana Swapnil	30174800
Khushi Choksi			30168167
Doug Strueby			30122048
Sukhpreet Kaur Sandhu	30174163
Soila Pertet			30102008
Chetas Patel			30173271
Lev Sinaga	    		30191948
Ayesha Saeed			30174420
Sadeeb Hossain			30145821
Nolan Ruzicki			30132405
Shahriar Bin Zaman		30111988
Anshi Khatri	    	30172820
Kazi Mysha Moontaha		30179818
Nabihah Hussaini		30175407
Hareem Arif		        30169729
Mahnoor Hameed			30076366
Maria Manosalva Rojas	30146319
Jui Das			        30176206
Wasif Ud Dowlah			30130706
Navid Rahman			30186149
Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software.Listener;

import com.thelocalmarketplace.software.CentralCheckoutStationLogic;

import powerutility.PowerGrid;

public class SessionStateListener extends CentralCheckoutStationLogic {
	
	private static SessionStateListener listener;
	private PowerGrid grid = PowerGrid.instance();

	private SessionStateListener(CentralCheckoutStationLogic logic) {
		super(logic);
		
		this.logic.hardware.getBaggingArea().plugIn(grid);
		this.logic.hardware.getScanningArea().plugIn(grid);
		
		this.logic.hardware.getBanknoteInput().connect(grid);
		this.logic.hardware.getBanknoteOutput().connect(grid);
		this.logic.hardware.getBanknoteStorage().connect(grid);
		this.logic.hardware.getBanknoteValidator().connect(grid);
		this.logic.hardware.getCardReader().plugIn(grid);
		this.logic.hardware.getCoinSlot().connect(grid);
		this.logic.hardware.getCoinStorage().connect(grid);
		this.logic.hardware.getCoinValidator().connect(grid);
		
		this.logic.hardware.getHandheldScanner().plugIn(grid);
		this.logic.hardware.getMainScanner().plugIn(grid);
		
		this.logic.hardware.getPrinter().plugIn(grid);
		
		this.logic.hardware.getReusableBagDispenser().plugIn(grid);
		
		this.logic.hardware.getScreen().plugIn(grid);
    	

    	
    	
		this.logic.hardware.getBaggingArea().turnOn();
		this.logic.hardware.getScanningArea().turnOn();
		
		this.logic.hardware.getBanknoteInput();
		this.logic.hardware.getBanknoteOutput().activate();
		this.logic.hardware.getBanknoteStorage().activate();
		this.logic.hardware.getBanknoteValidator().activate();
		this.logic.hardware.getCardReader().turnOn();
		this.logic.hardware.getCoinSlot().activate();
		this.logic.hardware.getCoinStorage().activate();
		this.logic.hardware.getCoinValidator().activate();
		
		this.logic.hardware.getHandheldScanner().turnOn();
		this.logic.hardware.getMainScanner().turnOn();
		
		this.logic.hardware.getPrinter().turnOn();
		
		this.logic.hardware.getReusableBagDispenser().turnOn();
		
		this.logic.hardware.getScreen().turnOn();
	}
	
	public static SessionStateListener getSessionStateListener(CentralCheckoutStationLogic logic) {
        if (listener == null) {
            listener = new SessionStateListener(logic);
        }
        return listener;
    }
	
	private SessionState currentState = SessionState.NORMAL;	
	
	public enum SessionState 
	{
		NORMAL,
		BLOCKED
	}
	
	public void setState(SessionState state) {
		this.currentState = state;
		if (this.currentState == SessionState.NORMAL) {
			this.onNormal(); 
		}
		if (this.currentState == SessionState.BLOCKED) {
			this.onBlock(); 
		}
	}
	
	public SessionState getCurrentState() {
		return currentState;
	}

	/**
	 * In blocking state, this blocks all devices in the station. 
	 */
	public void onBlock() {
		this.logic.hardware.getBaggingArea().disable();
		this.logic.hardware.getScanningArea().disable();
		
		this.logic.hardware.getBanknoteInput().disable();
		this.logic.hardware.getBanknoteOutput().disable();
		this.logic.hardware.getBanknoteStorage().disable();
		this.logic.hardware.getBanknoteValidator().disable();
		this.logic.hardware.getCardReader().disable();
		this.logic.hardware.getCoinSlot().disable();
		this.logic.hardware.getCoinStorage().disable();
		this.logic.hardware.getCoinValidator().disable();
		
		this.logic.hardware.getHandheldScanner().disable();
		this.logic.hardware.getMainScanner().disable();
		
		this.logic.hardware.getPrinter().disable();
		
		this.logic.hardware.getReusableBagDispenser().disable();
		
		this.logic.hardware.getScreen().disable();
	}
	
	/**
	 * In any normal state, this keeps all devices in the station enabled. 
	 */
	public void onNormal() {
		this.logic.hardware.getBaggingArea().enable();
		this.logic.hardware.getScanningArea().enable();
		
		this.logic.hardware.getBanknoteInput().enable();
		this.logic.hardware.getBanknoteOutput().enable();
		this.logic.hardware.getBanknoteStorage().enable();
		this.logic.hardware.getBanknoteValidator().enable();
		this.logic.hardware.getCardReader().enable();
		this.logic.hardware.getCoinSlot().enable();
		this.logic.hardware.getCoinStorage().enable();
		this.logic.hardware.getCoinValidator().enable();
		
		this.logic.hardware.getHandheldScanner().enable();
		this.logic.hardware.getMainScanner().enable();
		
		this.logic.hardware.getPrinter().enable();
		
		this.logic.hardware.getReusableBagDispenser().enable();
		
		this.logic.hardware.getScreen().enable();
	}
	
}