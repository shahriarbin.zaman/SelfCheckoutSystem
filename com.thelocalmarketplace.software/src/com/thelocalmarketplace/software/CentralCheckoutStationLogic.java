/*	GROUP MEMBERS' NAMES AND UCIDs:
 	Sukhpreet Kaur Sandhu	30174163
	Suiba Sultana Swapnil	30174800
	Soila Pertet	30102008
	Khushi Choksi	30168167
	Doug Strueby	30122048
	Chetas Patel	30173271
	Lev Sinaga	30191948
	Ayesha Saeed	30174420
	Sadeeb Hossain	30145821
	Nolan Ruzicki	30132405
	Shahriar Bin Zaman	30111988
	Anshi Khatri	30172820
	Kazi Mysha Moontaha	30179818
	Nabihah Hussaini	30175407
	Hareem Arif	30169729
	Mahnoor Hameed	30076366
	Maria Manosalva Rojas	30146319
	Jui Das	30176206
	Wasif Ud Dowlah	30130706
	Navid Rahman	30186149
	Shahriar Khan	30185337
*/

package com.thelocalmarketplace.software;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import powerutility.PowerGrid;
import com.jjjwelectronics.card.CardReaderBronze;
import com.thelocalmarketplace.software.Controller.PLUController;
import com.thelocalmarketplace.software.Controller.PredictIssuesController;
import com.jjjwelectronics.card.CardReaderGold;
import com.jjjwelectronics.card.CardReaderSilver;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.ElectronicScaleGold;
import com.jjjwelectronics.scale.ElectronicScaleSilver;
import com.thelocalmarketplace.hardware.AbstractSelfCheckoutStation;
import com.thelocalmarketplace.hardware.AttendantStation;
import com.thelocalmarketplace.hardware.external.CardIssuer;
import com.thelocalmarketplace.software.Controller.CardReaderController;
import com.thelocalmarketplace.software.Controller.CoinDispenserController;
import com.thelocalmarketplace.software.Controller.MembershipCardReaderController;
import com.thelocalmarketplace.software.Controller.ScaleController;
import com.thelocalmarketplace.software.Controller.ReceiptPrinterController;
import com.thelocalmarketplace.software.Controller.CoinController;
import com.thelocalmarketplace.software.Controller.BanknoteController;
import com.thelocalmarketplace.software.Controller.BanknoteDispenserController;
import com.thelocalmarketplace.software.Controller.ChangeController;
import com.thelocalmarketplace.software.Controller.ScannerController;
import com.thelocalmarketplace.software.Controller.SignalAttendantController;
import com.thelocalmarketplace.software.GUI.AttendantStationScreen;
import com.thelocalmarketplace.software.GUI.PrimaryGUI;
import com.thelocalmarketplace.software.Listener.SessionStateListener;
import com.thelocalmarketplace.software.Listener.SessionStateListener.SessionState;

import com.thelocalmarketplace.software.Controller.BagPurchaserController;

public class CentralCheckoutStationLogic 
{	
	public CentralCheckoutStationLogic logic;
	
	public AbstractSelfCheckoutStation hardware; 
		
	private MethodsOfPayment methodsOfPayment;
	private MethodsOfCardPayment methodsOfCardPayment ;
	private TypeOfCard typeOfCard;
	private CardIssuer cardIssuer;
	
	private String currentMemberShipCard;
	private HashMap<String, Boolean> membershipCards = new HashMap<>();
	
	public AttendantStationScreen attendantStationScreen;
	public PrimaryGUI primaryGUI;
	
	public CentralCheckoutStationLogic(CentralCheckoutStationLogic logic) 
	{
		 this.logic = logic;
	}
	
	/**
	 * Installs the logic onto a station.
	 * @param station The station where the logic will be installed.
	 * @return An instance of the central checkout station logic installed onto the station. 
	 */
	public static CentralCheckoutStationLogic installOn(AbstractSelfCheckoutStation station) {
		return new CentralCheckoutStationLogic(station);
	}
	
	/**
	 * Installs the logic onto a station.
	 * @param station The station where the logic will be installed.
	 * @Param attendant station the logic will be installed.
	 * @return An instance of the central checkout station logic installed onto the station. 
	 */
	public static CentralCheckoutStationLogic installOn(AbstractSelfCheckoutStation station, AttendantStation attendantStation) {
		return new CentralCheckoutStationLogic(station, attendantStation);
	}
	
	public enum MethodsOfPayment
	{
		CARD, 
		CASH,
		DEFAULT
	}	

	public enum TypeOfCard
	{
		CREDIT_CARD,
		DEBIT_CARD,
		MEMBERSHIP_CARD,
		DEFAULT
	}
	
	public enum MethodsOfCardPayment 
	{
		SWIPE,
		TAP,
		INSERT,
		DEFAULT
	}
	
	public enum TypeOfScanner 
	{
		MAIN,
		HANDHELD
	}
	
	public CoinController coinController;
	
	public BanknoteController banknoteController;
	
	public CoinDispenserController coinDispenserController;

	public CardReaderController cardReaderController;

	public ReceiptPrinterController receiptPrinterController;
	
	public ShoppingCart shoppingCartLogic;

	public ChangeController changeController;
	
	public SignalAttendantController signalAttendantController;
	
	public ScannerController scannerController;
	
	public PLUController priceLookupCodeController;
	
	public BagPurchaserController bagPurchaserController;
	
	public ScaleController scaleController;
	
	public PredictIssuesController predictIssuesController;
	
	public PaperManager paperManager;
	
	public CashAndCoinManager cashAndCoinManager;
	
	public InkManager inkManager;
	
	public StationDisabler stationDisabler;
	
	public SessionStarter sessionStarter;
	
	public SessionStateListener listener;
	
	public Map<BigDecimal, CoinDispenserController> coinDispenserControllers = new HashMap<>();
		
	public Map<BigDecimal, BanknoteDispenserController> banknoteDispenserControllers = new HashMap<>();
	
	public MembershipCardReaderController membershipCardReaderController;
	
	public AttendantStation attendantStation;
	
	/**
	 * To be used only in the class. Helps install the logic. 
	 * @param station The station that the logic is installed on.
	 */
	private CentralCheckoutStationLogic(AbstractSelfCheckoutStation station) 
	{
		this.hardware = station;
		
		sessionStarter = SessionStarter.getSessionStarter(this);
		banknoteController = new BanknoteController(this);
		predictIssuesController = new PredictIssuesController(this, sessionStarter);
		coinController = new CoinController(this);
		changeController = new ChangeController(this);
		scaleController = new ScaleController(this);
		
		PowerGrid grid = PowerGrid.instance();
		PowerGrid.engageUninterruptiblePowerSource();
		
		scaleController.getScaleScanningArea().plugIn(grid);
		
		
		paperManager = new PaperManager(100);
		cashAndCoinManager = new CashAndCoinManager(this);
		inkManager = new InkManager(100, "HP Catridge", this);
		
		stationDisabler = new StationDisabler();
		scannerController = new ScannerController(this);
		priceLookupCodeController = new PLUController(this);
		bagPurchaserController = new BagPurchaserController(this);
		receiptPrinterController = new ReceiptPrinterController(this);
		membershipCardReaderController = new MembershipCardReaderController(this);
		
		// added for attendant station
		attendantStation = new AttendantStation();
		attendantStation.add(station);
		signalAttendantController = new SignalAttendantController(logic, attendantStation); 

		listener = SessionStateListener.getSessionStateListener(this);
		listener.setState(SessionState.NORMAL);
	}
	
	private CentralCheckoutStationLogic(AbstractSelfCheckoutStation station, AttendantStation attendantStation) 
	{
		this.hardware = station;
		this.attendantStation = attendantStation;
		
		sessionStarter = SessionStarter.getSessionStarter(this);
		banknoteController = new BanknoteController(this);
		predictIssuesController = new PredictIssuesController(this, sessionStarter);
		coinController = new CoinController(this);
		changeController = new ChangeController(this);
		scaleController = new ScaleController(this);
		
		paperManager = new PaperManager(100);
		cashAndCoinManager = new CashAndCoinManager(this);
		inkManager = new InkManager(100, "HP Catridge", this);
		
		stationDisabler = new StationDisabler();
		scannerController = new ScannerController(this);
		priceLookupCodeController = new PLUController(this);
		bagPurchaserController = new BagPurchaserController(this);
		receiptPrinterController = new ReceiptPrinterController(this);
		membershipCardReaderController = new MembershipCardReaderController(this);
		
		//added for attendant station
		attendantStation.add(station);
		signalAttendantController = new SignalAttendantController(logic, attendantStation); 
	}
	
	public MethodsOfPayment getMethodsOfPayment() 
	{
		return this.methodsOfPayment;
	}

	public TypeOfCard getTypeOfCard() 
	{
		return this.typeOfCard;
	}

	public MethodsOfCardPayment getMethodsOfCardPayment() 
	{
		return this.methodsOfCardPayment;
	}
	
	public void addMembershipCard(String membershipNumber) {
		this.membershipCards.put(membershipNumber, true);
	}
	
	public HashMap getMembershipCards() {
        return membershipCards;
    }
	
	public void setCurrentMemberShipCard(String card) {
		this.currentMemberShipCard = card;
	}
	
	public String getCurrentMemberShipCard() {
		return this.currentMemberShipCard;
	}
	
	public void setMethodsOfPayment(MethodsOfPayment methodsOfPayment) 
	{
		this.methodsOfPayment = methodsOfPayment;
	}
	
	public void setTypeOfCard(TypeOfCard typeOfCard) 
	{
		this.typeOfCard = typeOfCard;
	}

	public void setMethodsOfCardPayment(MethodsOfCardPayment methodsOfCardPayment) 
	{
		this.methodsOfCardPayment = methodsOfCardPayment;
	}
	
	public String getCardReaderType() 
	{
        if (hardware.getCardReader() instanceof CardReaderBronze) 
        {
            return "Bronze";
        } 
        else if (hardware.getCardReader() instanceof CardReaderSilver) 
        {
            return "Silver";
        } 
        else if (hardware.getCardReader() instanceof CardReaderGold) 
        {
            return "Gold";
        } 
        else 
        {
            return "INVALID CARD READER TYPE";
        }
    }
	
	public CardIssuer getCardIssuer() 
	{
		return cardIssuer;
	}

	public void setCardIssuer(CardIssuer cardIssuer) 
	{
		this.cardIssuer = cardIssuer;
	}
	
	public String getScanningAreaType() 
	{
        if (hardware.getScanningArea() instanceof ElectronicScaleBronze) 
        {
            return "Bronze";
        } 
        else if (hardware.getScanningArea() instanceof ElectronicScaleSilver) 
        {
            return "Silver";
        } 
        else if (hardware.getScanningArea() instanceof ElectronicScaleGold) 
        {
            return "Gold";
        } 
        else 
        {
            return "INVALID SCANNING AREA SCALE TYPE";
        }
    }
	
	/**
	 * Initialise coin dispenser controllers to their corresponding denomination
	 **/
	private void initialiseCoinDispenserControllers(List<BigDecimal> denominations) {
		for(BigDecimal denomination: denominations) {
			this.coinDispenserControllers.put(denomination, new CoinDispenserController(this, denomination));
		}
	}
	
	/**
	 * Initialise bank note dispenser controllers to their corresponding denomination
	 **/
	private void initialiseBanknoteDispenserControllers(BigDecimal[] denominations) {
		for(BigDecimal denomination: denominations) {
			this.banknoteDispenserControllers.put(denomination, new BanknoteDispenserController(this, denomination));
		}
	}


	public void reorderInkCartridges() {
		// TODO Auto-generated method stub
		
	}


	public void replaceInkCartridge() {
		// TODO Auto-generated method stub
		
	}
	
}