/*
	Suiba Sultana Swapnil	30174800
	Khushi Choksi			30168167
	Doug Strueby			30122048
	Sukhpreet Kaur Sandhu	30174163
	Soila Pertet			30102008
	Chetas Patel			30173271
	Lev Sinaga	    		30191948
	Ayesha Saeed			30174420
	Sadeeb Hossain			30145821
	Nolan Ruzicki			30132405
	Shahriar Bin Zaman		30111988
	Anshi Khatri	    	30172820
	Kazi Mysha Moontaha		30179818
	Nabihah Hussaini		30175407
	Hareem Arif		        30169729
	Mahnoor Hameed			30076366
	Maria Manosalva Rojas	30146319
	Jui Das			        30176206
	Wasif Ud Dowlah			30130706
	Navid Rahman			30186149
	Shahriar Khan			30185337
*/

package com.thelocalmarketplace.software;
import java.math.BigDecimal;
import com.jjjwelectronics.Item;
import com.jjjwelectronics.Mass;
import com.jjjwelectronics.OverloadedDevice;
import com.jjjwelectronics.scale.AbstractElectronicScale;
import com.jjjwelectronics.scale.ElectronicScaleBronze;
import com.jjjwelectronics.scale.IElectronicScale;

public class OwnBagAdder extends CentralCheckoutStationLogic {
	
	private ShoppingCart shoppingCart;
	
	private boolean addBagState = false;
	private BigDecimal currentExpectedMass;
	private BigDecimal currentMass;
	private BigDecimal bagMass;
	
	/**
	 * Constructor for AddOwnBags.
	 */
	public  OwnBagAdder(CentralCheckoutStationLogic logic) {
		super(logic);
		this.shoppingCart = ShoppingCart.getShoppingCart(logic);
	}
	
	
	/**
	 * This method calculates the weight of each bag added to the scale by subtracting the expected weight from the actual weight of the scale.
	 */
	public void updateWeightWithBags(int numberOfBags) {
		if (numberOfBags <= 0) {
			this.addBagState = false;
		} else {
			this.startAddingBags();
			currentMass = shoppingCart.getTotalActualWeight();				// get actual weight on scale
			currentExpectedMass = shoppingCart.getTotalExpectedWeight();	// get expected weight on scale
			bagMass = currentMass.subtract(currentExpectedMass);			// difference of the two is the bag mass
			shoppingCart.addExpectedWeight(bagMass);						// add this difference to total expected weight to prevent weight discrepancy
			this.finishAddingBags();		
		}
	}
  
	/**
	 * Customer starts adding bags.
	 */
	public void startAddingBags() {
		addBagState = true;
	}
	
	/**
	 * Customer has finished adding bags.
	 */
	public void finishAddingBags() {
		addBagState = false;
	}
	
	/**
	 *
	 * @return The boolean state of add own bag. True if customer is adding own bags, false otherwise.
	 */
	public boolean getAddBagState() {
		return this.addBagState;
	}
	
	/**
	 * 
	 * @return The bag mass. 
	 */
	public BigDecimal getBagMass() {
		return this.bagMass;
	}
	

	/**
	 * Stub used for tests. 
	 * @param bigDecimal
	 */
	public void setCurrentMass(BigDecimal bigDecimal) {
		// TODO Auto-generated method stub
		
	}
}